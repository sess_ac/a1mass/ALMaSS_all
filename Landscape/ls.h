/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//
// ls.h
//


#ifndef LS_H
#define LS_H

#include <array>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
using namespace std;
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/LandscapeFarmingEnums.h"
#include "../Landscape/Configurator.h"
#include "../BatchALMaSS/ALMaSS_Random.h"
#include "../RodenticideModelling/Rodenticide.h"
#include "../Landscape/MapErrorMsg.h"
#include "../Landscape/Rastermap.h"
#include "../Landscape/Treatment.h"
#include "../Landscape/PollenNectar.h"
#include "../Landscape/Plants.h"
#include "../Landscape/Elements.h"
#include "../Landscape/Croprotation.h"
#include "../Landscape/Lowqueue.h"
#include "../Landscape/Treatment.h"
#include "../Landscape/Farm.h"
#include "../Landscape/Calendar.h"
#include "../Landscape/Weather.h"
#include "../BatchALMaSS/CurveClasses.h"
#include "../Landscape/Landscape.h"
#include "../Landscape/Pesticide.h"
#include "../Landscape/Misc.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../People/Farmers/Farmer.h"
#endif
