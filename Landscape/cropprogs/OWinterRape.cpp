//
// OWinterRape.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OWinterRape.h"


bool OWinterRape::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo ) {
  case owr_start:
    {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(15,8);
      // Start and stop dates for all events after harvest
      int noDates= 1;
      m_field->SetMDates(0,0,g_date->DayInYear(1,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(15,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OWinterRape::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.

      int d1;
      if ( ! m_ev->m_first_year )
      {
	int today=g_date->Date();
        // Are we before July 1st?
	d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
	if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, " OWinterRape::Do(): "
		 "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
	}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, " OWinterRape::Do(): "
		 "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear(5,7 ),
           owr_swarth, false );
         break;
      }
      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
      // OK, let's go.
      if ( m_farm->IsStockFarmer()) {
	SimpleEvent( d1, owr_fa_manure, false );
      } else {
	SimpleEvent( d1, owr_fp_manure, false );
      }
    }
    break;

  case owr_fa_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owr_fa_manure, true );
				break;
      }
    }
    SimpleEvent( g_date->Date() + 1, owr_autumn_plough, false );
    break;

  case owr_fp_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FP_Manure( m_field, 0.0,
           g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owr_fp_manure, true );
				break;
      }
    }
    SimpleEvent( g_date->Date() + 1, owr_autumn_plough, false );
    break;

  case owr_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 25, 8 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_autumn_plough, true );
      break;
    }
    SimpleEvent( g_date->Date() + 1, owr_autumn_harrow, false );
    break;

  case owr_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 27, 8 ) -
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_autumn_harrow, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10, 8 ),
		 owr_autumn_sow, false );
    break;

  case owr_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
         g_date->DayInYear( 27, 8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_autumn_sow, false );
      break;
    }
    {
      int d1 = g_date->Date() + 10;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 20, 8 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 20, 8 );
      SimpleEvent( d1, owr_row_cultivation_one, false );
    }
    break;

  case owr_row_cultivation_one:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 20, 9 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_row_cultivation_one, true );
      break;
    }
    {
      int d1 = g_date->Date() + 7;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 28, 8 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 28, 8 );
      SimpleEvent( d1, owr_row_cultivation_two, false );
    }
    break;

  case owr_row_cultivation_two:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 30, 9 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_row_cultivation_two, true );
      break;
    }
    {
      int d1 = g_date->OldDays() + g_date->DayInYear( 15, 3 ) + 365;
      if ( m_farm->IsStockFarmer()) {
	SimpleEvent( d1, owr_fa_slurry, false );
      } else {
	SimpleEvent( d1, owr_fp_slurry, false );
      }
    }
    break;

  case owr_fa_slurry:
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    g_date->DayInYear( 15, 4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_fa_slurry, true );
      break;
    }
    {
      int d1 = g_date->Date() + 1;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 4 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      SimpleEvent( d1, owr_row_cultivation_three, false );
    }
    break;

  case owr_fp_slurry:
    if (!m_farm->FP_Slurry( m_field, 0.0,
			    g_date->DayInYear( 15, 4 ) -
			    g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_fp_slurry, true );
      break;
    }
    {
      int d1 = g_date->Date() + 1;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 4 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      SimpleEvent( d1, owr_row_cultivation_three, false );
    }
    break;

  case owr_row_cultivation_three:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 30, 4 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_row_cultivation_three, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5, 7 ),
		 owr_swarth, false );
    break;

  case owr_swarth:
    if (m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if ( !m_farm->Swathing( m_field, 0.0,
            g_date->DayInYear( 25, 7 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owr_swarth, true );
        break;
      }
    }
    {
      int d1 = g_date->Date() + 5;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 10, 7 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 10, 7 );
      SimpleEvent( d1, owr_harvest, false );
    }
    break;

  case owr_harvest:
    if ( !m_farm->Harvest( m_field, 0.0,
			   g_date->DayInYear( 1, 8 ) -
			   g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_harvest, false );
      break;
    }
    SimpleEvent( g_date->Date(), owr_straw_chop, false );
    break;

  case owr_straw_chop:
    if ( !m_farm->StrawChopping( m_field, 0.0,
				 g_date->DayInYear( 1, 8 ) -
				 g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, owr_straw_chop, true );
      break;
    }
    {
      int d1 = g_date->Date() + 1;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 15, 7 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 15, 7 );
      SimpleEvent( d1, owr_stub_harrow, false );
    }
    break;

  case owr_stub_harrow:
    if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
      if ( !m_farm->StubbleHarrowing( m_field, 0.0,
            g_date->DayInYear( 15, 8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, owr_stub_harrow, true );
        break;
      }
    }
    done = true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OWinterRape::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}

