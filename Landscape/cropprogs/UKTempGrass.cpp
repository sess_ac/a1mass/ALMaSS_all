/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>UKTempGrass.cpp This file contains the source for the UKTempGrass class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
// Adapted for UK Temp Grass by Adam McVeigh, 2021
*/
//
// UKTempGrass.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/UKTempGrass.h"

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional TemporalGrassGrazed1Spring.
*/
bool UKTempGrass::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_UKTempGrass; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case uk_tg_start:
	{
		// uk_tg_start just sets up all the starting conditions and reference dates that are needed to start a uk_pot
		UK_TG_CUT_DATE = 0;
		UK_TG_WATER_DATE = 0;

		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 1 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		m_field->SetMDates(0, 0, g_date->DayInYear(20, 8)); //  last possible day of last cutting
		m_field->SetMDates(1, 0, g_date->DayInYear(30, 8)); //	last possible day of NPK application

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "UKTempGrass::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "UKTempGrass::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_tg_spring_sow, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		if (g_date->Date() >= d1) d1 += 365;
		// OK, let's go.
		// Here we queue up the first event
		SimpleEvent_(d1, uk_tg_preseeding_cultivator, false, m_farm, m_field);

	}
	break;

	// This is the first real farm operation
	case uk_tg_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(2, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_spring_sow, false, m_farm, m_field);
		break;
	case uk_tg_spring_sow:
		if (!m_farm->SpringSow(m_field, 0.0, g_date->DayInYear(3, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_spring_sow, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, uk_tg_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, uk_tg_ferti_p2, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p2:
		if (m_farm->DoIt_prob(0.90)) {
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->DoIt_prob(0.50)) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), uk_tg_cut_to_silage1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), uk_tg_cattle_out, false, m_farm, m_field);
		break;
	case uk_tg_ferti_s2:
		if (m_farm->DoIt_prob(0.90)) {
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->DoIt_prob(0.50)) {
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), uk_tg_cut_to_silage1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4), uk_tg_cattle_out, false, m_farm, m_field);
		break;
	case uk_tg_cut_to_silage1:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < UK_TG_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage1, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage1, true, m_farm, m_field);
				break;
			}
			UK_TG_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s3, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p3, false, m_farm, m_field);
			// Start water thread
			SimpleEvent_(g_date->Date() + 1, uk_tg_watering, false, m_farm, m_field);
			break;
		}
		break;

	case uk_tg_ferti_s3:
		if (!m_farm->FA_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s4, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p3:
		if (!m_farm->FP_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p4, false, m_farm, m_field);
		break;
	case uk_tg_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage2, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage2, false, m_farm, m_field);
		break;
	case uk_tg_cut_to_silage2:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < UK_TG_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage2, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(1, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage2, true, m_farm, m_field);
				break;
			}
			UK_TG_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s5, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p5, false, m_farm, m_field);
			break;
		}
		break;
	case uk_tg_ferti_s5:
		if (!m_farm->FA_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s6, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p5:
		if (!m_farm->FP_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p6, false, m_farm, m_field);
		break;
	case uk_tg_ferti_s6:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s6, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage3, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p6:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p6, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage3, false, m_farm, m_field);
		break;
	case uk_tg_cut_to_silage3:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < UK_TG_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage3, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(1, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage3, true, m_farm, m_field);
				break;
			}
			UK_TG_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s7, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p7, false, m_farm, m_field);
			break;
		}
		break;

	case uk_tg_ferti_s7:
		if (!m_farm->FA_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s7, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s8, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p7:
		if (!m_farm->FP_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p7, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p8, false, m_farm, m_field);
		break;
	case uk_tg_ferti_s8:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage4, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p8:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p8, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage4, false, m_farm, m_field);
		break;
	case uk_tg_cut_to_silage4:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < UK_TG_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage4, true, m_farm, m_field);
			break;
		}
		else
		{
			if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(1, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage4, true, m_farm, m_field);
				break;
			}
			UK_TG_CUT_DATE = g_date->DayInYear();
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s9, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p9, false, m_farm, m_field);
			break;
		}
		break;
	case uk_tg_ferti_s9:
		if (!m_farm->FA_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s9, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s10, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p9:
		if (!m_farm->FP_Slurry(m_field, 0.0, (UK_TG_CUT_DATE + 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p9, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p10, false, m_farm, m_field);
		break;
	case uk_tg_ferti_s10:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage5, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p10:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(10, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p10, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 21, uk_tg_cut_to_silage5, false, m_farm, m_field);
		break;
	case uk_tg_cut_to_silage5:
		// At least 7 days from last watering and 21 from last application of fertilizer.
		if (g_date->Date() < UK_TG_WATER_DATE + 7) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage5, true, m_farm, m_field);
			break;
		}
		else
		{
			if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
			{
				if (!m_farm->CutToSilage(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage5, true, m_farm, m_field);
					break;
				}
				UK_TG_CUT_DATE = g_date->DayInYear();
			}
			if (m_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(30, 8), uk_tg_ferti_s12, false, m_farm, m_field);
			}
			else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(30, 8), uk_tg_ferti_p12, false, m_farm, m_field);
			break;
		}
		break;
	case uk_tg_ferti_s12:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->FA_NPK(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "UKTempGrass::Do(): failure in 'NPK application' execution", "");
				exit(1);
			}
		}
		else {
			if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
			{
				if (!m_farm->FA_NPK(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s12, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		break;
	case uk_tg_ferti_p12:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->FP_NPK(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "UKTempGrass::Do(): failure in 'NPK application' execution", "");
				exit(1);
			}
		}
		else {
			if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
			{
				if (!m_farm->FP_NPK(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p12, true, m_farm, m_field);
					break;
				}
			}
		}
		done = true;
		break;
	case uk_tg_cattle_out:
		if (!m_farm->CattleOut(m_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cattle_out, true, m_farm, m_field);
			break;
		}
		// Keep them out there
		SimpleEvent_(g_date->Date(), uk_tg_cattle_is_out, false, m_farm, m_field);
		break;

	case uk_tg_cattle_is_out:    // Keep the cattle out there
								   // CattleIsOut() returns false if it is not time to stop grazing
		if (!m_farm->CattleIsOut(m_field, 0.0, g_date->DayInYear(14, 8) - g_date->DayInYear(), g_date->DayInYear(31, 8))) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cattle_is_out, false, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s11, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p11, false, m_farm, m_field);
		break;

	case uk_tg_ferti_s11:
		if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_s11, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 5, uk_tg_cut_to_silage6, false, m_farm, m_field);
		break;
	case uk_tg_ferti_p11:
		if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_ferti_p11, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 5, uk_tg_cut_to_silage6, false, m_farm, m_field);
		break;
	case uk_tg_cut_to_silage6:
		if (!m_farm->CutToSilage(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_tg_cut_to_silage6, true, m_farm, m_field);
			break;
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 8), uk_tg_ferti_s12, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 8), uk_tg_ferti_p12, false, m_farm, m_field);
		break;
	case uk_tg_watering:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (g_date->Date() < UK_TG_CUT_DATE + 3) {
				// Too close to silage cutting, so try again tomorrow.
				SimpleEvent_(g_date->Date() + 1, uk_tg_watering, true, m_farm, m_field);
			}
			else
			{
				if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_tg_watering, true, m_farm, m_field);
					break;
				}
				UK_TG_WATER_DATE = g_date->Date();
			}
		}
		// End of thread
		break;
	default:
		g_msg->Warn(WARN_BUG, "UKTempGrass::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}