//
// DE_OWinterRape.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OWinterRape.h"


extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;

bool DE_OWinterRape::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
    m_farm = a_farm;
    m_field = a_field;
    m_ev = a_ev;

    bool done = false;
    int d1 = 0;
    int noDates = 1;

 switch ( m_ev->m_todo ) 
  {
  case de_owr_start:
  {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(15,8);
      // Start and stop dates for all events after harvest
      int noDates = 4;
      m_field->SetMDates(0, 0, g_date->DayInYear(1, 8)); // last possible day of harvest
      m_field->SetMDates(1, 0, g_date->DayInYear(10, 8)); // last possible day of straw chopping, equal to harvest in this case
      m_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depends on previous treatment)
      m_field->SetMDates(1, 1, g_date->DayInYear(15, 8)); // end day of hay bailing
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

      //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
      //optimising farms not used for now so most of related code is removed (but not in 'start' case)
      if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

          if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
              if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
              {
                  g_msg->Warn(WARN_BUG, "DE_OWinterRape::Do(): ", "Harvest too late for the next crop to start!!!");
                  int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
                  exit(1);
              }
              // Now fix any late finishing problems
              for (int i = 0; i < noDates; i++) {
                  if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
                      m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
                  }
                  if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
                      m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
                      m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
                  }
              }
          }
          // Now no operations can be timed after the start of the next crop.

          if (!m_ev->m_first_year) {
              // Are we before July 1st?
              d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
              if (g_date->Date() < d1) {
                  // Yes, too early. We assumme this is because the last crop was late
                  printf("Poly: %d\n", m_field->GetPoly());
                  g_msg->Warn(WARN_BUG, "DE_OWinterRape::Do(): ", "Crop start attempt between 1st Jan & 1st July");
                  int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
                  g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
                  int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
              }
              else {
                  d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
                  if (g_date->Date() > d1) {
                      // Yes too late - should not happen - raise an error
                      g_msg->Warn(WARN_BUG, "DE_OWinterRape::Do(): ", "Crop start attempt after last possible start date");
                      g_msg->Warn(WARN_BUG, "Previous Crop ", "");
                      m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
                      int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                      g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
                  }
              }
          }
          else {
              // Is the first year
              // Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
              // Code for first spring treatment used
              if (m_farm->IsStockFarmer()) //Stock Farmer
              {
                  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3)+365, de_owr_fa_slurry, false, m_farm, m_field);
              }
              else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 3)+365, de_owr_fp_slurry, false, m_farm, m_field);
              break;
          }
      }//if


      // End single block date checking code. Please see next line comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear(15, 8);
      if (g_date->Date() > d1) {
          d1 = g_date->Date();
      }
      // OK, let's go.
      if ( m_farm->IsStockFarmer()) {
	SimpleEvent_( d1, de_owr_fa_manure, false, m_farm, m_field);
      } else {
	SimpleEvent_( d1, de_owr_fp_manure, false, m_farm, m_field);
      }
    }
    break;

  case de_owr_fa_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
        SimpleEvent_( g_date->Date() + 1, de_owr_fa_manure, true, m_farm, m_field);
				break;
      }
    }
    SimpleEvent_( g_date->Date() + 1, de_owr_autumn_plough, false, m_farm, m_field);
    break;

  case de_owr_fp_manure:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FP_Manure( m_field, 0.0,
           g_date->DayInYear( 25, 8 ) - g_date->DayInYear())) {
        SimpleEvent_( g_date->Date() + 1, de_owr_fp_manure, true, m_farm, m_field);
				break;
      }
    }
    SimpleEvent_( g_date->Date() + 1, de_owr_autumn_plough, false, m_farm, m_field);
    break;

  case de_owr_autumn_plough:
    if (!m_farm->AutumnPlough( m_field, 0.0,
			       g_date->DayInYear( 25, 8 ) -
			       g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_autumn_plough, true, m_farm, m_field);
      break;
    }
    SimpleEvent_( g_date->Date() + 1, de_owr_autumn_harrow, false, m_farm, m_field);
    break;

  case de_owr_autumn_harrow:
    if (!m_farm->AutumnHarrow( m_field, 0.0,
			       g_date->DayInYear( 27, 8 ) -
			       g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_autumn_harrow, false, m_farm, m_field);
      break;
    }
    SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 10, 8 ),
		 de_owr_autumn_sow, false, m_farm, m_field);
    break;

  case de_owr_autumn_sow:
    if (!m_farm->AutumnSow( m_field, 0.0,
         g_date->DayInYear( 10, 9 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_autumn_sow, false, m_farm, m_field);
      break;
    }
    SimpleEvent_(g_date->Date() + 14, de_owr_row_cultivation_one, false, m_farm, m_field);
    break;

  case de_owr_row_cultivation_one:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 30, 9 ) -
				 g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_row_cultivation_one, true, m_farm, m_field);
      break;
    }
    {
      int d1 = g_date->Date() + 7;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 8, 10 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 8, 10 );
      SimpleEvent_( d1, de_owr_row_cultivation_two, false, m_farm, m_field);
    }
    break;

  case de_owr_row_cultivation_two:
      if (m_ev->m_lock || m_farm->DoIt(20))
      {
        if (!m_farm->RowCultivation( m_field, 0.0,
				     g_date->DayInYear( 10, 10 ) -
				     g_date->DayInYear())) {
          SimpleEvent_( g_date->Date() + 1, de_owr_row_cultivation_two, true, m_farm, m_field);
                  break;
        }
      }
      // fork for Slurry in spring (MAIN THREAD) and two parallel events for Sulphur and Boron application
      {
          int d1 = g_date->OldDays() + g_date->DayInYear(15, 3) + 365;
          if (m_farm->IsStockFarmer()) {
              SimpleEvent_(d1, de_owr_fa_slurry, false, m_farm, m_field);
          }
          else {
              SimpleEvent_(d1, de_owr_fp_slurry, false, m_farm, m_field); // MAIN THREAD
          }
      }
      if (m_farm->IsStockFarmer()) //Stock Farmer Sulphur treatment
      {
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), de_owr_fertiFA_S, false, m_farm, m_field);	// Sulphur
      }
      else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), de_owr_fertiFP_S, false, m_farm, m_field);	// Sulphur
      if (m_farm->IsStockFarmer()) //Stock Farmer Boron treatment
      {
          SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), de_owr_fertiFA_B, false, m_farm, m_field);	// Boron
      }
      else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 10), de_owr_fertiFP_B, false, m_farm, m_field);	// Boron
      break;

  case de_owr_fertiFP_S:
      // Here comes the sulphur for arable farms
      if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_owr_fertiFP_S, true, m_farm, m_field);
              break;
          }
      break;

  case de_owr_fertiFA_S:
      // Here comes the sulphur thread for stock farms
      if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
          SimpleEvent_(g_date->Date() + 1, de_owr_fertiFA_S, true, m_farm, m_field);
          break;
      }
      break;

  case de_owr_fertiFP_B:
      if (m_ev->m_lock || m_farm->DoIt(80))
      {
          if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_owr_fertiFP_B, true, m_farm, m_field);
              break;
          }
      }
      break;

  case de_owr_fertiFA_B:
      if (m_ev->m_lock || m_farm->DoIt(80))
      {
          if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 10) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_owr_fertiFA_B, true, m_farm, m_field);
              break;
          }
      }
      break;
      // End of thread

  case de_owr_fa_slurry:
    // Main thread
    if (!m_farm->FA_Slurry( m_field, 0.0,
			    g_date->DayInYear( 15, 4 ) -
			    g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_fa_slurry, true, m_farm, m_field);
      break;
    }
    {
      int d1 = g_date->Date() + 1;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 4 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      SimpleEvent_( d1, de_owr_row_cultivation_three, false, m_farm, m_field);
    }
    break;

  case de_owr_fp_slurry:
    if (!m_farm->FP_Slurry( m_field, 0.0,
			    g_date->DayInYear( 15, 4 ) -
			    g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_fp_slurry, true, m_farm, m_field);
      break;
    }
    {
      int d1 = g_date->Date() + 1;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 1, 4 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      SimpleEvent_( d1, de_owr_row_cultivation_three, false, m_farm, m_field);
    }
    break;

  case de_owr_row_cultivation_three:
    if (!m_farm->RowCultivation( m_field, 0.0,
				 g_date->DayInYear( 30, 4 ) -
				 g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_row_cultivation_three, true, m_farm, m_field);
      break;
    }
    SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 5, 7 ),
		 de_owr_swath, false, m_farm, m_field);
    break;

  case de_owr_swath:
    if (m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if ( !m_farm->Swathing( m_field, 0.0,
            g_date->DayInYear( 25, 7 ) - g_date->DayInYear())) {
        SimpleEvent_( g_date->Date() + 1, de_owr_swath, true, m_farm, m_field);
        break;
      }
    }
    {
      int d1 = g_date->Date() + 5;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 10, 7 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 10, 7 );
      SimpleEvent_( d1, de_owr_harvest, false, m_farm, m_field);
    }
    break;

  case de_owr_harvest:
    if ( !m_farm->Harvest( m_field, 0.0,
			   g_date->DayInYear( 1, 8 ) -
			   g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_harvest, false, m_farm, m_field);
      break;
    }
    SimpleEvent_( g_date->Date(), de_owr_straw_chop, false, m_farm, m_field);
    break;

  case de_owr_straw_chop:
    if ( !m_farm->StrawChopping( m_field, 0.0,
				 g_date->DayInYear( 10, 8 ) -
				 g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_owr_straw_chop, true, m_farm, m_field);
      break;
    }
    {
      int d1 = g_date->Date() + 1;
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 15, 7 ))
	d1 = g_date->OldDays() + g_date->DayInYear( 15, 7 );
      SimpleEvent_( d1, de_owr_stub_harrow, false, m_farm, m_field);
    }
    break;

  case de_owr_stub_harrow:
    if ( m_ev->m_lock || m_farm->DoIt( 75 )) {
      if ( !m_farm->StubbleHarrowing( m_field, 0.0,
            g_date->DayInYear( 15, 8 ) - g_date->DayInYear())) {
        SimpleEvent_( g_date->Date() + 1, de_owr_stub_harrow, true, m_farm, m_field);
        break;
      }
    }
    done = true;
    break;

 default:
    g_msg->Warn( WARN_BUG, "DE_OWinterRape::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}

