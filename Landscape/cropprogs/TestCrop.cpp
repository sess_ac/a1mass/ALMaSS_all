/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/TestCrop.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_winterwheat_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WW_InsecticideDay;
extern CfgInt   cfg_WW_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;
extern CfgFloat	cfg_WW_conv_tillage_prop1;
extern CfgFloat	cfg_WW_conv_tillage_prop2;
extern CfgFloat	cfg_WW_NINV_tillage_prop1;
extern CfgFloat	cfg_WW_NINV_tillage_prop2;
extern CfgFloat	cfg_WW_isecticide_prop1;
extern CfgFloat	cfg_WW_isecticide_prop2;
extern CfgFloat	cfg_WW_isecticide_prop3;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool TestCrop::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Get the farmer's style
	FarmerStyle style = a_farm->GetFarmer()->GetFarmerStyle();

	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case testcrop_ww_start:
	{
		// testcrop_ww_start just sets up all the starting conditions and reference dates that are needed to start a testcrop_ww
		TC_WW_FERTI_P1 = false;
		TC_WW_FERTI_S1 = false;
		TC_WW_STUBBLE_PLOUGH = false;
		TC_WW_DECIDE_TO_GR = false;

		a_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 5 start and stop dates for all 'movable' events for this crop
		int noDates = 4;
		a_field->SetMDates(0, 0, g_date->DayInYear(5, 8)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(10, 8)); // last possible day of starw chopping, equal to harvest in this case
		a_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depend on previous treatment)
		a_field->SetMDates(1, 1, g_date->DayInYear(15, 8)); // end day of hay bailing
		a_field->SetMDates(0, 2, 0); // start day of RSM
		a_field->SetMDates(1, 2, g_date->DayInYear(25, 8)); // end day of RSM
		a_field->SetMDates(0, 3, 0); // start day of calcium application
		a_field->SetMDates(1, 3, g_date->DayInYear(25, 8)); // end day of calcium application
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", a_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex());
						int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				if (a_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), testcrop_ww_ferti_s5, false, a_farm, a_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), testcrop_ww_ferti_p5, false, a_farm, a_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(20, 7);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		if (a_farm->IsStockFarmer()) { // StockFarmer
			SimpleEvent_(d1, testcrop_ww_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, testcrop_ww_ferti_p1, false, a_farm, a_field);
	}
	break;

	// This is the first real farm operation
	case testcrop_ww_ferti_p1:
		// In total 10% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (5%) do it now
		if (a_ev->m_lock || a_farm->DoIt_prob(0.05 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
				// If we don't suceed on the first try, then try and try again (until 20/8 when we will suceed)
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p1, true, a_farm, a_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				TC_WW_FERTI_P1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_stubble_plough, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s1:
		// In total 80% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (40%) do it now
		if (a_ev->m_lock || a_farm->DoIt_prob(0.40 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s1, true, a_farm, a_field);
				break;
			}
			else
			{
				//Rest of farmers do slurry before autumn plough/stubble cultivation so we need to remeber who already did it
				TC_WW_FERTI_S1 = true;
			}
		}
		// Queue up the next event -in this case stubble ploughing
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_stubble_plough, false, a_farm, a_field);
		break;
	case testcrop_ww_stubble_plough:
		// some will do stubble plough, but rest will get away with non-inversion cultivation or non-tillage
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50 * cfg_WW_conv_tillage_prop1.value())) // initially 50% - can become part of style if wanted?
		{
			if (!a_farm->StubblePlough(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_stubble_plough, true, a_farm, a_field);
				break;
			}
			else
			{
				// 50% of farmers will do this, but the other 50% won't so we need to remember whether we are in one or the other group
				TC_WW_STUBBLE_PLOUGH = true;
				// Queue up the next event
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_harrow1, false, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_stubble_harrow, false, a_farm, a_field);
		break;
	case testcrop_ww_autumn_harrow1:
		if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_harrow1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 2, testcrop_ww_autumn_harrow2, false, a_farm, a_field);
		break;
	case testcrop_ww_autumn_harrow2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.40))
		{
			if (!a_farm->AutumnHarrow(a_field, 0.0, g_date->Date() + 7 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_harrow2, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 8);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, testcrop_ww_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, testcrop_ww_ferti_p2, false, a_farm, a_field);
		break;
	case testcrop_ww_stubble_harrow:
		// some will do stubble plough, but rest will get away with non-inversion cultivation or non-tillage
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00 * cfg_WW_NINV_tillage_prop1.value())) // initially 50% so no non-tillage
		{
			if (!a_farm->StubbleHarrowing(a_field, 0.0, g_date->DayInYear(25, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_stubble_harrow, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(20, 8)) {
			d1 = g_date->OldDays() + g_date->DayInYear(20, 8);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, testcrop_ww_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, testcrop_ww_ferti_p2, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p2:
		// In total 10% of arable farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (5%) do it now (if haven't done before)
		if (a_ev->m_lock || a_farm->DoIt_prob(5.0/95.0)&&(TC_WW_FERTI_P1==false))
		{
			if (!a_farm->FP_Slurry(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_plough, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s2:
		// In total 80% of stock farmers do slurry in the autumn, either before stubble plough/harrow or later before autumn plough/cultivation
		// We therefore assume that half of them (40%) do it now (if haven't done before)
		if (a_ev->m_lock || a_farm->DoIt_prob((40.0/60.0) * style.GetFertilizerStyle()) && (TC_WW_FERTI_S1==false))
		{
			if (!a_farm->FA_Slurry(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_plough, false, a_farm, a_field);
		break;
	case testcrop_ww_autumn_plough:
		// some will do autumn plough, but rest will get away with non-inversion cultivation or non-tillage
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50 * cfg_WW_conv_tillage_prop2.value())) // initially 80% but changed to 50% for meta-model fitting
		{
			if (!a_farm->AutumnPlough(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_plough, true, a_farm, a_field);
				break;
			}
			else
			{
				// Queue up the next event and if it's late (after 15/9) it has to be rolling, else next fertilizer
				d1 = g_date->OldDays() + g_date->DayInYear(15, 9);
				if (g_date->Date() > d1)
				{
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_roll, true, a_farm, a_field);
					break;
				}
				else
				{
					if (a_farm->IsStockFarmer()) //Stock Farmer
					{
						SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), testcrop_ww_ferti_s3, false, a_farm, a_field);
					}
					else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), testcrop_ww_ferti_p3, false, a_farm, a_field);
					break;
				}
			}
		}
		SimpleEvent_(g_date->Date(), testcrop_ww_stubble_cultivator_heavy, false, a_farm, a_field);
		break;
	case testcrop_ww_autumn_roll:
		if (!a_farm->AutumnRoll(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_roll, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), testcrop_ww_ferti_s3, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), testcrop_ww_ferti_p3, false, a_farm, a_field);
		break;
	case testcrop_ww_stubble_cultivator_heavy:
		// some will do autumn plough, but rest will get away with non-inversion cultivation or non-tillage
		if (a_ev->m_lock || a_farm->DoIt_prob(1.00 * cfg_WW_NINV_tillage_prop2.value())) // initially 20% so no non-tillage but changed to 50% (see comment above)
		//if (a_ev->m_lock || a_farm->DoIt_prob(100))
		{
			if (!a_farm->StubbleCultivatorHeavy(a_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_stubble_cultivator_heavy, true, a_farm, a_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), testcrop_ww_ferti_s3, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), testcrop_ww_ferti_p3, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.45 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_NPKS(a_field, 0.0, g_date->DayInYear(3, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p3, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_preseeding_cultivator, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.45 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_NPKS(a_field, 0.0, g_date->DayInYear(3, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s3, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_preseeding_cultivator, false, a_farm, a_field);
		break;
	case testcrop_ww_preseeding_cultivator:
		// 30% will do preseeding cultivation, the rest will do it together with sow
		if (a_ev->m_lock || a_farm->DoIt_prob(0.30))
		{
			if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(4, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_preseeding_cultivator, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_sow, false, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_preseeding_cultivator_sow, false, a_farm, a_field);
		break;
	case testcrop_ww_autumn_sow:
		if (!a_farm->AutumnSow(a_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_autumn_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, testcrop_ww_herbicide1, false, a_farm, a_field); // Herbidide thread
		SimpleEvent_(g_date->Date() + 3, testcrop_ww_fungicide1, false, a_farm, a_field);	// Fungicide thread
		SimpleEvent_(g_date->Date() + 20, testcrop_ww_insecticide1, false, a_farm, a_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_growth_regulator1, false, a_farm, a_field); // GR thread
		if (a_farm->IsStockFarmer()) //Stock Farmer					// PK thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_s4, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_p4, false, a_farm, a_field);
		if (a_farm->IsStockFarmer()) //Stock Farmer					// Main thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_s5, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_p5, false, a_farm, a_field);
		if (a_farm->IsStockFarmer()) //Stock Farmer					// Microelemnts thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_ferti_s8, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_ferti_p8, false, a_farm, a_field);
		break;
	case testcrop_ww_preseeding_cultivator_sow:
		// 70% will do preseeding cultivation with sow
		if (!a_farm->PreseedingCultivatorSow(a_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_preseeding_cultivator_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, testcrop_ww_herbicide1, false, a_farm, a_field); // Herbidide thread
		SimpleEvent_(g_date->Date() + 3, testcrop_ww_fungicide1, false, a_farm, a_field);	// Fungicide thread
		SimpleEvent_(g_date->Date() + 20, testcrop_ww_insecticide1, false, a_farm, a_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_growth_regulator1, false, a_farm, a_field); // GR thread
		if (a_farm->IsStockFarmer()) //Stock Farmer					// PK thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_s4, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_p4, false, a_farm, a_field);
		if (a_farm->IsStockFarmer()) //Stock Farmer					// Main thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_s5, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_ferti_p5, false, a_farm, a_field);
		if (a_farm->IsStockFarmer()) //Stock Farmer					// Microelemnts thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_ferti_s8, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_ferti_p8, false, a_farm, a_field);
		break;
	case testcrop_ww_herbicide1: // The first of the pesticide managements.
		// Here comes the herbicide thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80 * style.GetPesticideStyle()))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_herbicide1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, testcrop_ww_herbicide2, false, a_farm, a_field);
		break;
	case testcrop_ww_herbicide2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.73 * style.GetPesticideStyle()))
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(10, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_herbicide2, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_fungicide1:
		// Here comes the fungicide thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.05 * style.GetPesticideStyle()))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_fungicide1, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 4) + 365, testcrop_ww_fungicide2, false, a_farm, a_field);
		break;
	case testcrop_ww_fungicide2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80 * style.GetPesticideStyle()))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_fungicide2, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 5);
		}
		SimpleEvent_(d1, testcrop_ww_fungicide3, false, a_farm, a_field);
		break;
	case testcrop_ww_fungicide3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.68 * style.GetPesticideStyle()))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_fungicide3, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 14, testcrop_ww_fungicide4, false, a_farm, a_field);
		break;
	case testcrop_ww_fungicide4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.15 * style.GetPesticideStyle()))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(20, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_fungicide4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_insecticide1:
		// Here comes the insecticide thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.05 * style.GetPesticideStyle())) // initially 5%
		{
			if (a_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_insecticide1, true, a_farm, a_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				if (!cfg_pest_winterwheat_on.value() ||
					!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(15, 11) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, testcrop_ww_insecticide1, true, a_farm, a_field);
						break;
					}
				}
				else {
					a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
				}
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4) + 365, testcrop_ww_insecticide2, false, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 4) + 365, testcrop_ww_insecticide2, false, a_farm, a_field);
		break;
	case testcrop_ww_insecticide2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50 * style.GetPesticideStyle())) // initially 74% ale changed to 50% for meta model testing
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_winterwheat_on.value() ||
				!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_insecticide2, true, a_farm, a_field);
					break;
				}
			}
			else {
				a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		SimpleEvent_(g_date->Date() + 14, testcrop_ww_insecticide3, false, a_farm, a_field);
		break;
	case testcrop_ww_insecticide3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.25 * style.GetPesticideStyle())) // initially 38% but changed to 25% proportionally to insecticide2 change
		{
			// here we check wheter we are using ERA pesticide or not
			if (!cfg_pest_winterwheat_on.value() ||
				!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_insecticide3, true, a_farm, a_field);
					break;
				}
			}
			else {
				a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			}
		}
		// End of thread
		break;
	case testcrop_ww_growth_regulator1:
		// Here comes the GR thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.68 * style.GetPesticideStyle()))
		{
			if (!a_farm->GrowthRegulator(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_growth_regulator1, true, a_farm, a_field);
				break;
			}
			else
			{
				//We need to remeber who did GR I
				TC_WW_DECIDE_TO_GR = true;
			}
		}
		SimpleEvent_(g_date->Date() + 7, testcrop_ww_growth_regulator2, false, a_farm, a_field);
		break;
	case testcrop_ww_growth_regulator2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.24*TC_WW_DECIDE_TO_GR * style.GetPesticideStyle()))
		{
			if (!a_farm->GrowthRegulator(a_field, 0.0, g_date->Date() + 21 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_growth_regulator2, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_ferti_p4:
		// Here comes the PK thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_PK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_ferti_s4:
		// Here comes the PK thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_PK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_ferti_p5:
		// Here comes the MAIN thread
		if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p5, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, testcrop_ww_ferti_p6, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s5:
		if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s5, true, a_farm, a_field);
			break;
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, testcrop_ww_ferti_s6, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p6:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.95 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p6, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), testcrop_ww_ferti_p7, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s6:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.95 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s6, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), testcrop_ww_ferti_s7, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p7:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.40 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p7, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), testcrop_ww_harvest, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s7:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.40 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s7, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), testcrop_ww_harvest, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p8:
		// Here comes the mickroelements thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.63 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p8, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), testcrop_ww_ferti_p9, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s8:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.63 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s8, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), testcrop_ww_ferti_s9, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p9:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.08 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FP_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p9, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_ferti_s9:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.08 * style.GetFertilizerStyle()))
		{
			if (!a_farm->FA_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s9, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case testcrop_ww_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_harvest, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, testcrop_ww_straw_chopping, false, a_farm, a_field);
		break;


	case testcrop_ww_straw_chopping:
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			// 10% of stock farmers will do straw chopping, but rest will do hay bailing instead
			if (a_ev->m_lock || a_farm->DoIt_prob(0.10))
			{
				if (a_field->GetMConstants(0) == 0) {
					if (!a_farm->StrawChopping(a_field, 0.0, -1)) { // raise an error
						g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'StrawChopping' execution", "");
						exit(1);
					}
				}
				else {
					if (!a_farm->StrawChopping(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, testcrop_ww_straw_chopping, true, a_farm, a_field);
						break;
					}
					else
					{
						// Queue up the next event
						SimpleEvent_(g_date->Date(), testcrop_ww_ferti_s10, false, a_farm, a_field);
						break;
					}
				}

			}
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_hay_bailing, false, a_farm, a_field);
			break;
		}
		else
		{
			// 90% of arable farmers will do straw chopping, but rest will do hay bailing instead
			if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
			{
				if (a_field->GetMConstants(0) == 0) {
					if (!a_farm->StrawChopping(a_field, 0.0, -1)) { // raise an error
						g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'StrawChopping' execution", "");
						exit(1);
					}
				}
				else {
					if (!a_farm->StrawChopping(a_field, 0.0, a_field->GetMDates(1, 0) - g_date->DayInYear())) {
						SimpleEvent_(g_date->Date() + 1, testcrop_ww_straw_chopping, true, a_farm, a_field);
						break;
					}
					else
					{
						// Queue up the next event
						SimpleEvent_(g_date->Date(), testcrop_ww_ferti_p10, false, a_farm, a_field);
						break;
					}
				}

			}
			SimpleEvent_(g_date->Date() + 1, testcrop_ww_hay_bailing, false, a_farm, a_field);
			break;
		}
	case testcrop_ww_hay_bailing:
		if (a_field->GetMConstants(1) == 0) {
			if (!a_farm->HayBailing(a_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'HayBailing' execution", "");
				exit(1);
			}
		}
		else {
			if (!a_farm->HayBailing(a_field, 0.0, a_field->GetMDates(1,1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, testcrop_ww_hay_bailing, true, a_farm, a_field);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date(), testcrop_ww_ferti_s11, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date(), testcrop_ww_ferti_p11, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p10:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.18))
		{
			if (a_field->GetMConstants(2) == 0) {
				if (!a_farm->FP_RSM(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'FP_RSM' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->FP_RSM(a_field, 0.0, a_field->GetMDates(1,2) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p10, true, a_farm, a_field);
					break;
				}
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date(), testcrop_ww_ferti_s11, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date(), testcrop_ww_ferti_p11, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_s10:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.18))
		{
			if (a_field->GetMConstants(2) == 0) {
				if (!a_farm->FA_RSM(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'FA_RSM' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->FA_RSM(a_field, 0.0, a_field->GetMDates(1, 2) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s10, true, a_farm, a_field);
					break;
				}
			}
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date(), testcrop_ww_ferti_s11, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->Date(), testcrop_ww_ferti_p11, false, a_farm, a_field);
		break;
	case testcrop_ww_ferti_p11:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.23))
		{
			if (a_field->GetMConstants(3) == 0) {
				if (!a_farm->FP_Calcium(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'FP_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->FP_Calcium(a_field, 0.0, a_field->GetMDates(1,3) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_p11, true, a_farm, a_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case testcrop_ww_ferti_s11:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.23))
		{
			if (a_field->GetMConstants(3) == 0) {
				if (!a_farm->FA_Calcium(a_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): failure in 'FA_Calcium' execution", "");
					exit(1);
				}
			}
			else {
				if (!a_farm->FA_Calcium(a_field, 0.0, a_field->GetMDates(1, 3) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, testcrop_ww_ferti_s11, true, a_farm, a_field);
					break;
				}
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLWinterWheat::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}