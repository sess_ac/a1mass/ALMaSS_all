/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University, modified by Susanne Stein, JKI
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DE_WinterRye.cpp This file contains the source for the DE_WinterRye class</B> \n
*/
/**
\file
 by Chris J. Topping and Luna Kondrup Marcussen, modified by Susanne Stein \n
 Version of May 2021 \n
 All rights reserved. \n
 \n
*/
//
// DE_WinterRye.cpp
//
/*

Copyright (c) 2021, Christopher John Topping, University of Aarhus
All rights reserved.


Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

*) Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
*) Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
*) Neither the name of the NERI nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_WinterRye.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_fungi_app_prop2;
extern CfgFloat cfg_greg_app_prop;
extern CfgBool cfg_pest_winterrye_on;
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WR_InsecticideDay;
extern CfgInt   cfg_WR_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_WinterRye::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	bool flag = false;
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_DEWinterRye; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case de_wry_start:
	{
		// de_wry_start just sets up all the starting conditions and reference dates that are needed to start a de_wry
		DE_WRY_NOTILL = false;
		DE_WRY_DECIDE_TO_GR = false;
		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// Below is an example (you need to adjust it) of 2 start and stop dates for all 'movable' events for this crop; just need to adjust that
		int noDates = 2;
		m_field->SetMDates(0, 0, g_date->DayInYear(31, 8)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(5, 9)); // last possible day of straw chopping
		m_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depend on previous treatment)
		m_field->SetMDates(1, 1, g_date->DayInYear(10, 9)); // end day of hay bailing

		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to 0

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "DE_WinterRye::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "DE_WinterRye::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "DE_WinterRye::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				if (m_farm->IsStockFarmer()) //Stock Farmer					// Main thread
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_wry_ferti_s2, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), de_wry_ferti_p2, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// LKM: Here we queue up the first event - this is divides farmers (no-till or till) - suggesting 50% does each
		//
		SimpleEvent_(d1, de_wry_autumn_plough, false, m_farm, m_field);
	}
	break;
	// This is the first real farm operation - LKM: done if no-till management, before 1st of October

	case de_wry_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt(50))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_autumn_plough, true, m_farm, m_field);
				break;
			}
			// Queue up the next event - in this case autumn roll
			SimpleEvent_(g_date->Date() + 1, de_wry_autumn_roll, false, m_farm, m_field);
			break;
		}
		else SimpleEvent_(g_date->Date() + 1, de_wry_autumn_harrow_notill, false, m_farm, m_field);
		break;
	case de_wry_autumn_roll:
		if (!m_farm->AutumnRoll(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wry_autumn_roll, true, m_farm, m_field);
			break;
		}
		// Queue up the next event - in this case autumn sow 
		SimpleEvent_(g_date->Date(), de_wry_autumn_sow, false, m_farm, m_field);
		break;
	case de_wry_autumn_harrow_notill:
		// some will do stubble plough, but rest will get away with non-inversion cultivation or non-tillage
		if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(1, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wry_autumn_harrow_notill, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date(), de_wry_autumn_sow, false, m_farm, m_field);
		break;
		//LKM: no-till and till branches meet here
	case de_wry_autumn_sow:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(15, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wry_autumn_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 3, de_wry_herbicide1, false, m_farm, m_field); // Herbicide thread
		SimpleEvent_(g_date->Date() + 3, de_wry_insecticide1, false, m_farm, m_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 3) + 365, de_wry_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 4) + 365, de_wry_growth_regulator1, false, m_farm, m_field); // GR thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// P thread (K is applied before harvest)
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wry_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wry_ferti_p1, false, m_farm, m_field);
		if (m_farm->IsStockFarmer()) //Stock Farmer					// Main thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wry_ferti_s2, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wry_ferti_p2, false, m_farm, m_field);
		break;

	case de_wry_herbicide1: // The first of the pesticide managements.
		// Here comes the herbicide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 11) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3) + 365, de_wry_herbicide2, false, m_farm, m_field);
		break;
	case de_wry_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.70))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wry_insecticide1:
		// Here comes the insecticide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) // only 10% use insecticides
		{
			if (m_field->GetGreenBiomass() <= 0) {
				SimpleEvent_(g_date->Date() + 1, de_wry_insecticide1, true, m_farm, m_field);
			}
			else
			{
				// here we check wheter we are using ERA pesticide or not
				d1 = g_date->DayInYear(30, 11) - g_date->DayInYear();
				if (!cfg_pest_winterrye_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
				{
					flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
				}
				else {
					flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
				}
				if (!flag) {
					SimpleEvent_(g_date->Date() + 1, de_wry_insecticide1, true, m_farm, m_field);
					break;
				}
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5) + 365, de_wry_insecticide2, false, m_farm, m_field);
		break;
	case de_wry_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.10)) // 
		{
			// here we check wheter we are using ERA pesticide or not
			d1 = g_date->DayInYear(10, 6) - g_date->DayInYear();
			if (!cfg_pest_winterrye_on.value() || !m_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
			{
				flag = m_farm->InsecticideTreat(m_field, 0.0, d1);
			}
			else {
				flag = m_farm->ProductApplication(m_field, 0.0, d1, cfg_pest_product_1_amount.value(), ppp_1);
			}
			if (!flag) {
				SimpleEvent_(g_date->Date() + 1, de_wry_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wry_fungicide1:
		// Here comes the fungicide thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_fungicide1, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 14;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 5)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 5);
		}
		SimpleEvent_(d1, de_wry_fungicide2, false, m_farm, m_field);
		break;
	case de_wry_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.40))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		break;

		// End of thread
		break;
	case de_wry_growth_regulator1:
		// Here comes the GR thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_growth_regulator1, true, m_farm, m_field);
				break;
			}
			else
			{
				//We need to remember who did GR I
				DE_WRY_DECIDE_TO_GR = true;
			}
		}
		SimpleEvent_(g_date->Date() + 7, de_wry_growth_regulator2, false, m_farm, m_field);
		break;
	case de_wry_growth_regulator2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.60 * DE_WRY_DECIDE_TO_GR))
		{
			if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->Date() + 21 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_growth_regulator2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wry_ferti_s1:
		// Here comes the P thread
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30))
		{
			if (!m_farm->FP_P(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wry_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.30))
		{
			if (!m_farm->FA_P(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
	case de_wry_ferti_p2:
		// Here comes the MAIN thread
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wry_ferti_p2, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_wry_ferti_p3, false, m_farm, m_field);
		break;
	case de_wry_ferti_s2:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wry_ferti_s2, true, m_farm, m_field);
			break;
		}
		d1 = g_date->Date() + 10;
		if (d1 < g_date->OldDays() + g_date->DayInYear(1, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		}
		SimpleEvent_(d1, de_wry_ferti_s3, false, m_farm, m_field);
		break;
	case de_wry_ferti_p3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_ferti_p3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), de_wry_harvest, false, m_farm, m_field);
	case de_wry_ferti_s3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.80))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_ferti_s3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), de_wry_harvest, false, m_farm, m_field);
		break;
	case de_wry_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, de_wry_harvest, true, m_farm, m_field);
			break;
		}
		// 90% of farmers will leave straw on field and rest will do hay bailing
		if (m_farm->DoIt_prob(0.90))
		{
			SimpleEvent_(g_date->Date() + 1, de_wry_straw_chopping, false, m_farm, m_field);
		}
		else
		{
			SimpleEvent_(g_date->Date() + 1, de_wry_hay_bailing, false, m_farm, m_field);
		}
		break;
	case de_wry_straw_chopping:
		if (m_field->GetMConstants(0) == 0) {
			if (!m_farm->StrawChopping(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "DE_WinterRye::Do(): failure in 'StrawChopping' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_straw_chopping, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case de_wry_hay_bailing:
		if (m_field->GetMConstants(1) == 0) {
			if (!m_farm->HayBailing(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "DE_WinterRye::Do(): failure in 'HayBailing' execution", "");
			}
		}
		else {
			if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, de_wry_hay_bailing, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "DE_WinterRye::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}
