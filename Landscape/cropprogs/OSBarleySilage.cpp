//
// OSBarleySilage.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OSBarleySilage.h"

bool OSBarleySilage::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  bool done = false;

  switch ( m_ev->m_todo )
  {
  case osbs_start:
    {
      a_field->ClearManagementActionSum();

      // Set up the date management stuff
      m_last_date=g_date->DayInYear(10,10);
      // Start and stop dates for all events after harvest
      int noDates= 3;
      m_field->SetMDates(0,0,g_date->DayInYear(25,7));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(25,8));
      m_field->SetMDates(0,1,g_date->DayInYear(10,10));
      m_field->SetMDates(1,1,g_date->DayInYear(10,10));
      m_field->SetMDates(0,2,g_date->DayInYear(25,7));
      m_field->SetMDates(1,2,g_date->DayInYear(1,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary
      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "OSBarleySilage::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++)
        {
          if  (m_field->GetMDates(0,i)>=m_ev->m_startday)
                                     m_field->SetMDates(0,i,m_ev->m_startday-1);
          if  (m_field->GetMDates(1,i)>=m_ev->m_startday)
                                     m_field->SetMDates(1,i,m_ev->m_startday-1);
        }
      }
      // Now no operations can be timed after the start of the next crop.
	  int d1 = 0;
      if ( ! m_ev->m_first_year )
      {
		  int today=g_date->Date();
		  // Are we before July 1st?
		  d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		  if (today < d1)
		  {
			  // Yes, too early. We assumme this is because the last crop was late
			  g_msg->Warn( WARN_BUG, "OSBarleySilage::Do(): "
				  "Crop start attempt between 1st Jan & 1st July", "" );
			  exit( 1 );
		  }
		  else
		  {
			  d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
			  if (today > d1)
			  {
				  // Yes too late - should not happen - raise an error
				  g_msg->Warn( WARN_BUG, "OBarleySilage::Do(): "
					  "Crop start attempt after last possible start date", "" );
				  exit( 1 );
			  }
		  }
	  }
      // All OK, let's go.
      SimpleEvent( d1, osbs_ferti_zero, false );
    }
    break;

  case osbs_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear()))
      {
        SimpleEvent( g_date->Date() + 1, osbs_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->Date(),osbs_ferti_one, false );
    break;

  case osbs_ferti_one:
    if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbs_ferti_one, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 3 ),
                 osbs_spring_plough, false );
    break;

  case osbs_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbs_spring_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), osbs_spring_harrow, false );
    break;

  case osbs_spring_harrow:
      if (!m_farm->SpringHarrow( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbs_spring_harrow, true );
      break;
    }
    SimpleEvent( g_date->Date(), osbs_spring_roll, false );
    break;

  case osbs_spring_roll:
    if (!m_farm->SpringRoll( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbs_spring_roll, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20, 3 ),
                 osbs_spring_sow1, false );
    break;

  case osbs_spring_sow1:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 12, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbs_spring_sow1, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
                 osbs_spring_sow2, false );
    break;

  case osbs_spring_sow2:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 12, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbs_spring_sow2, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,6 ),
                 osbs_water1, false );
    break;

  case osbs_water1:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 15,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbs_water1, true );
        break;
      }
      // Must have watered so do it again
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,6 ),
                 osbs_water2, false );
      break;
    }
    // Must have failed to water so go straight to cut to silage
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,7 ),
                 osbs_cut_to_silage, false );
    break;

  case osbs_water2:
    if (!m_farm->Water( m_field, 0.0,
         g_date->DayInYear( 30,6 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbs_water2, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,2),
                 osbs_cut_to_silage, false );
    break;

  case osbs_cut_to_silage:
      if (!m_farm->CutToSilage( m_field, 0.0,
           m_field->GetMDates(1,2) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbs_cut_to_silage, true );
        break;
      }
      m_field->SetGrowthPhase( harvest1 );
      done=true;
      // END OF MAIN THREAD
      break;


  default:
    g_msg->Warn( WARN_BUG, "OSBarleySilage::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


