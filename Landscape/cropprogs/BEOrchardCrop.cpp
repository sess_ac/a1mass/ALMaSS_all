//
// BEOrchardCrop.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/BEOrchardCrop.h"

extern CfgBool cfg_pest_orchard_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_ORCH_InsecticideDay;
extern CfgInt   cfg_ORCH_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


bool BEOrchardCrop::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  bool done = false;  // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
  int d1 = 0;
  // Depending what event has occured jump to the correct bit of code
  switch ( a_ev->m_todo )
  {
  case BE_orch_start:
  {
	  BE_ORCH_INSECT = false;
	  BE_ORCH_ISECT_DAY = 0;


      // Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 1 start and stop dates for all 'movable' events for this crop
	  int noDates = 1;
      a_field->SetMDates(0,0,g_date->DayInYear(1,9));
      a_field->SetMDates(1,0,g_date->DayInYear(1,9));

      // Check the next crop for early start, uBEess it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (a_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (a_field->GetMDates(0,0) >=a_ev->m_startday){
          g_msg->Warn( WARN_BUG, "BEOrchardCrop::Do(): Harvest too late for the next crop to start!!!", "" );
		  int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
		  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
		  exit(1);
        }
        // Now fix any late finishing problems
        for (int i=0; i<noDates; i++) {
			if(a_field->GetMDates(0,i)>=a_ev->m_startday) { 
				a_field->SetMDates(0,i,a_ev->m_startday-1); //move the starting date
			}
			if(a_field->GetMDates(1,i)>=a_ev->m_startday){
				a_field->SetMConstants(i,0); 
				a_field->SetMDates(1,i,a_ev->m_startday-1); //move the finishing date
			}
		}
      }
	  // Now no operations can be timed after the start of the next crop.

	  if (!a_ev->m_first_year) {
		  d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
		  if (g_date->Date() > d1) {
			  // Yes too late - should not happen - raise an error
			  g_msg->Warn(WARN_BUG, "BEOrchardCrop::Do(): ", "Crop start attempt after last possible start date");
			  int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
			  g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
			  int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
			  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
			  exit(1);
		  }
	  }
	  else {
		  // Is the first year
		  // Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
		  // Code for first spring treatment used
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), BE_orch_cut1, false, a_farm, a_field);
		  break;
	  }
	 }//if

	  // End single block date checking code. Please see next line comment as well.
	  // Reinit d1 to first possible starting date.
	 d1 = g_date->OldDays() + g_date->DayInYear(15, 4);
	 if (g_date->Date() >= d1) d1 += 365;
	 // OK, let's go.
	 // Here we queue up the first event
	 SimpleEvent_(d1, BE_orch_insecticide1, false, a_farm, a_field);
	 SimpleEvent_(d1, BE_orch_cut1, false, a_farm, a_field);
  }
  break;
    
	
  case BE_orch_cut1:
	  if (BE_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, BE_orch_cut1, false, a_farm, a_field);
	  }
	  else
	  {
		  if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, BE_orch_cut1, true, a_farm, a_field);
			  break;
		  }
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 5), BE_orch_cut2, false, a_farm, a_field);
		  break;
	  }
	  break;
  case BE_orch_cut2:
	  if (BE_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, BE_orch_cut2, false, a_farm, a_field);
	  }
	  else
	  {
		  if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, BE_orch_cut2, true, a_farm, a_field);
			  break;
		  }
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 7), BE_orch_cut3, false, a_farm, a_field);
		  break;
	  }
	  break;
  case BE_orch_cut3:
	  if (BE_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, BE_orch_cut3, false, a_farm, a_field);
	  }
	  else
	  {
		  if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(20, 7) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, BE_orch_cut3, true, a_farm, a_field);
			  break;
		  }
		  SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 9), BE_orch_cut4, false, a_farm, a_field);
		  break;
	  }
	  break;
  case BE_orch_cut4:
	  if (BE_ORCH_ISECT_DAY >= g_date->Date() - 2) { // Should by at least 3 days after insecticide
		  SimpleEvent_(g_date->Date() + 1, BE_orch_cut4, false, a_farm, a_field);
	  }
	  else
	  {
		  if (!a_farm->CutOrch(a_field, 0.0, g_date->DayInYear(20, 9) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, BE_orch_cut4, true, a_farm, a_field);
			  break;
		  }
		  // End of thread
		  done = true;
		  break;
	  }
	  break;
  case BE_orch_insecticide1:
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.50))
	  {
		  // here we check wheter we are using ERA pesticide or not
		  if (!cfg_pest_orchard_on.value() ||
			  !a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				  SimpleEvent_(g_date->Date() + 1, BE_orch_insecticide1, true, a_farm, a_field);
				  break;
			  }
		  }
		  else {
			  a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  BE_ORCH_ISECT_DAY = g_date->Date();
		  SimpleEvent_(g_date->Date() + 60, BE_orch_insecticide3, false, a_farm, a_field);
		  break;
	  }
	  SimpleEvent_(g_date->Date() + 30, BE_orch_insecticide2, false, a_farm, a_field);
	  break;

  case BE_orch_insecticide2:
	  // here we check wheter we are using ERA pesticide or not
	  if (!cfg_pest_orchard_on.value() ||
		  !a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
	  {
		  if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 5) - g_date->DayInYear())) {
			  SimpleEvent_(g_date->Date() + 1, BE_orch_insecticide2, true, a_farm, a_field);
			  break;
		  }
	  }
	  else {
		  a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
	  }
	  BE_ORCH_ISECT_DAY = g_date->Date();
	  SimpleEvent_(g_date->Date() + 30, BE_orch_insecticide3, false, a_farm, a_field);
	  break;
  case BE_orch_insecticide3:
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
	  {
		  // here we check wheter we are using ERA pesticide or not
		  if (!cfg_pest_orchard_on.value() ||
			  !a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
				  SimpleEvent_(g_date->Date() + 1, BE_orch_insecticide3, true, a_farm, a_field);
				  break;
			  }
		  }
		  else {
			  a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  BE_ORCH_INSECT = true;
		  BE_ORCH_ISECT_DAY = g_date->Date();
	  }
	  SimpleEvent_(g_date->Date() + 60, BE_orch_insecticide4, false, a_farm, a_field);
	  break;
  case BE_orch_insecticide4:
	  if (a_ev->m_lock || a_farm->DoIt_prob(0.67) && (BE_ORCH_INSECT==1))
	  {
		  // here we check wheter we are using ERA pesticide or not
		  if (!cfg_pest_orchard_on.value() ||
			  !a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		  {
			  if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(30, 8) - g_date->DayInYear())) {
				  SimpleEvent_(g_date->Date() + 1, BE_orch_insecticide4, true, a_farm, a_field);
				  break;
			  }
		  }
		  else {
			  a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  BE_ORCH_ISECT_DAY = g_date->Date();
	  }
	  // End of thread
	  break;
  default:
	  g_msg->Warn(WARN_BUG, "BEOrchardCrop::Do(): "
		  "Unknown event type! ", "");
	  exit(1);
	}
	return done;
}


