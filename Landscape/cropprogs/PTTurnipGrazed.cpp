/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PTTurnipGrazed.cpp This file contains the source for the PTTurnipGrazed class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PTTurnipGrazed.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PTTurnipGrazed.h"

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional TurnipGrazed.
*/
bool PTTurnipGrazed::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_PTTurnipGrazed; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case pt_tg_start:
	{
		// pt_tg_start just sets up all the starting conditions and reference dates that are needed to start a pt_tg
		PT_FL1_CATTLEOUT_DATE = 0;

		// Set up the date management stuff

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(31, 12) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PTTurnipGrazed::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			// Now no operations can be timed after the start of the next crop.
			// AAS: Checking the past... after first year
			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PTTurnipGrazed::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PTTurnipGrazed::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				// AAS: Only for the first year... e.g. first date of one of the first events
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pt_tg_autumn_harrow, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(10, 10); //@AAS: First possible data to start the first event, in this case: stubble harrow
		// OK, let's go.
		// Here we queue up the first event
		SimpleEvent_(d1, pt_tg_stubble_harrow, false, m_farm, m_field);
	}
	break;

	// This is the first real farm operation
	case pt_tg_stubble_harrow:
		if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_tg_stubble_harrow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pt_tg_autumn_plough, false, m_farm, m_field);
		break;
	case pt_tg_autumn_plough:
		if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_tg_autumn_plough, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date()+1, pt_tg_autumn_harrow, false, m_farm, m_field);
		break;
	case pt_tg_autumn_harrow:
		if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_tg_autumn_harrow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pt_tg_autumn_sow, false, m_farm, m_field);
		break;
	case pt_tg_autumn_sow:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(10, 11) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_tg_autumn_sow, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 12), pt_tg_cattle_out, false, m_farm, m_field);
		break;
	case pt_tg_cattle_out:
		if (!m_farm->CattleOutLowGrazing(m_field, 0.0, g_date->DayInYear(5, 12) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pt_tg_cattle_out, true, m_farm, m_field);
			break;
		}
		PT_FL1_CATTLEOUT_DATE = g_date->DayInYear();
		SimpleEvent_(g_date->Date()+1, pt_tg_cattle_is_out, false, m_farm, m_field);
		break;
	case pt_tg_cattle_is_out:    // Keep the cattle out there
		if (!m_farm->CattleIsOutLow2(m_field, 0.0, g_date->DayInYear() - PT_FL1_CATTLEOUT_DATE, g_date->DayInYear(28, 12), 30)) {
			SimpleEvent_(g_date->Date() + 1, pt_tg_cattle_is_out, false, m_farm, m_field);
			break;
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "PTTurnipGrazed::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}