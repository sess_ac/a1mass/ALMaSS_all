//
// SpringBarley.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/SpringBarley.h"
#include "math.h"

extern CfgBool cfg_pest_springbarley_on;
extern CfgBool cfg_pest_winterwheat_on;

extern CfgFloat cfg_ins_app_prop1;
extern CfgFloat cfg_ins_app_prop2;
extern CfgFloat cfg_ins_app_prop3;
extern CfgFloat cfg_herbi_app_prop;
extern CfgFloat cfg_fungi_app_prop1;
extern CfgFloat cfg_greg_app_prop;
extern CfgInt 	cfg_SB_InsecticideDay;
extern CfgInt   cfg_SB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;

CfgBool cfg_SpringBarley_SkScrapes("CROP_SB_SK_SCRAPES",CFG_CUSTOM, false);


bool SpringBarley::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{

  double ins_app_prop=cfg_ins_app_prop1.value();
  double herbi_app_prop=cfg_herbi_app_prop.value();
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case sb_start:
  {
	SB_DECIDE_TO_HERB =1;
	SB_DECIDE_TO_FI =1;
    a_field->ClearManagementActionSum();

	  // Record whether skylark scrapes are present and adjust flag accordingly
	if (cfg_SpringBarley_SkScrapes.value()) {
		a_field->m_skylarkscrapes=true;
	} else {
		a_field->m_skylarkscrapes=false;
	}
      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date=g_date->DayInYear(30,8);
      // Start and stop dates for all events after harvest
      int noDates=2;
      m_field->SetMDates(0,0,g_date->DayInYear(20,8));
      // Determined by harvest date - used to see if at all possible
      m_field->SetMDates(1,0,g_date->DayInYear(10,8));
      m_field->SetMDates(0,1,g_date->DayInYear(10,8));
      m_field->SetMDates(1,1,g_date->DayInYear(30,8));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

	  //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	  int d1;
	  if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

      if (m_ev->m_startday>g_date->DayInYear(1,7))
      {
        if (m_field->GetMDates(0,0) >=m_ev->m_startday)
        {
          g_msg->Warn( WARN_BUG, "SpringBarley::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
          exit( 1 );
        }
        // Now fix any late finishing problems
		for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
      }
      // Now no operations can be timed after the start of the next crop.

      if ( ! m_ev->m_first_year )
      {
		int today=g_date->Date();
			// Are we before July 1st?
		d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
		if (today < d1)
        {
	  // Yes, too early. We assumme this is because the last crop was late
          g_msg->Warn( WARN_BUG, "SpringBarley::Do(): " "Crop start attempt between 1st Jan & 1st July", "" );
          exit( 1 );
		}
        else
        {
          d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
          if (today > d1)
          {
            // Yes too late - should not happen - raise an error
            g_msg->Warn( WARN_BUG, "SpringBarley::Do(): " "Crop start attempt after last possible start date", "" );
            exit( 1 );
          }
        }
      }
      else
      {
         SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,4 ),
                                                     sb_spring_plough, false );
         break;
      }
	  }//if

      // End single block date checking code. Please see next line
      // comment as well.
      // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 2,11 );
      if ( g_date->Date() > d1 ) {
	d1 = g_date->Date();
      }
	  

      // OK, let's go.
      SimpleEvent( d1, sb_autumn_plough, false );
      SB_SLURRY_DONE=false;
      SB_MANURE_DONE=false;
      SB_SLURRY_EXEC=false;
      SB_MANURE_EXEC=false;
      SB_DID_AUTUMN_PLOUGH = false;
    }
    break;

  case sb_autumn_plough:
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) // was 70
    {
      if (!m_farm->AutumnPlough( m_field, 0.0,
           g_date->DayInYear( 30,11 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_autumn_plough, true );
        break;
      }
      SB_DID_AUTUMN_PLOUGH = true;
    }
    // +365 for next year
    if (m_farm->IsStockFarmer()) // StockFarmer
    {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365, // was 15,3
               sb_fertslurry_stock, false );
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 )+365, // was 15,3
               sb_fertmanure_stock_one, false );
    }
    else {                        // PlantFarmer
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 )+365,  // was 20/3
               sb_spring_plough, false );
    }
    break;

  //*** The stock farmers thread
  case sb_fertslurry_stock:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {  // Was 15/4
        SimpleEvent( g_date->Date() + 1, sb_fertslurry_stock, true );
        break;
      }
      SB_SLURRY_EXEC=true;
    }
    SB_SLURRY_DONE = true;
    if ( SB_MANURE_DONE ) {
      // We are the last thread, so queue up spring plough.
      SimpleEvent( g_date->Date(), sb_spring_plough, false );
    }
    break;

  case sb_fertmanure_stock_one:
    if ( m_ev->m_lock || m_farm->DoIt( 67 ))
    {
      if (!m_farm->FA_Manure( m_field, 0.0,
           g_date->DayInYear( 15,5 ) - g_date->DayInYear())) { // Was 15/5
        SimpleEvent( g_date->Date() + 1, sb_fertmanure_stock_one, true );
        break;
      }
      SB_MANURE_EXEC=true;
    }
    SB_MANURE_DONE = true;
    if ( SB_SLURRY_DONE ) {
      SimpleEvent( g_date->Date(), sb_spring_plough, false );
    }
    break;

  case sb_spring_plough:
    if ( ! SB_DID_AUTUMN_PLOUGH )
    {
      if (!m_farm->SpringPlough( m_field, 0.0,
				 g_date->DayInYear( 10,4 ) - // was 10,4
				 g_date->DayInYear())) {
        SimpleEvent( g_date->Date()+1, sb_spring_plough, true );
        break;
      }
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 20,3 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 20,3 );
      }
      SimpleEvent( d1, sb_spring_harrow, false );
    }
    break;

  case sb_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 10,4 ) - g_date->DayInYear())) {  // WAS 10,4
      SimpleEvent( g_date->Date() + 1, sb_spring_harrow, true );
      break;
    }
    if (m_farm->IsStockFarmer()) {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,4 ), // was 5,4
		   sb_fertmanure_stock_two, false );
    } else {
      SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
		   sb_fertmanure_plant, false );
    }
   break;

  case sb_fertmanure_stock_two:
    if ( m_ev->m_lock || m_farm->DoIt( 65 ) ||
	 (!SB_SLURRY_EXEC && !SB_MANURE_EXEC))
    {
      if (!m_farm->FA_NPK( m_field, 0.0,
           g_date->DayInYear( 10,4 ) - g_date->DayInYear())) { // was 10,4
        SimpleEvent( g_date->Date() + 1, sb_fertmanure_stock_two, true );
        break;
      }
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,3 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 25,3 );
      }
      SimpleEvent( d1, sb_spring_sow, false );
    }
    break;

  case sb_fertmanure_plant:
    if ( m_ev->m_lock || m_farm->DoIt( 75 ))
    {
      if (!m_farm->FP_NPK( m_field, 0.0,
           g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {  // WAS 10,4
        SimpleEvent( g_date->Date() + 1, sb_fertmanure_plant, true );
        break;
      }
      // Did FP_NPK so go directly to spring sow.
      {
	int d1 = g_date->Date();
	if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,3 )) { // was 25,3
	  d1 = g_date->OldDays() + g_date->DayInYear( 25,3 ); // was 25,3
	}
	SimpleEvent( d1, sb_spring_sow, false );
      }
      break;
    }
    SimpleEvent( g_date->Date(), sb_fertlnh3_plant, false );
    break;

  case sb_fertlnh3_plant:
    if (!m_farm->FP_LiquidNH3( m_field, 0.0,
			       g_date->DayInYear( 10, 4 ) - // WAS 10,4
			       g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sb_fertlnh3_plant, true );
      break;
    }
    SimpleEvent( g_date->Date(), sb_fertpk_plant, false );
    break;

  case sb_fertpk_plant:
    if (!m_farm->FP_PK( m_field, 0.0,
			g_date->DayInYear( 10, 4 ) -   // WAS 10,4
			g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sb_fertpk_plant, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,3 )) { // was 25,3
	d1 = g_date->OldDays() + g_date->DayInYear( 25,3 ); // was 25,3
      }
      SimpleEvent( d1, sb_spring_sow, false );
    }
    break;

  case sb_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 10,4 ) - g_date->DayInYear())) {   // WAS 10,4
      SimpleEvent( g_date->Date() + 1, sb_spring_sow, true );
      break;
    }
    {
      int d1 = g_date->Date();
      if ( d1 < g_date->OldDays() + g_date->DayInYear( 5,4 )) {
	d1 = g_date->OldDays() + g_date->DayInYear( 5,4 );
      }
      SimpleEvent( d1, sb_spring_roll, false );
    }
    break;

  case sb_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 0 )) // was 30
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_spring_roll, true );
        break;
      }
    }
    SB_HERBI_DATE  = 0;
    SB_GR_DATE     = 0;
    SB_FUNGI_DATE  = 0;
    SB_WATER_DATE  = 0;
    SB_INSECT_DATE = 0;
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,5 ),
		 sb_herbicide_one, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
                 sb_fungicide_one, false ); 
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 10,5 ),
                 sb_GR, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,5 ),
                 sb_water_one, false );
	SimpleEvent(g_date->OldDays() + g_date->DayInYear(cfg_SB_InsecticideDay.value(), cfg_SB_InsecticideMonth.value()), // Was 15,5 - changed for skylark testing
		         sb_insecticide, false ); // MAIN THREAD
	break;

  // Herbicide thread

	//ask for your BIherb: >0 - want to spray, otherwise - don't spray even if you should

  case sb_herbicide_one:
    if ( g_date->Date() < SB_GR_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_herbicide_one, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(0.5+70*herbi_app_prop * m_farm->Prob_multiplier()))) //modified probability
    {
			if (!m_farm->HerbicideTreat( m_field, 0.0, g_date->DayInYear( 15,5 ) - g_date->DayInYear()))
			{
				SimpleEvent( g_date->Date() + 1, sb_herbicide_one, true );
				break;
			}
		
		SB_HERBI_DATE = g_date->Date();
		// Did first spray so see if should do another, 14 days later (min)
		{
		int d1 = g_date->Date() + 10;
		if ( d1 < g_date->OldDays() + g_date->DayInYear( 16,5 )) {
			d1 = g_date->OldDays() + g_date->DayInYear( 16,5 );
		}
		SimpleEvent( d1, sb_herbicide_two, false );
      }
    }
    break;

  case sb_herbicide_two:
    if ( g_date->Date() < SB_GR_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_herbicide_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(0.5+47*herbi_app_prop*SB_DECIDE_TO_HERB * m_farm->Prob_multiplier()))) //modified probability
    {
      if (!m_farm->HerbicideTreat( m_field, 0.0,
           g_date->DayInYear( 30,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_herbicide_two, true );
        break;
      }
      SB_HERBI_DATE = g_date->Date();
    }
    break;

  // GReg thread
  case sb_GR:
    if ( g_date->Date() < SB_HERBI_DATE + 1 ||
	 g_date->Date() < SB_FUNGI_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_GR, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(0.5+5*cfg_greg_app_prop.value())))
    {
      if (!m_farm->GrowthRegulator( m_field, 0.0,
           g_date->DayInYear( 25,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_GR, true );
        break;
      }
      SB_GR_DATE = g_date->Date();
    }
    break;

  // Fungicide thread
  case sb_fungicide_one:
    if ( g_date->Date() < SB_HERBI_DATE + 1 || g_date->Date() < SB_GR_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_fungicide_one, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(0.5+30*cfg_fungi_app_prop1.value() * m_farm->Prob_multiplier()))) //modified probability
    {
			if (!m_farm->FungicideTreat( m_field, 0.0, g_date->DayInYear( 25,5 ) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, sb_fungicide_one, true );
				break;
			}
		SB_FUNGI_DATE = g_date->Date();
		{
		int d1 = g_date->Date() + 10;
		if ( d1 < g_date->OldDays() + g_date->DayInYear( 25,5 )) {
			d1 = g_date->OldDays() + g_date->DayInYear( 25,5 );
		}
		SimpleEvent( d1, sb_fungicide_two, false );
		}
    }
    break;

  case sb_fungicide_two:
    if ( g_date->Date() < SB_HERBI_DATE + 1 ||
	 g_date->Date() < SB_GR_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_fungicide_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( (int) floor(0.5+10*cfg_fungi_app_prop1.value()*SB_DECIDE_TO_FI * m_farm->Prob_multiplier()) )) //modified probability
    {
      if (!m_farm->FungicideTreat( m_field, 0.0,
           g_date->DayInYear( 10,6 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_fungicide_two, true );
        break;
      }
      SB_FUNGI_DATE = g_date->Date();
    }
    break;

  // Water thread
  case sb_water_one:
    if ( g_date->Date() < SB_HERBI_DATE + 1 ||
	 g_date->Date() < SB_FUNGI_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_water_one, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->Water( m_field, 0.0,
        g_date->DayInYear( 30,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_water_one, true );
        break;
      }
      SB_WATER_DATE = g_date->Date();
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,6 ),
		 sb_water_two, false );
    break;

  case sb_water_two:
    if ( g_date->Date() < SB_INSECT_DATE + 1 ) {
      SimpleEvent( g_date->Date() + 1, sb_water_two, m_ev->m_lock );
      break;
    }
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->Water( m_field, 0.0,
           g_date->DayInYear( 1,7 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, sb_water_two, true );
        break;
      }
      SB_WATER_DATE = g_date->Date();
    }
    break;

  // Insecticide thread  & MAIN THREAD
  case sb_insecticide:
	  if (g_date->Date() < SB_WATER_DATE + 1) {
		  SimpleEvent(g_date->Date() + 1, sb_insecticide, m_ev->m_lock);
		  break;
	  }
	  if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * ins_app_prop*m_farm->Prob_multiplier()))) //modified probability
	  {
			  // Here we check wheter we are using ERA pesticide or not
			  if (!cfg_pest_springbarley_on.value() || !a_field->GetLandscape()->SupplyShouldSpray()) {
				  if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
					  SimpleEvent(g_date->Date() + 1, sb_insecticide, true);
					  break;
				  }
			  else {
				  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
			  }
			  SB_INSECT_DATE = g_date->Date();
		  }
	  }
	  ChooseNextCrop(2);
	  SimpleEvent(g_date->Date() + 14, sb_insecticide2, false);
	  SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 8), sb_harvest, false);
	  break;

  case sb_insecticide2:
	  if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop2.value()*m_farm->Prob_multiplier()))) //modified probability
	  {
		  if (cfg_pest_springbarley_on.value() && a_field->GetLandscape()->SupplyShouldSpray())
		  {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  else
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, SB_INSECT_DATE+14 - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, sb_insecticide2, true);
			  }
		  }
	  }
	  SimpleEvent(g_date->Date() + 14, sb_insecticide3, false);
	  break;

  case sb_insecticide3:
	  if (m_ev->m_lock || m_farm->DoIt((int)floor(0.5 + 35 * cfg_ins_app_prop3.value()*m_farm->Prob_multiplier()))) //modified probability
	  {
		  if (cfg_pest_springbarley_on.value() && a_field->GetLandscape()->SupplyShouldSpray())
		  {
			  m_farm->ProductApplication_DateLimited(m_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		  }
		  else
		  {
			  if (!m_farm->InsecticideTreat(m_field, 0.0, SB_INSECT_DATE+28 - g_date->DayInYear())) {
				  SimpleEvent(g_date->Date() + 1, sb_insecticide3, true);
			  }
		  }
	  }
	  break; // End of thread


  case sb_harvest:
    if (!m_farm->Harvest( m_field, 0.0, g_date->DayInYear( 20,8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, sb_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), sb_straw_chopping, false );
    break;

  case sb_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 50 )) // was 50
    {
      // Force straw chopping to happen on the same day as harvest.
      if (!m_farm->StrawChopping( m_field, 0.0, 0 )) {
	// Shouldn't happen.
        SimpleEvent( g_date->Date(), sb_straw_chopping, true );
        break;
      }
      // Did chop, so go directly to stubble harrowing.
      SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
                sb_stubble_harrow, false );
      break;
    }
    // Do hay baling first.
    SimpleEvent( g_date->Date(), sb_hay_baling, false );
    break;

  case sb_hay_baling:
	 if (m_field->GetMConstants(0)==0) {
				if (!m_farm->HayBailing( m_field, 0.0, -1)) { //raise an error
					g_msg->Warn( WARN_BUG, "SpringBarley::Do(): failure in 'HayBailing' execution", "" );
					exit( 1 );
				} 
	}
	else {
		if (!m_farm->HayBailing( m_field, 0.0, m_field->GetMDates(1,0) - g_date->DayInYear())) {
		  SimpleEvent( g_date->Date() + 1, sb_hay_baling, true );
		  break;
		}
	}
    SimpleEvent( g_date->OldDays() + m_field->GetMDates(0,1),
                sb_stubble_harrow, false );
    break;

  case sb_stubble_harrow:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))  // WAS 60
    {
		if (m_field->GetMConstants(1)==0) {
				if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
					g_msg->Warn( WARN_BUG, "SpringBarley::Do(): failure in 'StubbleHarrowing' execution", "" );
					exit( 1 );
				} 
		}
		else {
		  if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
			SimpleEvent( g_date->Date() + 1, sb_stubble_harrow, true );
			break;
		  }
		}
    }
    // END MAIN THREAD
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "SpringBarley::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}


