/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>UKWinterRape.cpp This file contains the source for the UKWinterRape class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
 // Adapted for UK Rape by Adam McVeigh, 2021
*/
//
// UKWinterRape.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/UKWinterRape.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_winterrape_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_WR_InsecticideDay;
extern CfgInt   cfg_WR_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter rape.
*/
bool UKWinterRape::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_UKWinterRape; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case uk_wr_start:
	{
		// uk_wr_start just sets up all the starting conditions and reference dates that are needed to start a uk_wr
		UK_WR_FERTI_P1 = false;
		UK_WR_FERTI_S1 = false;
		UK_WR_STUBBLE_PLOUGH = false;
		UK_WR_DECIDE_TO_GR = false;

		m_field->ClearManagementActionSum();

		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 5 start and stop dates for all 'movable' events for this crop
		// The first non - moveable date is always 0, 0, then the crop operations are numbered x, 1, x, 2 etc for each subsequent operation. 0, y is the start date and 1, y is the end date.

		int noDates = 4;
		m_field->SetMDates(0, 0, g_date->DayInYear(20, 8)); // last possible day of harvest
		m_field->SetMDates(1, 0, g_date->DayInYear(25, 8)); // last possible day of straw chopping, equal to harvest in this case
		m_field->SetMDates(0, 1, 0); // start day of hay bailing (not used as it depends on previous treatment)
		m_field->SetMDates(1, 1, g_date->DayInYear(25, 8)); // end day of hay bailing
		m_field->SetMDates(0, 2, 0); // start day of RSM
		m_field->SetMDates(1, 2, g_date->DayInYear(28, 8)); // end day of RSM
		m_field->SetMDates(0, 3, 0); // start day of calcium application
		m_field->SetMDates(1, 3, g_date->DayInYear(30, 8)); // end day of calcium application
		// Can be up to 10 of these. If the shortening code is triggered
		// then these will be reduced in value to at lowest the value of the "0,0" entry

		m_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "UKWinterRape::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
						m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
					}
					if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
						m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "UKWinterRape::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
				else {
					d1 = g_date->OldDays() + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "UKWinterRape::Do(): ", "Crop start attempt after last possible start date");
						int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
						g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
						exit(1);
					}
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				if (m_farm->IsStockFarmer()) //Stock Farmer
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 2), uk_wr_ferti_s3, false, m_farm, m_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 2), uk_wr_ferti_p3, false, m_farm, m_field);
				break;
			}
		}//if

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 9);
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have a
		// stock or arable farmer
		if (m_farm->IsStockFarmer()) { // StockFarmer
			SimpleEvent_(d1, uk_wr_ferti_s1, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, uk_wr_ferti_p1, false, m_farm, m_field);
	}
	break;

	// This is the first real farm operation
	// Slurry 50%
	case uk_wr_ferti_p1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FP_Slurry(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(6, 9), uk_wr_stubble_plough, false, m_farm, m_field);
		break;
	case uk_wr_ferti_s1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->FA_Slurry(m_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(6, 9), uk_wr_stubble_plough, false, m_farm, m_field);
		break;
		// Stubble plough 50%
	case uk_wr_stubble_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->StubblePlough(m_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_stubble_plough, true, m_farm, m_field);
				break;
			}
			else
			{
				SimpleEvent_(g_date->Date() + 1, uk_wr_autumn_plough, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(6, 9), uk_wr_autumn_harrow, false, m_farm, m_field);
		break;
		// Autumn harrow 50%
	case uk_wr_autumn_harrow: // The first of the  managements.
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->AutumnHarrow(m_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_autumn_harrow, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_stubble_harrow, false, m_farm, m_field);
		break;
		// Autumn Plough 25%
	case uk_wr_autumn_plough:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->AutumnPlough(m_field, 0.0, g_date->DayInYear(13, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_autumn_plough, true, m_farm, m_field);
				break;

				if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is N fertiliser application
				{
					SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s2, false, m_farm, m_field);

				}
				else SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p2, false, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_stubble_harrow, false, m_farm, m_field);
		break;
		// Stubble cultivate 25%
	case uk_wr_stubble_harrow:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (!m_farm->StubbleHarrowing(m_field, 0.0, g_date->DayInYear(13, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_stubble_harrow, true, m_farm, m_field);
				break;
			}
			
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer // setting the next step which is N fertiliser application
		{
			SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s2, false, m_farm, m_field);

		}
		else SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p2, false, m_farm, m_field);
		break;
		// NPK 90%
	case uk_wr_ferti_p2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FP_NPK(m_field, 0.0, g_date->DayInYear(14, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_preseeding_cultivator, false, m_farm, m_field);
		break;
	case uk_wr_ferti_s2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.90))
		{
			if (!m_farm->FA_NPK(m_field, 0.0, g_date->DayInYear(14, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_preseeding_cultivator, false, m_farm, m_field);
		break;
		// Preseeding cultivation 100%
	case uk_wr_preseeding_cultivator:
		if (!m_farm->PreseedingCultivator(m_field, 0.0, g_date->DayInYear(15, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_preseeding_cultivator, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_autumn_sow, false, m_farm, m_field);
		break;
		// Autumn sow 100%
	case uk_wr_autumn_sow:
		if (!m_farm->AutumnSow(m_field, 0.0, g_date->DayInYear(17, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_autumn_sow, true, m_farm, m_field);
			break;
		}
		// Here is a fork leading to parallel events. Harrowing. Herbicide. Fungicide. Insecticide. Growth Regulator.
		SimpleEvent_(g_date->Date() + 1, uk_wr_herbicide1, false, m_farm, m_field); // Herbidide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 10), uk_wr_fungicide1, false, m_farm, m_field);	// Fungicide thread
		SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide1, false, m_farm, m_field);	// Insecticide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7) + 365, uk_wr_growth_regulator, false, m_farm, m_field);	// GR thread
		if (m_farm->IsStockFarmer()) //Stock Farmer					// N thread = Main thread
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 2) + 365, uk_wr_ferti_s3, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 2) + 365, uk_wr_ferti_p3, false, m_farm, m_field);
		break;

		// N thread = Main thread
		// N I 100% March
	case uk_wr_ferti_p3:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3), uk_wr_ferti_p4, false, m_farm, m_field);
		break;
	case uk_wr_ferti_s3:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(10, 3) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s3, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3), uk_wr_ferti_s4, false, m_farm, m_field);
		break;
		// NII 50%
	case uk_wr_ferti_p4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p4, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 3), uk_wr_ferti_p5, false, m_farm, m_field);
		break;
	case uk_wr_ferti_s4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s4, true, m_farm, m_field);
				break;
			} 
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 3), uk_wr_ferti_s5, false, m_farm, m_field);
		break;
		// N III 100%
	case uk_wr_ferti_p5:
		if (!m_farm->FP_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(5,4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_p5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), uk_wr_harvest, false, m_farm, m_field);
		break;
	case uk_wr_ferti_s5:
		if (!m_farm->FA_AmmoniumSulphate(m_field, 0.0, g_date->DayInYear(5, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_ferti_s5, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 8), uk_wr_harvest, false, m_farm, m_field);
		break;

		// Herbicide thread
	case uk_wr_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.9))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(18, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 3)+365, uk_wr_herbicide2, false, m_farm, m_field);
		break;
	case uk_wr_herbicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(20, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_herbicide2, true, m_farm, m_field);
				break;
			}
		}
		break;

		// Insecticide thread
	case uk_wr_insecticide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.9))
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(19, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide2, false, m_farm, m_field);
		break;
	case uk_wr_insecticide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(30, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide2, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide3, false, m_farm, m_field);
		break;
	case uk_wr_insecticide3:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.8))
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(5, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide3, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide4, false, m_farm, m_field);
		break;
	case uk_wr_insecticide4:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->InsecticideTreat(m_field, 0.0, g_date->DayInYear(1, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_insecticide4, true, m_farm, m_field);
				break;
			}
		}
		break;
		// Fungicide thread
		// Fungicide 1
	case uk_wr_fungicide1:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.7))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_fungicide1, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), uk_wr_fungicide2, false, m_farm, m_field);
		break;
		// Fungicide 2
	case uk_wr_fungicide2:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.5))
		{
			if (!m_farm->FungicideTreat(m_field, 0.0, g_date->DayInYear(1, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_wr_fungicide2, true, m_farm, m_field);
				break;
			}
		}
		// End of thread
		break;
		// Growth Regulator thread
	case uk_wr_growth_regulator:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.75))
		{
			if (!m_farm->GrowthRegulator(m_field, 0.0, g_date->DayInYear(19, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 14, uk_wr_growth_regulator, true, m_farm, m_field);
				break;
			}
		}
		// Harvest 100%
	case uk_wr_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!m_farm->Harvest(m_field, 0.0, m_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, uk_wr_harvest, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_straw_chopping, false, m_farm, m_field);
		break;
		// Straw chopping 50% after harvest
	case uk_wr_straw_chopping:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50))
		{
			if (m_field->GetMConstants(0) == 0) {
				if (!m_farm->StrawChopping(m_field, 0.0, -1)) { // raise an error
					g_msg->Warn(WARN_BUG, "UKWinterRape::Do(): failure in 'StrawChopping' execution", "");
					exit(1);
				}
			}
			else {
				if (!m_farm->StrawChopping(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, uk_wr_straw_chopping, true, m_farm, m_field);
					break;
				}
			}
			done = true;
			break;
		}
		SimpleEvent_(g_date->Date() + 1, uk_wr_hay_bailing, false, m_farm, m_field);
		break;
		// Hay bailing 50% after harvest
	case uk_wr_hay_bailing:
		if (m_field->GetMConstants(1) == 0) {
			if (!m_farm->HayBailing(m_field, 0.0, -1)) { // raise an error
				g_msg->Warn(WARN_BUG, "UKWinterRape::Do(): failure in 'HayBailing' execution", "");
				exit(1);
			}
		}
		else {
			if (!m_farm->HayBailing(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, uk_wr_hay_bailing, true, m_farm, m_field);
				break;
			}
		}
		done = true;
		break;
	default:
		g_msg->Warn(WARN_BUG, "UKWinterRape::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}