/*
*******************************************************************************************************
Copyright (c) 2021, Christopher John Topping, Aarhus University - modified by Luna Kondrup Marcussen, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>DK_OBushFruit_Perm2.cpp This file contains the source for the DK_OBushFruit_Perm2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of July 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_OBushFruit_Perm2.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DK_OBushFruit_Perm2.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_cabbage_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_OBFP2_InsecticideDay;
extern CfgInt   cfg_OBFP2_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop.
*/
bool DK_OBushFruit_Perm2::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_DKOBushFruit_Perm2;
	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case dk_obfp2_start:
	{
		// dk_obfp2_start just sets up all the starting conditions and reference dates that are needed to start a dk_obfp2

		a_field->ClearManagementActionSum();

		m_last_date = g_date->DayInYear(31, 10); // Should match the last flexdate below
		//Create a 2d array of 1 plus the number of operations you use. Change only 4+1 to what you need in the line below
		std::vector<std::vector<int>> flexdates(1 + 1, std::vector<int>(2, 0));
		// Set up the date management stuff
				// Start and stop dates for all events after harvest
		flexdates[0][1] = g_date->DayInYear(31, 10); // last possible day of harvest - this is in effect day before the earliest date that a following crop can use - NO harvest here - this is hwater3 instead
		// Now these are done in pairs, start & end for each operation. If its not used then -1
		flexdates[1][0] = -1; // This date will be moved back as far as necessary and potentially to flexdates 1 (start op 1)
		flexdates[1][1] = g_date->DayInYear(31, 10); // This date will be moved back as far as necessary and potentially to flexdates 1 (end op 1) 

		// Below if this is a spring crop use 365, otherwise first parameter is always 0, second parameter is fixed, and the third is the start up operation in the first year
		int isSpring = 365;
		if (StartUpCrop(isSpring, flexdates, int(dk_obfp2_water1))) break;

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 3) + isSpring;
		// OK, let's go.
		// LKM: Here we queue up the first event 
		SimpleEvent(d1, dk_obfp2_water1, false);
		break;
	}
	break;

	// LKM: This is the first real farm operation - molluscicide if many snails
	case dk_obfp2_water1:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(29, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water1, true);
				break;
			}
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer1_s, false);
			break;
		}
		else 
		SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer1_p, false);
		break;

	case dk_obfp2_fertilizer1_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer1_s, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_obfp2_row_cultivation1, false);
		break;

	case dk_obfp2_fertilizer1_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NK(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer1_p, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 4), dk_obfp2_row_cultivation1, false);
		break;

	case dk_obfp2_row_cultivation1:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation1, true);
			break;
		}

		//fork of parallel events:
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_molluscicide, false);
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 5), dk_obfp2_water2, false);
		SimpleEvent(g_date->Date() + 10, dk_obfp2_row_cultivation2, false); // main thread
		break;
	case dk_obfp2_molluscicide:
		if (m_ev->m_lock || m_farm->DoIt_prob(0.50)) {
			if (!m_farm->Molluscicide(m_field, 0.0, g_date->DayInYear(1, 9) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_molluscicide, true);
				break;
			}
		}
		break; // end of molluscicide thread
	case dk_obfp2_water2:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00)) {
			if (!m_farm->Water(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_water2, true);
				break;
			}
		}
		break; // end of water thread
	case dk_obfp2_row_cultivation2:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation2, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp2_row_cultivation3, false);
		break;
	case dk_obfp2_row_cultivation3:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(20, 5) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation3, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 6), dk_obfp2_row_cultivation4, false);
		break;
	case dk_obfp2_row_cultivation4:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(25, 6) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation4, true);
			break;
		}
		SimpleEvent(g_date->Date()+10, dk_obfp2_row_cultivation5, false);
		break;
	case dk_obfp2_row_cultivation5:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(5, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation5, true);
			break;
		}
		SimpleEvent(g_date->Date() + 10, dk_obfp2_row_cultivation6, false);
		break;
	case dk_obfp2_row_cultivation6:
		if (!a_farm->RowCultivation(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_row_cultivation6, true);
			break;
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(15, 7), dk_obfp2_harvest, false);
		break;
	case dk_obfp2_harvest:
		if (!a_farm->HarvestBF_Machine(m_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_harvest, true);
			break;
		}
		SimpleEvent(g_date->Date() + 1, dk_obfp2_cut_bushes, false);
		break;
	case dk_obfp2_cut_bushes:
		if (!a_farm->CutOrch(m_field, 0.0, g_date->DayInYear(16, 8) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_cut_bushes, true);
			break;
		}
		if (a_farm->IsStockFarmer()) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer2_s, false);
			break;
		}
		else 
		SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer2_p, false);
		break;
	case dk_obfp2_fertilizer2_s:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FA_NK(m_field, 0.0, g_date->DayInYear(17, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer2_s, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp2_water3, false);
		break;

	case dk_obfp2_fertilizer2_p:
		if (m_ev->m_lock || m_farm->DoIt_prob(1.00))
		{
			if (!a_farm->FP_NK(m_field, 0.0, g_date->DayInYear(17, 8) - g_date->DayInYear())) {
				SimpleEvent(g_date->Date() + 1, dk_obfp2_fertilizer2_p, true);
				break;
			}
		}
		SimpleEvent(g_date->OldDays() + g_date->DayInYear(1, 9), dk_obfp2_water3, false);
		break;
	case dk_obfp2_water3:
		if (!a_farm->Water(m_field, 0.0, m_field->GetMDates(1, 0) - g_date->DayInYear())) {
			SimpleEvent(g_date->Date() + 1, dk_obfp2_water3, true);
			break;
		}
		done = true;
		break;
		// So we are done, and somewhere else the farmer will queue up the start event of the next crop (DK_BushFruit_Perm2)
		// END of MAIN THREAD
		break;
		default:
		g_msg->Warn(WARN_BUG, "DK_OBushFruit_Perm2::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}