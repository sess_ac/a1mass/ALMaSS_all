//
// DE_OPotatoes.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/DE_OPotatoes.h"

extern CfgFloat cfg_strigling_prop;

/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional winter wheat.
*/
bool DE_OPotatoes::Do(Farm* a_farm, LE* a_field, FarmEvent* a_ev)
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
//  int d1;

  bool done = false;

  TTypesOfVegetation l_tov = tov_DEOPotatoes; // The current type - change to match the crop you have
 switch (m_ev->m_todo)
  {
  case de_opot_start:
  {
      DE_OPOT_MANURE_DATE = 1;
      DE_OPOT_SOW_DATE = 1;
      DE_OPOT_HILLING_THREE = 1;


      a_field->ClearManagementActionSum();

      m_field->SetVegPatchy(true); // Root crop so is open until tall
      // Set up the date management stuff
      // Could save the start day in case it is needed later
      // m_field->m_startday = m_ev->m_startday;
      m_last_date = g_date->DayInYear(1, 10);
      // Start and stop dates for all events after harvest
      int noDates = 1;
      m_field->SetMDates(0, 0, g_date->DayInYear(1, 10));
      // 0,0 determined by harvest date - used to see if at all possible
      m_field->SetMDates(1, 0, g_date->DayInYear(1, 10));
      // Check the next crop for early start, unless it is a spring crop
      // in which case we ASSUME that no checking is necessary!!!!
      // So DO NOT implement a crop that runs over the year boundary

      //new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
      int d1;
      if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

          if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
              if (m_field->GetMDates(0, 0) >= m_ev->m_startday)
              {
                  g_msg->Warn(WARN_BUG, "DE_OPotatoe::Do(): ", "Harvest too late for the next crop to start!!!");
                  int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
                  exit(1);
              }
              // Now fix any late finishing problems
              for (int i = 0; i < noDates; i++) {
                  if (m_field->GetMDates(0, i) >= m_ev->m_startday) {
                      m_field->SetMDates(0, i, m_ev->m_startday - 1); //move the starting date
                  }
                  if (m_field->GetMDates(1, i) >= m_ev->m_startday) {
                      m_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
                      m_field->SetMDates(1, i, m_ev->m_startday - 1); //move the finishing date
                  }
              }
          }


          if (!m_ev->m_first_year) {
              // Are we before July 1st?
              d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
              if (g_date->Date() < d1) {
                  // Yes, too early. We assumme this is because the last crop was late
                  printf("Poly: %d\n", m_field->GetPoly());
                  g_msg->Warn(WARN_BUG, "DE_OPotatoes::Do(): ", "Crop start attempt between 1st Jan & 1st July");
                  int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
                  g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
                  int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                  g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
              }
              else {
                  d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
                  if (g_date->Date() > d1) {
                      // Yes too late - should not happen - raise an error
                      g_msg->Warn(WARN_BUG, "DE_OPotatoes::Do(): ", "Crop start attempt after last possible start date");
                      g_msg->Warn(WARN_BUG, "Previous Crop ", "");
                      m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
                      int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
                      g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
                  }
              }
          }
      }//if
  // OK, let's go.
     // Reinit d1 to first possible starting date.
      d1 = g_date->OldDays() + m_first_date + 365; // Add 365 for spring crop
      if (g_date->Date() > d1) {
          d1 = g_date->Date();
      }
  // Here we queue up the first event
  //
      if ( m_farm->IsStockFarmer()) {
      SimpleEvent_( d1, de_opot_fa_manure, false, m_farm, m_field);
      } 
      else SimpleEvent_( d1, de_opot_fp_manure, false, m_farm, m_field);
  }
      break;

  case de_opot_fa_manure:
    if (!m_farm->FA_Manure( m_field, 0.0,
         g_date->DayInYear( 20, 3 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_fa_manure, false, m_farm, m_field);
      break;
    }
    DE_OPOT_MANURE_DATE = g_date->DayInYear();
    SimpleEvent_( g_date->Date() + 1, de_opot_fa_slurry, false, m_farm, m_field);
    break;

  case de_opot_fa_slurry:
    if ( m_ev->m_lock || m_farm->DoIt( 40 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0, DE_OPOT_MANURE_DATE -
			      g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_fa_slurry, true, m_farm, m_field);
	break;
      }
    }
    SimpleEvent_( g_date->Date(), de_opot_spring_plough, false, m_farm, m_field);
    break;

  case de_opot_fp_manure:
    if (!m_farm->FP_Manure( m_field, 0.0,
         g_date->DayInYear( 20, 3 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_fp_manure, false, m_farm, m_field);
      break;
    }
    DE_OPOT_MANURE_DATE = g_date->DayInYear();
    SimpleEvent_( g_date->Date() + 1, de_opot_fp_slurry, false, m_farm, m_field);
    break;

  case de_opot_fp_slurry:
    if ( m_ev->m_lock || m_farm->DoIt( 15 )) {
      if (!m_farm->FP_Slurry( m_field, 0.0, DE_OPOT_MANURE_DATE -
			      g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_fp_slurry, true, m_farm, m_field);
	break;
      }
    }
    SimpleEvent_( g_date->Date()+1, de_opot_spring_plough, false, m_farm, m_field);
    break;

  case de_opot_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
			       DE_OPOT_MANURE_DATE -
			       g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_spring_plough, false, m_farm, m_field);
      break;
    }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 3), de_opot_spring_harrow, false, m_farm, m_field);
    break;

  case de_opot_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_spring_harrow, false, m_farm, m_field);
      break;
    }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 4), de_opot_spring_sow, false, m_farm, m_field);
    break;

  case de_opot_spring_sow:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 15, 4 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_spring_sow, false, m_farm, m_field);
      break;
    }
    // Fork of events
    if (m_farm->DoIt(10)) {
        // Flaming
        SimpleEvent_(g_date->Date() + 7, de_opot_flaming_one, false, m_farm, m_field);
    } 
    else         // Strigling
    SimpleEvent_(g_date->Date() + 14, de_opot_strigling_one, false, m_farm, m_field); // Strigling followed by hilling = MAIN THREAD
    if (m_farm->IsStockFarmer()) //Stock Farmer
    {
        SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), de_opot_fertiFA_S, false, m_farm, m_field);	// Sulphur followed by Boron
    }
    else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6), de_opot_fertiFP_S, false, m_farm, m_field);	// Sulphur followed by Boron
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), de_opot_fungicide, false, m_farm, m_field); // Fungicide treat
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), de_opot_insecticide, false, m_farm, m_field); // Insecticide treat
    break;

  case de_opot_flaming_one:
    if ( m_ev->m_lock || m_farm->DoIt( 10 )) {
      if (!m_farm->Swathing( m_field, 0.0,
			     g_date->DayInYear( 10, 5 ) -
			     g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_flaming_one, true, m_farm, m_field);
	break;
      }
    }
    break;

  case de_opot_strigling_one:
    if (!m_farm->StriglingHill( m_field, 0.0,
         g_date->DayInYear( 20, 5 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_strigling_one, false, m_farm, m_field);
      break;
    }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(19, 4), de_opot_water_one, false, m_farm, m_field);
    break;

  case de_opot_water_one:
      if (m_ev->m_lock || m_farm->DoIt(80)) {
          if (!m_farm->Water(m_field, 0.0,
              g_date->DayInYear(30, 5) -
              g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_water_one, true, m_farm, m_field);
              break;
          }
      }
      SimpleEvent_(g_date->Date() + 10, de_opot_strigling_two, false, m_farm, m_field);
      break;

  case de_opot_strigling_two:
    if (!m_farm->StriglingHill( m_field, 0.0,
         g_date->DayInYear( 30, 5 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_strigling_two, false, m_farm, m_field);
      break;
    }
    SimpleEvent_(g_date->Date() + 7, de_opot_strigling_three, false, m_farm, m_field);
    break;

  case de_opot_strigling_three:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 75 ))) {
      if (!m_farm->StriglingHill( m_field, 0.0,
			      g_date->DayInYear( 12, 6 ) -
			      g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_strigling_three, true, m_farm, m_field);
	break;
      }
    }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 7),
        de_opot_water_two, false, m_farm, m_field);
    break;

  case de_opot_insecticide:
      // here comes the insecticide thread
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->InsecticideTreat( m_field, 0.0,
			  g_date->DayInYear( 10, 6 ) -
			  g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_insecticide, true, m_farm, m_field);
	break;
      }
    }
    break;
    // End of thread


  case de_opot_fertiFP_S:
      // Here comes the microelements thread for arable farms
      if (m_ev->m_lock || m_farm->DoIt(25))
      {
          if (!m_farm->FP_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_fertiFP_S, true, m_farm, m_field);
              break;
          }
      }
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6),
          de_opot_fertiFP_B, false, m_farm, m_field);
      break;

  case de_opot_fertiFP_B:
      if (m_ev->m_lock || m_farm->DoIt(25))
      {
          if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_fertiFP_B, true, m_farm, m_field);
              break;
          }
      }
      break;
      // End of thread
  case de_opot_fertiFA_S:
      // Here comes the microelements thread for stock farms
      if (m_ev->m_lock || m_farm->DoIt(25))
      {
          if (!m_farm->FA_ManganeseSulphate(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_fertiFA_S, true, m_farm, m_field);
              break;
          }
      }
      SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 6),
          de_opot_fertiFA_B, false, m_farm, m_field);
      break;

  case de_opot_fertiFA_B:
      if (m_ev->m_lock || m_farm->DoIt(25))
      {
          if (!m_farm->FP_Boron(m_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_fertiFA_B, true, m_farm, m_field);
              break;
          }
      }
      break;
      // End of thread

  case de_opot_fungicide:
      // Here comes the fungicide thread
      if (m_ev->m_lock || m_farm->DoIt(80)) {
          if (!m_farm->FungicideTreat(m_field, 0.0,
              g_date->DayInYear(15, 7) -
              g_date->DayInYear())) {
              SimpleEvent_(g_date->Date() + 1, de_opot_fungicide, true, m_farm, m_field);
              break;
          }
      }
      break;
      // End of thread

  case de_opot_water_two:
      // MAIN THREAD
    if ( m_ev->m_lock || m_farm->DoIt( 70 )) {
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30, 7 ) -
			  g_date->DayInYear())) {
	SimpleEvent_( g_date->Date() + 1, de_opot_water_two, true, m_farm, m_field);
	break;
      }
    }
    SimpleEvent_( g_date->OldDays() + g_date->DayInYear( 30, 7 ),
		 de_opot_flaming_two, false, m_farm, m_field);
    break;

  case de_opot_flaming_two:
    if ( m_ev->m_lock || m_farm->DoIt( 10 ))
    {
      if (!m_farm->Swathing( m_field, 0.0,
			     g_date->DayInYear( 15, 8 ) -
			     g_date->DayInYear()))
      {
	SimpleEvent_( g_date->Date() + 1, de_opot_flaming_two, true, m_farm, m_field);
	break;
      }
    }
    SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 8),
        de_opot_harvest, false, m_farm, m_field);
    break;

  case de_opot_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         g_date->DayInYear( 1, 10 ) - g_date->DayInYear())) {
      SimpleEvent_( g_date->Date() + 1, de_opot_harvest, false, m_farm, m_field);
      break;
    }
    // So we are done, and somewhere else the farmer will queue up the start event of the next crop
    // END of MAIN THREAD
    done = true;
    break;

  default:
      g_msg->Warn(WARN_BUG, "DK_OSpringBarley::Do(): "
          "Unknown event type! ", "");
      exit(1);
  }
  return done;
}