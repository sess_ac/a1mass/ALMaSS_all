/**
\file
\brief
<B>DK_BushFruit_Perm2.h This file contains the source for the DK_BushFruit_Perm2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Luna Kondrup Marcussen \n
Version of July 2021 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// DK_BushFruit_Perm2.h
//


#ifndef DK_BUSHFRUIT2_P_H
#define DK_BUSHFRUIT2_P_H

#define DK_BFP2_EARLY_HARVEST a_field->m_user[1]

#define DK_BFP2_BASE 62600
/**
\brief A flag used to indicate autumn ploughing status
*/

/** Below is the list of things that a farmer can do if he is growing cabbage, at least following this basic plan. 
So all we have to do is figure out when to do the different things. 
Once we have done some kind of management, then an event is triggered and the fact that this particular management is done is registered with the particular polygon. 
This information is available for any ALMaSS components to inspect - e.g. animals & birds
*/
typedef enum {
	dk_bfp2_start = 1, // Compulsory, must always be 1 (one).
	dk_bfp2_sleep_all_day = DK_BFP2_BASE,
	dk_bfp2_cover_on,
	dk_bfp2_cover_off,
	dk_bfp2_fertilizer1_s,
	dk_bfp2_fertilizer1_p,
	dk_bfp2_water1,
	dk_bfp2_water1_cover,
	dk_bfp2_row_cultivation1,
	dk_bfp2_row_cultivation2,
	dk_bfp2_row_cultivation3,
	dk_bfp2_herbicide1,
	dk_bfp2_insecticide,	
	dk_bfp2_fungicide1,
	dk_bfp2_fungicide2,
	dk_bfp2_water2,
	dk_bfp2_straw_cover,
	dk_bfp2_harvest_early1,
	dk_bfp2_harvest_early2,
	dk_bfp2_harvest_early3,
	dk_bfp2_harvest1,
	dk_bfp2_harvest2,
	dk_bfp2_harvest3,
	dk_bfp2_harvest4,
	dk_bfp2_harvest5,
	dk_bfp2_harvest6,
	dk_bfp2_harvest7,
	dk_bfp2_harvest8,
	dk_bfp2_harvest9,
	dk_bfp2_harvest10,
	dk_bfp2_harvest11,
	dk_bfp2_herbicide2,
	dk_bfp2_fertilizer2_s,
	dk_bfp2_fertilizer2_p,
	dk_bfp2_water3,
	dk_bfp2_herbicide3,
	dk_bfp2_foobar,
} DK_BushFruit_Perm2ToDo;


/**
\brief
DK_BushFruit_Perm2 class
\n
*/
/**
See DK_BushFruit_Perm2.h::DK_BushFruit_Perm2ToDo for a complete list of all possible events triggered codes by the management plan. When triggered these events are handled by Farm and are available as information for other objects such as animal and bird models.
*/
class DK_BushFruit_Perm2: public Crop{
 public:
   virtual bool  Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev );
   DK_BushFruit_Perm2(TTypesOfVegetation a_tov, TTypesOfCrops a_toc) : Crop(a_tov, a_toc)
   {
		// When we start it off, the first possible date for a farm operation is ...
		// This information is used by other crops when they decide how much post processing of 
		// the management is allowed after harvest before the next crop starts.
		m_first_date=g_date->DayInYear( 1,1 );
		SetUpFarmCategoryInformation();
   }
   void SetUpFarmCategoryInformation() {
	   const int elements = 2 + (dk_bfp2_foobar - DK_BFP2_BASE);
	   m_base_elements_no = DK_BFP2_BASE - 2;

	   FarmManagementCategory catlist[elements] =
	   {
			fmc_Others,	// zero element unused but must be here	
			fmc_Others,	//	dk_bfp2_start = 1, // Compulsory, must always be 1 (one).
			fmc_Others,	//	dk_bfp2_sleep_all_day = DK_BFP2_BASE,
			fmc_Others,	//	dk_bfp2_cover_on,
			fmc_Others,	//	dk_bfp2_cover_off,
			fmc_Fertilizer,	//	dk_bfp2_fertilizer1_s,
			fmc_Fertilizer,	//	dk_bfp2_fertilizer1_p,
			fmc_Watering,	//	dk_bfp2_water1,
			fmc_Watering,	//	dk_bfp2_water1_cover,
			fmc_Cultivation,	//	dk_bfp2_row_cultivation1,
			fmc_Cultivation,	//	dk_bfp2_row_cultivation2,
			fmc_Cultivation,	//	dk_bfp2_row_cultivation3,
			fmc_Herbicide,	//	dk_bfp2_herbicide1,
			fmc_Insecticide,	//	dk_bfp2_insecticide,
			fmc_Fungicide,	//	dk_bfp2_fungicide1,
			fmc_Fungicide,	//	dk_bfp2_fungicide2,
			fmc_Watering,	//	dk_bfp2_water2,
			fmc_Others,	//	dk_bfp2_straw_cover,
			fmc_Harvest,	//	dk_bfp2_harvest_early1,
			fmc_Harvest,	//	dk_bfp2_harvest_early2,
			fmc_Harvest,	//	dk_bfp2_harvest_early3,
			fmc_Harvest,	//	dk_bfp2_harvest1,
			fmc_Harvest,	//	dk_bfp2_harvest2,
			fmc_Harvest,	//	dk_bfp2_harvest3,
			fmc_Harvest,	//	dk_bfp2_harvest4,
			fmc_Harvest,	//	dk_bfp2_harvest5,
			fmc_Harvest,	//	dk_bfp2_harvest6,
			fmc_Harvest,	//	dk_bfp2_harvest7,
			fmc_Harvest,	//	dk_bfp2_harvest8,
			fmc_Harvest,	//	dk_bfp2_harvest9,
			fmc_Harvest,	//	dk_bfp2_harvest10,
			fmc_Harvest,	//	dk_bfp2_harvest11,
			fmc_Herbicide,	//	dk_bfp2_herbicide2,
			fmc_Fertilizer,	//	dk_bfp2_fertilizer2_s,
			fmc_Fertilizer,	//	dk_bfp2_fertilizer2_p,
			fmc_Watering,	//	dk_bfp2_water3,
			fmc_Herbicide	//	dk_bfp2_herbicide3,

				// no foobar entry			

	   };
	   // Iterate over the catlist elements and copy them to vector						
	   copy(begin(catlist), end(catlist), back_inserter(m_ManagementCategories));
   }
};


#endif // DK_BushFruit_Perm1_H

