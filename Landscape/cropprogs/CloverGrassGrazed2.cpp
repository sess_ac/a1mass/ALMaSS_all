/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

//#define __EcoSol_01
#define _CRT_SECURE_NO_DEPRECATE
#include "../../BatchALMaSS/ALMaSS_Setup.h"
#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/CloverGrassGrazed2.h"


extern CfgFloat cfg_silage_prop;
extern CfgInt cfg_pest_productapplic_startdate;
extern CfgInt cfg_pest_productapplic_period;
extern CfgFloat cfg_pest_product_1_amount;

using namespace std;

bool CloverGrassGrazed2::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;
  long d1 = 0;
  int noDates=2;
  bool done = false;

  switch ( m_ev->m_todo ) {

  case cgg2_start:

    CGG2_CUT_DATE    = 0;
    CGG2_WATER_DATE  = 0;
    CGG2_DO_STUBBLE  = 0;
    CGG2_STOP_CATTLE = 0;
    a_field->ClearManagementActionSum();


    m_last_date=g_date->DayInYear(1,10);

    // Start and stop dates for all events after harvest
    m_field->SetMDates(0,0,g_date->DayInYear(25, 6));

    // Determined by harvest date - used to see if at all possible
    m_field->SetMDates(1,0,g_date->DayInYear(25, 6));
    m_field->SetMDates(0,1,g_date->DayInYear(1,9));
    m_field->SetMDates(1,1,g_date->DayInYear(1,10));

    // Check the next crop for early start, unless it is a spring crop
    // Check the next crop for early start, unless it is a spring crop
    // in which case we ASSUME that no checking is necessary!!!!
    // So DO NOT implement a crop that runs over the year boundary

	//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
	 if(!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber()>0)){

    if (m_ev->m_startday>g_date->DayInYear(1,7))
    {
      if (m_field->GetMDates(0,0) >=m_ev->m_startday)
      {
        g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): "
		 "Harvest too late for the next crop to start!!!", "" );
        exit( 1 );
      }
      // Now fix any late finishing problems
      for (int i=0; i<noDates; i++) {
			if(m_field->GetMDates(0,i)>=m_ev->m_startday) { 
				m_field->SetMDates(0,i,m_ev->m_startday-1); //move the starting date
			}
			if(m_field->GetMDates(1,i)>=m_ev->m_startday){
				m_field->SetMConstants(i,0); 
				m_field->SetMDates(1,i,m_ev->m_startday-1); //move the finishing date
			}
		}
    }
    // Now no operations can be timed after the start of the next crop.
    // Added test for management plan testing. FN, 19/5-2003.
    if ( ! ( m_ev->m_first_year //|| g_farm_test_crop.value()
		)) {
      // Are we before July 1st?
      d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
      if (g_date->Date() < d1) {
			// Yes, too early. We assumme this is because the last crop was late
			g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): "
					 "Crop start attempt between 1st Jan & 1st July", "" );
			exit( 1 );
      } 
	  else 
	  {
		d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
		if (g_date->Date() > d1) {
		  // Yes too late - should not happen - raise an error
		  g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): "
				   "Crop start attempt after last possible start date",
				   "" );
		  exit( 1 );
		}
      }
    } else {
      // First (invisible) year or testing. Force the correct starting date.
      d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
      // If testing and not the first year, then push all events
      // into next year (start event is called in the autumn after
      // the current year has finished). FN, 20/5-2003.
      if (!m_ev->m_first_year) d1 += 365;
	 }
	}//if
	m_field->SetLastSownVeg( m_field->GetVegType() ); //Force last sown, needed for goose habitat classification
	d1 = g_date->OldDays() + m_first_date+365; 


    SimpleEvent( d1, cgg2_ferti_zero, false );
    SimpleEvent( d1, cgg2_ferti_one, false );
    
    // LCT: Test programming of product/pesticide application 
    // SimpleEvent(d1 + 7, cgg2_productapplic_one, false);

#ifdef __EcoSol_01
    // THIS CODE IS ONLY NEEDED IF WE ARE TESTING A HERBICIDE WITH THE PESTICIDE ENGINE
	/* */
	if ( a_field->GetLandscape()->SupplyShouldSpray() ) {
	d1 = g_date->OldDays() + cfg_pest_productapplic_startdate.value();
    if (g_date->Date() >= d1) d1 += 365;
    SimpleEvent( d1, cgg2_productapplic_one, false );
		}
    /* */
#endif
	break;

  case cgg2_productapplic_one:
	if ( m_ev->m_lock || m_farm->DoIt( 100 ))
    {
      if (!m_farm->ProductApplication( m_field, 0.0,(cfg_pest_productapplic_startdate.value()+cfg_pest_productapplic_period.value()) - g_date->DayInYear(), cfg_pest_product_1_amount.value(), ppp_1)) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, cgg2_productapplic_one, true );
        break;
      }
    }
    break;

  case cgg2_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 30 )) {
      if (!m_farm->FA_Slurry( m_field, 0.0,
			      g_date->DayInYear( 25, 4 ) -
			      g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, cgg2_ferti_zero, true );
        break;
      }
    }
    break;

  case cgg2_ferti_one:
    if ( m_ev->m_lock || m_farm->DoIt( 80 )) {
      if (!m_farm->FA_NPK( m_field, 0.0,
			   g_date->DayInYear( 25, 4 ) -
			   g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, cgg2_ferti_one, true );
        break;
      }
    }

	ChooseNextCrop (2);


    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
                 cgg2_cattle_out, false );
//    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
//                   cgg2_cut_to_silage, false );

    // Start a watering thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
                 cgg2_water_zero, false );
    break;


  case cgg2_cattle_out:
    if (  m_ev->m_lock || m_farm->DoIt( (int) (100- ((cfg_silage_prop.value()*40) )))) {
		if (m_field->GetMConstants(1)==0) {
			if (!m_farm->CattleOut( m_field, 0.0, -1)) { //raise an error
				g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): failure in 'CattleOut' execution", "" );
				exit( 1 );
			} 
		}
		else { 
			if (!m_farm->CattleOut( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
				SimpleEvent( g_date->Date() + 1, cgg2_cattle_out, true );
				break;
			}
		}
      // Success
      // Keep them out there
      SimpleEvent( g_date->Date() + m_field->GetMConstants(1), cgg2_cattle_is_out, false );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ), cgg2_cut_to_silage, true ); // Must do this
    break;

  case cgg2_cattle_is_out:    // Keep the cattle out there
    //CattleIsOut() returns false if it is not time to stop grazing
    /*
	HERE I CANNOT SEE WHY WE NEED THE FORCING OF a_days to be -1 
	if (m_field->GetMConstants(1)==0) {
		if (!m_farm->CattleIsOut( m_field, 0.0, -1, m_field->GetMDates(1,1))) { 
			//added 27.08 - issue a warning only if we fail on the last day that this can be done, i.e. MDate
			if(g_date->Date() == m_field->GetMDates(1,1)){
				g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): failure in 'CattleIsOut' execution", "" );
				exit( 1 );
			}
			SimpleEvent( g_date->Date()+1, cgg2_cattle_is_out, false );
			break;
		} 
	}
	else { 
		if ( !m_farm->CattleIsOut( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear(), m_field->GetMDates(1,1))) {
			SimpleEvent( g_date->Date() + 1, cgg2_cattle_is_out, false );
			break;
		}
	}
	*/
	  if (!m_farm->CattleIsOut(m_field, 0.0, m_field->GetMDates(1, 1) - g_date->DayInYear(), m_field->GetMDates(1, 1))) {
		  //added 27.08 - issue a warning only if we fail on the last day that this can be done, i.e. MDate
		  if (g_date->Date() == m_field->GetMDates(1, 1)){
			  g_msg->Warn(WARN_BUG, "CloverGrassGrazed2::Do(): failure in 'CattleIsOut' execution", "Over-stepped the last possible date");
			  exit(1);
		  }
		  SimpleEvent(g_date->Date() + 1, cgg2_cattle_is_out, false);
		  break;
	  }
	  done = true;
    break;

  case cgg2_cut_to_silage:
  if ( m_ev->m_lock ) {
    if ( g_date->Date() < CGG2_WATER_DATE + 7 ) {
      SimpleEvent( g_date->Date() + 1, cgg2_cut_to_silage, true );
      break;
    }
    if (!m_farm->CutToSilage( m_field, 0.0,
                              g_date->DayInYear( 25, 6 ) -
                              g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, cgg2_cut_to_silage, true );
      break;
    }
    CGG2_CUT_DATE = g_date->Date();
    SimpleEvent(g_date->OldDays() + m_field->GetMDates(0,1), cgg2_stubble_harrow, false );
    // But we need to graze too
    SimpleEvent( g_date->Date()+14, cgg2_cattle_out, true );
  }
  break;

  case cgg2_stubble_harrow:
  if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
    // But first if we are grazing then we must stop this
    //
	if (m_field->GetMConstants(1)==0) {
		if (!m_farm->StubbleHarrowing( m_field, 0.0, -1)) { //raise an error
			g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): failure in 'StubbleHarrowing' execution", "" );
			exit( 1 );
		} 
	}
	else { 
		if (!m_farm->StubbleHarrowing( m_field, 0.0, m_field->GetMDates(1,1) - g_date->DayInYear())) {
		  // We didn't do it today, try again tomorrow.
		  SimpleEvent( g_date->Date() + 1, cgg2_stubble_harrow, true );
		  break;
		}
	}
    // If we ran, then we also happen to be the last event,
    // so terminate the program by stopping grazing
    m_field->SetMDates(1,1,g_date->DayInYear()-1);
  }
  break;

  case cgg2_water_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
      if ( g_date->Date() < CGG2_CUT_DATE + 3 ) {
	// Try again tomorrow, too close to silage cutting.
	SimpleEvent( g_date->Date() + 1, cgg2_water_zero, true );
	break;
      }
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 30, 5 ) -
			  g_date->DayInYear())) {
	// We didn't do it today, try again tomorrow.
	SimpleEvent( g_date->Date() + 1, cgg2_water_zero, true );
	break;
      }
      CGG2_WATER_DATE = g_date->Date();

      long newdate1 = g_date->OldDays() + g_date->DayInYear( 1,6 );
      long newdate2 = g_date->Date() + 7;
      if ( newdate2 > newdate1 )
	newdate1 = newdate2;
      SimpleEvent( newdate1, cgg2_water_one, false );
    }
    break;

  case cgg2_water_one:
    if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
      if ( g_date->Date() < CGG2_CUT_DATE + 3 ) {
	// Try again tomorrow, too close to silage cutting.
	SimpleEvent( g_date->Date() + 1, cgg2_water_one, true );
	break;
      }
      if (!m_farm->Water( m_field, 0.0,
			  g_date->DayInYear( 15, 6 ) -
			  g_date->DayInYear())) {
	// We didn't do it today, try again tomorrow.
	SimpleEvent( g_date->Date() + 1, cgg2_water_one, true );
	break;
      }
      CGG2_WATER_DATE = g_date->Date();
    }
    break;
    // End of watering thread.

  default:
    g_msg->Warn( WARN_BUG, "CloverGrassGrazed2::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }
  return done;
}
//---------------------------------------------------------------------------
