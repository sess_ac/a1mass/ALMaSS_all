//
// OSpringBarleyPigs.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OSpringBarleyPigs.h"

extern CfgFloat cfg_strigling_prop;

bool OSpringBarleyPigs::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
  m_farm  = a_farm;
  m_field = a_field;
  m_ev    = a_ev;

  bool done = false;

  switch ( m_ev->m_todo )
  {
  case osbp_start:
    {
      a_field->ClearManagementActionSum();

      // Start date checking block.
      int d1 = g_date->OldDays() + g_date->DayInYear( 10, 4 );
      if ( ! m_ev->m_first_year ) {
	if (g_date->Date() > d1) {
	  // No, later. It might be possible to start next spring if
	  // we are called very late in the year.
	  d1 = g_date->OldDays() + g_date->DayInYear( 1, 7 );
	  if (g_date->Date() >= d1) {
	    // We can run next spring!
	    // Calculate starting date.
	    d1 = g_date->OldDays() + g_date->DayInYear( 1, 3 ) + 365;
	  } else {
	    // Not possible to run. Bail out.
	    done = true;
	    break;
	  }
	}
      }
      // End date checking block.
      SimpleEvent( d1, osbp_spring_plough, false );
      SimpleEvent( d1, osbp_ferti, false );
    }
    break;


  case osbp_spring_plough:
    if (!m_farm->SpringPlough( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbp_spring_plough, true );
      break;
    }
    SimpleEvent( g_date->Date(), osbp_spring_harrow, false );
    break;

  case osbp_ferti:
    if (!m_farm->FP_Slurry( m_field, 0.0,
         g_date->DayInYear( 10, 4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbp_ferti, true );
      break;
    }
    // end of thread
    break;

  case osbp_spring_harrow:
    if (!m_farm->SpringHarrow( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbp_spring_harrow, true );
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15,3 ),
                 osbp_spring_sow1, false );
    break;

  case osbp_spring_sow1:
    if (!m_farm->SpringSow( m_field, 0.0,
         g_date->DayInYear( 15,4 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbp_spring_sow1, true );
      break;
    }
    OSBP_SOW_DATE=g_date->DayInYear();
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 16,3 ),
                 osbp_spring_roll, false );
    break;

  case osbp_spring_roll:
    if ( m_ev->m_lock || m_farm->DoIt( 90 ))
    {
      if (!m_farm->SpringRoll( m_field, 0.0,
           g_date->DayInYear( 25,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbp_spring_roll, true );
        break;
      }
    }
    if (OSBP_SOW_DATE+5 >g_date->DayInYear( 20,3 ))
	    SimpleEvent( g_date->OldDays() + OSBP_SOW_DATE+5,
  	               osbp_strigling1, false );
    else
    	SimpleEvent( g_date->OldDays() + g_date->DayInYear( 20,3 ),
      	           osbp_strigling1, false );
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,3 ),
                 osbp_strigling_sow, false );
    break;

  case osbp_strigling1:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 90 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 20,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbp_strigling1, true );
        break;
      }
      OSBP_STRIGLING_DATE=g_date->Date();
      // End of thread
    }
   break;

  case osbp_strigling_sow:
    if ( m_ev->m_lock || m_farm->DoIt( 60 ))
    {
      if (!m_farm->StriglingSow( m_field, 0.0,
           g_date->DayInYear( 30,4 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbp_strigling_sow, true );
        break;
      }
      else
      {
        SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                 osbp_harvest, false );
        break;
      }
    }
    {
      long newdate1 = g_date->OldDays() + g_date->DayInYear( 25,3 );
      long newdate2 = OSBP_STRIGLING_DATE + 5;
      if ( newdate2 > newdate1 )
      newdate1 = newdate2;
      SimpleEvent( newdate1, osbp_strigling2, false );
    }
    // End of thread
    break;

  case osbp_strigling2:
    if (!m_farm->Strigling( m_field, 0.0,
 	       g_date->DayInYear( 5,5 ) - g_date->DayInYear())) {
   	  SimpleEvent( g_date->Date() + 1, osbp_strigling2, true );
     	break;
    }
    {
      long newdate1 = g_date->OldDays() + g_date->DayInYear( 30,3 );
      long newdate2 = g_date->Date() + 5;
      if ( newdate2 > newdate1 )
      newdate1 = newdate2;
      SimpleEvent( newdate1, osbp_strigling3, false );
    }
    break;

  case osbp_strigling3:
    if ( m_ev->m_lock || (cfg_strigling_prop.value() * m_farm->DoIt( 89 )))
    {
      if (!m_farm->Strigling( m_field, 0.0,
           g_date->DayInYear( 15,5 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbp_strigling3, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,8 ),
                 osbp_harvest, false );
    break;

  case osbp_harvest:
    if (!m_farm->Harvest( m_field, 0.0,
         g_date->DayInYear( 15,8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbp_harvest, true );
      break;
    }
    SimpleEvent( g_date->Date(), osbp_straw_chopping, false );
    break;

  case osbp_straw_chopping:
    if ( m_ev->m_lock || m_farm->DoIt( 20 ))
    {
      if (!m_farm->StrawChopping( m_field, 0.0,
           g_date->DayInYear( 15,8 ) - g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, osbp_straw_chopping, true );
        break;
      }
      // END MAIN THREAD
      done=true;
      break;
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 5,8  ),
                 osbp_hay, false );
    break;

  case osbp_hay:
    if (!m_farm->HayBailing( m_field, 0.0,
         g_date->DayInYear( 20,8 ) - g_date->DayInYear())) {
      SimpleEvent( g_date->Date() + 1, osbp_hay, true );
      break;
    }
    // END MAIN THREAD
    done=true;
    break;

  default:
    g_msg->Warn( WARN_BUG, "OSpringBarleyPigs::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}


