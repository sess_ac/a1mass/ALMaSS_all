/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLMaizeSpring.cpp This file contains the source for the NLMaizeSpring class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
 With additions as noted in: \n
 Doxygen formatted comments in July 2008 \n
*/
//
// NLMaizeSpring.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLMaizeSpring.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_springwheat_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_MS_InsecticideDay;
extern CfgInt   cfg_MS_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional maize.
*/
bool NLMaizeSpring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	m_farm = a_farm;
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	TTypesOfVegetation l_tov = tov_NLMaizeSpring; // The current type
	int l_nextcropstartdate;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_ms_start:
	{
		// nl_ms_start just sets up all the starting conditions and reference dates that are needed to start a nl_maize
		NL_MS_START_FERTI = false;
		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.
		a_field->ClearManagementActionSum();

		// This crop calls a catch crop, so there can be no date shortening here.
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(31, 12)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(31, 12)); // last possible day of straw chopping
		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "NLMaizeSpring::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum);
					exit(l_tov);
				}
			}
			if (!a_ev->m_first_year) {
				d1 = g_date->OldDays() + 365 + m_first_date; 
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "NLMaizeSpring::Do(): ", "Crop start attempt after last possible start date");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum);
					exit(l_tov);
				}
			}
			else {
				// Is the first year
				// Some special code to cope with that first start-up year in ALMaSS - ignore for all practical purposes
				// Code for first spring treatment used
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 4), nl_ms_spring_sow, false, a_farm, a_field);
				break;
			}
		}

		// End single block date checking code. Please see next line comment as well.
		// Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(1, 4);
		if (g_date->Date() >= d1) d1 += 365;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(d1, nl_ms_spring_plough_sandy, false, a_farm, a_field);
		}
		else
		{
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
			if (g_date->Date() >= d1) d1 += 365;
			SimpleEvent_(d1, nl_ms_preseeding_cultivator, false, a_farm, a_field);
		}
	}
	break;

	// This is the first real farm operation
	case nl_ms_spring_plough_sandy:
		if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_spring_plough_sandy, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 10, nl_ms_preseeding_cultivator, false, a_farm, a_field);
		break;
	case nl_ms_preseeding_cultivator:
		if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(4, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_preseeding_cultivator, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow_with_ferti, false, a_farm, a_field);
		break;
	case nl_ms_spring_sow_with_ferti:
		// 75% will do sow with fertilizer 
		if (a_ev->m_lock || a_farm->DoIt_prob(0.75))
		{
			if (!a_farm->SpringSowWithFerti(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow_with_ferti, true, a_farm, a_field);
				break;
			}
			else
			{
				// 75% of farmers will do this, but the other 25% won't so we need to remember whether we are in one or the other group
				NL_MS_START_FERTI = true;
				// Here is a fork leading to parallel events
				SimpleEvent_(g_date->Date() + 3, nl_ms_harrow, false, a_farm, a_field);
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_ms_herbicide1, false, a_farm, a_field); // Herbidide thread
				if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
				{
					SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_s2, false, a_farm, a_field);
				}
				else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_p2, false, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow, false, a_farm, a_field);
		break;
	case nl_ms_spring_sow:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(5, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_spring_sow, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to parallel events
		SimpleEvent_(g_date->Date() + 3, nl_ms_harrow, false, a_farm, a_field);
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), nl_ms_herbicide1, false, a_farm, a_field); // Herbidide thread
		if (a_farm->IsStockFarmer()) //Stock Farmer					// N thread = MAIN
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_s2, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), nl_ms_ferti_p2, false, a_farm, a_field);
		break;
	case nl_ms_ferti_p2:
		// Here comes N thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60) && (NL_MS_START_FERTI == 0))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ms_ferti_p2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), nl_ms_harvest, false, a_farm, a_field);
		break;
	case nl_ms_ferti_s2:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60) && (NL_MS_START_FERTI == 0))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(10, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_ms_ferti_s2, true, a_farm, a_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), nl_ms_harvest, false, a_farm, a_field);
		break;
	case nl_ms_herbicide1:
		// Here comes the herbicide thread
		if (a_field->GetGreenBiomass() <= 0)
		{
			SimpleEvent_(g_date->Date() + 1, nl_ms_herbicide1, false, a_farm, a_field);
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.90))
			{
				if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_ms_herbicide1, true, a_farm, a_field);
					break;
				}
			}
		}
		// End of thread
		break;
	case nl_ms_harrow:
		// Here comes the MAIN thread
		if (a_ev->m_lock || a_farm->DoIt_prob(0.15))
		{
			if (a_field->GetGreenBiomass() <= 0)
			{
				if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_ms_harrow, true, a_farm, a_field);
					break;
				}
			}
			else { SimpleEvent_(g_date->Date() + 1, nl_ms_harrow, true, a_farm, a_field); }
		}
		// End of thread
		break;
	case nl_ms_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_harvest, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_ms_straw_chopping, false, a_farm, a_field);
		break;
	case nl_ms_straw_chopping:
		if (!a_farm->StrawChopping(a_field, 0.0, g_date->DayInYear(10, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_ms_straw_chopping, true, a_farm, a_field);
			break;
		}
		// So we are done,but this crop uses a catch crop
		l_nextcropstartdate = m_farm->GetNextCropStartDate(m_ev->m_field, l_tov);
		m_field->SetVegPatchy(false); // reverse the patchy before the next crop
		m_farm->AddNewEvent(tov_NLCatchCropPea, g_date->Date(), m_ev->m_field, PROG_START, m_ev->m_field->GetRunNum(), false, l_nextcropstartdate, false, l_tov, fmc_Others, false);
		m_field->SetVegType(tov_NLCatchCropPea, tov_Undefined); //  Two vegetation curves are specified 
		// NB no "done = true" because this crop effectively continues into the catch crop.
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLMaizeSpring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}