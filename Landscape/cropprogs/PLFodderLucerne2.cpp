/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>PLFodderLucerne2.cpp This file contains the source for the PLFodderLucerne2 class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of January 2018 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// PLFodderLucerne2.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/PLFodderLucerne2.h"



/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with m_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional fodder lucerne in the second/third year.
*/
bool PLFodderLucerne2::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	/******************** This block can be added to the top of all Crop::Do methods ***********************************************/
	m_farm = a_farm; // These assignments are necessary, not for this method but for related event calls
	m_field = a_field;
	m_ev = a_ev;
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	int noDates = 1;
	TTypesOfVegetation l_tov = tov_PLFodderLucerne2; // The current type - change to match the crop you have
/**********************************************To Here *************************************************************************/

	// Depending what event has occured jump to the correct bit of code
	switch (m_ev->m_todo)
	{
	case pl_fl2_start:
	{
		// Set up the date management stuff

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.
		m_field->ClearManagementActionSum();

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(m_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (m_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (g_date->DayInYear(5, 10) >= m_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "PLFodderLucerne2::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!m_ev->m_first_year) {
				// Are we before July 1st?
				d1 = g_date->OldDays() + g_date->DayInYear(1, 7);
				if (g_date->Date() < d1) {
					// Yes, too early. We assumme this is because the last crop was late
					printf("Poly: %d\n", m_field->GetPoly());
					g_msg->Warn(WARN_BUG, "PLFodderLucerne2::Do(): ", "Crop start attempt between 1st Jan & 1st July");
					int prev = m_field->GetLandscape()->BackTranslateVegTypes(m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				else {
					d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
					if (g_date->Date() > d1) {
						// Yes too late - should not happen - raise an error
						g_msg->Warn(WARN_BUG, "PLFodderLucerne2::Do(): ", "Crop start attempt after last possible start date");
						g_msg->Warn(WARN_BUG, "Previous Crop ", "");
						m_field->GetOwner()->GetPreviousTov(m_field->GetRotIndex());
						int almassnum = m_field->GetLandscape()->BackTranslateVegTypes(m_ev->m_next_tov);
						g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					}
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 3), pl_fl2_spring_harrow, false, m_farm, m_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.


		d1 = g_date->OldDays() + g_date->DayInYear(1, 3);
		if (g_date->Date() >= d1) d1 += 365;
		SimpleEvent_(d1, pl_fl2_spring_harrow, false, m_farm, m_field);
		break;
	}
	break;

	// This is the first real farm operation
	case pl_fl2_spring_harrow:
		if ((m_ev->m_lock) || m_farm->DoIt(50))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(31, 3) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_spring_harrow, true, m_farm, m_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(5, 3)) {
			d1 = g_date->OldDays() + g_date->DayInYear(5, 3);
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, pl_fl2_ferti_s0, false, m_farm, m_field);
		}
		else SimpleEvent_(d1, pl_fl2_ferti_p0, false, m_farm, m_field);
		break;
	case pl_fl2_ferti_p0:
		if (m_ev->m_lock || m_farm->DoIt(66))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_p0, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_fl2_herbicide1, false, m_farm, m_field);
		break;
	case pl_fl2_ferti_s0:
		if (m_ev->m_lock || m_farm->DoIt(66))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->DayInYear(20, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_s0, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 5), pl_fl2_herbicide1, false, m_farm, m_field);
		break;
	case pl_fl2_herbicide1:
		if (m_ev->m_lock || m_farm->DoIt(31))
		{
			if (!m_farm->HerbicideTreat(m_field, 0.0, g_date->DayInYear(10, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_herbicide1, true, m_farm, m_field);
				break;
			}
		}
		// 70% of farmers will do cutting 4 times, the rest only 3 times
		if (m_farm->DoIt(70))
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 5), pl_fl2_cut_to_silage1B, false, m_farm, m_field);
		}
		else
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 6), pl_fl2_cut_to_silage1A, false, m_farm, m_field);
		}
		break;
	case pl_fl2_cut_to_silage1A:
		// Here comes cutting with 3 cutting events
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage1A, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow1A, false, m_farm, m_field);
		break;
	case pl_fl2_harrow1A:
		if (m_ev->m_lock || m_farm->DoIt(33))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow1A, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_s1A, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_p1A, false, m_farm, m_field);
		break;
	case pl_fl2_ferti_p1A:
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->Date() + 10 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_p1A, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), pl_fl2_cut_to_silage2A, false, m_farm, m_field);
		break;
	case pl_fl2_ferti_s1A:
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->Date() + 10 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_s1A, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 7), pl_fl2_cut_to_silage2A, false, m_farm, m_field);
		break;
	case pl_fl2_cut_to_silage2A:
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage2A, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow2A, false, m_farm, m_field);
		break;
	case pl_fl2_harrow2A:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(5, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow2A, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 9), pl_fl2_cut_to_silage3A, false, m_farm, m_field);
		break;
	case pl_fl2_cut_to_silage3A:
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(25, 9) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage3A, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	case pl_fl2_cut_to_silage1B:
		// Here comes cutting with 4 cutting events
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage1B, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow1B, false, m_farm, m_field);
		break;
	case pl_fl2_harrow1B:
		if (m_ev->m_lock || m_farm->DoIt(33))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(5, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow1B, true, m_farm, m_field);
				break;
			}
		}
		if (m_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_s1B, false, m_farm, m_field);
		}
		else SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_p1B, false, m_farm, m_field);
		break;
	case pl_fl2_ferti_p1B:
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->FP_PK(m_field, 0.0, g_date->Date() + 10 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_p1B, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pl_fl2_cut_to_silage2B, false, m_farm, m_field);
		break;
	case pl_fl2_ferti_s1B:
		if (m_ev->m_lock || m_farm->DoIt(60))
		{
			if (!m_farm->FA_PK(m_field, 0.0, g_date->Date() + 10 - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_ferti_s1B, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), pl_fl2_cut_to_silage2B, false, m_farm, m_field);
		break;
	case pl_fl2_cut_to_silage2B:
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage2B, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow2B, false, m_farm, m_field);
		break;
	case pl_fl2_harrow2B:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(15, 7) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow2B, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(10, 8), pl_fl2_cut_to_silage3B, false, m_farm, m_field);
		break;
	case pl_fl2_cut_to_silage3B:
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(25, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage3B, true, m_farm, m_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow3B, false, m_farm, m_field);
		break;


	case pl_fl2_harrow3B:
		if (m_ev->m_lock || m_farm->DoIt(25))
		{
			if (!m_farm->ShallowHarrow(m_field, 0.0, g_date->DayInYear(25, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, pl_fl2_harrow3B, true, m_farm, m_field);
				break;
			}
		}
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(20, 9), pl_fl2_cut_to_silage4B, false, m_farm, m_field);
		break;
	case pl_fl2_cut_to_silage4B:
		if (!m_farm->CutToSilage(m_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, pl_fl2_cut_to_silage4B, true, m_farm, m_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "PLFodderLucerne2::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}