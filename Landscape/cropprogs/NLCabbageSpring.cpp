/*
*******************************************************************************************************
Copyright (c) 2017, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
CAB LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CABUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/**
\file
\brief
<B>NLCabbageSpring.cpp This file contains the source for the NLCabbageSpring class</B> \n
*/
/**
\file
by Chris J. Topping \n
modified by Elzbieta Ziolkowska \n
Version of October 2017 \n
All rights reserved. \n
With additions as noted in: \n
Doxygen formatted comments in July 2008 \n
*/
//
// NLCabbageSpring.cpp
//


#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/NLCabbageSpring.h"

// Some things that are defined externally - in this case these variables allow
// scaling of the percentage application figures for insecticides, herbicides etc..
extern CfgBool cfg_pest_cabbage_on;
// check if below are needed
extern CfgFloat l_pest_insecticide_amount;
extern CfgInt 	cfg_CAB_InsecticideDay;
extern CfgInt   cfg_CAB_InsecticideMonth;
extern CfgFloat cfg_pest_product_1_amount;


/**
\brief
The one and only method for a crop management plan. All farm actions go through here.
*/
/**
Called every time something is done to the crop by the farmer in the first instance it is always called with a_ev->todo set to start, but susequently will be called whenever the farmer wants to carry out a new operation. \n
This method details all the management and relationships between operations necessary to grow and ALMaSS crop - in this case conventional cabbage.
*/
bool NLCabbageSpring::Do(Farm *a_farm, LE *a_field, FarmEvent *a_ev)
{
	bool done = false; // The boolean value done indicates when we are totally finished with this plan (i.e. it is set to true).
	int d1 = 0;
	// Depending what event has occured jump to the correct bit of code
	switch (a_ev->m_todo)
	{
	case nl_cabs_start:
	{
		// nl_cabs_start just sets up all the starting conditions and reference dates that are needed to start a nl_ca
		NL_CABS_WINTER_PLOUGH = false;
		m_field->ClearManagementActionSum();


		// Set up the date management stuff
		// The next bit of code just allows for altering dates after harvest if it is necessary
		// to allow for a crop which starts its management early.

		// 2 start and stop dates for all 'movable' events for this crop
		int noDates = 1;
		a_field->SetMDates(0, 0, g_date->DayInYear(30, 10)); // last possible day of harvest
		a_field->SetMDates(1, 0, g_date->DayInYear(30, 10));

		a_field->SetMConstants(0, 1);

		// Check the next crop for early start, unless it is a spring crop
		// in which case we ASSUME that no checking is necessary!!!!
		// So DO NOT implement a crop that runs over the year boundary (i.e. from spring to spring!), at least not without fixing this.

		//new if: do the check only for non-optimising farms and if year>0. (030713 - m_rotation used only in the hidden year, so I modified the condition from >7 to >0)
		//optimising farms not used for now so most of related code is removed (but not in 'start' case)
		if (!(a_farm->GetType() == tof_OptimisingFarm && g_date->GetYearNumber() > 0)) {

			if (a_ev->m_startday > g_date->DayInYear(1, 7)) {
				if (a_field->GetMDates(0, 0) >= a_ev->m_startday)
				{
					g_msg->Warn(WARN_BUG, "NLCabbageSpring::Do(): ", "Harvest too late for the next crop to start!!!");
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
				}
				// Now fix any late finishing problems
				for (int i = 0; i < noDates; i++) {
					if (a_field->GetMDates(0, i) >= a_ev->m_startday) {
						a_field->SetMDates(0, i, a_ev->m_startday - 1); //move the starting date
					}
					if (a_field->GetMDates(1, i) >= a_ev->m_startday) {
						a_field->SetMConstants(i, 0); //change the default value of the MConst (=1) to 0 (necessary to correctly execute farm events in case the finishing date (MDate) was moved)
						a_field->SetMDates(1, i, a_ev->m_startday - 1); //move the finishing date
					}
				}
			}
			// Now no operations can be timed after the start of the next crop.

			if (!a_ev->m_first_year) {
				d1 = g_date->OldDays() + 365 + m_first_date; // Add 365 for spring crop
				if (g_date->Date() > d1) {
					// Yes too late - should not happen - raise an error
					g_msg->Warn(WARN_BUG, "NLCabbageSpring::Do(): ", "Crop start attempt after last possible start date");
					int prev = a_field->GetLandscape()->BackTranslateVegTypes(a_field->GetOwner()->GetPreviousTov(a_field->GetRotIndex()));
					g_msg->Warn(WARN_BUG, "Previous Crop ", prev);
					int almassnum = a_field->GetLandscape()->BackTranslateVegTypes(a_ev->m_next_tov);
					g_msg->Warn("Next Crop ", (double)almassnum); // this causes exit
					exit(1);
				}
			}
			else {
				SimpleEvent_(g_date->OldDays() + g_date->DayInYear(25, 4), nl_cabs_spring_planting, false, a_farm, a_field);
				break;
			}
		}//if

		 // End single block date checking code. Please see next line comment as well.
		 // Reinit d1 to first possible starting date.
		d1 = g_date->OldDays() + g_date->DayInYear(15, 3);
		if (g_date->Date() >= d1) d1 += 365;
		// OK, let's go.
		// Here we queue up the first event - this differs depending on whether we have field on snady or clay soils
		if (a_field->GetSoilType() == 2 || a_field->GetSoilType() == 6) { // on sandy soils (NL ZAND & LOSS)
			SimpleEvent_(d1, nl_cabs_spring_plough_sandy, false, a_farm, a_field);
		}
		else
		{
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
			if (g_date->Date() >= d1) d1 += 365;
			if (a_farm->IsStockFarmer()) //Stock Farmer
			{
				SimpleEvent_(d1, nl_cabs_ferti_s1, false, a_farm, a_field);
			}
			else SimpleEvent_(d1, nl_cabs_ferti_p1, false, a_farm, a_field);
		}
	}
	break;

	// This is the first real farm operation

	case nl_cabs_spring_plough_sandy:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->SpringPlough(a_field, 0.0, g_date->DayInYear(15, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_spring_plough_sandy, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 7;
		if (d1 < g_date->OldDays() + g_date->DayInYear(10, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(10, 4);
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(d1, nl_cabs_ferti_s1, false, a_farm, a_field);
		}
		else SimpleEvent_(d1, nl_cabs_ferti_p1, false, a_farm, a_field);
		break;
	case nl_cabs_ferti_s1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->FA_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_s1, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(25, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(25, 4);
		}
		SimpleEvent_(d1, nl_cabs_preseeding_cultivator, false, a_farm, a_field);
		break;
	case nl_cabs_ferti_p1:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->FP_NPK(a_field, 0.0, g_date->DayInYear(30, 4) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_p1, true, a_farm, a_field);
				break;
			}
		}
		d1 = g_date->Date() + 1;
		if (d1 < g_date->OldDays() + g_date->DayInYear(25, 4)) {
			d1 = g_date->OldDays() + g_date->DayInYear(25, 4);
		}
		SimpleEvent_(d1, nl_cabs_preseeding_cultivator, false, a_farm, a_field);
		break;
	case nl_cabs_preseeding_cultivator:
		if (!a_farm->PreseedingCultivator(a_field, 0.0, g_date->DayInYear(14, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 3, nl_cabs_preseeding_cultivator, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 1, nl_cabs_spring_planting, false, a_farm, a_field);
		break;

	case nl_cabs_spring_planting:
		if (!a_farm->SpringSow(a_field, 0.0, g_date->DayInYear(15, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_spring_planting, true, a_farm, a_field);
			break;
		}
		// Here is a fork leading to four parallel events
		SimpleEvent_(g_date->Date() + 5, nl_cabs_weeding1, false, a_farm, a_field);	// Weeding + Herbicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 7), nl_cabs_fungicide1, false, a_farm, a_field);	// Fungicide thread
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), nl_cabs_insecticide1, false, a_farm, a_field);	// Insecticide thread = MAIN THREAD
		SimpleEvent_(g_date->OldDays() + g_date->DayInYear(1, 7), nl_cabs_watering, false, a_farm, a_field);	// Watering thread
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), nl_cabs_ferti_s2, false, a_farm, a_field); // N thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), nl_cabs_ferti_p2, false, a_farm, a_field);
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), nl_cabs_ferti_s4, false, a_farm, a_field); // microelements thread
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(5, 5), nl_cabs_ferti_p4, false, a_farm, a_field);
		break;
	case nl_cabs_ferti_s2:
		if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_s2, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), nl_cabs_ferti_s3, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), nl_cabs_ferti_p3, false, a_farm, a_field);
		break;
	case nl_cabs_ferti_p2:
		if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_p2, true, a_farm, a_field);
			break;
		}
		if (a_farm->IsStockFarmer()) //Stock Farmer
		{
			SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), nl_cabs_ferti_s3, false, a_farm, a_field);
		}
		else SimpleEvent_(g_date->OldDays() + g_date->DayInYear(15, 6), nl_cabs_ferti_p3, false, a_farm, a_field);
		break;
	case nl_cabs_ferti_s3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			if (!a_farm->FA_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_s3, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_cabs_ferti_p3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.60))
		{
			if (!a_farm->FP_AmmoniumSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_p3, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_cabs_ferti_s4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FA_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_s4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_cabs_ferti_p4:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50))
		{
			if (!a_farm->FP_ManganeseSulphate(a_field, 0.0, g_date->DayInYear(30, 6) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_ferti_p4, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_cabs_watering:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.50))
		{
			if (!a_farm->Water(a_field, 0.0, g_date->DayInYear(15, 8) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_watering, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_cabs_weeding1:
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_weeding1, false, a_farm, a_field);
		}
		else
		{
			if (a_ev->m_lock || a_farm->DoIt_prob(0.20))
			{
				if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
					SimpleEvent_(g_date->Date() + 1, nl_cabs_weeding1, true, a_farm, a_field);
					break;
				}
				// end of thread
				break;
			}
			SimpleEvent_(g_date->Date() + 1, nl_cabs_herbicide1, false, a_farm, a_field);
			break;
		}
		break;
	case nl_cabs_herbicide1:
		if (a_field->GetGreenBiomass() <= 0) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_herbicide1, false, a_farm, a_field);
		}
		else
		{
			if (!a_farm->HerbicideTreat(a_field, 0.0, g_date->DayInYear(25, 5) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_herbicide1, true, a_farm, a_field);
				break;
			}
			SimpleEvent_(g_date->Date() + 14, nl_cabs_weeding2, false, a_farm, a_field);
			break;
		}
		break;
	case nl_cabs_weeding2:
		if (!a_farm->ShallowHarrow(a_field, 0.0, g_date->DayInYear(15, 6) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_weeding2, true, a_farm, a_field);
			break;
		}
		// end of thread
		break;
	case nl_cabs_fungicide1:
		// Here comes the fungicide thread
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(25, 7) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_fungicide1, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, nl_cabs_fungicide2, false, a_farm, a_field);
		break;
	case nl_cabs_fungicide2:
		if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(20, 8) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_fungicide2, true, a_farm, a_field);
			break;
		}
		SimpleEvent_(g_date->Date() + 14, nl_cabs_fungicide3, false, a_farm, a_field);
		break;
	case nl_cabs_fungicide3:
		if (a_ev->m_lock || a_farm->DoIt_prob(0.80))
		{
			if (!a_farm->FungicideTreat(a_field, 0.0, g_date->DayInYear(5, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_fungicide3, true, a_farm, a_field);
				break;
			}
		}
		// End of thread
		break;
	case nl_cabs_insecticide1:
		// Here comes the insecticide thread
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_cabbage_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(10, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_insecticide1, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 14, nl_cabs_insecticide2, false, a_farm, a_field);
		break;
	case nl_cabs_insecticide2:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_cabbage_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(25, 9) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_insecticide2, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 14, nl_cabs_insecticide3, false, a_farm, a_field);
		break;
	case nl_cabs_insecticide3:
		// here we check wheter we are using ERA pesticide or not
		if (!cfg_pest_cabbage_on.value() ||
			!a_field->GetLandscape()->SupplyShouldSpray()) // Not using pesticide spray
		{
			if (!a_farm->InsecticideTreat(a_field, 0.0, g_date->DayInYear(5, 10) - g_date->DayInYear())) {
				SimpleEvent_(g_date->Date() + 1, nl_cabs_insecticide3, true, a_farm, a_field);
				break;
			}
		}
		else {
			a_farm->ProductApplication(a_field, 0.0, 0, cfg_pest_product_1_amount.value(), ppp_1);
		}
		SimpleEvent_(g_date->Date() + 10, nl_cabs_harvest, false, a_farm, a_field);
		break;
	case nl_cabs_harvest:
		// Here the MAIN thread continues
		// We don't move harvest days
		if (!a_farm->Harvest(a_field, 0.0, a_field->GetMDates(0, 0) - g_date->DayInYear())) {
			SimpleEvent_(g_date->Date() + 1, nl_cabs_harvest, true, a_farm, a_field);
			break;
		}
		done = true;
		// So we are done, and somwhere else the farmer will queue up the start event of the next crop
		// END of MAIN THREAD
		break;
	default:
		g_msg->Warn(WARN_BUG, "NLCabbageSpring::Do(): "
			"Unknown event type! ", "");
		exit(1);
	}
	return done;
}