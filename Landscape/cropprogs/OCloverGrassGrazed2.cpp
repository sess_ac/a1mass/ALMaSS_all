//
//  OCloverGrassGrazed2.cpp
//
/*
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/

#include "../../Landscape/ls.h"
#include "../../Landscape/cropprogs/OCloverGrassGrazed2.h"



using namespace std;

extern CfgBool cfg_organic_extensive;
extern CfgFloat cfg_silage_prop;

bool OCloverGrassGrazed2::Do( Farm *a_farm, LE *a_field, FarmEvent *a_ev )
{
	m_farm  = a_farm;
	m_field = a_field;
	m_ev    = a_ev;
	bool done = false;
	long d1 = 0;
	int noDates= 2;

	switch ( m_ev->m_todo ) {
  case ocgg2_start:
		  OCGG2_CUT_DATE    = 0;
		  OCGG2_WATER_DATE  = 0;
          a_field->ClearManagementActionSum();


		  // Set up the date management stuff
		  // Could save the start day in case it is needed later
		  // m_field->m_startday = m_ev->m_startday;
		  m_last_date=g_date->DayInYear(10,10);
		  // Start and stop dates for all events after harvest
		  m_field->SetMDates(0,0,g_date->DayInYear(27,7));
		  // Determined by harvest date - used to see if at all possible
		  m_field->SetMDates(1,0,g_date->DayInYear(10,10));
		  m_field->SetMDates(0,1,g_date->DayInYear(1,9));
		  m_field->SetMDates(1,1,g_date->DayInYear(1,10));
		  // Check the next crop for early start, unless it is a spring crop
		  // in which case we ASSUME that no checking is necessary!!!!
		  // So DO NOT implement a crop that runs over the year boundary
		  if (m_ev->m_startday>g_date->DayInYear(1,7))
		  {
			  if (m_field->GetMDates(0,0) >=m_ev->m_startday)
			  {
				  g_msg->Warn( WARN_BUG, "OCloverGrassGrazed2::Do(): "
					  "Harvest too late for the next crop to start!!!", "" );
				  exit( 1 );
			  }
			  // Now fix any late finishing problems
			  for (int i=0; i<noDates; i++)
			  {
				  if  (m_field->GetMDates(0,i)>=m_ev->m_startday) m_field->SetMDates(0,i,m_ev->m_startday-1);
				  if  (m_field->GetMDates(1,i)>=m_ev->m_startday) m_field->SetMDates(1,i,m_ev->m_startday-1);
			  }
		  }
		  // Now no operations can be timed after the start of the next crop.
		  // Added test for management plan testing. FN, 19/5-2003.
		  if ( ! ( m_ev->m_first_year  )) {
			  // Are we before July 1st?
			  d1 = g_date->OldDays() + g_date->DayInYear( 1,7 );
			  if (g_date->Date() < d1) {
				  // Yes, too early. We assumme this is because the last crop was late
				  g_msg->Warn( WARN_BUG, "OCloverGrassGrazed2::Do(): "
					  "Crop start attempt between 1st Jan & 1st July", "" );
				  exit( 1 );
			  } else {
				  d1 = g_date->OldDays() + m_first_date+365; // Add 365 for spring crop
				  if (g_date->Date() > d1) {
					  // Yes too late - should not happen - raise an error
					  g_msg->Warn( WARN_BUG, "OCloverGrassGrazed2::Do(): "
						  "Crop start attempt after last possible start date",
						  "" );
					  exit( 1 );
				  }
			  }
		  } else {
			  // First (invisible) year or testing. Force the correct starting date.
			  d1 = g_date->OldDays() + g_date->DayInYear( 1, 4 );
			  // If testing and not the first year, then push all events
			  // into next year (start event is called in the autumn after
			  // the current year has finished). FN, 20/5-2003.
			  if (!m_ev->m_first_year) d1 += 365;
		  }
		  m_field->SetLastSownVeg( m_field->GetOwner()->GetNextCrop( m_field->GetRotIndex() ) );
		  // OK, let's go.
		  OCGG2_CUT_DATE=0;
		  SimpleEvent( d1, ocgg2_ferti_zero, false );
		  break;
  case ocgg2_ferti_zero:
    if ( m_ev->m_lock || m_farm->DoIt( 50 ))
    {
      if (!m_farm->FA_Slurry( m_field, 0.0,
           g_date->DayInYear( 30, 4 ) - g_date->DayInYear())) {
        // We didn't do it today, try again tomorrow.
        SimpleEvent( g_date->Date() + 1, ocgg2_ferti_zero, true );
        break;
      }
    }
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 1,5 ),
             ocgg2_cattle_out, false );
//    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
//                   cgg2_cut_to_silage, false );

// Start a watering thread
    SimpleEvent( g_date->OldDays() + g_date->DayInYear( 15, 5 ),
             ocgg2_water_zero, false );
    break;


case ocgg2_cattle_out:
  if (  m_ev->m_lock || m_farm->DoIt( (int) (100-(cfg_silage_prop.value()*40) ))) {
    if (cfg_organic_extensive.value()) {
      if (!m_farm->CattleOutLowGrazing( m_field, 0.0,
                              m_field->GetMDates(1,1) -
                              g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ocgg2_cattle_out, true );
              break;
      }
    }
    else {
      if (!m_farm->CattleOut( m_field, 0.0,
                              m_field->GetMDates(1,1) -
                              g_date->DayInYear())) {
        SimpleEvent( g_date->Date() + 1, ocgg2_cattle_out, true );
              break;
      }
    }
    // Success
    // Keep them out there
    SimpleEvent( g_date->Date() + 1, ocgg2_cattle_is_out, false );
    break;
  }
  SimpleEvent( g_date->OldDays() + g_date->DayInYear( 25,5 ),
                 ocgg2_cut_to_silage, false );
  break;

case ocgg2_cattle_is_out:    // Keep the cattle out there
  // CattleIsOut() returns false if it is not time to stop grazing
  if (cfg_organic_extensive.value()) {
    if (!m_farm->CattleIsOutLow( m_field, 0.0, m_field->GetMDates(1,1) -
                               g_date->DayInYear(),m_field->GetMDates(1,1))) {
    SimpleEvent( g_date->Date() + 1, ocgg2_cattle_is_out, false );
    break;
    }
  }
  else {
  if ( !m_farm->CattleIsOut( m_field, 0.0,
                               m_field->GetMDates(1,1) -
                               g_date->DayInYear(),m_field->GetMDates(1,1)))
    {
      SimpleEvent( g_date->Date() + 1, ocgg2_cattle_is_out, false );
      break;
    }
  }
  done=true;
  break;

case ocgg2_cut_to_silage:
if ( m_ev->m_lock || m_farm->DoIt( 100 )) {
  if ( g_date->Date() < OCGG2_WATER_DATE + 7 ) {
    SimpleEvent( g_date->Date() + 1, ocgg2_cut_to_silage, true );
    break;
  }
  if (!m_farm->CutToSilage( m_field, 0.0,
                            g_date->DayInYear( 25, 6 ) -
                            g_date->DayInYear())) {
    // We didn't do it today, try again tomorrow.
    SimpleEvent( g_date->Date() + 1, ocgg2_cut_to_silage, true );
    break;
  }
  OCGG2_CUT_DATE = g_date->Date();
  SimpleEvent(g_date->OldDays() + m_field->GetMDates(0,1), ocgg2_stubble_harrow, false );
  // But we need to graze too
  SimpleEvent( g_date->Date()+14, ocgg2_cattle_out, true );
}
break;

case ocgg2_stubble_harrow:
if ( m_ev->m_lock || m_farm->DoIt( 20 )) {
  // But first if we are grazing then we must stop this
  //
  if (!m_farm->StubbleHarrowing( m_field, 0.0,
                                 m_field->GetMDates(1,1) -
                                 g_date->DayInYear())) {
    // We didn't do it today, try again tomorrow.
    SimpleEvent( g_date->Date() + 1, ocgg2_stubble_harrow, true );
    break;
  }
  // If we ran, then we also happen to be the last event,
  // so terminate the program by stopping grazing
  m_field->SetMDates(1,1,g_date->DayInYear()-1);
}
break;

case ocgg2_water_zero:
  if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
    if ( g_date->Date() < OCGG2_CUT_DATE + 3 ) {
      // Try again tomorrow, too close to silage cutting.
      SimpleEvent( g_date->Date() + 1, ocgg2_water_zero, true );
      break;
    }
    if (!m_farm->Water( m_field, 0.0,
                        g_date->DayInYear( 30, 5 ) -
                        g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, ocgg2_water_zero, true );
      break;
    }
    OCGG2_WATER_DATE = g_date->Date();

    long newdate1 = g_date->OldDays() + g_date->DayInYear( 1,6 );
    long newdate2 = g_date->Date() + 7;
    if ( newdate2 > newdate1 )
      newdate1 = newdate2;
    SimpleEvent( newdate1, ocgg2_water_one, false );
  }
  break;

case ocgg2_water_one:
  if ( m_ev->m_lock || m_farm->DoIt( 25 )) {
    if ( g_date->Date() < OCGG2_CUT_DATE + 3 ) {
      // Try again tomorrow, too close to silage cutting.
      SimpleEvent( g_date->Date() + 1, ocgg2_water_one, true );
      break;
    }
    if (!m_farm->Water( m_field, 0.0,
                        g_date->DayInYear( 15, 6 ) -
                        g_date->DayInYear())) {
      // We didn't do it today, try again tomorrow.
      SimpleEvent( g_date->Date() + 1, ocgg2_water_one, true );
      break;
    }
    OCGG2_WATER_DATE = g_date->Date();
  }
  break;
  // End of watering thread.


  default:
    g_msg->Warn( WARN_BUG, "OCloverGrassGrazed2::Do(): "
		 "Unknown event type! ", "" );
    exit( 1 );
  }

  return done;
}
//---------------------------------------------------------------------------
