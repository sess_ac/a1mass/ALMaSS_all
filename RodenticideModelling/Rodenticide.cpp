#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include "../Landscape/ls.h"


//#define __RODENTICIDE_DISTRIBUTIONTEST

/**
The rate of diffusion of the mice per day assumed. Mice diffuse from each cell to surrounding cells as a proportion per time step, this proportion
is given by this parameter. High diffusion rates lead to longer distances but lower densities.
*/
CfgFloat cfg_rodenticide_diffusionRate( "RODENTICIDE_DIFFUSIONRATE", CFG_CUSTOM, 0.5 );
/**
The rate of immigration to the baitl area assumed. This parameter is used to ensure that the population of mice does not simply
die out after a few days as if forms a travelling wave from the bait. If this is set to non-zero then the immigration rate will determine the extent
to which the mice will reach (distance from bait) under stable bait replacement conditions.
*/
CfgFloat cfg_rodenticide_immigrationRate( "RODENTICIDE_IMMIGRATIONRATE", CFG_CUSTOM, 1.0 );
/**
The rate of death of poisoned mice. This is assumed to be a constant for simplicity and also determines the distance to which the mice can
disperse when poisoned. It is not the death rate of the mice themselves!
*/
CfgFloat cfg_rodenticide_deathRate( "RODENTICIDE_DEATHRATE", CFG_CUSTOM, 0.048307 );
/**
This is the power of 2 to raise the grid size to, so 0 = 1x1m, 1 = 2x2m 2 = 4x4m etc..
*/
CfgInt cfg_rodenticide_gridsize("RODENTICIDE_GRIDSIZE",CFG_CUSTOM,5);
/**
The annual probability of bait placement at a town location.
*/
CfgFloat cfg_rodenticide_BLtown_AnnFreq("RODENTICIDE_BLTYPE_TOWN_ANNFREQ",CFG_CUSTOM,0.05);
/**
The annual probability of bait placement at a farm/rural house location.
*/
CfgFloat cfg_rodenticide_BLcountry_AnnFreq("RODENTICIDE_BLTYPE_COUNTRY_ANNFREQ",CFG_CUSTOM,0.33);
/**
The annual probability of bait placement at a woodland location.
*/
CfgFloat cfg_rodenticide_BLwoodland_AnnFreq("RODENTICIDE_BLTYPE_WOODLAND_ANNFREQ",CFG_CUSTOM,0.01);
/**
The annual probability of bait placement at a woodland location.
*/
CfgFloat cfg_rodenticide_BLchristmastrees_AnnFreq("RODENTICIDE_BLTYPE_CTREES_ANNFREQ", CFG_CUSTOM, 0.05 * (5 / 9)); // 9 year rotation, treatment in yrs 2-6 in 5%
/**
The annual probability of bait placement at a woodland location.
*/
CfgFloat cfg_rodenticide_BLgrass_AnnFreq("RODENTICIDE_BLTYPE_GRASS_ANNFREQ", CFG_CUSTOM, 0.05 * (5 / 9)); // 9 year rotation, treatment in yrs 2-6 in 5%

/**
The length of time in days that bait is available after placement in a town location.
*/
CfgInt cfg_rodenticide_BLtown_Length("RODENTICIDE_BLTYPE_TOWN_LENGTH",CFG_CUSTOM,34);
/**
The length of time in days that bait is available after placement in a rural building location.
*/
CfgInt cfg_rodenticide_BLcountry_Length("RODENTICIDE_BLTYPE_COUNTRY_LENGTH",CFG_CUSTOM,34);
/**
The length of time in days that bait is available after placement in a woodland location.
*/
CfgInt cfg_rodenticide_BLwoodland_Length("RODENTICIDE_BLTYPE_WOODLAND_LENGTH",CFG_CUSTOM,82);
/**
The length of time in days that bait is available after placement in a christmass tree plantation.
*/
CfgInt cfg_rodenticide_BLchristmasstrees_Length("RODENTICIDE_BLTYPE_CTREE_LENGTH", CFG_CUSTOM, 82);
/**
The length of time in days that bait is available after placement in a grassland
*/
CfgInt cfg_rodenticide_BLgrass_Length("RODENTICIDE_BLTYPE_GRASS_LENGTH", CFG_CUSTOM, 82);

/**
The day in the year to dump the rodenticide map.
*/
CfgInt cfg_rodenticide_dumpmapday("RODENTICIDE_DUMPMAPDAY",CFG_CUSTOM,180);

/**
The first year to dump the rodenticide map.
*/
CfgInt cfg_rodenticide_dumpmapstartyear("RODENTICIDE_DUMPMAPSTARTYEAR",CFG_CUSTOM,99999);

/**
The last year to dump the rodenticide map.
*/
CfgInt cfg_rodenticide_dumpmapendyear("RODENTICIDE_DUMPMAPENDYEAR",CFG_CUSTOM,-1);

/**
The interval of days during the year after which to dump the polygon information. 180 will result in 3 dumps, day 0 day 180 and day 360.
*/
CfgInt cfg_rodenticide_dumppolyinterval("RODENTICIDE_DUMPPOLYINTERVAL",CFG_CUSTOM,180);

/**
The first year to dump the rodenticide polygon information. Note that defaults are set so as not to dump this information.
*/
CfgInt cfg_rodenticide_dumppolystartyear("RODENTICIDE_DUMPPOLYSTARTYEAR",CFG_CUSTOM,99999);

/**
The last year to dump the rodenticide polygon information. Note that defaults are set so as not to dump this information.
*/
CfgInt cfg_rodenticide_dumppolyendyear("RODENTICIDE_DUMPPOLYENDYEAR",CFG_CUSTOM,-1);


using namespace std;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------

BaitLocation::BaitLocation(int a_x, int a_y, TTypesBaitLocation a_type)
{
	SetX(a_x);
	SetY(a_y);
	SetBLtype(a_type);
	Reset();
}

BaitLocation::~BaitLocation(void)
{
	;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------

RodenticideManager::RodenticideManager(string a_fname, Landscape* a_land)
{
	// Read in input parameters
	m_gridsize = cfg_rodenticide_gridsize.value();
	m_deathRate = 1.0-cfg_rodenticide_deathRate.value();
	m_diffusionRate = cfg_rodenticide_diffusionRate.value();
	m_immigrationRate = cfg_rodenticide_immigrationRate.value();
	/**
	Uses the file name to open a file and read in all bait locations required for managemnet
	*/
	ifstream ifile(a_fname.c_str(), ios::in);
	if ( !ifile.is_open() )
	{
		cout << "Cannot open input file " << a_fname << endl;
		char ch;
		cin >> ch;
		exit(1);
	}
	int noBaits, x, y, blt;
	TTypesBaitLocation BL_type = tbl_foobar;
	BaitLocation Bait(0,0,BL_type);
	ifile >> noBaits;
	for (int i = 0; i < noBaits; i++)
	{
		ifile >> x >> y >> blt;
		Bait.SetX(x >> m_gridsize);
		Bait.SetY(y >> m_gridsize);
		Bait.SetBLtype((TTypesBaitLocation)blt);
		m_BaitList.push_back(Bait);
	}
	// Need to put actual baits out in the landscape
	m_land = a_land;
	/**
	* Creates the rodenticide grid with its twin-map and fills them with zeros.  The maps have a resolution which is defined by cfg_rodenticide_gridsize
	* Cannot have diffusion of >= map width or height - this needs checking to prevent wrapping around the world.
	*/
	m_width = m_land->SupplySimAreaWidth() >> m_gridsize;
	m_height = m_land->SupplySimAreaHeight() >> m_gridsize;
	m_heightm1 = m_height-1; // Saves many many calculations later.
	m_pmouse_map_main.resize((m_width+1) * (m_height+1));
	m_pmouse_map_twin.resize((m_width+1) * (m_height+1));
	for (int i=0; i<m_width+1; ++i) for (int j=0; j<m_height+1; ++j)
	{
		m_pmouse_map_main[j*m_height + i] = 0;
		m_pmouse_map_twin[j*m_height + i] = 0;
	}
	/**
	Read in and handle the probability distributions for seasonal start dates for bait locations.
	BaitLocationsSeasonalStartProbs.txt holds a column of daily probability for bait placement for each
	building type ()values range 0.0 to 1.0).
	*/
	ifstream i2file("BaitLocationsSeasonalStartProbs.txt", ios::in);
	if ( !ifile.is_open() )
	{
		cout << "Cannot open input file " << "BaitLocationsSeasonalStartProbs.txt" << endl;
		char ch;
		cin >> ch;
		exit(1);
	}
	double temp[tbl_foobar][365];
	for (int i = 0; i < 365; i++)
	{
		for (int j=0; j<tbl_foobar; j++)
		{
			i2file >> temp[j][i];
		}
	}
	// BaitLocationsSeasonalStartProbs.txt data exists in temp.
	// Next for each building type fill in the probability array in fractions of 0.1
	// each entry corresponds to one day when bait might be started
	for (int j=0; j<tbl_foobar; j++)
	{
		int index = 0;
		for (int i = 0; i < 365; i++)
		{
			int prob = (int) floor((temp[j][i]*10)+0.5) + index; // input data sums to 100, so  multiply by 10 is 1000
			if (prob>1000) prob=1000;
			for (int k=index; k<prob; k++) m_seasonstartprob[j][k] = i;
			index=prob;
			if (index>=1000) i=365;
		}
	}
	InitiateRodenticidePolyData("RodenticidePolyData.txt");
}


RodenticideManager::~RodenticideManager(void)
{
	;
}

void RodenticideManager::Tick(void)
{
	/** Check for bait location placement each day, but also do intialisation on Jan 1st. \n */
	DoPlaceBait();
	/** For each bait location - first add immigration to the main map. \n */
	DoImmigration();
	/** Then calculate diffusion for every cell - this is time consuming! Diffusion is added to twin map. \n */
	DoDiffuse();
	/** Apply mortalities to all cells in the twin map. \n */
	DoDeath();
	/** Finally swap the twin map and main map, then clear the twin map. \n */
	m_pmouse_map_twin.swap(m_pmouse_map_main); // Swap the twim map to the main map
	m_pmouse_map_twin.assign((m_width+1) * (m_height+1), 0); // clears the twin map
	// OUTPUT
	int year = m_land->SupplyYearNumber();
	int today = m_land->SupplyDayInYear();
	if ((cfg_rodenticide_dumpmapstartyear.value()>=year)  && (cfg_rodenticide_dumpmapstartyear.value()<=year) &&(cfg_rodenticide_dumpmapday.value()==today) )
	{
		DumpRodenticideMap("RodenticideMap.txt");
	}
	if ((year >= cfg_rodenticide_dumppolystartyear.value())  && (year <= cfg_rodenticide_dumppolyendyear.value()) )
	{
		if (today % cfg_rodenticide_dumppolyinterval.value()==0) RecordRodenticidePolyData("RodenticidePolyData.txt");
	}
}

void RodenticideManager::DoImmigration()
{
	unsigned sz = (unsigned) m_BaitList.size();
	for (unsigned i=0; i<sz; i++)
	{
		if (m_BaitList[i].GetMass()>0)
		{
			m_BaitList[i].ReduceMass(int(m_immigrationRate)); // subtract 1 m_immigrationRate from mass
			int x = m_BaitList[i].GetX();
			int y = m_BaitList[i].GetY();
			m_pmouse_map_main[y*m_width + x] += m_immigrationRate;
		}
	}
}

void RodenticideManager::Diffuse(int a_x, int a_y)
{
	double before = GetMap(a_x, a_y);
	if (before < 0.00001)
	{
		SetMapTwinP(a_x, a_y,0); // Prevents infinitely small calculations
		return;
	}
	double after = before * m_diffusionRate;
	double diff_out = after/8;
	SetMapTwinP(a_x, a_y,before-after);
	DistributeRing(a_x,a_y,diff_out);
}

void RodenticideManager::DistributeRing(int a_x, int a_y, double a_amount)
{
	/** Distribute a_amount to the eight cells around m_x,m_y */
	int x1 = a_x - 1;
	int y1 = a_y - 1;
	int y2 = y1 + 2;
	int x2 = x1 + 2;
	if ((y1<0) || (y2==m_height) ||	(x1<0) || (x2==m_width))
	{
		// We need to do this to prevent the diffusion building a wall around the edge, but it could be assessed as to whether the speed penalty is worth it
		x1 = (x1 + m_width) % m_width;
		x2 = (x2 + m_width) % m_width;
		y1 = (y1 + m_height) % m_height;
		y2 = (y2 + m_height) % m_height;
	}
	SetMapTwinP(x1,y1,a_amount);
	SetMapTwinP(x1,y2,a_amount);
	SetMapTwinP(x1,a_y,a_amount);
	SetMapTwinP(x2,y1,a_amount);
	SetMapTwinP(x2,y2,a_amount);
	SetMapTwinP(x2,a_y,a_amount);
	SetMapTwinP(a_x,y1,a_amount);
	SetMapTwinP(a_x,y2,a_amount);
}

bool RodenticideManager::ShouldPlaceBait(TTypesBaitLocation a_BLt)
{
#ifdef __RODENTICIDE_DISTRIBUTIONTEST
	return true;
#endif
	double chance = g_rand_uni();
	switch (a_BLt)
	{
	case tbl_town:
		if (chance < cfg_rodenticide_BLtown_AnnFreq.value()) return true;
		break;
	case tbl_country:
		if (chance < cfg_rodenticide_BLcountry_AnnFreq.value()) return true;
		break;
	case tbl_woodland:
		if (chance < cfg_rodenticide_BLwoodland_AnnFreq.value()) return true;
		break;
	case tbl_christmasstrees:
		if (chance < cfg_rodenticide_BLchristmastrees_AnnFreq.value()) return true;
		break;
	case tbl_grass:
		if (chance < cfg_rodenticide_BLgrass_AnnFreq.value()) return true;
		break;
	case tbl_foobar:
		cout << "Unknown building type" << endl;
		char ch;
		cin >> ch;
		exit(1);
		break;
	}
	return false;
}

int RodenticideManager::GetBaitStartDate(TTypesBaitLocation a_BLt)
{
	int chance = (int) floor((g_rand_uni() * 1000)+0.5);
	return m_seasonstartprob[a_BLt][chance];
}

void RodenticideManager::DoPlaceBait(void)
{
	/**
	* Depending upon the bait type, whether the bait has been used this year and whether it is destined to be used this method will add mass
	* to the bait locations. \n
	* Housekeeping on Jan 1st is to remove all masses and zero all flags for the start of the new year. \n
	* The first test is of date, since whether a BL is used is determined by the frequency of that BL type on Jan 1st. \n
	* The next test is on the season, determining the start of bait placement. \n
	* NB this not so time critical so no optimising of the loops is done here.
	*/
	int day = m_land->SupplyDayInYear();
	unsigned sz = (unsigned) m_BaitList.size();
	if (day==0)
	{
		for (unsigned i=0; i<sz; i++)
		{
			m_BaitList[i].Reset();
			if (ShouldPlaceBait(m_BaitList[i].GetBLtype()))
			{
				m_BaitList[i].SetUseFlag(true);
				m_BaitList[i].SetstartDay(GetBaitStartDate(m_BaitList[i].GetBLtype()));
			}
		}
	}
	// Loop through all the BLs and initiate any needed doing today
	for (unsigned i=0; i<sz; i++)
	{
		if (m_BaitList[i].GetUseFlag())
		{
			if (m_BaitList[i].GetstartDay() == day )
			{
				// Due to start today, so set bait mass & unset use flag because we have used it now.
				// Find the bait mass
				int mass;
				switch (m_BaitList[i].GetBLtype())
				{
				case tbl_town:
					mass = cfg_rodenticide_BLtown_Length.value();
					break;
				case tbl_country:
					mass = cfg_rodenticide_BLcountry_Length.value();
					break;
				case tbl_woodland:
					mass = cfg_rodenticide_BLwoodland_Length.value();
					break;
				case tbl_christmasstrees:
					mass = cfg_rodenticide_BLchristmasstrees_Length.value(); // uses same as woodland
					break;
				case tbl_grass:
					mass = cfg_rodenticide_BLgrass_Length.value(); // uses same as woodland
					break;
				default:
					g_msg->Warn( WARN_FILE, "RodenticideManager::DoPlaceBait(void): Unknown bait type","");
					exit( 0 );
				}
				m_BaitList[i].SetMass(mass);
				m_BaitList[i].SetUseFlag(false);
				// Add the bait to the main map
				SetMap(m_BaitList[i].GetX(),m_BaitList[i].GetY(),1.0);
			}
		}
	}

}


void RodenticideManager::DumpRodenticideMap(string a_fname)
{
	/**
	Uses the file name to open a file and saves the rodenticide value for each cell at the time this method is called.
	*/
	ofstream ofile(a_fname.c_str(), ios::out);
	if ( !ofile.is_open() )
	{
		cout << "Cannot open output file for rodenticide map dump " << a_fname << endl;
		char ch;
		cin >> ch;
		exit(1);
	}
	int xext = m_land->SupplySimAreaWidth();
	int yext = m_land->SupplySimAreaHeight();
	ofile << "Width: "<< xext << endl;
	ofile << "Height: "<< yext << endl;
	for (int y=0; y<yext; y++)
	{
		for (int x=0; x<xext; x++)
		{
			ofile << m_land->SupplyRodenticide(x,y) << '\t';
		}
		ofile << endl;
	}
	ofile.close();
}


void RodenticideManager::InitiateRodenticidePolyData(string a_fname)
{
	/**
	Uses the file name to open a file and saves the rodenticide value for each polygon at the time this method is called.
	*/
	ofstream ofile(a_fname.c_str(), ios::out);
	if ( !ofile.is_open() )
	{
		cout << "Cannot open output file for rodenticide poly initiate " << a_fname << endl;
		char ch;
		cin >> ch;
		exit(1);
	}
	int noPoly=m_land->SupplyNumberOfPolygons();
	ofile << noPoly << endl;
	for (int p=0; p<(noPoly-1); p++)
	{
		ofile << m_land->SupplyPolyRefVector(p) << '\t';
	}
	ofile << m_land->SupplyPolyRefVector(noPoly-1) << endl;
	for (int p=0; p<(noPoly-1); p++)
	{
		ofile << m_land->SupplyElementTypeFromVector(p) << '\t';
	}
	ofile << m_land->SupplyElementTypeFromVector(noPoly-1) << endl;
	for (int p=0; p<(noPoly-1); p++)
	{
		int area = (int) m_land->SupplyPolygonAreaVector(p);
		ofile << area << '\t';
	}
	ofile << m_land->SupplyPolygonAreaVector(noPoly-1);
	ofile.close();
}

void RodenticideManager::RecordRodenticidePolyData(string a_fname)
{
	/**
	Uses the file name to open a file and saves the rodenticide value for each polygon at the time this method is called.
	*/
	ofstream ofile(a_fname.c_str(), ios::app);
	if ( !ofile.is_open() )
	{
		cout << "Cannot open output file for rodenticide poly data " << a_fname << endl;
		char ch;
		cin >> ch;
		exit(1);
	}
	int noPoly=m_land->SupplyLargestPolyNumUsed()+1;
	double* polyR =	new double [noPoly];
	for (int p=0; p<(noPoly); p++) polyR[p]=0.0;
	int xext = m_land->SupplySimAreaWidth();
	int yext = m_land->SupplySimAreaHeight();
	for (int y=0; y<yext; y++)
	{
		for (int x=0; x<xext; x++)
		{
			double Rdt = m_land->SupplyRodenticide(x,y);
			int pindex = m_land->SupplyPolyRefIndex(x,y);
			polyR[pindex]+=Rdt;
		}
	}
	ofile << endl; // Got to be here to easily allow an eof test later
	for (int p=0; p<(noPoly-1); p++)
	{
		ofile << polyR[p] << '\t';
	}
	ofile << polyR[noPoly-1];
	ofile.close();
	delete polyR;
}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
