#pragma once

using namespace std;

class Landscape;

/** \brief An enumeration listing the types of bait locations possible. */
typedef enum
{
  tbl_town=0,
  tbl_country,
  tbl_woodland,
  tbl_christmasstrees,
  tbl_grass,
  tbl_foobar
} TTypesBaitLocation;

/** \brief Class used for describing the rodenticide bait location. */
class BaitLocation
{
/** This class has no functionality apart from get/set and arithmetic manipulation of class members. */
protected:
    /** \brief x-coordinate */
	int m_x;
     /** \brief y-coordinate */
	int m_y;
    /** \brief bait mass */
	int m_mass;
    /** \brief bait replenishment frequency */
	TTypesBaitLocation m_BLtype;
    /** \brief flag for bait location use this year */
	bool m_useThisYear;
    /** \brief date for placement of first bait */
	int m_startDay;

public:
    /** \brief BaitLocation constructor */
	BaitLocation( int a_x, int a_y, TTypesBaitLocation a_type );
    /** \brief BaitLocation destructor */
	virtual ~BaitLocation(void);
    /** \brief Get x-coordinate */
	int GetX(void) { return m_x; }
    /** \brief Get y-coordinate */
	int GetY(void) { return m_y; }
    /** \brief Get bait mass */
	int GetMass(void) { return m_mass; }
    /** \brief Get start day */
	int GetstartDay(void) { return m_startDay; }
    /** \brief Get bait location type */
	TTypesBaitLocation GetBLtype(void) { return m_BLtype; }
	/** \brief Get use flag */
	bool GetUseFlag() { return m_useThisYear; }
    /** \brief Set x-coordinate */
	void SetX(int a_x) { m_x = a_x; }
    /** \brief Set y-coordinate */
	void SetY(int a_y) { m_y = a_y; }
    /** \brief Set start day */
	void SetstartDay(int a_startDay) { m_startDay = a_startDay; }
    /** \brief Set bait mass */
	void SetMass(int a_mass) { m_mass = a_mass; }
    /** \brief Set bait mass */
	void ReduceMass(int a_mass) { m_mass -= a_mass; }
    /** \brief Set bait location type */
	void SetBLtype(TTypesBaitLocation a_BLt) { m_BLtype = a_BLt; }
    /** \brief Reset all flags and mass */
	void Reset() { m_mass = 0; m_useThisYear = false; m_startDay=-1;}
	/** \brief Set use flag */
	void SetUseFlag(bool a_flag) { m_useThisYear = a_flag; }
};

/** \brief Class for management of bait locations */
class RodenticideManager
{
protected:
	/** \brief List of bait locations */
	vector<BaitLocation> m_BaitList;
	/** \brief Map of probability of encountering a poisoned mouse */
	vector<double> m_pmouse_map_main;
	/** \brief Temporary map for calculation poisoned mouse diffusion */
	vector<double> m_pmouse_map_twin;
	/** \brief Landscape pointer */
	Landscape* m_land;
	/** \brief Landscape dimensions */
	int m_width, m_height, m_heightm1;
	/** \brief rodenticide grid size - also affect diffusion rate */
	int m_gridsize;
    /** \brief poisoned mice diffusion rate */
	double m_diffusionRate;
    /** \brief poisoned mice immigration rate */
	double m_immigrationRate;
    /** \brief poisoned mice death rate */
	double m_deathRate;
	/** \brief season start probability distribution */
	int m_seasonstartprob[tbl_foobar][1000];

public:
    /** \brief RodenticideManager constructor */
	RodenticideManager(string fname, Landscape* a_land);
    /** \brief RodenticideManager destructor */
	virtual ~RodenticideManager(void);
	/** \brief Advance one day */
	void Tick(void);
	/** \brief Initiates bait placement for those bait locations necessary */
	void DoPlaceBait();
	/** \brief Determines when the bait will be placed within a season */
	int GetBaitStartDate(TTypesBaitLocation);
	/** \brief Place bait dependent upon a frequency test */
	bool ShouldPlaceBait(TTypesBaitLocation);
    /* \brief  Add newly poisoned mice at the bait locations */
	void DoImmigration(void);
	/* \brief  For the whole map call the diffuse cell if it is above the minimum concentration of poisoned mice. */
	/** \brief Move poisoned mice around to twin map */
	void Diffuse(int a_x, int a_y);
	void DoDiffuse(void)
	{
		for (int i=0;i<m_width; i++)
			for (int j=0; j<m_height; j++)	this->Diffuse(i,j);
	}
	/** \brief Reduce each cell in twin map by m_deathRate */
	void DoDeath(void)
	{
		for (int i=0;i<m_width; i++)
			for (int j=0; j<m_height; j++) SetMapTwinMult(i,j,m_deathRate);
	}
	/** \brief Distribute an amount to the twin map in a ring */
	inline void DistributeRing(int a_x, int a_y, double a_amount);
	// Get/Set methods
    /** \brief Get poisoned mice at x,y */
	double GetMap(int a_x, int a_y) { return m_pmouse_map_main[a_y*m_width + a_x]; }
    /** \brief Get poisoned mice at x,y on twin map */
	double GetMapTwin(int a_x, int a_y) { return m_pmouse_map_twin[a_y*m_width + a_x]; }
    /** \brief Set poisoned mice at x,y */
	void SetMap(int a_x, int a_y, double a_p) { m_pmouse_map_main[a_y*m_width + a_x] = a_p; }
    /** \brief Set poisoned mice at x,y on twin map */
	void SetMapTwin(int a_x, int a_y, double a_p) { m_pmouse_map_twin[a_y*m_width + a_x] = a_p; }
    /** \brief Multiply poisoned mice at x,y on twin map by constant */
	void SetMapTwinMult(int a_x, int a_y, double a_p) { m_pmouse_map_twin[a_y*m_width + a_x] *= a_p; }
    /** \brief Add to poisoned mice at x,y on twin map */
	void SetMapTwinP(int a_x, int a_y, double a_p) { m_pmouse_map_twin[a_y*m_width + a_x] += a_p; }
	/** \brief Return the poisoned mice value at x,y */
	double GetRodenticide(unsigned a_x, int a_y) { return m_pmouse_map_main[(a_x >> m_gridsize) + (a_y >> m_gridsize)*m_width]; }
	/** \brief Dumps a map of rodenticide loads per 1m2 cells */
	void DumpRodenticideMap(string a_fname);
    /** \brief Opens and initiates the output file for polygon rodenticide information */
	void InitiateRodenticidePolyData(string a_fname);
	/** \brief Records rodenticide loads per polygon */
	void RecordRodenticidePolyData(string a_fname);
};


