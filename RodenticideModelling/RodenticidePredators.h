/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file RodenticidePredators.h
\brief <B>The header file for all Rodenticide predator and population manager classes</B>
*/
/**  \file RodenticidePredators.h
Version of  08 December 2013. \n
By Chris J. Topping

*/
//---------------------------------------------------------------------------
#ifndef RodenticidePredatorsH
#define RodenticidePredatorsH
//---------------------------------------------------------------------------

#include "../BatchALMaSS/PopulationManager.h"

/**
* \brief A data struct to hold RodenticidePredatory territory details
*/
struct RP_Territory
{
	int m_x;
	int m_y;
	int m_range;
	double m_exposure;
};

class RodenticidePredators_Population_Manager : public Population_Manager
{
public:
// Methods
	virtual ~RodenticidePredators_Population_Manager();
	RodenticidePredators_Population_Manager( Landscape* L );
	/**
	* \brief Updates the rodenticide exposure for all predator territories
	*/
	void Tick();
	void PreCachePoly(int a_poly) 
	{
		m_qual_cache[a_poly] = EvaluatePoly(a_poly);
	}
	vector <RP_Territory> *SupplyTerritories() { return &m_Territories; }
	int HowManyTerritoriesHere(int a_x, int a_y);
protected:
// 	Attributes
	int m_sim_w;
	int m_sim_h;
	int m_sim_w_div_10;
	int m_sim_h_div_10;
    int m_hash_size;
	double* m_qual_cache;
    double* m_qual_grid;
	bool * m_occupancy_grid;
	double m_habquals[tole_Foobar];
	int m_predator_minHR;
	int m_predator_maxHR;
	double m_predator_targetQual;
	vector <RP_Territory> m_Territories;
	int m_NoTerritories;
	/**
	* \brief Keeps track of the number of days we have rodenticide exposure summation for
	*/
	double m_summeddays;

// Methods
	/**
	* \brief Get the habitat quality based on polygon type
	*/
	double EvaluatePoly(int a_poly);
	/**
	* \brief Read in specific predator type habitat scores
	*/
    void ReadHabitatValues(int a_type);
	/**
	* \brief Evaluate the landscape and create the habitat quality grid
	*/
	void CreatHabitatQualGrid(void);
	/**
	* \brief Populates the landscape with predatory territories
	*/
	void CreateTerritories();
	/**
	* \brief Output functions for predator territories
	*/
	void PredatorTerritoryOutput(bool a_initial);
	/**
	* \brief Output functions for predator territories
	*/
	void PredatorTerritoryInput();
	/**
	* \brief Tests whether a square has any occupied cells
	*/
	bool IsGridPositionValid( int & x, int & y, int range );
	/**
	* \brief Gets the total habitat score for a square UL x,y diameter of range
	*/
	double EvaluateHabitat( int a_x, int a_y, int a_range );
	/**
	* \brief Create a new territory and claim the grid
	*/
	void NewTerritory( int a_x, int a_y, int a_range, double a_qual );

};
//---------------------------------------------------------------------------


#endif
