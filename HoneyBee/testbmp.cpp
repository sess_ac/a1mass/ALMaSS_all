
#include <string>
#include <fstream>
#include "bmp.h"
#include "hive.h"


using json = nlohmann::json;

int main1(int argc, char* argv[])
{
   std::string jsonfile;
   std::cout << "nargs: " << argc << std::endl;
   
   if (argc != 2)
   {
      jsonfile = "bmp2.json";
   }
   else
   {
      jsonfile = argv[1];
   }
   std::cout << "Using json file: " << jsonfile << std::endl;

   Hive hive("hiveconfig.json");

   blitz::firstIndex i;
   blitz::secondIndex j;
   blitz::thirdIndex k;

   hive.beeCount=k;

   std::cout << "shape of array: " << hive.beeCount.shape() << std::endl;

   std::cout << "view Sides\n"; 
   for (int i = 0; i < 2; ++i)
   {
      auto ss = hive.side<int>(hive.beeCount, 0,i,0);
      std::cout << "Frame: " << i << ":" << 0 << std::endl;
      std::cout <<  ss << std::endl;
      std::cout << "Frame: " << i << ":" << 1 << std::endl;
      auto sss = hive.side<int>(hive.beeCount, 0,i,1);
      std::cout <<  sss << std::endl;
   }
   
   std::cout << "Copy Sides\n"; 
   for (int i = 0; i < 2; ++i)
   {
      blitz::Array<int,3> ss;
      auto sz = hive.sideDims(0);
      ss.resize(sz[0],sz[1],sz[2]);
      ss  = hive.side<int>(hive.beeCount, 0,i,0);
      std::cout << "Frame: " << i << ":" << 0 << std::endl;
      std::cout <<  ss << std::endl;
      std::cout << "Frame: " << i << ":" << 1 << std::endl;
      ss = hive.side<int>(hive.beeCount, 0,i,1);
      std::cout <<  ss << std::endl;
   }

//////////////////////////////7   
   std::cout << "view Frames\n"; 
   for (int i = 0; i < 2; ++i)
   {
      auto ss = hive.frame<int>(hive.beeCount, 0,i);
      std::cout << "Frame: " << i << std::endl;
      std::cout <<  ss << std::endl;
   }
   
   std::cout << "Copy Frames\n"; 
   for (int i = 0; i < 2; ++i)
   {
      blitz::Array<int,3> ss;
      auto sz = hive.frameDims(0);
      ss.resize(sz[0],sz[1],sz[2]);
      ss  = hive.frame<int>(hive.beeCount, 0,i);
      std::cout << "Frame: " << i << std::endl;
      std::cout <<  ss << std::endl;
   }

//////////////////////////////7   
   std::cout << "view Boxes\n"; 
   for (int i = 0; i < 1; ++i)
   {
      auto ss = hive.box<int>(hive.beeCount, 0);
      std::cout << "Frame: " << i << std::endl;
      std::cout <<  ss << std::endl;
   }
   
   std::cout << "Copy Frames\n"; 
   for (int i = 0; i < 1; ++i)
   {
      blitz::Array<int,3> ss;
      auto sz = hive.boxDims(0);
      ss.resize(sz[0],sz[1],sz[2]);
      ss  = hive.box<int>(hive.beeCount, 0);
      std::cout << "Box: " << i << std::endl;
      std::cout <<  ss << std::endl;
   }

   // Swap Frames
   hive.swapFrames<int>(hive.beeCount,0,0,0,1);
   std::cout << hive.beeCount << std::endl;

   // Add Box
   hive.addBox(5);
   /*
   HONEYBEE::BMP bmp(jsonfile, &hive);
   bmp.checkEvents();
   */
}


int main(int argc, char* argv[])
{
   std::string hiveConfigFile;
   std::string bmpFile;
   
   if (argc != 3)
   {
      hiveConfigFile = "hiveconfig.json";
      bmpFile="bmp.json";
   }
   else
   {
      hiveConfigFile = argv[1];
      bmpFile = argv[1];
   }
   std::cout << "Using hive config file: " << hiveConfigFile << std::endl;
   std::cout << "Using BMP file: " << bmpFile << std::endl;

   Hive hive(hiveConfigFile);

   HONEYBEE::BMP bmp(bmpFile, &hive);
   bmp.checkEvents();
}

