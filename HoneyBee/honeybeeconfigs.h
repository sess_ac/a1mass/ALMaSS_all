#ifndef HONEYBEECONFIGS_H
#define HONEYBEECONFIGS_H

#include "../Landscape/Configurator.h"

extern CfgInt cfg_BeeMinimumTimeStep;
static CfgFloat cfg_hb_sugar_per_metabolic_rate("HB_SUGAR_PER_METABOLIC_RATE",CFG_CUSTOM,1.0);
static CfgFloat cfg_hb_pool_concentration("HB_POOL_CONCENTRATION",CFG_CUSTOM,0.2);
static CfgFloat cfg_hb_honey_concentration("HB_HONEY_CONCENTRATION",CFG_CUSTOM,0.8);
static CfgFloat cfg_hb_nectar_load("HB_NECTAR_LOAD",CFG_CUSTOM,0.000005); // 50 mg water
static CfgFloat cfg_hb_honeycell_volume("HB_honeycell_volume",CFG_CUSTOM,0.001);

#endif // HONEYBEECONFIGS_H

