#ifndef BMP_H
#define BMP_H

#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>
#include "hive.h"


namespace HONEYBEE {

   using json = nlohmann::json;
  
   void dumpDate(tm& tm)
   {
      std::cout << "Year: " << (tm.tm_year + 1900)
		<< " Month: " << tm.tm_mon
		<< " Day: " << tm.tm_mday
		<< " Hour: " << tm.tm_hour
		<< " Min: " << tm.tm_min << std::endl;
   }

   struct BMP
   {
      json events;
      std::tm modelTime;
      std::map<std::string,float> variables;
      Hive * hive;
      
      BMP(std::string jsonfile, Hive * thehive)
      {
	 hive=thehive;
	 if (!readJson(jsonfile))
	 {
	    std::cerr << "Falied to open " << jsonfile << std::endl;
	    exit(1);
	 }
	 modelTime = {0,30,21,22,3,119};
	 variables["numberOfWorkers"]=40000;
	 variables["numberOfEggs"]=2000;
      }
    
      int readJson(std::string jsonfile)
      {
	 std::ifstream reader(jsonfile) ;
	 if (reader)
	 {
	    reader >> events;
	    reader.close();
	    return 1;
	 }
	 else
	 {
	    return 0;
	 }
      }
    
      void dumpEvent(json event)
      {
	 std::cout << event.dump(4) << std::endl;
      }
    
      bool timeTriggered(json cond)
      {
	 std::tm tm1 = {};
	 auto dtstring = cond["datetime"].get<std::string>();
	 std::stringstream ss(dtstring);
	 ss >> std::get_time(&tm1, "%b %d %Y %H:%M");
	 double d = difftime(mktime(&tm1),mktime(&modelTime))/60;
	 return ((d > 0) && (d <10));
      }
    
      bool valueTriggered(json cond)
      {
	 auto vstring = cond["variable"].get<std::string>();
	 int value = cond["value"].get<int>();
	 auto comparison = cond["comparison"].get<std::string>();

	 if (variables.count(vstring))
	 {
	    if (comparison == "<")
	    {
	       if (variables[vstring] < value)
		  return true;
	       else
		  return false;
	    }
	   
	    if (comparison == ">")
	    {
	       if (variables[vstring] > value)
		  return true;
	       else
		  return false;
	    }
	   
	    if (comparison == "=")
	    {
	       if (variables[vstring] == value)
		  return true;
	       else
		  return false;
	    }
	
	    if (comparison == "<=")
	    {
	       if (variables[vstring] <= value)
		  return true;
	       else
		  return false;
	    }
	
	    if (comparison == ">=")
	    {
	       if (variables[vstring] >= value)
		  return true;
	       else
		  return false;
	    }

	    if ((comparison != "<") ||
		(comparison != ">") ||
		(comparison != ">=") ||
		(comparison != "<=") ||
		(comparison != "="))
	    {
	       std::cout << comparison << " is not a valid comparison string." << std::endl;
	       exit(1);
	    }
	
	 }
	 else
	 {
	    std::cout << vstring << " is not in the variable list."
		      << std::endl;
	 }
      }
  
      bool triggered(json event)
      {
	 bool trig = true;
	 for (json cond : event["EVENT"]["conditions"])
	 {
	    std::string t = cond["type"].get<std::string>();
	    if (t=="timed")
	    {
	       // Note: using binary and on bool for logigal and.
	       // (Should be safe.)
	       trig &= timeTriggered(cond);
	    }
	    if (t=="value")
	    {
	       // Note: using binary and on bool for logigal and.
	       // (Should be safe.)
	       trig &= valueTriggered(cond);
	    }
	 }
	 return trig;
      }
     
      int checkEvents()
      {
	 for (auto e : events["events"])
	 {
	    if (triggered(e))
	    {
	       if (e["EVENT"].count("comment"))
	       {
		  std::cout << e["EVENT"]["comment"].get<std::string>()
			    << std::endl;
	       }
	       executeJobs(e);
	    }
	 }
      }

      void executeJobs(json event)
      {
	 json jobs = event["EVENT"]["jobs"];
	 for (auto job : jobs)
	 {
	    executeJob(job);
	 }
      }

      void executeJob(json job)
      {
	 // KILL BEES
	 if (job["task"].get<std::string>()=="kill")
	 {
	    if (job.count("number"))
	    {
	       kill(job["popdata"]["type"].get<std::string>(),
		    job["popdata"]["minage"].get<int>(),
		    job["popdata"]["maxage"].get<int>(),
		    job["number"].get<int>());
	    }
	    else
	    {
	       killPercent(job["popdata"]["type"].get<std::string>(),
			   job["popdata"]["minage"].get<int>(),
			   job["popdata"]["maxage"].get<int>(),
			   job["percent"].get<int>());
	    }
	 }

	 // KILL VAROA
	 if (job["task"].get<std::string>()=="killvaroa")
	 {
	    killVaroa(job["percent"]);
	 }

	 // SWAP FRAMES
	 if (job["task"].get<std::string>()=="swapframes")
	 {
	    swapFrames(job["frame1"]["box"],
		       job["frame1"]["frame"],
		       job["frame2"]["box"],
		       job["frame2"]["frame"]);
	 }

	 // ADD BOX
	 if (job["task"].get<std::string>()=="addbox")
	 {
	    addBox(job["height"].get<int>(),job["position"].get<int>());
	 }

	 // REMOVE BOX
	 if (job["task"].get<std::string>()=="removebox")
	 {
	    removeBox(job["position"].get<int>());
	 }

	 // FEED
	 if (job["task"].get<std::string>()=="feed")
	 {
	    if (job["product"] == "fondant")
	    {
	       feedFondant(job["mass"].get<int>());
	    }
	    if (job["product"] == "syrup")
	    {
	       feedSyrup(job["volume"].get<int>());
	    }
	 }

      
      }

      //
      // WORK FUNCTIONS
      //
      int kill(const std::string& btype,
	       int minage,
	       int maxage,
	       int number)
      {
	 std::cout << "Killing " << number << " " << btype << " bees aged "
		   << minage << " to " << maxage << std::endl; 

	 return -1;
	  }

      int killPercent(const std::string& btype,
		      int minage,
		      int maxage,
		      int percent)
      {
	 std::cout << "Killing " << percent << " % of " << btype << " bees aged "
		   << minage << " to " << maxage << std::endl; 

	 return -1;
	  }

      int killVaroa(int percentage)
      {
	 std::cout << "Killing " << percentage << " percent of varroa" << std::endl;

	 return -1;
	  }
  
      int swapFrames(int box1, int frame1,
		     int box2, int frame2)
      {
	 std::cout << "Swapping box:frame " << box1 << ":" << frame1
		   << " with box:frame " << box2 << ":"
		   << frame2 << std::endl;
	 hive->swapFrames(box1, frame1, box2, frame2);

	 return -1;
	  }

      int addBox(int height, int position)
      {
		  return -1;
      }

      int removeBox(int position)
      {
		  return -1;
      }

      int addFrame(int box1, int position)
      {
		  return -1;
      }

      int feedFondant(int mass)
      {
	 std::cout << "Feed fondant " << mass << " g" << std::endl;

	 return -1;
	  }
    
      int feedSyrup(int volume)
      {
	 std::cout << "Feed syrup " << volume << " ml" << std::endl;

	 return -1;
	  }
    
   };
  
} // Namespace HONEYBEE

#endif // BMP_H

/*
int main()
{
   HONEYBEE::BMP bmp;
   bmp.checkEvents();
}
*/
