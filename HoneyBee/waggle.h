#ifndef WAGGLE_H
#define WAGGLE_H

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>


namespace HONEYBEE
{
   struct Waggle
   {
      struct Dance
      {
	 int direction;
	 int distance;
	 float quality;
	 int travelTime;
	 float nectarConcentration;
	 float pollenVariation;
	 int ndances;
	 int age;

	 Dance(int dir, int dis, float qual=0, int ttime=0)
	 : direction(dir), distance(dis), quality(qual), travelTime(ttime),
	    ndances(1), age(0),nectarConcentration(0), pollenVariation(0)
	 {}
	 
         Dance()
	 : direction(0), distance(0), quality(0), travelTime(0),
	  ndances(1), age(0),nectarConcentration(0), pollenVariation(0)
	 {}
      };


      int purgeAge=1;
      std::vector<Dance> dances;
   
      bool haveDance(int dir, int dis)
      {
	 Dance lookup = {dir,dis};
	 std::vector<Dance>::iterator it = std::find_if(dances.begin(),
							dances.end(),
							[&lookup](const Waggle::Dance& d)
							{
							   return ((d.direction == lookup.direction) &&
								   (d.distance == lookup.distance));
							});
	 return (it != dances.end());
      }

      bool haveDance(const Dance& d)
      {
	 return haveDance(d.direction,d.distance);
      }

      int danceIndex(int dir, int dis)
      {
	 Dance lookup = {dir,dis};
	 std::vector<Dance>::iterator it = std::find_if(dances.begin(),
							dances.end(),
							[&lookup](const Waggle::Dance& d)
							{
							   return ((d.direction == lookup.direction) &&
								   (d.distance == lookup.distance));
							});
	 return it-dances.begin();
      }

      int danceIndex(const Dance& d)
      {
	 return danceIndex(d.direction,d.distance);
      }

      bool hasDance()
      {
	 return dances.size() > 0;
      }
      
      void addDance(int dir, int dis, float qual, int ttime)
      {
	 // If new dance then add else inc existing dance.
	 int dindex=danceIndex(dir,dis);
	 if (dindex==dances.size()) // Not found
	 {
	    dances.push_back({dir,dis,qual,ttime});
	 }
	 else
	 {
	    dances[dindex].ndances++;
	    dances[dindex].quality=qual; // Update quality to latest value
	 }
      }

      // Probabaly want to make this a function of distance too.
      int bestReward()
      {
	 auto me = std::max_element(dances.begin(), dances.end(), [](const Waggle::Dance& l, const Waggle::Dance r)
				    {return l.quality < r.quality;});

	 return me - dances.begin();
      }

      Waggle::Dance selectDance()
      {
	 return dances[bestReward()];
      }

      void tick()
      {
	 for (auto& d : dances)
	 {
	    d.age++;
	 }
      }
      
      void purge()
      {
	 auto new_end = std::remove_if(dances.begin(), dances.end(),
			[&](const Dance& d)
				       { return d.age > purgeAge; } );
	 dances.erase(new_end, dances.end());

      }
   };

   std::ostream& operator<<(std::ostream& os, const Waggle::Dance& d);
} // Namespace HONEYBEE
#endif // WAGGLE_H

