/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationPopulationManager.cpp
\brief <B>The main source code for subpopulation population manager class</B>
*/
/**  \file SubPopulationPopulationManager.cpp
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"

using namespace std;
using namespace blitz;

extern CfgBool cfg_ReallyBigOutput_used;

//---------------------------------------------------------------------------


SubPopulation_Population_Manager::SubPopulation_Population_Manager(Landscape* L, string DevReproFile, int a_sub_w, int a_sub_h, int a_num_life_stage, int a_max_long_dist, int a_peak_long_dist, float a_scale_wind_speed, float a_max_wind_speed, int a_wind_direc_num, int a_wind_speed_step_size, int a_max_alive_day) : Population_Manager_Base(L)
{
	m_num_life_stage = a_num_life_stage;
	m_ListNameLength = m_num_life_stage;
	m_sub_w = a_sub_w;
	m_sub_h = a_sub_h;
	m_lowest_temp_dev.resize(m_num_life_stage);
	m_index_next_life_stage.resize(m_num_life_stage);
	m_max_alive_days = a_max_alive_day;

	//flying related parameters
	//create the movement masks
	m_current_flying_array.resize(m_max_alive_days);
	m_current_flying_array = 0.0;
	m_current_landing_array.resize(m_max_alive_days);
	m_current_landing_array = 0.0;
	m_max_long_distance = a_max_long_dist;
	m_peak_long_distance = a_peak_long_dist;
	m_scale_wind_speed = a_scale_wind_speed;
	m_max_flying_wind_speed = a_max_wind_speed;
	//the default wind direction number is 8
	m_wind_direction_num = a_wind_direc_num;
	m_wind_speed_step_size = a_wind_speed_step_size;
	//get the sampling nubmers for the wind speed with the given the step size
	m_wind_speed_num = ceil(m_max_flying_wind_speed / m_wind_speed_step_size);

	//initialise the lookup table for puting the given wind speed to a range for the sake of speed. Each range is like [)
	m_wind_speed_lookup_table.resize(ceil(m_max_flying_wind_speed));
	for(int i = 0; i<=m_max_flying_wind_speed; i++){
		m_wind_speed_lookup_table(i)= i/m_wind_speed_step_size;
	}

	//initialise the long-distance flying mask
	m_wind_direction_array.resize(m_wind_direction_num, 2);

	//convert the longest distance to the number of cells
	//we need to make it with the largest distance multiplied by the incremental factor
	m_long_move_mask_x_num_half_array.resize(m_wind_speed_num);
	m_long_move_mask_y_num_half_array.resize(m_wind_speed_num);
	for (int i=0; i<m_wind_speed_num;i++){
		m_long_move_mask_x_num_half_array(i) = m_max_long_distance*pow(a_scale_wind_speed, i)/m_sub_w;
		m_long_move_mask_y_num_half_array(i) = m_max_long_distance*pow(a_scale_wind_speed, i)/m_sub_h;
	}
	double temp_longest_factored = m_max_long_distance*pow(a_scale_wind_speed, m_wind_speed_num-1);
	m_long_move_mask_x_num_half = temp_longest_factored/m_sub_w;
	m_long_move_mask_x_num = 2*m_long_move_mask_x_num_half +1;
	m_long_move_mask_y_num_half = temp_longest_factored/m_sub_h;
	m_long_move_mask_y_num = 2*m_long_move_mask_y_num_half+1;
	m_long_move_mask.resize(m_wind_speed_num, m_wind_direction_num, m_long_move_mask_x_num, m_long_move_mask_y_num);
	m_long_move_mask = 0.0;
	calLongMovementMask();

	//hard coded 
	m_short_move_mask_x_num = 3;
	m_short_move_mask_y_num = 3;
	m_short_move_mask.resize(m_short_move_mask_x_num, m_short_move_mask_y_num);
	m_short_move_mask = 0.0;
	if(DevReproFile==""){
		readDevReproFile("Subpopulation/subpopulation_default_info.txt");
	}
	else{
		readDevReproFile(DevReproFile);
	}
		
	// Initialize the default population size to zero
	m_total_num_each_stage.resize(m_num_life_stage);
	m_total_num_each_stage=0;

	//set the number of cells
	m_num_x_range = SimW/m_sub_w;
	m_num_y_range = SimH/m_sub_h;
	m_size_cell = m_sub_w*m_sub_h;

	//Initialize the weighted population density array with zero
	m_cell_popu_density.resize(m_num_x_range, m_num_y_range);
	m_cell_popu_density = 0.0;

	//Initialize the suitability for each cell with zero
	m_cell_suitability.resize(m_num_x_range, m_num_y_range);
	m_cell_suitability = 0.0;

	//Initialize the pointer array for the subpopulation objects
	m_the_subpopulation_array.resize(m_num_x_range, m_num_y_range);	
	m_the_subpopulation_array = NULL;

	m_first_flag_life_stage.resize(m_num_life_stage);
	m_first_flag_life_stage = true;

	m_hibernated_hatch_flag = false;

	m_index_next_life_stage= -1;

	//initialize the mortality related arrays
	m_current_mortality_array.resize(m_num_life_stage, m_max_alive_days);
	m_current_mortality_array = 0.0;
	m_lowest_temperature_die.resize(m_num_life_stage);
	m_lowest_temperature_die = -10.0;
	m_highest_temperature_die.resize(m_num_life_stage);
	m_highest_temperature_die = 40.0;
	m_optimal_temperature.resize(m_num_life_stage);
	m_optimal_temperature = 15;

	//create the arrays for calculation of degree days
	m_accumu_degree_days.resize(m_num_life_stage, m_max_alive_days);
	m_accumu_degree_days = 0.0;
	m_index_new_old.resize(m_num_life_stage, 2); //the first is the newest index and the second is the oldest index
	m_index_new_old = 0; //They start from zero from the beginning.

	//do the initialisation
	initialisePopulation();

}

SubPopulation_Population_Manager::~SubPopulation_Population_Manager(){
	for (int i=0; i<m_num_x_range; i++){
		for (int j=0; j<m_num_y_range; j++){
			if (m_the_subpopulation_array(i,j) !=  NULL){
				delete m_the_subpopulation_array(i,j);
			}
		}
	}
}

void SubPopulation_Population_Manager::updateWholePopulationArray(int a_listindex, double number)
{	
	double temp_number = m_total_num_each_stage(a_listindex);
	temp_number += number;
	if (temp_number < 0){
		m_total_num_each_stage(a_listindex)=0;
	}
	else
	{
		m_total_num_each_stage(a_listindex)= temp_number;
	}
}

/**
This is the main scheduling method for the population manager using subpopulation model.
*/
void SubPopulation_Population_Manager::Run( int NoTSteps ) {
	
  for ( int TSteps = 0; TSteps < NoTSteps; TSteps++ )
  {	  
    DoFirst();
    // call the step-method of all objects
	for (int i = 0; i < m_num_x_range; i++){
	//parallel_for(m_num_x_range[&](int start, int end){
		for (int j = 0; j < m_num_y_range; j++){
			if(m_the_subpopulation_array(i,j)!=NULL){
				m_the_subpopulation_array(i,j)->Step();
			}
		}
	}
	DoLast();
  } // End of time step loop
}

double SubPopulation_Population_Manager::getTotalSubpopulationInCell(int x_indx, int y_indx){
	if(m_the_subpopulation_array(x_indx, y_indx)==NULL){
		return 0;
	}
	return m_the_subpopulation_array(x_indx, y_indx)->getTotalSubpopulation();
}

SubPopulation* SubPopulation_Population_Manager::supplySubPopulationPointer(int indx, int indy){
	return m_the_subpopulation_array(indx, indy);
}

double SubPopulation_Population_Manager::supplyAllPopulationGivenStage(int index){
	if(index<m_num_life_stage){
		return m_total_num_each_stage(index);
	} 
	return 0;
}

unsigned SubPopulation_Population_Manager::GetLiveArraySize(int a_listindex) {
	return unsigned(supplyAllPopulationGivenStage(a_listindex));
}

void SubPopulation_Population_Manager::readDevReproFile(string inputfile){
	ifstream ist(inputfile, ios::in);
	int row_num;
    ist >> row_num;
    ist >> m_num_life_stage;
    m_development_degree_day.resize(row_num, m_num_life_stage);

	//load the name for life stages
	for(int j=0; j<m_num_life_stage;j++){
		string temp_name;
		ist >> temp_name;
	}

	//m_max_alive_days = -1;

	//load the development and reproducing degree days
    for (int i=0; i<row_num-1; i++){
        for(int j=0; j<m_num_life_stage;j++){
            int temp_int;
            ist >> temp_int;
            m_development_degree_day(i,j)=temp_int;
            //cout<<m_activities_priorities(i,j);
			//if(i==0){
			//	if (m_max_alive_days < temp_int){
			//		m_max_alive_days = temp_int;
			//	}
			//}
        }
    }

	//load the lowest development temperature
	for (int j=0; j<m_num_life_stage; j++){
		double temp_dou;
		ist >> temp_dou;
		m_lowest_temp_dev(j) = temp_dou;
	}
}

void SubPopulation_Population_Manager::relocatePopulation(){
	;
}

void SubPopulation_Population_Manager::DoLast(){
	relocatePopulation(); //let the species move
	doDevelopment();
	addNewDay();
}
void SubPopulation_Population_Manager::calLongMovementMask(){

	for (int i=0; i<m_wind_speed_num; i++){
		for ( int x=-m_long_move_mask_x_num_half_array(i); x<m_long_move_mask_x_num_half_array(i); x++){
			for (int y=-m_long_move_mask_y_num_half_array(i); y<m_long_move_mask_y_num_half_array(i); y++){
				//we calculate the base mask without the consideration of the wind direction first
				double temp_mask_value = calLandingCurve(sqrt(pow(x*m_sub_w,2)+pow(y*m_sub_h,2)), m_peak_long_distance*pow(m_scale_wind_speed, i), m_max_long_distance*pow(m_scale_wind_speed, i));
				//apply the wind direction
				for(int j=0; j<m_wind_direction_num; j++){
					//calculate the cosine value between the direction of the cell and the wind direction
					double temp_cos = (y*m_wind_direction_array(j,1)+x*m_wind_direction_array(j,0))/(sqrt(x*x+y*y)*sqrt(m_wind_direction_array(j,0)*m_wind_direction_array(j,0)+m_wind_direction_array(j,1)*m_wind_direction_array(j,1)));
					if (temp_cos>0){
						m_long_move_mask(i, j, m_long_move_mask_x_num_half+x, m_long_move_mask_y_num_half+y) *= temp_cos;
					}
					else{
						m_long_move_mask(i, j, m_long_move_mask_x_num_half+x, m_long_move_mask_y_num_half+y) = 0.0;
					}
				}
			}
		}
	}

	/* not used
	blitz::Array<double, 1> mean_vector_temp (2);
	mean_vector_temp = 0;
	blitz::Array<double, 2> covariance_temp (2, 2);
	covariance_temp = 1,0,0,1;

	//We first calculate the long distance movement masks.

	//Get the center of the mask.
	double center_x = (m_long_move_mask_x_num-1)/2;
	double center_y = (m_long_move_mask_y_num-1)/2;

	//Temporary location array
	blitz::Array<double, 2> temp_local_array(m_long_move_mask_x_num, m_long_move_mask_y_num);
	blitz::firstIndex i;
	blitz::secondIndex j;
	//temp_local_array(i, blitz::Range::all())=j;
	blitz::thirdIndex k;
	blitz::fourthIndex l;

	m_long_move_mask_y_num = 0;
	*/
	m_long_move_mask(blitz::Range::all(), blitz::Range::all(), 0, 0) = 1.0;
	//std::cout << m_long_move_mask << std::endl;
}

void SubPopulation_Population_Manager::doDevelopment(){
	double current_temp = m_TheLandscape->SupplyTemp();
	double current_degree_days = 0;
	//std::cout<<m_index_next_life_stage<<endl;
	m_index_next_life_stage = -1;
	for (int i=0; i < m_num_life_stage; i++){
		//First, calculate the degree days for today
		//THe degree days can only be accumulated when the curent temperature is higher than the lowest development temperature
		//for over winter eggs, no need to accumulate degree days
		if (i==0 && m_development_degree_day(0, i) < 0) {
			if (m_hibernated_hatch_flag){
				m_index_next_life_stage(i) = 0; //for the overwintering eggs, they always stay at zero.
			}
			continue;
		}

		else{
			if (current_temp > m_lowest_temp_dev(i)){
				current_degree_days = current_temp;// - m_lowest_temp_dev(i);
				//decide which ones should be added
				if(m_index_new_old(i,0)<m_index_new_old(i,1)){
					m_accumu_degree_days(i, blitz::Range(0, m_index_new_old(i, 0))) += current_degree_days;
					m_accumu_degree_days(i, blitz::Range(m_index_new_old(i,1), blitz::toEnd)) += current_degree_days;
				}
				else if(m_index_new_old(i, 0)==m_index_new_old(i, 1)){
					m_accumu_degree_days(i, m_index_new_old(i, 1)) += current_degree_days;
				}
				else{
					m_accumu_degree_days(i, blitz::Range(m_index_new_old(i, 1), m_index_new_old(i, 0))) += current_degree_days;
				}
				//check whether they need to go to next life stage or die
				if(m_accumu_degree_days(i, m_index_new_old(i,1))>=m_development_degree_day(0, i)){
					m_index_next_life_stage(i) = m_index_new_old(i, 1);
					//set it to zero
					m_accumu_degree_days(i, m_index_new_old(i, 1)) = 0.0;
					m_index_new_old(i, 1) += 1;
					//when it reaches the end, set it to zero
					if (m_index_new_old(i,1) >= m_max_alive_days){
						m_index_new_old(i,1) = 0;
					}
				}				
			}
		}
	}
	//std::cout << current_temp <<endl;
	//std::cout << m_index_new_old <<endl;
	//std::cout << m_accumu_degree_days <<endl;
}

void SubPopulation_Population_Manager::addNewDay(){
	m_index_new_old(blitz::Range::all(),0)+=1;
	//Set it to zero when they reaches the end
	for (int i=0; i < m_num_life_stage; i++){
		if (m_index_new_old(i,0) >= m_max_alive_days){
			m_index_new_old(i,0) = 0;
		}
	}
}

int SubPopulation_Population_Manager::calNextStage(int current_stage){
	int next_stage;

	if(current_stage == 2) //to adult, randomly choose female or male
	{
		next_stage = g_rand_uni()>0.5 ? current_stage+1 :current_stage+2;
	}
	else if (current_stage == 3 || current_stage ==4) // already adult, die next
	{
		next_stage = -1;
	}
	else // otherwise next life stage
	{
		next_stage = current_stage+1;
	}	
	return next_stage;
}

void SubPopulation_Population_Manager::DoFirst(){
   updateDevelopmentSeason();
   updateMortalityArray();
}

void SubPopulation_Population_Manager::setOldIndex(int life_stage, int p_value){
	//1 is the old one
	m_accumu_degree_days(life_stage, blitz::Range(m_index_new_old(life_stage, 1), p_value)) = 0;
	m_index_new_old(life_stage,1) = p_value;
}


void SubPopulation_Population_Manager::doFlying(int life_stage, int index_x, int index_y){
	
	m_current_flying_array = m_the_subpopulation_array(index_x, index_y)->getArrayForOneLifeStage(life_stage);

	//get the wind direction and speed
	int current_wind_direction = m_TheLandscape->SupplyWindDirection();
	double current_wind_speed = m_TheLandscape->SupplyWind();
	int current_wind_speed_index = m_wind_speed_lookup_table(int(current_wind_speed));
	int temp_index_mask_x = -1;
	int temp_index_mask_y = -1;
	for (int x = index_x-m_long_move_mask_x_num_half; x<=index_x+m_long_move_mask_x_num_half;x++){
		//out of range for x
		temp_index_mask_x++;
		if(x<0 || x>=m_num_x_range){
			continue;
		}
		for (int y = index_y-m_long_move_mask_y_num_half; y<=index_y+m_long_move_mask_y_num_half;y++){
			//out of range for y
			temp_index_mask_y++;
			if (y<0 || y>=m_num_y_range){
				continue;
			}

			//landing them
			if(m_long_move_mask(current_wind_speed_index, current_wind_direction, temp_index_mask_x,temp_index_mask_y)>0){
				m_current_landing_array=m_long_move_mask(current_wind_speed_index, current_wind_direction, temp_index_mask_x,temp_index_mask_y)*m_current_flying_array;
				m_the_subpopulation_array(x,y)->addAnimalNumGivenStage(life_stage, &m_current_landing_array);		
			}
		}
	}
	m_current_flying_array *= -1;
	m_the_subpopulation_array(index_x, index_y)->addAnimalNumGivenStage(life_stage, &m_current_flying_array);
}

bool SubPopulation_Population_Manager::isWinterHost(TTypesOfLandscapeElement a_ele){
	if (std::count(m_winter_hosts.begin(), m_winter_hosts.end(), a_ele)){
		return true;
	}

	else {
		return false;
	}
}

bool SubPopulation_Population_Manager::isSummerHost(TTypesOfLandscapeElement a_ele){
	if (std::count(m_summer_hosts.begin(), m_summer_hosts.end(), a_ele)){
		return true;
	}

	else {
		return false;
	}
}

void SubPopulation_Population_Manager :: updateWholePopulationArray(blitz::Array<double, 1> a_array){
	m_total_num_each_stage += a_array;
}

void SubPopulation_Population_Manager :: doLocalMovement(vector <int> life_stages, int index_x, int index_y, double proportion){
	m_short_move_mask_y_num = 0.0;
	int index_for_mask_x = -1;
	int index_for_mask_y = -1;
	for (int i = index_x-1; i<=index_y+1; i++){
		index_for_mask_x++;
		if(i<0 || i>=m_num_x_range){
			continue;
		}
		index_for_mask_y = -1;
		for (int j = index_y-1; j<=index_y+1; j++){
			index_for_mask_y++;
			if(j<0 || j>=m_num_y_range){
				continue;
			}
			else{
				m_short_move_mask(index_for_mask_x, index_for_mask_y) = m_cell_popu_density(i,j);
			}
		}
	}
}

void SubPopulation_Population_Manager :: updateMortalityArray(){
	//get the current temperature
	double current_temperature = m_TheLandscape->SupplyTemp();
	for (int i=0; i<m_num_life_stage; i++){
		if (current_temperature < m_optimal_temperature(i)){
			m_current_mortality_array(i, blitz::Range::all()) = current_temperature/(m_lowest_temperature_die(i)-m_optimal_temperature(i))-m_optimal_temperature(i)/(m_lowest_temperature_die(i)-m_optimal_temperature(i));
		}
		else{
			m_current_mortality_array(i, blitz::Range::all()) = current_temperature/(m_highest_temperature_die(i)-m_optimal_temperature(i))-m_optimal_temperature(i)/(m_highest_temperature_die(i)-m_optimal_temperature(i));
		}		
	}
}

double SubPopulation_Population_Manager :: calLandingCurve(double a_dis, double a_peak, double a_furthest){
	if(a_dis<=a_peak && a_dis>0){
		return -1*a_dis*a_dis + 2*a_peak;
	}
	if(a_dis>a_peak && a_dis <= a_furthest){
		return a_peak*a_peak/(pow(a_furthest-a_peak, 2)*pow(a_furthest-a_dis, 2));
	}
	return 0.0;
}

void SubPopulation_Population_Manager :: initialisePopulation(){
	//this is one a general subpopulation species
	m_ListNames[0]="Egg";
	m_ListNames[1]="Larva";
	m_ListNames[2]="Pupa";
	m_ListNames[3]="Female";
	m_ListNames[4]="Male";
	m_SimulationName = "General Subpopulation Simulation";

	//suppose we only have one type of host
	m_summer_hosts.push_back(tole_Field);
	//we add 1000 eggs to the grid when it is a suitable one
	int index_cell_i = 0;
	int index_cell_j = 0;
	struct_SubPopulation* sp;
	sp = new struct_SubPopulation;
	sp->NPM = this;
	sp->L = m_TheLandscape;
	//creat the aphid in each cell
	for (int i=0; i<SimW; i=i+m_sub_w)
	{
		index_cell_j = 0;
		for(int j=0; j<SimH; j=j+m_sub_h)
		{
			sp->x = i;
			sp->y = j;
			sp->w = m_sub_w;
			sp->h = m_sub_h;
			sp->NPM = this;
			sp->empty_flag = false;
			sp->index_x = index_cell_i;
			sp->index_y = index_cell_j;
			TTypesOfLandscapeElement current_landtype = m_TheLandscape->SupplyElementType(i, j);
			sp->starting_popu_density = &(m_cell_popu_density(index_cell_i, index_cell_j));
			sp->starting_suitability = &(m_cell_suitability(index_cell_i, index_cell_j));
			
			double current_biomass = m_TheLandscape->SupplyVegBiomass(i, j);

			//for the default subpopulation species, we only have one host
			bool current_winter_host_flag = isSummerHost(current_landtype);
			if(current_winter_host_flag>0){
				//sp->starting_suitability = 1;
				m_the_subpopulation_array(index_cell_i, index_cell_j) = CreateObjects(NULL, sp, 1000);
				m_cell_suitability(index_cell_i, index_cell_j) = 1;
			}
			else{
				sp->empty_flag = true;
				//sp->starting_suitability = 0;
				m_the_subpopulation_array(index_cell_i, index_cell_j) = CreateObjects(NULL, sp, 0);
				m_cell_suitability(index_cell_i, index_cell_j) = 0;
			}			
			index_cell_j++;
		}
		index_cell_i++;
	}
	delete sp;
}

SubPopulation* SubPopulation_Population_Manager::CreateObjects(TAnimal *pvo, struct_SubPopulation* data, int number){
   SubPopulation*  new_Sub;
   new_Sub = new SubPopulation(data->x, data->y, data->L, data->NPM, data->empty_flag, data->index_x, data->index_y, data->starting_suitability, data->starting_popu_density, number);
   // always 0 since it is a subpopulation model

   //the simulation starts from Jan, we only have some eggs from the beginning.
   if(number > 0){
	//updateWholePopulationArray(toa_Egg, number);
	new_Sub -> addAnimalNumGivenStageColumn(tos_Egg, 0, number);
	}
   return new_Sub;
}


int SubPopulation_Population_Manager::supplyAgeInDay(int lifestage_index, int column_index){
	int current_newest_index = supplyNewestIndex(lifestage_index);
	if(current_newest_index>=column_index){
		return current_newest_index-column_index;
	}
	else{
		return m_max_alive_days-column_index+current_newest_index;
	}
}