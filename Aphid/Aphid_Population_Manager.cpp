/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file SubPopulationPopulationManager.cpp
\brief <B>The main source code for subpopulation population manager class</B>
*/
/**  \file SubPopulationPopulationManager.cpp
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------

#include <string.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include <blitz/array.h>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../SubPopulation/SubPopulation.h"
#include "../SubPopulation/SubPopulation_Population_Manager.h"
#include "../Aphid/Aphid.h"
#include "../Aphid/Aphid_Population_Manager.h"

using namespace std;
using namespace blitz;

CfgStr cfg_AphidDevelopmentFile("APHID_DEVELOPMENT_FILE", CFG_CUSTOM, "Subpopulation/aphid_info.txt");
static CfgInt cfg_AphidEggStartingNum("APHID_EGG_STARINGNUM", CFG_CUSTOM, 1000);
/** \brief The longest distance that an aphid with wings can fly. */
CfgFloat cfg_AphidMaxShortDistance("APHID_MAX_SHORT_DISTANCE", CFG_CUSTOM, 1000);
/** \brief The longest distance that an aphid WITHOUT wings can fly. (meters)*/
CfgFloat cfg_AphidMaxLongDistance("APHID_MAX_LONG_DISTANCE", CFG_CUSTOM, 1000);
/** \brief The distance for the peak amount when the winged adults fly. (meters) */
CfgFloat cfg_AphidPeakLongDistance("APHID_PEAK_LONG_DISTANCE", CFG_CUSTOM, 100);
/** \brief The scale for the wind speed. */
CfgFloat cfg_AphidScaleWindSpeed("APHID_SCALE_WIND_SPEED", CFG_CUSTOM, 1.01);
/** \brief The maximum speed that allows an aphid to fly. */
CfgFloat cfg_AphidMaxWindSpeed("APHID_MAX_WIND_SPEED", CFG_CUSTOM, 10);
/** \brief The number of wind directions used to calculate the movement masks. */
CfgFloat cfg_AphidMovMaskWindDirectionNum("APHID_MOV_MASK_WIND_DIRECTION_NUM", CFG_CUSTOM, 8);
/** \brief The sampling number of wind speed used to calculate the movement masks. */
CfgFloat cfg_AphidMovMaskWindSpeedStepSize("APHID_MOV_MASK_WIND_SPEED_STEP_SIZE", CFG_CUSTOM, 2);
/** \brief The maximum days for all the lifestages. */
CfgInt cfg_AphidMaxAliveDay("APHID_MAX_ALIVE_DAY", CFG_CUSTOM, 200);
/** \brief The number of life stages for the aphids. */
CfgInt cfg_AphidLifeStageNum("APHID_LIFE_STAGE_NUM", CFG_CUSTOM, 7);
/** \brief The width of the cell on the landscape for the aphids. -x */
CfgInt cfg_AphidCellWidth("APHID_CELL_WIDTH", CFG_CUSTOM, 100);
/** \brief The height of the cell on the landscape for the aphids. -y*/
CfgInt cfg_AphidCellHeight("APHID_CELL_HEIGHT", CFG_CUSTOM, 100);
/** \brief This variable decdices which aphid species to run. */
CfgStr cfg_AphidSpeciesName("APHID_SPECIES_NAME", CFG_CUSTOM, "Aphis fabae");


//---------------------------------------------------------------------------

Aphid_Population_Manager::Aphid_Population_Manager(Landscape* L, int a_sub_w, int a_sub_h, int a_num_life_stage) : SubPopulation_Population_Manager(L, cfg_AphidDevelopmentFile.value(), cfg_AphidCellWidth.value(), cfg_AphidCellHeight.value(), a_num_life_stage, cfg_AphidMaxLongDistance.value(), cfg_AphidPeakLongDistance.value(), cfg_AphidScaleWindSpeed.value(), cfg_AphidMaxWindSpeed.value(), cfg_AphidMovMaskWindDirectionNum.value(), cfg_AphidMovMaskWindSpeedStepSize.value(), cfg_AphidMaxAliveDay.value())
{
	m_ListNames[0]="Egg";
	m_ListNames[1]="Nymph";
	m_ListNames[2]="Aptera";
	m_ListNames[3]="Alate";
	m_ListNames[4]="Winged Male";
	m_ListNames[5]="Winged Female";
	m_ListNames[6]="Unwinged Female";
	m_ListNameLength = 7;
    m_SimulationName = "Aphid Simulation";

	//initialise the winter and summer hosts vector, we need to turn these into a configuration variable
	m_winter_hosts.push_back(tole_RiversideTrees);
	m_winter_hosts.push_back(tole_DeciduousForest);
	m_winter_hosts.push_back(tole_MixedForest);
	m_winter_hosts.push_back(tole_ConiferousForest);
	m_winter_hosts.push_back(tole_YoungForest);

	//summer hosts
	m_summer_hosts.push_back(tole_Field);
	

	int index_cell_i = 0;
	int index_cell_j = 0;
	struct_Aphid* sp;
	sp = new struct_Aphid;
	sp->NPM = this;
	sp->L = m_TheLandscape;
	//creat the aphid in each cell
	for (int i=0; i<SimW; i=i+m_sub_w)
	{
		index_cell_j = 0;
		for(int j=0; j<SimH; j=j+m_sub_h)
		{
			sp->x = i;
			sp->y = j;
			sp->w = m_sub_w;
			sp->h = m_sub_h;
			sp->NPM = this;
			sp->empty_flag = false;
			sp->index_x = index_cell_i;
			sp->index_y = index_cell_j;
			TTypesOfLandscapeElement current_landtype = m_TheLandscape->SupplyElementType(i, j);
			sp->starting_popu_density = &(m_cell_popu_density(index_cell_i, index_cell_j));
			sp->starting_suitability = &(m_cell_suitability(index_cell_i, index_cell_j));
			
			double current_biomass = m_TheLandscape->SupplyVegBiomass(i, j);

			//check whether it is a winter host. If yes, initialise with some eggs
			bool current_winter_host_flag = isWinterHost(current_landtype);
			if(current_winter_host_flag>0){
				//sp->starting_suitability = 1;
				m_the_subpopulation_array(index_cell_i, index_cell_j) = CreateObjects(NULL, sp, cfg_AphidEggStartingNum.value());
				m_cell_suitability(index_cell_i, index_cell_j) = 1;
			}
			else{
				sp->empty_flag = true;
				//sp->starting_suitability = 0;
				m_the_subpopulation_array(index_cell_i, index_cell_j) = CreateObjects(NULL, sp, 0);
				m_cell_suitability(index_cell_i, index_cell_j) = 0;
			}
				
			
			index_cell_j++;
		}
		index_cell_i++;
	}
	m_current_developtype = toAphidHibernate;
	delete sp;
}

Aphid* Aphid_Population_Manager::CreateObjects(TAnimal *pvo, struct_Aphid* data, int number){
   Aphid*  new_Aphid;
   new_Aphid = new Aphid(data->x, data->y, data->w, data->h, data->L, data->NPM, data->empty_flag, data->starting_suitability, number, data->index_x, data->index_y);
   // always 0 since it is a subpopulation model

   //the simulation starts from Jan, we only have some eggs from the beginning.
   if(number > 0){
	//updateWholePopulationArray(toa_Egg, number);
	new_Aphid -> addAnimalNumGivenStageColumn(toa_Egg, 0, number);
	}
   return new_Aphid;
}

int Aphid_Population_Manager::calNextStage(int current_stage){
	//Spring
	if(m_current_developtype == toAphidSpringDev){
		if (current_stage == toa_Egg){
			return toa_Aptera;
		}
	}

	if(current_stage == toa_Aptera || current_stage == toa_Alate || current_stage == toa_UnwingedFemale || current_stage == toa_WingedMale || current_stage == toa_WingedFemale){
		return -1; // let them die
	}
	
	if(current_stage == toa_Nymph){
		if (m_current_developtype == toAphidSpringDev || m_current_developtype == toAphidLateSpringDev){
			return toa_Alate;
		}

		if (m_current_developtype == toAphidSummerDev){
			return toa_Aptera;
		}

		if (m_current_developtype == toAphidFallDev){
			return toa_WingedFemale;
		}
	}
}

void Aphid_Population_Manager::updateDevelopmentSeason(){
	//For now, it is only time dependent, more are needed to add here
	//Early spring eggs hatch
	if(m_TheLandscape->SupplyMonth()==4 && m_current_developtype != toAphidSpringDev){
		m_current_developtype = toAphidSpringDev;
		m_hibernated_hatch_flag = true;
	}

	//Late spring
	if(m_TheLandscape->SupplyMonth()==5 && m_current_developtype != toAphidLateSpringDev){
		m_current_developtype = toAphidLateSpringDev;
	}

	//summer
	if(m_TheLandscape->SupplyMonth()==6 && m_current_developtype != toAphidSummerDev){
		m_current_developtype = toAphidSummerDev;
	}

	//autumn
	if(m_TheLandscape->SupplyMonth()==9 && m_current_developtype != toAphidFallDev){
		m_current_developtype = toAphidFallDev;
	}

	//winter
	if(m_TheLandscape->SupplyMonth()==12 && m_current_developtype != toAphidHibernate){
		m_current_developtype = toAphidHibernate;
	}

}