/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Aphid_Population_Manager.h 
\brief <B>The main header code for Aphid population manager</B>
*/
/**  \file Aphid_Population_Manager.h
Version of  Feb. 2021 \n
By Xiaodong Duan \n \n
*/

//---------------------------------------------------------------------------
#ifndef Aphid_Population_ManagerH
#define Aphid_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


class Aphid;
class Aphid_Population_Manager;

/**
\brief
Used for creation of a new Aphid object
*/
class struct_Aphid
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief area width */
  int w;
  /** \brief area height */
  int h;
  /** \brief species ID */
  int species;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief Aphid_Population_Manager pointer */
  Aphid_Population_Manager * NPM;
  /** \brief Indicator show whether it is an empty subpopulation. */
  bool empty_flag;
  /** \brief Starting suitability for the subpopulation. */
  double* starting_suitability;
  /** \brief Starting weighted population density for the subpopulation. */
  double* starting_popu_density;
  int index_x;
  int index_y;
};

enum TTypesOfAphidDevelopmentSeason: unsigned
{
  toAphidSpringDev=0,
  toAphidLateSpringDev,
  toAphidSummerDev,
  toAphidFallDev,
  toAphidHibernate
};

/**
\brief
The class to handle all Aphid population related matters in the whole landscape
*/
class Aphid_Population_Manager : public SubPopulation_Population_Manager
{
   /** \brief The variable to show the development season. */
   TTypesOfAphidDevelopmentSeason m_current_developtype;
   
public:
   Aphid_Population_Manager(Landscape* L, int a_sub_w=10, int a_sub_h=10, int a_num_life_stage=7);
   virtual ~Aphid_Population_Manager(){};
   Aphid* CreateObjects(TAnimal *pvo, struct_Aphid* data, int number);
   virtual int calNextStage(int current_stage);
   /** \brief The function to update the development season. */
   virtual void updateDevelopmentSeason();
   /** \brief The function to supply the development season. */
   virtual unsigned supplyDevelopmentSeason () {return m_current_developtype;}
};

#endif
