/*
*******************************************************************************************************
Copyright (c) 2021, Xiaodong Duan, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Aphid.h
\brief <B>The main header code for Aphid class.</B>
*/
/**  \file Aphid.h
Version of  Feb. 2021 \n
By Xiaodong \n \n
*/

//---------------------------------------------------------------------------
#ifndef AphidH
#define AphidH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class Aphid_Population_Manager;
//------------------------------------------------------------------------------

/**
 * \brief
Enumerator for aphid object types
*/
 enum Aphid_Object : int{
   toa_Egg=0,
   toa_Nymph,
	toa_Aptera,
	toa_Alate,
   toa_WingedMale,
   toa_WingedFemale,
   toa_UnwingedFemale,
	toa_count
} ;

/**
\brief
The class for base Aphid using subpopulation method.
*/
class Aphid : public SubPopulation
{
private:
   /** \brief Variable to hold the plant juice in the cell. */
   double m_plant_juice;
public:
   /** \brief Aphid constructor */
   Aphid(int p_x, int p_y, int p_w, int p_h, Landscape* p_L, Aphid_Population_Manager* p_NPM, bool a_empty_flag, double* p_suitability, int number, int a_index_x, int a_index_y, int a_SpeciesID=919);
   
   virtual void calPopuDensity(void);
   virtual void calSuitability(void);
   virtual void doReproduction(void);
   virtual void doMovement(void);
   virtual void doDropingWings(void);
};

#endif