// ----------------------------------------------------------------------------
// private classes
// ----------------------------------------------------------------------------

class ALMaSSGUI;
class Landscape;
class Population_Manager;
class TPredator_Population_Manager;

#include "wx/event.h"

// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
public:
    // override base class virtuals
    // ----------------------------

    // this one is called on application startup and is a good place for the app
    // initialization (doing it here and not in the ctor allows to have an error
    // return: if OnInit() returns false, the application terminates)
   virtual bool OnInit();
   virtual int OnRun();
   ALMaSSGUI *m_frame;
};

class MyMap: public wxPanel
{
public:
    //MyMap(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style =wxTAB_TRAVERSAL, const wxString& name = "panel1");
    MyMap(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style =wxTAB_TRAVERSAL);
	~MyMap();
    ALMaSSGUI* m_MainForm;
    wxImage* wxIm;
    Landscape* m_ALandscape;
    unsigned char* idata;
    bool m_TrackOn;
    float m_scalingW, m_scalingH;
    int m_TLx,m_TLy,m_BRx,m_BRy,m_Width,m_Height,m_Zoom;
    int m_SimW;
    int m_SimH;
	double m_ddegs;
// Methods
    void DrawLandscape();
    void SetLandscape(Landscape* l) {m_ALandscape=l;}
    void SetScale(int Width, int Height);
    void SetSimExtent(int Width, int Height);
    void SetUpColours();
    void SetUpMapImage();
    void ShowBitmap(wxPaintEvent& event);
    void ShowBitmap2();
    void LeftMouseDown(wxMouseEvent& event);
    void RightMouseDown(wxMouseEvent& event);
    void ChangeZoom(int x, int y);
    void Centre(int x, int y);
    void ZoomIn(int x, int y);
	void ZoomOut(int x, int y);
	void FindAnimal(int x, int y);
	void DisplayMapInfo(int x, int y);
    void Spot(int cref, int x, int y);
	DECLARE_EVENT_TABLE()
};

// Define a new frame type: this is going to be our main frame
class ALMaSSGUI: public wxFrame
{
public:
    /// Constructors
    ALMaSSGUI(const wxString& title, const wxPoint& pos, const wxSize& size );
    ~ALMaSSGUI( );

    /// Creation
    bool Create();
    /// Creates the controls and sizers
    void CreateControls();
	bool ImmediateRun(int a_species);

////@begin ALMaSSGUI event handler declarations

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for ID_BUTTON
    void OnButton0Click( wxCommandEvent& event );
    void OnButton1Click( wxCommandEvent& event );
    void OnButton2Click( wxCommandEvent& event );
    void OnButton3Click( wxCommandEvent& event );
	void ChooseDisplay( wxCommandEvent&  );

////@end ALMaSSGUI event handler declarations

////@begin ALMaSSGUI member function declarations

////@end ALMaSSGUI member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ALMaSSGUI member variables
////@end ALMaSSGUI member variables
protected:
  // Controls
    wxButton* m_PauseButton;
    wxButton* m_HaltButton;
    //wxButton* m_BatchButton;
    wxButton* m_StartButton;
    wxSpinCtrl* item7;
    wxChoice* item8;
    ALMaSSGUI* item1;
    wxStaticBitmap * item11;
    wxListBox* m_ListPad;
    wxTextCtrl* m_DateBox;
    wxChoice* item9;
    wxListBox* m_ProbesBox;
    //wxTextCtrl* m_BatchFileEdit;
    //wxTextCtrl* m_IniFileEdit;
    wxCheckBox* m_CheckBox1;
    wxCheckBox* m_CheckBox2;
    wxCheckBox* m_CheckBox3;
    wxCheckBox* m_CheckBox4;
    wxCheckBox* m_CheckBox5;
    wxCheckBox* m_CheckBox6;
    wxCheckBox* m_CheckBox7;
    wxCheckBox* m_CheckBox8;
    wxCheckBox* m_CheckBox9;
    wxCheckBox* m_CheckBox10;
    wxCheckBox* m_CheckBox11;
    wxCheckBox* m_CheckBox12;
public:
    MyMap* m_aMap;
	wxChoice*  item15;
	wxSpinCtrl* m_subselectionspin;

    char ColoursRed[100];
    char ColoursGreen[100];
    char ColoursBlue[100];
    // Animal Population Stuff
    Population_Manager *m_AManager;
	Hunter_Population_Manager* m_Hunter_Population_Manager;
    TPredator_Population_Manager *m_PredatorManager;
	bool GetSimInitiated() { return m_SimInitiated; }
	bool IsAnimalChecked(int a_index);
protected:
  // Attributes
    wxString m_bmpout;
    bool m_Paused;
    bool m_StopSignal;
    int m_torun;
    bool m_SimInitiated;
    char* m_files[100];
    char* m_Predfiles[100];
    char m_ResultsDir[255];
    char m_PredResultsDir[255];
    int m_time;
    int m_Year;
    int m_Steps;
    int m_NoProbes;
    bool m_ProbesSet;
    int m_NoPredProbes;
    Landscape* m_ALandscape;
    bool m_BatchRun;
  // Methods
    void DataInit( );
    void Go();
    int GetTimeToRun();
    void RunTheSim();
    bool ParseBatchLine( char* a_line );
    //void RunBatch1Click();
    void CloseDownSim();
    bool ReadBatchINI();
    void GetProbeInput_ini();
    bool CreatePopulationManager();
    void CreateLandscape();
	bool DumpMap(int day);
    //void SpeciesSpecificReporting();
    //void ProbeReport(int Time);
	//void ImpactProbeReport( int m_time );
    //void ProbeReportNow( int Time );
    //void ProbeReport135(int Time);
    //bool BeginningOfMonth();
    //void PredProbeReportDay0(int Time);
    void SetUpForm();
    void ShowAnimals();
public:
  // Methods
	void UpdateGraphics();
    void ClearPad() {m_ListPad->Clear();}
    void AddToPad(wxString str) {m_ListPad->Append(str);}
  // Attributes
     wxBitmap *item11Bitmap;
   
     DECLARE_EVENT_TABLE() 
};

////@begin control identifiers
 enum {
	ID_DIALOG,
	ID_BUTTON,
	ID_BUTTON1,
	ID_BUTTON2,
	ID_SPINCTRL,
	ID_CHOICE,
	ID_CHOICE2,
	ID_CHOICE3,
	ID_PANEL,
	ID_LISTBOX,
	ID_TEXTCTRL,
	ID_BUTTON3,
	ID_TEXTCTRL1,
	ID_LISTBOX1,
	ID_CHECKBOX,
	ID_CHECKBOX1,
	ID_CHECKBOX2,
	ID_CHECKBOX3,
	ID_CHECKBOX4,
	ID_CHECKBOX5,
	ID_SPINCTRL2
 };
 ////@end control identifiers
 
