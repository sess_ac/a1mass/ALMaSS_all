//
// roe_male.cpp
//

#include <iostream>
#include <fstream>

#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"

#include "Roe_all.h"
#include "Roe_constants.h"

extern int FastModulus( int a_val, int a_modulus );



//---------------------------------------------------------------------------
//                              Roe_Male
//---------------------------------------------------------------------------

/**
Roe_Male::MOnMature - Checks if area is good enough to establish range. The function ScanGrid()
adds the quality values stored in m_grid starting with a minimum area of
400 x 400 m. If not sufficient it extends outwards in all directions
until max range is reached (800 x 800 m). It also checks if amount of other males nearby is below
a male density threshold. It returns the size of the resulting area if good enough and 0 if not 
good enough
Calls ScanGrid(), SupplyMaleRC().
*/
TRoeDeerStates Roe_Male::MOnMature()
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MOnMature(): Deadcode warning!","");
    exit( 1 );
  }
  #endif

    //Check whether this area is good enough. The function ScanGrid()
    //adds the quality values stored in m_grid starting with a minimum area of
    //400 x 400 m. If not sufficient it extends outwards in all directions
    //until max range is reached (800 x 800 m). It returns the size of the
    //resulting area if good enough and 0 if not good enough

      int range = m_OurPopulation->ScanGrid(m_Location_x,m_Location_y,false);
      if (range > 0) //good enough
      {
        //how many other males already in area?
        ListOfMales* males= m_OurPopulation->
                                SupplyMaleRC(m_Location_x,m_Location_y,range);
        if(males->size() < MaleDensityThreshold)  //OK
        {
          m_SearchRange = range;
          delete males;
          return rds_MEstablishRange; // establish range
        }
        delete males;
      }
      return rds_MDisperse;  //no options in this area, so disperse
}
//----------------------------------------------------------------------------
/**
Roe_Male::MEstablishRange - sets up range for male object. Contrary to the females, 
males does not just set up range in their mothers�territory. They have to position themselves
according to the distribution of male territories in the area. Two strategies: 1)satellite and 
2)periphere. Males heavier than average for their age class choose satellite and set rangecentre=
oldest male in area. Others choose periphere and avoid other males. Sets range center.
Returns state feed. Calls functions SupplyTMales(), SupplyInfo(), AddSatellite()
*/
TRoeDeerStates Roe_Male::MEstablishRange()   
{
  #ifdef JUJ__Debug3
  if(IsAlive()!=0x0DEADC0DE)
  {
    m_OurLandscape ->Warn("Roe_Male::MEstablishRange(): Deadcode warning!","");
    exit( 1 );
  }
  #endif
  //contrary to the females, males does not just set up range in their mothers
  //territory. They have to position themselves according to the distribution of male
  //territories in the area. Two strategies: 1)satellite and 2)periphere. Males
  //heavier than average for their ageclass choose satellite and set rangecentre=
  //oldest male in area. Others choose periphere and avoid other males
  // 

  //Get list of TMales in area
  ListOfMales* males = m_OurPopulation->SupplyTMales(m_Location_x,m_Location_y,1000);
  if((males->size()>0)&&(m_Size > m_OurPopulation->m_AverageMaleSize)) // Checks to see if male is above or below average current size
  { //use satellite strategy
    //Choose the oldest male on list
    int oldest=0;
    int found = -1;
    for (unsigned i = 0; i < males->size(); i++)
    {
        RoeDeerInfo info = (*males)[i]->SupplyInfo();
        if (info.m_Age > oldest)
        {
            oldest = info.m_Age;
            m_RangeCentre = info.m_Range;  //set your rangecentre=his
            found = i;
        }
    }
  }
  else  //Either a periphere male or no males in area. Establish range on spot
  {
    m_RangeCentre.m_x=m_Location_x;
    m_RangeCentre.m_y=m_Location_y;
  }
  m_HaveRange=true;
  m_Disperse=false;
  m_float=false;
  m_DispCount=0;  //end of dipersal
  delete males;
  return rds_MFeed;
}
//---------------------------------------------------------------------------
/**
Roe_Male::MDisperse - models dispersal state of male objects. Removes satellite males, sets state to 
dispersal. It then keeps track of amount of time in the dispersal
state and calculates probability of switching to feeding, keep dispersing, or dying if spending too long
in dispersal state.Calls functions RemoveSatellite(), DirectionTo(), NextStep().
*/
TRoeDeerStates Roe_Male::MDisperse (void)
{
  m_DispCount++;
  m_HaveRange=false;  //a dispersing animal cannot be a rangeholder per def.
  m_Status=rdstatus_satellite;
  m_Disperse=true;  //indicates that dispersal has started
  //allow buck the opportunity to return to this state after disturbance
  m_LastState=rds_MDisperse;
  if(m_DispCount==1)  //first timestep in disperse
  {
    //store range centre coordinates while dispersing
    m_OldRange.m_x=m_RangeCentre.m_x;
    m_OldRange.m_y=m_RangeCentre.m_y;
  }
  m_RangeCentre.m_x=m_Location_x; //these are drifting depending on bucks position
  m_RangeCentre.m_y=m_Location_y;
  //int dist=DistanceTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
  int dir= DirectionTo(m_Location_x,m_Location_y,m_OldRange.m_x,m_OldRange.m_y);
  int rand=0;
  if(m_DispCount>70)
  {
    for(int i=0;i<3;i++)
    {
        if (NextStep(0, 0, 0) == -1)
        {
#ifdef __DEBUG_CJT1
            m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_road);
#endif
            return rds_MDie;
        }
    }
  }
  else
  {
    for(int i=0;i<3;i++)
    {
        if (NextStep(1, ((dir + 4) & 0x07), 0) == -1)
        {
#ifdef __DEBUG_CJT1
            m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_road);
#endif
            return rds_MDie;
        }
    }
  }

  switch (m_DispCount)
  {
    case 40:
      rand=40;
      break;
    case 50:
      rand=60;
      break;
    case 60:
      rand=80;
      break;
    default:
      if(m_DispCount>=70) rand=90;
      break;
  }

  if (rand>random(100))
  {
    return rds_MFeed;
  }
  else
  {
      return rds_MDisperse;
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::MFeed - Models feeding of male object, including energy budget and search for better feeding 
area. Also determines need to ruminate. In theory a roe deer can spend time 
in more than 1 type of habitat within 1 timestep. For the energy budget only the habitat 
type where roe deer is at the beginning of each time step is recorded. Need to keep counter of minutes
in feed since the end of last ruminating period (m_FeedCount) to know when to return to ruminate. Returns values
for dispersal, ruminate or keep feeding (or die).
Calls function NutriValue()
*/
TRoeDeerStates Roe_Male::MFeed(void)
{
    //In theory a roe can spend time in more than 1 type of habitat within 1 timestep.
    //For the energy budget only the habitat type where roe is at the beginning of
    //each time step is recorded. Need to keep counter of minutes
    //in feed since the end of last ruminating period (m_FeedCount) to know when to
    //return to ruminate
    m_FeedCount++;   //add 1 to count for this feeding bout
    //allow roe the opportunity to return to this state after disturbance
    m_LastState = rds_MFeed;  //feed
    //Find the nutritional value here
    int nutri = NutriValue(m_Location_x, m_Location_y);  //possible energy gain per minut
    if (nutri < MinNutri)   //not good enough for feeding
    { //search for better place
        SeekNutri(MinNutri);
        Feeding(m_Disperse);
    }
    else  //can feed here
    {
        m_EnergyGained += nutri;   //add this to total count for today
        Feeding(m_Disperse);
    }
    if (m_FeedCount > m_LengthFeedPeriod)   //need to ruminate
    {
        //for how long do you need to ruminate?
        int month = m_OurLandscape->SupplyMonth();
        m_LengthRuminate = (int)(m_FeedCount * IntakeRate[month - 1] * 0.1);
        m_FeedCount = 0;   //counter to zero
        return rds_MRuminate;  //ruminate
    }
    else if (m_Disperse == true) //a disperser, go back to disperse with 50:50 chance
    //"Cheap" solution. No basis for setting this value. Return to this later....
    {
        int rand = random(100);
        if (rand < 50)
        {
            m_FeedCount = 0;
            return rds_MDisperse; //Disperse
        }
    }
    return rds_MFeed;  //stay in feed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MEstablishTerritory - establishment of territory by male object. If male object is satellite male,
the male challenges his host and adds him to fightlist. If Male is stronger it takes over the territory, otherwise
no territory. If not a satellite male check all nearby males and establish rank by fighting them. If
win take over their territory. Always returns state feeding.
Calls AddToFightList(), On_Rank(), On_Expelled(), SupplyInfo(), SupplyTMales(), 
*/

TRoeDeerStates Roe_Male::MEstablishTerritory(void)
{
    bool result = false;
    RoeMaleFights aFight;
        ListOfMales* TMales = m_OurPopulation->SupplyTMales(m_Location_x, m_Location_y, 2 * m_SearchRange); // Only gets a list of territorial males, not satellites
        if (TMales->size() > 0)
        {
            for (unsigned i = 0; i < TMales->size(); i++) //for every male in area
            {
                if (!FightResult((*TMales)[i], aFight))
                {
                    aFight.m_Result = (*TMales)[i]->On_Rank(this, double(m_Size), m_Age, m_NoOfMatings, false);
                    aFight.m_Opponent = (*TMales)[i];
                    aFight.m_AsTerritoryOwner = false;
                    m_MyFightList.push_back(aFight);
                    if (FightMortality(aFight.m_Result))
                    {
#ifdef __DEBUG_CJT1
                        m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_fight);
#endif
                        return rds_MDie;
                    }
                }
                //m_MinInState[reu_EstablRangeFight]++;  //to place cost on male for 1 fight
                if (aFight.m_Result)   //this male won the fight
                {
                    RoeDeerInfo range = (*TMales)[i]->SupplyInfo();
                    m_Status = rdstatus_territorial;
                    m_RangeCentre = range.m_Range;  //set your rangecentre=his
                    //tell that male that he's been expelled from his territory 
                    (*TMales)[i]->On_Expelled(this);
                    m_Disperse = false;
                    m_DispCount = 0;
                    break;
                }
            } //all males checked
        }
        else  //list must have been empty
        {
            m_Status = rdstatus_territorial;
            m_RangeCentre.m_x = m_Location_x;
            m_RangeCentre.m_y = m_Location_y;
            m_Disperse = false;
            m_DispCount = 0;
        }
        delete TMales;
    return rds_MFeed;//Feed
}
//---------------------------------------------------------------------------

/**
Roe_Male::MRuminate - Function for ruminating. Checks for got spot to rest and amount of time 
spent ruminating. Ruminate activity cannot be seperated from other kinds of inactivity. 
It is also assumed that ruminating takes place while recovering after disturbance. 
Need to count amount of time spend in this state during this period of ruminating and in total last 24 hrs. 
Returns values for either stay ruminating, feed or dispersal (or die).Calls function Cover().
*/
TRoeDeerStates Roe_Male::MRuminate(void)
{
 //ruminate activity cannot be seperated from other kinds of inactivity.
  //It is also assumed that ruminating takes place while recovering after
  //disturbance. Need to count amount of time spend in this state during
  //this period of ruminating and in total last 24 hrs
  int min;
  //First check if this is first timestep in this state
  //allow roe the opportunity to return to this state after disturbance
  m_LastState=rds_MRuminate;
  if (m_RumiCount==0)   //first step so make sure this is a good spot to rest
  {
    int cover=Cover(m_Location_x,m_Location_y);
     //get month
    int month=m_OurLandscape->SupplyMonth();
    if((month>=5)&&(month<=9)) min=CoverThreshold2; //summer
    else min=CoverThreshold1; //winter
    if (cover<min)// habitat unsuitable or cover very poor, so look for better spot
    {
       SeekCover(2);
       if (m_IsDead)
       {
#ifdef __DEBUG_CJT1
           m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_seekcover);
#endif
           return rds_MDie;  //Die
       }
       else
       {
         m_RumiCount++;
         return rds_MRuminate;
       }
    }
    else //cover OK
    {
      m_RumiCount++;       //add 1 to counter
      return rds_MRuminate;    //return to this state next time step
    }
  }
  else if(m_RumiCount<m_LengthRuminate)
  {
    m_RumiCount++;  //add to counter
    return rds_MRuminate;     //stay
  }
  else
  {
    if(m_Disperse==true)  return rds_MDisperse;
    else  //not disperser
    {
       if (m_RumiCount>=m_LengthRuminate)
       {
         m_RumiCount=0;
         return rds_MFeed;   //feed
       }
       else
       {
         int rand = random(100);
         if(rand<50)
         {
           m_RumiCount++;
           return rds_MRuminate;  //stay
         }
         else
         {
           m_RumiCount=0;
           return rds_MFeed;
         }
       }
    }
  }
}
//----------------------------------------------------------------------------

/**
Roe_Male::MRecover - Governs recovery after running from danger. Function counts 
amount of time in state during the present period of recovovery (m_RecovCount) and 
sets this to 0 when state is left. Female only transfers to this state if safe, whichs means
in good habitat. Returns states: feeding, recovery. 

*/
TRoeDeerStates Roe_Male::MRecover (void)
{
  //Need to count amount of time in state during the present
  //period of recov. (m_RecovCount) and set this to 0 when state is left.
  //Roe only transfers to this state if safe (i.e. cover>=3) so we know it is in
  //good habitat

  //allow roe the opportunity to return to this state after disturbance
  m_LastState=rds_MRecover; //recover
  if (m_RecovCount<=AverageRecoveryTime)
  {
    m_RecovCount++;   //add 1 to time spend in recover and stay here
    return rds_MRecover; //recover
  }
  else if ((m_RecovCount>AverageRecoveryTime)&&(m_RecovCount<MaxRecoveryTime))
  //return to feed with certain probability
  {
    int stop= random(100);
    if (stop<10)
    {
      m_RecovCount=0;    //leaves state, so set counter=0
      m_FeedCount=0;   //ruminating also takes place during recovery, so start
                       //from scratch in Feed
      return rds_MFeed;   //feed
    }
    else
    {
      m_RecovCount++;   //add 1 to time spend in this state
      return rds_MRecover;  //recover
    }
  }
  else   //if still around after this long return to feeding
  {
    m_RecovCount=0;
    return rds_MFeed;
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::MRun - Sets the male object to be in a running state - and checks if 
safe after 1 time step in this state.
Calls Running().
*/

TRoeDeerStates Roe_Male::MRun() //State 20
{

  bool IsSafe=Running(m_danger_x,m_danger_y);
  if (m_IsDead)
  {
#ifdef __DEBUG_CJT1
      m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_running);
#endif
      return rds_MDie; //Die
  }
  else if (!IsSafe)   //still not safe after 1 time step
  {
    return rds_MRun;
  }
  else
  {
    m_RecovCount++; //add 1 to time spend in recovery
    return rds_MRecover;   //safe
  }
}
//---------------------------------------------------------------------------

/**
Roe_Male::MEvade - checks direction to threath and takes 50 steps in opposite direction.
Returns to same state as before disturbance.
Calls DirectionTo(), NextStep()
*/
TRoeDeerStates Roe_Male::MEvade ()
{
  int weight=3;
  //get direction to threat
  int dir=DirectionTo(m_Location_x,m_Location_y,m_danger_x,m_danger_y);
  NextStep(weight,((dir+4) & 0x07),0);

  //return to same state as before disturbance
  return m_LastState;
}
//---------------------------------------------------------------------------

/**
Roe_Male::MAttendFemale - function for male attending female in heat. Checks if has mate, if not
return to feeding. If has a mate, male stays in this state (attend female) until new day. Then 
he returns to normal activity. The male will attend to the female, and follow her closely. He 
does not eat during that day and looses energy comparable to walking.
Calls SupplyPosition(), WalkTo().
*/
TRoeDeerStates Roe_Male::MAttendFemale (void)    //State 31
{
  //male stays in this state until new day. Then he returns to normal activity
  if(m_My_Mate==NULL) return rds_MFeed;
  else
  {
    //allow roe to return after disturbance
    m_LastState=rds_MAttendFemale;
    //While attending the female the male follows her closely. He does not eat
    //during that day and looses energy comparable to walking.
    //get females position
    AnimalPosition xy=m_My_Mate->SupplyPosition();
    WalkTo(xy.m_x,xy.m_y);
    return rds_MAttendFemale;  //Attendfemale
  }
}
//---------------------------------------------------------------------------
/**
Roe_Male::MOnNewDay - updates age of male object (+ 1 day), decides if life expectancy 
has been reached, which depends on age (Floaters have a higher mortality (added FloatMort)).
Checks whether dispersers and floaters can set up range by checking habitat, closeness to mothers range
and amount of males already in the area. If failed to find a range before october 1st become a floater.
Also governs establishement of territory dependent on age. Returns values to set the following states: 
establish range, establish territory, mate, update energy (and die).
Calls functions DistanceTo(), ScanGrid(), SupplyMaleRC().
*/
void Roe_Male::MOnNewDay(void)
{
    m_Age++;  //1 day older

    m_NewState = rds_MFeed; // Safe state if this is not replaced later
    int dist;
    int mort;
    //First decide if life expectancy has been reached. Depends on age. Floaters
    //have a higher mortality (added FloatMort)

    long int die = random(100000);  //since mort rates are multiplied with 100000 to
                                  //produce int values
    if (m_Age <= 730)
        mort = (int)MaleDMR[0]; //age in days
    else if (m_Age <= 2555)   //7 yrs
        mort = (int)MaleDMR[1];
    else if (m_Age <= 3650)  //10 yrs
        mort = (int)MaleDMR[2];
    else mort = (int)MaleDMR[3]; //>10 yrs, 90 % mortality

    if ((m_float) || (m_Disperse)) mort += FloatMort;
    if (die < mort)
    {
#ifdef __DEBUG_CJT1
        m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_random);
#endif
        m_NewState = rds_MDie;  //MDie
        return;
    }
    else   //still alive
    {
        // 1: DISPERSAL
        if (m_Disperse)
        {
            if (!m_float)
            {
                //get distance to mothers RangeCentre
                dist = DistanceTo(m_Location_x, m_Location_y, m_OldRange.m_x, m_OldRange.m_y);
            }
            else
            { //floaters check distance beck to last failed attempt to set up range
                dist = DistanceTo(m_Location_x, m_Location_y, m_float_x, m_float_y);
            }
            if (dist > MinDispDistance) //check if this is good spot to set up range
            {
                //both floaters and regular dispersers go in here
                bool found = false;
                int month = m_OurLandscape->SupplyMonth();
                int range = -1;
                if ((month >= 0) && (month <= 12)) // 3 & 8
                {
                    if (m_OurPopulation->SupplyIsFemale(m_Location_x, m_Location_y, MaxRange)) range = 1;
                }
                else range = m_OurPopulation->ScanGrid(m_Location_x, m_Location_y, false);
 
                if (range > 0)  //OK
                {
                    //how many other males have already their range in this area?
                    ListOfMales* males = m_OurPopulation->SupplyMaleRC(m_Location_x, m_Location_y, range);
                    if (males->size() < MaleDensityThreshold)   //OK
                    {
                        m_SearchRange = range;
                        found = true;
                        m_NewState = rds_MEstablishRange; // establish range
                    }
                    delete males;
                }
                if (!found) //quality too poor or still too close to mother, so return to disperse
                {
                    //if failed to find a range before october 1st become a floater
                    if (m_OurLandscape->SupplyDayInYear() > 270)
                    { //floaters store present position for use in next attempt to set up range
                        m_float_x = m_Location_x;
                        m_float_y = m_Location_y;
                        m_float = true;
                    }
                    m_NewState = rds_MDisperse;   //MDisperse
                }
            }
        } // end dispersal
        else
        {
            if (m_Age >= 730) //can attempt to establish territory
            {
                int month = m_OurLandscape->SupplyMonth();
                if ((month >= 0) && (month <= 12))
                {
                    //During the breeding season the male will simply move if there is not a female close enough resulting in dispersaland new range establishment
                    if (!m_OurPopulation->SupplyIsFemale(m_Location_x, m_Location_y, MaxRange))
                    {
                        m_NewState = rds_MDisperse; // MaxRange
                        return;
                    }
                }
                if ((m_Status!=rdstatus_territorial) && (m_HaveRange == true))

                {
                    m_NewState = rds_MEstablishTerritory;         //MEstablish territory
                }
            }
            if (m_My_Mate != NULL)  //is attending female
            {
                m_NewState = rds_MMate;  //mate
            }
        }
    }
    MUpdateEnergy(); //MUpdateEnergy
}
//---------------------------------------------------------------------------
/**
Roe_Male::MUpdateEnergy - Calculates how much energy is used (in cal/minute) in different activities.
Calculates net energy (gained-used) and determines whether excess enery is added to reserves or is used to
put on weight. A negative net energy causes the male object to loose weight - if roe deer weight falls
below a minimum value, the deer will die. If the roe deer is in very bad condition, forage time is increased.
returns states such as mate, feed, newState or die.

*/
void Roe_Male::MUpdateEnergy(void)  //State 5
{
    //All energetic calculations in cal/minute!
    //Calculate how much energy used in different activities
    double EnergyUsed = 0;
    int month = m_OurLandscape->SupplyMonth();
    for (int i = 0; i < 12; i++)
    {
        double cost;
        if (CostMaleAct[i] == 1)   cost = CostMaleAct[i] * MaleRMR;  //rest activity
        else cost = (double)CostMaleAct[i];
        EnergyUsed += double((cost * m_MinInState[i] * m_Size));
    }
    double result = m_EnergyGained - EnergyUsed;     //in cal
    if (result > 0)
    {
        if (m_Reserves <= 10) m_Reserves++; //add to reserves
        else
        {
            m_Size += floor(result * Anabolic_Inv);  //put on weight
        }
    }
    else if (result < 0)
    {
        if (m_Reserves > 0)  m_Reserves--;
        else
        {
            m_Size += floor(result * Catabolic_Inv);   //loose weight
        }
    }

    //Update m_Size. If m_Size < MinSize roe dies
    if ((m_Reserves == 0) && (m_Size < m_OurPopulation->m_MinWeight_Males))
    {
#ifdef __DEBUG_CJT1
        m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_starve);
#endif
        m_NewState = rds_MDie;  //MDie
        return;
    }
    else if ((m_Reserves == 0) && (m_Size < m_OurPopulation->m_CriticalWeight_Males))
    {
        //in very bad condition, need to increase forage time
        m_LengthFeedPeriod = (int)((5 * m_Size) / (double)Male_FeedBouts[month - 1]);
    }
    else
    {
        m_LengthFeedPeriod = (int)((2.3 * m_Size) / Male_FeedBouts[month - 1]);
        if (m_LengthFeedPeriod > 3) m_LengthFeedPeriod = 3;
    }
    //set counter of daily energy gain to zero
    m_EnergyGained = 0;
    for (int i = 0; i < 12; i++)
    {
        m_MinInState[i] = 0;
    }
    //if LastState was AttendFemale, go to mate
    if (m_LastState == rds_MAttendFemale)
    {
        m_NewState = rds_MMate; //Mate
    }
}

bool Roe_Male::FightResult(Roe_Male* a_male, RoeMaleFights& a_fightresult)
{
    /**
    * Searches through the fight list for a matching male and returns the fight result if found by changing a_fightresult. If not found, returns false
    */
    for (vector<RoeMaleFights>::iterator it = m_MyFightList.begin(); it != m_MyFightList.end(); ++it)
    {
        if ((*it).m_Opponent == a_male)
        {
            a_fightresult = (*it);
            return true;
        }
    }
    return false;
}

inline bool Roe_Male::FightMortality(bool a_win_loss)
{
    /** a_win_loss is true for a win */
    if (a_win_loss) {
        if (g_rand_uni() < FightMortalityWinLoss[0]) return true; else return false; // FightMortalityWinLoss[0] has the probability of death if winning the fight
    }
    else if (g_rand_uni() < FightMortalityWinLoss[1]) return true; else return false; // FightMortalityWinLoss[0] has the probability of death if losing the fight
}

//---------------------------------------------------------------------------
/**
Roe_Male::MMate - add 1 to number of matings, stop attending female. Return to feed

*/
TRoeDeerStates Roe_Male::MMate (void)
{
  //add 1 to matings
  m_NoOfMatings++;
  //remove pointer to the female
  m_My_Mate=NULL;
  //last state must have been AttendFemale, so continue to Feed
  return rds_MFeed;  //MFeed
}
//---------------------------------------------------------------------------
/**
Roe_Male::MDie - function for managing death of a male object. The male object
removes itself from all fightlists and deletes its own fightlist. The fixlist of the male object
is emptied and the male furthermore adds itself to list of dead animals. 
If a territoryholder sends message to every male in the neighbourhood abput death. Messages potential mate 
and satellite males about death. Function returns dead state.
Calls functions DeleteFightList(), RemoveFromFightList(), RemoveFixList()and On_IsDead().
*/
TRoeDeerStates Roe_Male::MDie (void)
{
  m_CurrentStateNo = -1;
  
  //add yourself to list of dead animals
  m_OurPopulation->m_DeadListMales.push_back(m_Age);  //tell everybody who needs to know
  if(Mum!=NULL) //tell mum
  {
     Mum->On_IsDead(this,3,false);  //'3' indicates offspring
     Mum=NULL;  //remove pointer
  }

  if (m_My_Mate!=NULL)   //tell mate
  {
     m_My_Mate->On_IsDead(this,2,false);  //'2' indicates mate
     m_My_Mate=NULL;  //remove pointer
  }

  return rds_MDeathState; //MDeathState
}
//---------------------------------------------------------------------------

    
/**
Roe_Male::BeginStep goes through all male objects in the populations. In the territorial season, March 1st - Sept. 1st, each male will check its surroundings for other males.
Young satellite males always try to stay clear of their older hosts and check distance every timestep and evade if too close. If they notice a new territorial male they do not know they will challenge.
Peripheral males will check to see if they are able to improve status to satellites, if not will evade all of higher status.
Calls SupplyMonth(), SupplyMales(), On_Rank(), AddToFightList(), SupplyPosition(), DistanceTo().
*/
void Roe_Male::BeginStep(void)
{
    // If already dead don't waste time here
    if (CurrentRState == rds_MDeathState) return;
    if (CurrentRState != rds_MOnNewDay) { // Don't do this on the start of a new day otherwise you can skip the new day code
        if (m_OurPopulation->GetThreat(m_Location_x, m_Location_y))
        {
            On_ApproachOfDanger(m_Location_x, m_Location_y);
        }
    }
    if (timestep % 144 == 0) // Just once per day
    {

        m_NewState = CurrentRState;
        MOnNewDay();
        MUpdateEnergy();
        MFight();
        CurrentRState = m_NewState;

    }
}

void Roe_Male::MFight(void)
{
    // Check for season
    int month = m_OurLandscape->SupplyMonth();
    if ((month >= 3) && (month <= 8))
    {
        if (m_Status != rdstatus_territorial) // Only care about non-territorials here
        {
            if (m_Size > m_OurPopulation->m_AverageMaleSize) // Checks to see if male is above or below average current size, set appropriate status
            {
                m_Status = rdstatus_satellite;
            }
            else
            {
                m_Status = rdstatus_peripheral;
            }
            //get other males in area
            ListOfMales* males = m_OurPopulation->SupplyMales(m_Location_x, m_Location_y, m_SearchRange + 100);  // List of all males within 100m of range
            if (males != NULL)  //only if other males are present
            {
                RoeMaleFights aFight;
                for (unsigned i = 0; i < males->size(); i++)
                {
                    if ((*males)[i] != this)  //don't challenge yourself
                    {
                        // If you are big enough to have aspirations to territories then look around for chances
                        if (m_Status == rdstatus_satellite)
                        {
                            // Do we know this deer?
                            if (!FightResult((*males)[i], aFight))
                            {
                                // Stranger, so fight
                                aFight.m_Result = (*males)[i]->On_Rank(this, m_Size, m_Age, m_NoOfMatings, false);
                                aFight.m_Opponent = (*males)[i];
                                aFight.m_AsTerritoryOwner = false;
                                m_MyFightList.push_back(aFight);
                                if (FightMortality(aFight.m_Result))
                                {
#ifdef __DEBUG_CJT2
                                    m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_fight);
#endif
                                    m_NewState = rds_MDie;
                                    return;
                                }
                                m_MinInState[reu_EstablRangeFight]++;  //to place cost per fight in update energy
                                m_StepDone = true;
                            }
                            else
                            {
                                if (aFight.m_Result)
                                {
                                    //if this is a territory fight, I can expell him from his territory if I want to
                                    if ((*males)[i]->GetStatus() == rdstatus_territorial)
                                    {
                                        (*males)[i]->On_Expelled(this);
                                        RoeDeerInfo info = (*males)[i]->SupplyInfo();
                                        m_RangeCentre = info.m_Range;
                                        m_Status = rdstatus_territorial;
                                        m_HaveRange = true;
                                        m_Disperse = false;
                                        m_DispCount = 0;
                                        m_float = false;
                                        m_NewState = rds_MFeed;
                                        break;
                                    }
                                    else {
                                        // Fight lost or not fought, but this deer is bigger than me, so evade it
                                        AnimalPosition xy = (*males)[i]->SupplyPosition();
                                        if (DistanceTo(m_Location_x, m_Location_y, xy.m_x, xy.m_y) < 50)
                                        {
                                            m_NewState = rds_MEvade;
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            if ((*males)[i]->GetStatus() != rdstatus_peripheral)
                            {
                                // Small guy, keep away from all other males of higher status
                                AnimalPosition xy = (*males)[i]->SupplyPosition();
                                if (DistanceTo(m_Location_x, m_Location_y, xy.m_x, xy.m_y) < 50)
                                {
                                    m_NewState = rds_MEvade;
                                }
                            }
                        }
                    }
                }
            }
            delete males;
        }

    }
}
//---------------------------------------------------------------------------
/**
Roe_Male::Step - function called every time step. The functions checks if done with a given state (m_StepDone)
and governs states and transitions to other states. Calls functions: MOnMature(), MDie(), MOnNewDay(),
MMate(), MEstablishRange(), MEstablishTerritory(), MDisperse(), MFeed(), MRecover(), MRUn(), MEvade(),
MRuminate(), MFight(), MAttendFemale(), MUpdateEnergy()
*/
void Roe_Male::Step(void)
{
    if (m_skipstep) { // In case we have been busy in the last 10 minutes
        m_StepDone = true;
        m_skipstep = false;
    }
    if (m_StepDone || (CurrentRState == rds_MDeathState)) return;

#ifdef _CJT_Debug2
    HostSanityCheck();
#endif;

    switch (CurrentRState)
    {
    case rds_Initialise:
        CurrentRState = rds_MOnMature;
        break;
    case rds_MOnMature:
        // Can return  rds_MDisperse; rds_MEstablishRange;
        CurrentRState = MOnMature();
        m_StepDone = true;
        break;
    case rds_MDie:
        CurrentRState = MDie();
        m_OurPopulation->AddToDiedAdultsThisYear();
        m_StepDone = true;
        break;
    case rds_MMate:
        CurrentRState = MMate();
        // Can return rds_MFeed;
        m_MinInState[reu_Mate]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MEstablishRange:
        CurrentRState = MEstablishRange();
        // Can return   rds_MFeed;
        m_MinInState[reu_EstablRangeFight]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MEstablishTerritory:
        CurrentRState = MEstablishTerritory(); // ret�rns die or feed
        m_MinInState[reu_EstablRangeFight]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MDisperse:
        // Can return rds_Die, rds_Feed, rds_Disperse
        CurrentRState = MDisperse();
        m_MinInState[reu_Disperse]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MFeed:
        CurrentRState = MFeed();
        // Can return rds_MDisperse rds_MRuminate rds_MFeed, rds_MDie
        m_MinInState[reu_Feed]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MRecover:
        CurrentRState = MRecover();
        // Can return rds_MFeed rds_MRecover;
        m_MinInState[reu_Recover]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MRun:
        CurrentRState = MRun();
        // Can return rds_MRun rds_MRecover
        m_MinInState[reu_Run]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MEvade:
        CurrentRState = MEvade();
        // returns to same state as before disturbance
        m_MinInState[reu_Evade]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MRuminate:
        // Can return rds_Die, rds_Feed, rds_Ruminate
        CurrentRState = MRuminate();
        m_MinInState[reu_Rumi]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    case rds_MAttendFemale:
        // Can return rds_MAttendFemale, rds_Feed
        CurrentRState = MAttendFemale();
        m_MinInState[reu_InHeatAttend]++; //add 1 to time spend in this state
        m_StepDone = true;
        break;
    default:
        m_OurLandscape->Warn("Roe_Male::Step():No matching case", "");
        g_msg->Warn("Tried to do ", double(CurrentRState));
        exit(1);
    }
}
//---------------------------------------------------------------------------
/**
Roe_Male::EndStep - checks if male object is dead and adds to number of timesteps.
If 1 whole day has passed, current state is set to FOnNewDay
*/
void Roe_Male::EndStep (void)
{
 if (CurrentRState==rds_MDeathState) return;
 
 timestep++;
 if (timestep %144==0) //1 day has passed
 {
    m_weightedstep=0;
    CurrentRState=rds_MOnNewDay;
    m_LastState = m_LastState = rds_MFeed;
    ;
 }

}
//---------------------------------------------------------------------------
/**
Roe_Male::Roe_Male -  constructor for male object. Sets all attributes. It also adds fawns
to mother's offspring list.

*/
Roe_Male::Roe_Male(Deer_struct* p_data) :Roe_Adult_Base(p_data->x, p_data->y, p_data->size, p_data->age, p_data->L, p_data->Pop)
{
    Init(p_data);
}
//---------------------------------------------------------------------------

void Roe_Male::ReInit(Deer_struct* p_data)
{
    Roe_Adult_Base::ReInit(p_data->x, p_data->y, p_data->size, p_data->age, p_data->L, p_data->Pop);
    Init(p_data);
}
//---------------------------------------------------------------------------

void Roe_Male::Init(Deer_struct* p_data)
{
    m_Age = p_data->age;
    m_skipstep = false;
    m_Sex = true;
    Mum = p_data->mum;
    //add yourself to mothers offspring list
    if (Mum != NULL)
    {
        Mum->AddYoungToList(this);
    }
    m_EnergyGained = 0;
    m_Status = rdstatus_peripheral;
    m_HaveRange = false;
    m_My_Mate = NULL;
    m_Reserves = 30;
    m_IsDead = false;
    m_LastState = rds_MFeed; //Feed. Could be any safe state
    m_NoOfMatings = 0;
    if (p_data->size == -1) //1st generation roe
    {
        double rand = 2.0 * g_rand_uni();
        m_Size = 17.000 + rand;  //just for the first generation, to make sure that
                              //some are dispersers and some are not
    }
    else m_Size = p_data->size;
    for (int i = 0; i < 12; i++) m_MinInState[i] = 0;
}
//---------------------------------------------------------------------------

Roe_Male::~Roe_Male ()
{
  m_My_Mate=NULL;
  Mum=NULL;
}
//--------------------------------------------------------------------------
/**
Roe_Male::On_ApproachOfDanger - Determines whether to run or evade a threat depending on
cover and distance to threath. In good cover, male will run if threath is less than 30 m 
away and else perform evasive behavior, In intermediate cover the flight distance is 50 m, 
and in poor cover it is 70 m.
Returns states evade or run.
Calls Cover(), DistanceTo().
*/
void Roe_Male::On_ApproachOfDanger(int p_To_x,int p_To_y)
{
  m_danger_x=p_To_x;
  m_danger_y=p_To_y;
  //get cover
  int cov=Cover(m_Location_x,m_Location_y);
  //get distance to threat
  int dist = DistanceTo(m_Location_x, m_Location_y, p_To_x, p_To_y);

  if (cov >= CoverThreshold2) //good cover
  {
    if(dist >= 30)
    {
      CurrentRState=rds_MEvade;
    }
    else CurrentRState=rds_MRun;
  }
  else if (cov >= CoverThreshold1)  //intermediate cover
  {
    if(dist>=50)
    {
      CurrentRState=rds_MEvade;
    }
    else  CurrentRState=rds_MRun;
  }
  else //poor cover
  {
    if(dist>=70)
    {
      CurrentRState=rds_MEvade;
    }
    else  CurrentRState=rds_MRun;
  }
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
/**
Roe_Male::On_IsDead - governs a male objects "response" to death of another object. Checks
who is dead (mom, male, mate) and sets them to NULL. If a male object is dead, the "responding" male object
checks if dead object is host ("responding" object is satellite male). If so it will send message 
to the dead host's other satellites. Rank is checked among satellites and if "responding" male wins a fight,
he will take over territory of dead male. If the dead object is a satellite of the "responding" male, it will
be removed from satellite list. If dead object holds a territory and "responding" male is not a satellite to the dead
object but is also not a territory holder, he can challenge nearby interested males (also without territory)
and win territory. Herefter if satellite to another host, the "responding" male will send a message and 
remove itself from satellite list.
If dead object is the "responding" male's mate, the "responding" male will seize to attend the female.
Calls On-Rank(), SupplyInfo(), SupplyMales(), RemoveSatellites().

*/
void Roe_Male::On_IsDead(Roe_Base* Base, int whos_dead, bool fawn)
{
    Roe_Male* male;
    RoeDeerInfo info;
    bool rank = true;
    bool found = false;
    //whos_dead: 0=mum 1=other male, 2=mate
    switch (whos_dead)
    {
    case 0:  //mum
        if (Base != Mum)
        {
            m_OurLandscape->Warn("Roe_Male::On_IsDead():Bad pointer, Base!", "");
            exit(1);
        }
        else Mum = NULL;
        break;
    case 1: //a male is dead, nothing to do
    break;
    case 2://sender is your mate
        m_My_Mate = NULL;
        //give up attending female
        CurrentRState = rds_MFeed;
        break;

    default:
        m_OurLandscape->Warn("Roe_Male::On_IsDead():No matching case!", "");
        exit(1);
    } //end of switch
}
//-----------------------------------------------------------------------------

/**
Roe_Male::On_InHeat - response of male object to "In Heat" message sent by female object. Check if already
have mate and if not check if distance to female is too large. If not too large, the male checks for rivals
around, checks rank (also dependent on whether he is in his own territory), challenges them and adds them to 
fightlist. If the male wins, he will attend the female otherwise he will give up attending this female.
Calls SupplyTMales(), DistanceTo(), On_Rank(), AddToFightList().
*/

bool Roe_Male::On_InHeat(Roe_Female* female,int p_x,int p_y)
{
  //want to attend this female if not attending one already
  if (m_My_Mate == NULL)
  {
    WalkTo(p_x,p_y);
    int dist = DistanceTo(m_Location_x,m_Location_y,p_x,p_y);
    if (dist < 50)   //close enough to female,
    //now find out if there are potential rivals around
    {
      ListOfMales * Tmales = m_OurPopulation->SupplyTMales(m_Location_x, m_Location_y,100);
      bool home=false;
      bool rank=true; //initiated to true and only altered if there are stronger rivals
      //are you still in your own territory?
      if (DistanceTo(m_Location_x,m_Location_y, m_RangeCentre.m_x,m_RangeCentre.m_y)<400)
      {
        home=true;  //yes
      }

      for(unsigned i=0;i<Tmales->size();i++)  //for every male in area
      {
        rank=(*Tmales)[i]->On_Rank(this,(double) m_Size, m_Age, m_NoOfMatings,home);
        if (FightMortality(rank))
        {
#ifdef __DEBUG_CJT1
            m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Male, rm_fight);
#endif
            CurrentRState = rds_MDie;
        }
        if (rank==false)  break;  //lost...give up attending this female
      }
      if (rank==true)
      {
         m_My_Mate = female;
         CurrentRState=rds_MAttendFemale; //attend female
      }
      delete Tmales;
      return rank; //also returns true if no rivals around
    }
    else return false; //still too far away
  }
  else return false; //will not attend
}
//----------------------------------------------------------------------------

/**
Roe_Male::On_Rank is used to establish ranks between male object and rival male object. 
The rank is dependent on male size, age, whether the males is in his own territory,
number of previous matings, and number of previous  fights. It also checks for location 
(if in adversarys territory, winning male can expell territory holder - and call On_Expelled()).
If male object is taking over a territory from a rival male, the male object will receive the 
rival's list of satllite males.
Calls DistanceTo(), SupplyInfor(), On_Expelled(), RemoveSatellite().
*/
bool Roe_Male::On_Rank(Roe_Male* a_rival, double a_size, int a_age, int a_matings, bool a_where)
{
    // Called to this behaviour 'out of time' so cannot record  the time in a standard way, but still want to add the energy cost so we need to skip the next timestep
    m_skipstep = true; 
    m_MinInState[reu_EstablRangeFight] += 1;

    RoeMaleFights aFight;
    int MyScore = 0;
    int HisScore = 0;
    aFight.m_Opponent = a_rival;

    //Establish rank:
    //size
    if (m_Size > a_size) MyScore++;
    else if (a_size > m_Size) HisScore++;
    else
    {
        MyScore++;
        HisScore++;
    }
    //age
    if (m_Age == a_age)
    {
        MyScore++;
        HisScore++;
    }
    else
    {
        if ((m_Age <= MalePrime) && (a_age <= MalePrime))  //both young
        {
            if (m_Age > a_age)  MyScore++;  //advantage to older roe
            else HisScore++;
        }
        else if ((m_Age > MalePrime) && (a_age > MalePrime))  //both old
        {
            if (m_Age > a_age)   HisScore++;
            else MyScore++;
        }
        else if ((m_Age > MalePrime) && (a_age <= MalePrime)) HisScore++;
        else MyScore++;
    }
    //matings
    if (m_NoOfMatings == a_matings)
    {
        MyScore++;
        HisScore++;
    }
    else
    {
        if (m_NoOfMatings > a_matings) MyScore++;
        else HisScore++;
    }
    //where are we
    //first check if you are in own territory (< 400 m from rangecentre)
    bool home = false;
    if ((DistanceTo(m_Location_x, m_Location_y, m_RangeCentre.m_x, m_RangeCentre.m_y) <= 400) && (!m_float))
    {
        home = true;
    }
    aFight.m_AsTerritoryOwner = home;
    if ((home == true) && (a_where == true))  //in overlap area
    {
        MyScore++;
        HisScore++;
    }
    else   //one or both are false
    {
        if (home == true) MyScore++;
        else if (a_where == true) HisScore++;
    }
    //now sum up score
    if (MyScore == HisScore)    //random outcome
    {
        int rand = random(100);
        if (rand < 50)    //I win
        {
            aFight.m_Result = false;
        }
        else aFight.m_Result = true;    //rival wins
    }
    else if (MyScore > HisScore)
    {
        aFight.m_Result = false;  //I win
    }
    else aFight.m_Result = true; //rival wins
    m_MyFightList.push_back(aFight); // Record the fight
    // Determine fight mortalities if any
    if (FightMortality(aFight.m_Result)) m_CurrentStateNo = rds_MDie;
    return aFight.m_Result;
}
//--------------------------------------------------------------------------
/**
Following the On_Rank() function - if a male territory holder loses a fight and 
gets expelled from his territory. Clears satellite list, sets object to be a floater 
and current state to dispersal
Calls OnNewHost().
*/
void Roe_Male::On_Expelled(Roe_Male* rival)
{
    m_float = true;
    m_float_x = m_Location_x;
    m_float_y = m_Location_y;
    CurrentRState = rds_MDisperse; //floater
}
//---------------------------------------------------------------------------
