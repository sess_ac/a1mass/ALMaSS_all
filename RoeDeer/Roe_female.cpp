//
// roe_female.cpp
//

#include <iostream>
#include <fstream>
#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"


#include "Roe_all.h"
#include "Roe_constants.h"


extern int FastModulus(int a_val, int a_modulus);




//---------------------------------------------------------------------------
//                              Roe_Female
//---------------------------------------------------------------------------


/**
Roe_Female::FOnMature - Checks if area is good enough to establish range. The function ScanGrid()
adds the quality values stored in m_grid starting with a minimum area of
400 x 400 m. If not sufficient it extends outwards in all directions
until max range is reached (800 x 800 m). It also checks if amount of other females nearby is below
a female density threshold. It returns the size of the resulting area if good enough and 0 if not
good enough.
Calls ScanGrid(), SupplyFemaleRC().
*/
TRoeDeerStates Roe_Female::FOnMature(void)   //State 1
{
    //Check whether this area is good enough. The function ScanGrid()
    //adds the quality values stored in m_grid starting with a minimum area of
    //400 x 400 m. If not sufficient it extends outwards in all directions
    //until max range is reached (800 x 800 m). It returns the size of the
    //resulting area if good enough and 0 if not good enough

    int range = m_OurPopulation->ScanGrid(m_Location_x, m_Location_y, false);
    if (range > 0) //good enough
    {
        //how many other females already in area?
        ListOfFemales* females = m_OurPopulation->SupplyFemaleRC(APoint(m_Location_x, m_Location_y), range);
        if (females->size() < FemaleDensityThreshold)  //OK
        {
            m_SearchRange = range;
            delete females;
            return rds_FEstablishRange; // establish range
        }
        delete females;
    }
    return rds_FDisperse;  //no options in this area, so disperse
}
//---------------------------------------------------------------------------
/**
Roe_Female::FEstablishRange - sets up range for female object. Sets object to not be a disperser
or to have completed dispersal. Checks if object has a group and if not, assign it to a group.
Makes sure that range centre is not too close to road or houses and sets a range center.
When new range is set, calculate optimal group size in range based on % forest
Always returns state feed.
Calls functions CreateNewGroup(), SupplyElementType(), DistanceTo(), PercentForest().
*/
TRoeDeerStates Roe_Female::FEstablishRange()
{
    /**
   * Activity timeand energy accounting \n
   * Caring cost fixed by CostFemaleAct[reu_EstablRangeFight] * weight in kg
   * One unit of time spent
   */
    m_MinInState[reu_EstablRangeFight] += 1; //add 1 to time spend in this state
    m_EnergyGained -= CostFemaleAct[reu_EstablRangeFight] * m_Size;
    // End time energy accounting
    m_HaveRange = true;
    m_Disperse = false;   //either not disperser or dispersal is completed
    m_float = false;
    //this animal is now established, so check if it has a group, else give it one
    if (m_MyGroup == -1)
    {
        m_MyGroup = m_OurPopulation->CreateNewGroup(this);
    }
    //make sure that range centre is not too close to road or houses
    int t[8];
    int dir = random(8);   //start looking in any direction
    //int nogood[8]= {0};
    bool move = false;
    int Dist[8] = { 0,0,0,0,0,0,0,0 };
    int x = m_Location_x;
    int y = m_Location_y;

    if ((x <= 200) || (x >= SimW - 200) || (y <= 200) || (y >= SimH - 200)) //next step could take you out
    {
        //need to correct coordinates
        x = m_Location_y + SimW;
        y = m_Location_y + SimH;
        for (int i = 200; i >= 0; i -= 2)   //every 2 meter closing in
        {
            t[0] = dir;
            t[0] = t[0] & 7;
            t[1] = dir + 1;
            t[1] = t[1] & 7;
            t[2] = dir - 1;
            t[2] = (t[2] + 8) & 7;
            t[3] = dir + 2;
            t[3] = (t[3] + 8) & 7;
            t[4] = dir - 2;
            t[4] = (t[4] + 8) & 7;
            t[5] = dir + 3;
            t[5] = (t[5] + 8) & 7;
            t[6] = dir - 3;
            t[6] = (t[6] + 8) & 7;
            t[7] = dir + 4;
            t[7] = (t[7] + 8) & 7;

            for (int direc = 0; direc < 8; direc++)
            {
                if (m_OurLandscape->SupplyIsHumanDominated(m_OurLandscape->SupplyElementTypeCC((x + (Vector_x[t[direc]] * i)), (y + (Vector_y[t[direc]] * i)))))
                {
                    //too close in this direction
                    //nogood[direc]=1;
                    move = true;
                    Dist[direc] = DistanceTo(m_Location_x, m_Location_y, (x + (Vector_x[t[direc]] * i)), (y + (Vector_y[t[direc]] * i)));
                }
            }
        }
        if (move == true)  //need to adjust range centre
        {
            int thisway = 0;
            int closest = 0;
            int furthest = 0;
            for (int i = 0; i < 8; i++)
            {
                if (Dist[i] < closest)  closest = Dist[i];
                if (Dist[i] > furthest)
                {
                    furthest = Dist[i];
                    thisway = i;
                }
            }

            if (closest < furthest / 2)  //move range centre halfway along furthest
            {
                for (int i = furthest / 2; i < 200; i++)
                {
                    int xi = x + Vector_x[t[thisway]] * i;
                    int yi = y + Vector_y[t[thisway]] * i;
                    m_OurLandscape->CorrectCoords(xi, yi);
                    int quali = LegalHabitatCC(xi, yi);
                    if (quali > 0)
                    {
                        m_RangeCentre.m_x = m_OurLandscape->CorrectWidth(m_Location_x + (Vector_x[t[thisway]] * i));
                        m_RangeCentre.m_y = m_OurLandscape->CorrectHeight(m_Location_y + (Vector_y[t[thisway]] * i));
                    }
                }
            }
        }
        //nothing gained by moving or no reason to move, so stay
        m_RangeCentre.m_x = m_Location_x;
        m_RangeCentre.m_y = m_Location_y;
    }
    else //no need to correct coordinates
    {
        int x = m_Location_x;
        int y = m_Location_y;
        for (int i = 200; i >= 0; i -= 2)   //every 2 meter closing in
        {
            t[0] = dir;
            t[0] = t[0] & 7;
            t[1] = dir + 1;
            t[1] = t[1] & 7;
            t[2] = dir - 1;
            t[2] = (t[2] + 8) & 7;
            t[3] = dir + 2;
            t[3] = (t[3] + 8) & 7;
            t[4] = dir - 2;
            t[4] = (t[4] + 8) & 7;
            t[5] = dir + 3;
            t[5] = (t[5] + 8) & 7;
            t[6] = dir - 3;
            t[6] = (t[6] + 8) & 7;
            t[7] = dir + 4;
            t[7] = (t[7] + 8) & 7;

            for (int direc = 0; direc < 8; direc++)
            {
                if (m_OurLandscape->SupplyIsHumanDominated(m_OurLandscape->SupplyElementType((x + (Vector_x[t[direc]] * i)), (y + (Vector_y[t[direc]] * i)))))
                {
                    //too close in this direction
                    //nogood[direc]=1;
                    move = true;
                    Dist[direc] = DistanceTo(m_Location_x, m_Location_y, (x + (Vector_x[t[direc]] * i)), (y + (Vector_y[t[direc]] * i)));
                }
            }
        }
        if (move == true)  //need to adjust range centre
        {
            int thisway = 0;
            int closest = 0;
            int furthest = 0;
            for (int i = 0; i < 8; i++)
            {
                if (Dist[i] < closest)  closest = Dist[i];
                if (Dist[i] > furthest)
                {
                    furthest = Dist[i];
                    thisway = i;
                }
            }

            if (closest < furthest / 2)  //move range centre halfway along furthest
            {
                for (int i = furthest / 2; i < 200; i++)
                {
                    int quali = LegalHabitat((x + (Vector_x[t[thisway]] * i)),
                        (y + (Vector_y[t[thisway]] * i)));
                    if (quali > 0)
                    {
                        m_RangeCentre.m_x = m_Location_x + (Vector_x[t[thisway]] * i);
                        m_RangeCentre.m_y = m_Location_y + (Vector_y[t[thisway]] * i);
                    }
                }
            }
        }
        //nothing gained by moving or no reason to move, so stay
        m_RangeCentre.m_x = m_Location_x;
        m_RangeCentre.m_y = m_Location_y;
        m_OldRange.m_x = m_Location_x; //OldRange used to store range while in group
        m_OldRange.m_y = m_Location_y;
    }

    //new range is set, so calculate optimal group size in your range
    //first step is to get % forest
    double forest = m_OurPopulation->PercentForest(m_RangeCentre, m_SearchRange);
    if (forest < 4) m_OptGroupSize = 10; //max group size
    else m_OptGroupSize = (int)(17.1 * (pow(forest, -0.49))); //relation between % forest
   //and observed group sizes.
    m_DispCount = 0;
    return rds_NewBehaviour;
}
//--------------------------------------------------------------------------

/**
Roe_Female::MDisperse - models dispersal state of female objects. Sets state to
dispersal. It then keeps track of amount of time in the dispersal
state and calculate probability of switching to feeding, keep dispersing, or dying if spending too long
in dispersal state.Calls functions DirectionTo() and NextStep().
*/
TRoeDeerStates Roe_Female::FDisperse(void)  //disperse
{
    /**
   * Activity timeand energy accounting \n
   * Caring cost fixed by CostFemaleAct[reu_Disperse] * weight in kg
   * One unit of time spent
   */
    m_DispCount++;
    m_MinInState[reu_Disperse] += 1; //add 1 to time spend in this state
    m_EnergyGained -= CostFemaleAct[reu_Disperse] * m_Size;
    // End time energy accounting
    m_HaveRange = false;  //a dispersing animal cannot be a rangeholder per def.
    m_Disperse = true;  //indicates that dispersal has started
    //allow roe the opportunity to return to this state after disturbance
    m_LastState = rds_FDisperse;
    if (m_DispCount == 1)  //first timestep in disperse
    {
        //store range centre coordinates while dispersing
        m_OldRange.m_x = m_RangeCentre.m_x;
        m_OldRange.m_y = m_RangeCentre.m_y;
    }

    m_RangeCentre.m_x = m_Location_x; //these are drifting depending on roes position
    m_RangeCentre.m_y = m_Location_y;
    //int dist=DistanceTo(m_Location_x,m_Location_y,m_OldRange_x,m_OldRange_y);
    int dir = DirectionTo(m_Location_x, m_Location_y, m_OldRange.m_x, m_OldRange.m_y);
    int rand = 0;

    if (m_DispCount > 70)
    {
        for (int i = 0; i < 3; i++)
        {
            if (NextStep(0, 0, 0) == -1)
            {
#ifdef __DEBUG_CJT1
                m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Female, rm_road);
#endif
                return rds_FDie;
            }
        }
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            if (NextStep(1, ((dir + 4) & 0x07), 0) == -1) {
#ifdef __DEBUG_CJT1
                m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Female, rm_road);
#endif
                return rds_FDie;
            }

        }
    }
    return rds_NewBehaviour;
}
//--------------------------------------------------------------------------
/**
Roe_Female::FFeed - Models feeding of female object, including energy budget and search for better feeding
area. Also determines need to ruminate. In theory a roe deer can spend time
in more than 1 type of habitat within 1 timestep. For the energy budget only the habitat
type where roe deer is at the beginning of each time step is recorded. Need to keep counter of minutes
in feed since the end of last ruminating period (m_FeedCount) to know when to return to ruminate. Returns values
for dispersal, ruminate or keep feeding (or die).
Calls function NutriValue()
*/
TRoeDeerStates Roe_Female::FFeed()     //State 35
{
    //In theory a roe can spend time in more than 1 type of habitat within 1 timestep.
    //For the energy budget only the habitat type where roe is at the beginning of
    //each time step is recorded. m_AmFeeding is set to true on 2nd feeding step
    //(i.e. whenever roe decides to stay in feed). Need to keep counter of minuts
    //in feed since the end of last ruminating period (m_FeedCount) to know when to
    //return to ruminate
    //allow roe the opportunity to return to this state after disturbance
    m_LastState = rds_FFeed;  //Feed
    //Find the nutritional value here
    int nutri = NutriValue(m_Location_x, m_Location_y);  //possible energy gain per minut
    if (nutri < MinNutri)  //not good enough for feeding
    {
        SeekNutri(MinNutri); //look for a better place first
        Feeding(m_Disperse);
    }
    else  //can feed here
    {
        m_EnergyGained += nutri;   //add this to total count for today
        Feeding(m_Disperse);
    }
    /**
    *Activity time and energy accounting \n
    * Feeding cost multiplied by size kg
    * One unit of time spent
    */
    m_MinInState[reu_Feed] += 1; //add 1 to time spend in this state
    m_EnergyGained -= CostFemaleAct[reu_Feed] * m_Size;
    // End time energy accounting
    return rds_NewBehaviour;
}

//---------------------------------------------------------------------------
/**
Roe_Female::FRuminate - Function for ruminating. Checks for got spot to rest and amount of time
spent ruminating. Ruminate activity cannot be seperated from other kinds of inactivity.
It is also assumed that ruminating takes place while recovering after disturbance.
Need to count amount of time spend in this state during this period of ruminating and in total last 24 hrs.
Returns values for either stay ruminating, feed or dispersal (or die).Calls function Cover().
*/
TRoeDeerStates Roe_Female::FRuminate()  //Ruminate
{
    //ruminate activity cannot be seperated from other kinds of inactivity.
    //It is also assumed that ruminating takes place while recovering after
    //disturbance. Need to count amount of time spend in this state during
    //this period of ruminating and in total last 24 hrs
    int min;
    //First check if this is first timestep in this state
    if (m_RumiCount == 1)   //first step so make sure this is a good spot to rest
    {
        int cover = Cover(m_Location_x, m_Location_y);
        //get month
        int month = m_OurLandscape->SupplyMonth();
        if ((month >= 5) && (month <= 9)) min = CoverThreshold2; //summer
        else min = CoverThreshold1; //winter
        if (cover < min)// habitat unsuitable or cover very poor, so look for better spot
        {
            /**
           *Activity time and energy accounting \n
           * Seek cover cost multiplied by size kg
           * One unit of time spent
           */
            m_EnergyGained -= CostFemaleAct[reu_SeekCover] * m_Size;
            m_MinInState[reu_SeekCover] += 1; //add 1 to time spend in this state
            // End time energy accounting
            SeekCover(2);
            if (m_IsDead)
            {
#ifdef __DEBUG_CJT1
                m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Female, rm_seekcover);
#endif
                return rds_FDie; //Die
            }
            if (m_seekingcover == true) return rds_NewBehaviour; // Not good look for cover
            else {
                m_StepDone = true;
                return rds_FRuminate;
            }
        }
    }    
    // All good ruminate
    /**
   *Activity time and energy accounting \n
   * Ruminating assumed to be same as RMR
   * One unit of time spent
   */
    m_EnergyGained -= m_RMR;
    m_MinInState[reu_Rumi] += 1; //add 1 to time spend in this state
    // End time energy accounting
    return rds_NewBehaviour;     //feed
}

//---------------------------------------------------------------------------
/**
Roe_Female::FCareForYoung - sets female object to be in care for young state. Care periods
are initiated by message from fawn to mother. Fawn keeps counter of minuts care (=suckling)
per hour. If counter drops below min value fawn initiates extra care period. If care is done
returns to feeding.
*/
TRoeDeerStates Roe_Female::FCareForYoung(void) //CareForYoung
{
    //Care periods are initiated by message from fawn to mother. Fawn
    //keeps counter of minuts care (=suckling) per hour. If counter drops below
    //min value fawn initiates extra care period.
    /**
    * Activity timeand energy accounting \n
    * Caring cost fixed by CostFemaleAct[reu_Care] * weight in kg
    * One unit of time spent
    */
    m_EnergyGained -= CostFemaleAct[reu_Care]*m_Size;
    m_MinInState[reu_Care] += 1; //add 1 to time spend in this state
    // End time energy accounting
    return rds_NewBehaviour;
}
//--------------------------------------------------------------------------

TRoeDeerStates Roe_Female::FInHeat()
{
    m_HeatCount = 0;
    return rds_NewBehaviour;
}
//--------------------------------------------------------------------------

/**
Roe_Female::InHeat - called from NewBehaviour - sets female object to be in heat. Female object Start new day in this state
and stays here until it has found at mate or heat is over. Female heat lasts for 3 days. The female will
get a list of all males in area and send them a message. If no males are around, the female will
search a larger area. If find a male that will mate and is close enough to attend, male list will be deleted
and this particular male is set as the female's mate.
Calls SupplyMales(), On_InHeat().
*/
void Roe_Female::InHeat()
{
    //allow roe the opportunity to return to this state after disturbance
    bool CanMate = false;
    ListOfMales* males;
    m_HeatCount++; //female heat lasts for 3 days
    if (m_HeatCount % 3 == 0) //every 30 minutes
    {
        //get a list of all Males in area and send them a message
        males = m_OurPopulation->SupplyMales(m_Location_x, m_Location_y,
            2 * m_SearchRange);
        if (males->size() == 0)  //no one around, so search a larger area
        {
            delete males;
            males = m_OurPopulation->SupplyMales(m_Location_x, m_Location_y, 4 * m_SearchRange);
        }
        for (unsigned i = 0; i < males->size(); i++)
        {
            //send them message
            CanMate = (*males)[i]->On_InHeat(this, m_Location_x, m_Location_y);
            if (CanMate == true) //this male will mate and is close enough to attend
            {
                m_My_Mate = (*males)[i];
                delete males;
                return ;  //return to normal activity
            }
        }
        delete males;
    }
    if (m_HeatCount >= 432) //have to go back to normal activity after 3 days
    {
        m_HeatCount = -1;
    }
}
//------------------------------------------------------------------------------
/**
Roe_Female::FOnNewDay - updates age of female object (+ 1 day), decides if life expectancy
has been reached, which depends on age (Floaters have a higher mortality (added FloatMort)).
Checks whether dispersers and floaters can set up range by checking habitat and closeness to
mothers range and amount of males already in the area.
If failed to find a range before october 1st become a floater. Also governs whether to dissolve or stay in
female group - dissolvement dependent on date. If dissolved group, sends female back to old range.
Returns values to set the following states:
establish range, Form group, update energy (and die).
Calls functions DistanceTo(), ScanGrid(), SupplyFemaleRC(), RemoveFromGroup().
*/
TRoeDeerStates Roe_Female::FOnNewDay()
{
    m_Age++;  //1 day older
    m_NewState = rds_NewBehaviour;
    //First decide if life expectancy has been reached. Depends on age. Floaters
    //have a higher mortality than other (added FloatMort)
    int mort;
    int dist;
    long int die = random(100000);  //since mort rates are multiplied with 100000 to
                                  //produce int values
    if (m_Age <= 730)
        mort = (int)FemaleDMR[0]; //age in days
    else if (m_Age <= 2555)   //7 yrs
        mort = (int)FemaleDMR[1];
    else if (m_Age <= 3650)  //10 yrs
        mort = (int)FemaleDMR[2];
    else mort = (int)FemaleDMR[3]; //>10 yrs

    if ((m_float) || (m_Disperse)) mort += FloatMort;

    if (die < mort)
    {
#ifdef __DEBUG_CJT1
        m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Female, rm_random);
#endif
        return rds_FDie;  //Die
    }
    else   //still alive
    {
        //RANGES
        if (m_Disperse) //disperser that needs a range
        {
            if (!m_float)
            {
                //dispersers check dist back to mothers RangeCentre
                dist = DistanceTo(m_Location_x, m_Location_y, m_OldRange.m_x, m_OldRange.m_y);
            }
            else
            { //floaters check dist back to last attempt to set up range
                dist = DistanceTo(m_Location_x, m_Location_y, m_float_x, m_float_y);
            }
            if (dist > MinDispDistance)
            {
                //both floaters and regular dispersers go in here
                bool found = false;
                int range = m_OurPopulation->ScanGrid(m_Location_x, m_Location_y, false);
                if (range > 0)   //OK
                {
                    //how many other females have already their range in this area?
                    ListOfFemales* females = m_OurPopulation->
                        SupplyFemaleRC(APoint(m_Location_x, m_Location_y), range);

                    if (females->size() < FemaleDensityThreshold)   //OK
                    {
                        //OK to settle here, so return to EstablishRange after this
                        found = true;
                        m_SearchRange = range;
                        m_NewState = rds_FEstablishRange;        //establishrange
                    }
                    delete females;
                }
                if (!found)  //quality too poor or still too close to mother, so return to disperse
                {
                    //if failed to find a range before Sept. 1st become a floater
                    if (m_OurLandscape->SupplyDayInYear() >= 242)
                    { //floaters store present location for use in next attempt to set up range
                        m_float = true;
                        m_float_x = m_Location_x;
                        m_float_y = m_Location_y;
                        //remove this deer from its group
                        if (m_MyGroup != -1) {
                            m_OurPopulation->RemoveFromGroup(this, m_MyGroup);
                            m_MyGroup = -1;
                        }
                    }
                    m_DispCount = 0;
                    return rds_FDisperse;   //disperse
                }
            }
        }

        //GROUPS
        if (!m_float)
        {
            //get day
            int day = m_OurLandscape->SupplyDayInYear();
            if ((day == 120) && (m_MyGroup!=-1))  //time to dissolve groups
            {
                m_OurPopulation->DissolveGroup(m_MyGroup);
            }
            if ((day == 274) && (m_MyGroup == -1)) //after Oct. 1st and still not in group
            {
                return rds_FFormGroup;   //FormGroup
            }
        }
    }
    return rds_NewBehaviour; //update energy
}
//---------------------------------------------------------------------------
/**
Roe_Female::FUpdateEnergy - Calculates how much energy is used (in cal/minute) in different activities.
Calculates net energy (gained-used) and determines whether excess enery is added to reserves or is used to
put on weight. A negative net energy causes the female object to loose weight - if roe deer weight falls
below a minimum value, the deer will die. If the roe deer is in very bad condition, the female may abandon
fawns and increase forage time.


*/
TRoeDeerStates Roe_Female::FUpdateEnergy(void)
{
    /**
    Possible returns are rds_FDie and rds_FUpdateGestation
    */
    //All energetic calculations in cal/timestep
    //find resting metabolic rate
    m_RMR = FemaleRMR[0];
    double EnergyUsed = 0;
    int month = m_OurLandscape->SupplyMonth();
    //FemaleRMR depends on reproductive state, so find out if pregnant
    if (m_Pregnant == true)
    {
        if (m_Gestationdays >= 240)    m_RMR = FemaleRMR[1];  //late pregnancy
    }
    else if ((m_DaysSinceParturition > 0) && (m_DaysSinceParturition <= 120))  //young fawn
    {
        if (m_DaysSinceParturition <= 15)  m_RMR = FemaleRMR[2];
        else if (m_DaysSinceParturition <= 30)   m_RMR = FemaleRMR[3];
        else if (m_DaysSinceParturition <= 60)   m_RMR = FemaleRMR[4];
        else if (m_DaysSinceParturition <= 90)   m_RMR = FemaleRMR[5];
        else  m_RMR = FemaleRMR[6];
    }
    /*
    //Calculate how much energy used in different activities
    for (int i = 0; i < 12; i++)
    {
        xxxxx
        double cost;
        if (CostFemaleAct[i] == 1)   cost = CostFemaleAct[i] * m_RMR; // Only ruminating uses this.
        else cost = CostFemaleAct[i];
        EnergyUsed += (cost * m_MinInState[i] );
    }
    double result = m_EnergyGained - EnergyUsed;     //in cal
    if (result > 0)
    */
    if (m_EnergyGained>0)
    {
        if (m_Reserves < 10)  m_Reserves++;
        else
        {
            m_Size += m_EnergyGained * Anabolic_Inv;  //put on weight
        }
    }
    else if (m_EnergyGained < 0)
    {
        if (m_Reserves > 0) m_Reserves--;  //subtract 1 from reserves
        else
        {
            m_Size += m_EnergyGained * Catabolic_Inv;   //loose weight
        }
    }
    m_EnergyGained = 0;
    //Update m_Size. If m_Size is approaching a critical value, roe gives up its
    //range and starts dispersing. Fawns younger than 2 months dies.
    //If m_Size < MinSize roe dies

    if ((m_Reserves == 0) && (m_Pregnant == false))
    {
        if (m_Size < m_OurPopulation->m_MinWeight_Females)
        {
#ifdef __DEBUG_CJT1
            m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Female, rm_starve);
#endif

            return rds_FDie; //Die
        }
        else if (m_Size < m_OurPopulation->m_CriticalWeight_Females)
        {  // in very bad condition, abandon fawns and increase foraging time
            if (m_NoOfYoung > 0)
            {
                for (unsigned i = int(m_MyYoung.size()); i-- > 0; )
                {
                    if (m_MyYoung[i]->m_Age <= 365)
                    {
                        m_MyYoung[i]->On_MumAbandon(this);   //abandon the fawns
                        m_MyYoung.erase(m_MyYoung.begin() + i);
                        m_NoOfYoung--;
                    }
                }
            }
            m_LengthFeedPeriod = (int)(5 / Female_FeedBouts[month - 1]);
            m_NewState = rds_NewBehaviour;  //go to feed after this
        }
        else //everything OK
        {
            m_LengthFeedPeriod = (int)(3  /Female_FeedBouts[month - 1]);
            if (m_LengthFeedPeriod > 3) m_LengthFeedPeriod = 3;
        }
    }
    //set counter of daily energy gain and MinInState to zero
    m_EnergyGained = 0;
    for (int i = 0; i < 12; i++)
    {
        m_MinInState[i] = 0;
    }
    return rds_FUpdateGestation; //Update gestation
}
//---------------------------------------------------------------------------
/**
Roe_Female::FDie - function for managing death of a female object. The female object
sends message to offspring list and removes herself from group list. If attended by male
when dying, she sends message to this male as well. The fixlist of the female object is emptied and
the female furthermore adds itself to list of dead animals. Function returns dead state.
Calls functions RemoveFixList()and On_IsDead().
*/
TRoeDeerStates Roe_Female::FDie(void)  //Die
{
    //When female dies she sends message to offspring list and removes
    //herself from group list
    //If attended by male when dying, she sends message to this male as well
    //add yourself to list of dead animals
    m_OurPopulation->m_DeadListFemales.push_back(m_Age);

    //tell mum
    if (Mum != NULL)
    {
        Mum->On_IsDead(this, 3, false);  //'3' indicates offspring
        Mum = NULL;
    }
    //tell offspring
    for (auto it = begin(m_MyYoung); it != end(m_MyYoung); ++it) {
        (*it)->On_IsDead(this, 0, false);  //'0' indicates Mum;
    }
    // Is there a male attending, if so send him a message
    if (m_My_Mate != NULL)
    {
        m_My_Mate->On_IsDead(this, 2, false); //'2' indicates mate
        m_My_Mate = NULL;
    }
    m_CurrentStateNo = -1;
    //remove yourself from group list
    if (m_MyGroup != -1)
    {
        m_OurPopulation->RemoveFromGroup(this, m_MyGroup);
        m_MyGroup = -1;
    }
    return rds_FDeathState;
}
//---------------------------------------------------------------------------
/**
Roe_Female::FUpdateGestation - Checks if it is time to give birth if pregnant. Controls taking care
of young, mating, the probability of going into heat (dependent on age and time of year).
Returns states giveBirth, inHeat, CareFOrYoung or mate.

*/
TRoeDeerStates Roe_Female::FUpdateGestation(void)
{
    m_NewState = rds_NewBehaviour; // default return
    int day = m_OurLandscape->SupplyDayInYear();
    if (day == 0) m_WasInHeat = false;  //new season
    if (m_Pregnant == true)
    {
        if ((m_Gestationdays == GestationPeriod))
        {
            m_NewState = rds_FCareForYoung;   //CareForYoung on start of new day (if not before)
            return rds_FGiveBirth; //GiveBirth
        }
        else m_Gestationdays++;
    }
    else  //not pregnant
    {
        if (m_My_Mate != NULL)
        {
            return rds_FMate; //Mate
        }

        else if (m_DaysSinceParturition >= 0) //with young fawn
        {
            if (m_DaysSinceParturition < 30) m_DaysSinceParturition++;
            else m_DaysSinceParturition = -1; //no need to count anymore
        }
        else  //not mated and not with fawn
        {
            if ((m_WasInHeat == false) && (m_HaveRange == true) && (day >= 230) && (day <= 250) && (m_Age <= 730))
            {
                int YearlHeat;
                YearlHeat = random(100);

                if (YearlHeat < 20)  //20, small probability each day, 60 % ends up being mated
                {  //can go in heat
                    m_NewState = rds_FInHeat;   //start next day in InHeat
                    m_WasInHeat = true;  //only 1 heat period per season
                }
            }
            else if ((m_WasInHeat == false) && (m_HaveRange == true) && (day >= 230)
                && (day <= 250)
                && (m_Age > 730))
            {
                //can go in heat
                int AdultHeat;
                AdultHeat = random(100);
                if (AdultHeat < 30)      //30, slightly larger prob., 85 % ends up being mated
                {
                    m_NewState = rds_FInHeat;   //start next day in InHeat
                    m_WasInHeat = true;
                }
            }
        }
    }
    return m_NewState;
}
//---------------------------------------------------------------------------

/**
Roe_Female::FMate - sets female object to be pregnant and sets gestation days to
be zero. Also, function stops female from being attended by male. Female returns to same
state as before OnNewDay.
*/
TRoeDeerStates Roe_Female::FMate(void)
{
    /**
    * Activity timeand energy accounting \n
    * Caring cost fixed by CostFemaleAct[reu_Mate] * weight in kg
    * One unit of time spent
    */
    m_EnergyGained -= CostFemaleAct[reu_Mate] * m_Size;
    m_MinInState[reu_Mate] += 1; //add 1 to time spend in this state
// End time energy accounting
    m_Gestationdays = 0;
    m_Pregnant = true;
    m_My_Mate = NULL;  //not attended anymore
    return rds_NewBehaviour;
}
//---------------------------------------------------------------------------

/**
Roe_Female::FFormGroup - Function for formation of female group. All deer in groups
of size less than optimum, will ask around for group sizes of other groups. If closer
to optimum than their own, they can change groups. If this is the case  a roe has to add
itself to new group, remove itself from old group and message this years fawns about new group.
The roe gets a list of females within a certatin range of the roe location then send message
to all these females that they can join group. The roe then evaluate answers and decide whether to change
group. A new group needs to be larger than old group but still <=optimal group size. Once in a group
oldes female in group is found and the range centre of this female serves as group range center.
Returns update energy.
Calls Supply_GroupSize(), SupplyFemales(), On_CanJoinGroup(), SupplyGroupNo(), RemoveFromGroup(),
AddToGroup(), SupplyInfo() On_ChangeGroup(), On_UpdateGroup()
*/
TRoeDeerStates Roe_Female::FFormGroup(void)    //FormGroup
{
    /**
    All deer in groups of size less than optimum, will ask around for groupsizes
    of other groups. If closer to optimum than their own, they can change groups
    */
    m_WantGroup = false;
    size_t Group_Size = 1;
    if (m_MyGroup == -1) //was a floater
    {
        m_WantGroup = true;
    }
    else
    {
        Group_Size = m_OurPopulation->Supply_GroupSize(m_MyGroup); //my group size
        if (Group_Size < m_OptGroupSize) //always want to join another group if this is true
        {
            m_WantGroup = true;
        }
        else if ((m_Disperse) && (m_NatalGroup)) //disperser without a group of its own
        {
            m_WantGroup = true;
        }
    }

    if (m_WantGroup) //look for a better group
    {
        ListOfFemales* RFL;
        //first get list of female
        RFL = m_OurPopulation->SupplyFemales(m_Location_x, m_Location_y, m_SearchRange);
        int result;
        int bestgroup = m_MyGroup;

        for (unsigned i = 0; i < RFL->size(); i++)
        {
            if ((*RFL)[i] != this)    //don't send to yourself
            {
                //Send message "CanJoinGroup" to all these females
                result = (*RFL)[i]->On_CanJoinGroup(this);
                //evaluate answers and decide whether to change group. new group needs
                //to be larger than old group but still <=optimal group size.
                if ((result > Group_Size) && (result <= m_OptGroupSize))
                {
                    bestgroup = (*RFL)[i]->SupplyGroupNo();  //better than mine
                    break;  //better group found
                }
            }
        }
        delete RFL;
        if (bestgroup != m_MyGroup) //another group is better than mine
        {
            //remove yourself from your old group
            m_OurPopulation->RemoveFromGroup(this, m_MyGroup);
            // Get the new group number
            m_MyGroup = bestgroup;
            //Add yourself to the new group
            m_OurPopulation->AddToGroup(this, m_MyGroup);
            //make sure that this years fawns know about new group
            if (m_NoOfYoung != 0)
            {
                for (auto it = begin(m_MyYoung); it != end(m_MyYoung); ++it)
                {
                    if ((*it)->m_Age <= 365) (*it)->On_ChangeGroup(m_MyGroup);
                }
            }
        }

    }
    if (m_GroupUpdated == false)
    { 
        m_OurPopulation->UpdateGroup(m_MyGroup);
    }
    m_WantGroup = false;
    return rds_NewBehaviour;
}
//-----------------------------------------------------------------------
/**
Roe_Female::FGiveBirth - Function for female object giving birth to fawns.
Litter size is related to maternal weight, Littersize=0.15*W - 0.68,
where 'W'=maternal weight in kg. After litter size has been calculated, the corresponding
number of fawn objects are created. Fawns are randomly assinged sex (50:50), sizes of fawn are
calculated as female size ^0.9 * 0.166 //Oftedal 1985, mum is assigned
and fawns are added to mother's offspring list and group list.
Calls CreateObjects().

*/
TRoeDeerStates Roe_Female::FGiveBirth(void)
{
    /**
    * Activity timeand energy accounting \n
    * Caring cost fixed by CostFemaleAct[reu_GiveBirth] * weight in kg
    * One unit of time spent
    */
    m_EnergyGained -= double( CostFemaleAct[reu_GiveBirth] * m_Size);
    m_MinInState[reu_GiveBirth] += 1; //add 1 to time spend in this state
    // End time energy accounting
    Deer_struct* fds;
    fds = new Deer_struct;
    bool sex;
    //litter size is related to maternal weight (Hewison 1996)
    //Littersize=0.15*W - 0.68, where 'W'=maternal weight in kg. Multiply by
    //To round to nearest int, add 0.5 and round down
    double young = (0.15 * m_Size) - 0.68;
    m_NoOfYoung = (int)floor(young + 0.5);

    for (int i = 0; i < m_NoOfYoung; i++)
    {
        if (g_rand_uni() < prop_femalefawns)
        {
            sex = false;  //female
        }
        else
        {
            sex = true;  //males
        }
        //call CreateObjects to create the right no. of fawns
        fds->Pop = m_OurPopulation;
        fds->L = m_OurLandscape;
        fds->sex = sex;
        fds->x = m_Location_x;
        fds->y = m_Location_y;
        fds->mum = this;
        fds->size = pow((double)m_Size, 0.9) * 0.166; //Oftedal 1985
        fds->group = m_MyGroup;
        //debug

        fds->ID = -1; //don't know yet
        m_OurPopulation->CreateObjects(0, this, NULL, fds, 1);
        //fawns are added to offspring list and mothers group list
    }
    delete fds;

    m_Pregnant = false;
    m_DaysSinceParturition = 0;
    m_Gestationdays = -1;
    return rds_FCareForYoung; //CareForYoung
}
//---------------------------------------------------------------------------
/**
 Roe_Female::AddFawnToList - adds fawns to offfspring list of female object.
 Calls AddToGroup().
*/
void Roe_Female::AddYoungToList(Roe_Base* young)  //called for each fawn from createobjects
{
    //add to the end of offspring list
    m_MyYoung.push_back(young);
    //group list

    if (m_MyGroup != -1)
    {
        m_OurPopulation->AddToGroup(young, m_MyGroup);
    }
}
//----------------------------------------------------------------------------

//---------------------------------------------------------------------------
/**
Roe_Female::BeginStep - checks if female object is dead
*/
void Roe_Female::BeginStep(void)
{
    if (timestep % 144 == 0) // Just once per day
    {
        m_weightedstep = 0;
        m_NewState = CurrentRState;
        // the two methods below may change m_NewState
        FOnNewDay();
        FUpdateEnergy();
        CurrentRState = m_NewState;
    }
    if (m_OurPopulation->GetThreat(m_Location_x, m_Location_y))
    {
        On_ApproachOfDanger(m_Location_x, m_Location_y);
    }
}
//---------------------------------------------------------------------------
/**
Roe_Female::Step - function called every time step. The functions checks if done with a given state (m_StepDone)
and governs states and transitions to other states. Calls functions: FOnMature(), FDie(), FOnNewDay(),
FUpdateGestation(),FMate(), FFormGroup(), FGiveBirth(), FEstablishRange(), FDisperse(), FFeed(), FRecover(), FRUn(), FEvade(),
FRuminate, FCareForYoung(), FUpdateEnergy(), FInHeat().
*/
void Roe_Female::Step(void)
{
    if (m_StepDone || (CurrentRState == rds_FDeathState)) return;

    switch (CurrentRState)
    {
    case rds_Initialise:
        CurrentRState = rds_FOnMature;
        break;
    case rds_NewBehaviour:
        CurrentRState = NewBehaviour();
        break;
    case rds_FOnMature:
        // Can return rds_FEstablishRange rds_FDisperse
        CurrentRState = FOnMature();
        break;
    case rds_FDie:
        // Can return rds_FDeathState
        CurrentRState = FDie();
        m_StepDone = true;
        m_OurPopulation->AddToDiedAdultsThisYear();
        break;
    case rds_FUpdateGestation:
        // Can return rds_FEstablishRange rds_FDisperse rds_FInHeat rds_FGiveBirth rds_FCareForYoung rds_FRecover rds_FFeed rds_FMate
        CurrentRState = FUpdateGestation();
        break;
    case rds_FMate:
        // Can return rds_FDisperse rds_FInHeat rds_FCareForYoung rds_FRecover rds_FRuminate rds_FFeed
        CurrentRState = FMate();
        m_StepDone = true;
        break;
    case rds_FFormGroup:
        // Can return rds_FUpdateEnergy
        CurrentRState = FFormGroup();
        break;
    case rds_FGiveBirth:
        // Can return rds_FCareForYoung;
        CurrentRState = FGiveBirth();
        break;
    case rds_FEstablishRange:
        // Can return rds_FFeed 
        CurrentRState = FEstablishRange();
        m_StepDone = true;
        break;
    case rds_FDisperse:
        // Can return rds_FFeed rds_FEstablishRange rds_FDisperse rds_FDie
        CurrentRState = FDisperse();
        m_StepDone = true;
        break;
    case rds_FFeed:
        // Can return rds_FDisperse rds_FRuminate rds_FDie 
        CurrentRState = FFeed();
        m_StepDone = true;
        break;
    case rds_FRecover:
        // Can return rds_FFeed rds_FRecover
        CurrentRState = FRecover();
        m_StepDone = true;
        break;
    case rds_FRun:
        // Can return rds_FRecover rds_FRun
        CurrentRState = FRun();
        m_StepDone = true;
        break;
    case rds_FEvade:
        // Can return rds_FDisperse rds_FInHeat rds_FCareForYoung rds_FRecover rds_FRuminate rds_FFeed
        CurrentRState = FEvade();
        m_StepDone = true;
        break;
    case rds_FRuminate:
        // Can return rds_FDisperse rds_FFeed rds_FRuminate rds_FDie
        CurrentRState = FRuminate();
        m_StepDone = true;
        break;
    case rds_FCareForYoung:
        // Can return rds_FFeed rds_FCareForYoung
        // Entering and leaving this state is controlled primarily by the fawn
        CurrentRState = FCareForYoung();
        m_StepDone = true;
        break;
    case rds_FInHeat:
        // Can return rds_FFeed rds_FInheat
        CurrentRState = FInHeat();
        break;
    default:
        g_msg->Warn("Roe_Female::Step(): No matching case!", "");
        g_msg->Warn("Tried to do ", double(CurrentRState));
        exit(1);
    }
}
//---------------------------------------------------------------------------

void Roe_Female::EndStep(void)
{
    /**
    Roe_Female::EndStep - increments the timestep counter, unless dead
    */
    if (CurrentRState == rds_FDeathState) return;

    timestep++;
}
//---------------------------------------------------------------------------

TRoeDeerStates Roe_Female::NewBehaviour(void)
{
    /**
    * Roe_Female::NewBehaviour - is the brain of the roe deer that takes over when there is no direct stimulus e.g. a threat or young needing care.\n
    */
    
    /** Some imperatives are handled first related to being in heat, seeking cover or caring for young */
    if (m_HeatCount == -1) InHeat();
    if (m_seekingcover) {
        SeekCover(2);
        m_MinInState[reu_SeekCover]++;
        m_StepDone = true;
        return rds_NewBehaviour;
    }
    if (m_Care == true) return rds_FCareForYoung; 
    /** IF the deer is dispersing then the probability of continuing is determined here next */
    if (m_Disperse == true) {
        double rand = 0;
        switch (m_DispCount)
        {
        case 4:
            rand = 0.4;
            break;
        case 50:
            rand = 0.60;
            break;
        case 60:
            rand = 0.80;
            break;
        default:
            if (m_DispCount >= 70) rand = 0.90;
            break;
        }
        if (rand > g_rand_uni())   //go to feed
        {
            m_Disperse = false;
            m_DispCount = 0;
        }
        else {
            return rds_FDisperse; 
        }
    }
    /* 
    * Feeding and rumination is then controlled here.
    * Once we start to feed the m_FeedCount in incremented and the m_RumiCount set to 0. 
    * The ruminate weight is based on a number of feeds. Have we fed a lot? 
    * If so the the rumination period is proportional.
    * Once ruminating the reverse happensan m_RumiCount is incremented and m_FeedCount is set to 0;
    * The m_LenthFeedPeriod is set depending on the current forage quality and determines the feeding count length during OnNewDay
    */
    // Decision probabilities are an array of pressures corresponding to Feed & Ruminate
    int weights[2] = { 1,1 };
    if (m_FeedCount > 0)
    {
        if (m_FeedCount >= m_LengthFeedPeriod) // Stop feeding
        {
            m_FeedCount = 0;
            weights[0] += 100; //need to ruminate is strong
        }
        else
        {
            // Not finished feeding
            weights[1] += 100 * (m_LengthFeedPeriod - m_FeedCount);
        }
    }
    if (m_RumiCount > 0)
    {
        if (m_RumiCount >= m_LengthRuminate) // m_LengthRuminate is set below
        {
            m_RumiCount = 0;
        }
        else 
        {
            weights[1] += 100 * m_LengthRuminate-m_RumiCount;
        }
    }
    // Diurnal activity
    if (m_OurPopulation->GetIsDayLight())  weights[1] += 100; // try to avoid activity during day, so rest/ruminate more
    // Make the decision
    int weight = weights[0] + weights[1];
    int choose = random(weight);
    if (choose < weights[0])
    {
        // Ruminate
        int month = m_OurLandscape->SupplyMonth();
        m_LengthRuminate = (int)(m_FeedCount * IntakeRate[month - 1] * 0.1);
        m_RumiCount++;
        return rds_FRuminate; //Ruminate
    }
    else {
        // Feed
        m_FeedCount++;
        return rds_FFeed;
    }
}
//---------------------------------------------------------------------------

/**
Roe_Female::Roe_Female - constructor for female object. Sets all attributes and creates groups for 1st
generation roes. It also removes a matured fawn from group and adds it as adult female. It also adds fawns
to mother's offspring list.
Calls CreateNewGroup(), AddToGroup().
*/
Roe_Female::Roe_Female(Deer_struct* p_data) :Roe_Adult_Base(p_data->x, p_data->y, p_data->size, p_data->age, p_data->L, p_data->Pop)
{
    ReInit(p_data);
}

void Roe_Female::ReInit(Deer_struct* p_data)
{
    Roe_Adult_Base::ReInit(p_data->x, p_data->y, p_data->size, p_data->age, p_data->L, p_data->Pop);
    Init(p_data);
}

void Roe_Female::Init(Deer_struct* p_data)
{
    //set pointers to NULL
    m_MyYoung.clear();


    m_My_Mate = NULL;
    Mum = p_data->mum;  //NULL for 1st generation roes
    if (p_data->group == -1)  //1st generation roe
    {
        //create a new group and add yourself to that group
        m_MyGroup = m_OurPopulation->CreateNewGroup(this);
        m_GroupUpdated = true;  //Jan. 1st
    }
    else //2nd generation or more
    {
        m_MyGroup = p_data->group;
        m_GroupUpdated = false;  //summer
        m_OurPopulation->AddToGroup(this, m_MyGroup); //matured fawn removes itself
        //from group and the new female is added again
    }
    //add yourself to mothers offspring list
    if (Mum != NULL)
    {
        Mum->AddYoungToList(this);
    }
    m_Age = p_data->age;
    m_Pregnant = false;
    m_Reserves = 30;
    m_Gestationdays = -1;
    m_Sex = false;
    m_HeatCount = -1;
    m_WasInHeat = false;
    m_LastState = rds_NewBehaviour; //Feed. Could be any safe state
    m_HaveRange = false;
    m_OptGroupSize = 1;  //as long as roe doesn't have a range
    m_NoOfYoung = 0;
    m_Care = false;
    m_NatalGroup = true;
    m_IsDead = false;
    m_DaysSinceParturition = -1;
    if (p_data->size == -1) //1st generation roe
    {
        double rand = 2.0 * g_rand_uni();
        m_Size = 16.000 + rand;  //size is set for the first generation

    }
    else m_Size = p_data->size;
    for (int i = 0; i < 12; i++) m_MinInState[i] = 0;

}
//---------------------------------------------------------------------------
Roe_Female::~Roe_Female()
{
}

//---------------------------------------------------------------------------

void Roe_Female::On_IsDead(Roe_Base* Someone, int whos_dead, bool fawn)
{
    /**
    Roe_Female::On_IsDead - governs a female objects "response" to death of another object. Checks
    who is dead (mom, female, mate) and sets them to NULL. If mother is dead, the "responding" female object
    deletes pointer to mom. If mate is dead, the female will go back into heat. If offspring is dead (both fawns
    and adults), they will be removed from offspring list.

    */
    switch (whos_dead)
    {
    case 0:   //mother
    {
        if (Someone == Mum)  Mum = NULL;    //delete pointer to mother
    }
    break;
    case 2:   //attending male
    {
        if (m_My_Mate == dynamic_cast<Roe_Male*> (Someone))
        {
            m_My_Mate = NULL;  //remove pointer to male
            CurrentRState = rds_FInHeat;
        }
    }
    break;
    case 3:  //offspring, both fawns and adult offspring
    {
        for (auto it = begin(m_MyYoung); it != end(m_MyYoung); ++it)
        {
            if ((*it) == Someone) {
                m_MyYoung.erase(it);
                break;
            }
        }
        //subtract 1 from NoOfYoung if Someone is a fawn from this year;
        if (fawn == true)
        {
            m_NoOfYoung--;
        }
    }
    break;
    }
}
//-----------------------------------------------------------------------------

void Roe_Female::On_ApproachOfDanger(int p_To_x, int p_To_y)
{
    /**
    Roe_Female::On_ApproachOfDanger - Determines whether to run or evade a threat depending on
    cover and distance to threath. In good cover, female will run if threat is less than 30 m
    away and else perform evasive behavior, In intermediate cover the flight distance is 50 m,
    and in poor cover it is 70 m.
    Returns states evade or run.
    Calls Cover(), DistanceTo().
    */
    m_danger_x = p_To_x;
    m_danger_y = p_To_y;
    //get cover
    int cov = Cover(m_Location_x, m_Location_y);
    //get distance to threat
    int dist = DistanceTo(m_Location_x, m_Location_y, p_To_x, p_To_y);

    if (cov >= CoverThreshold2) //good cover
    {
        if (dist >= 30)
        {
            CurrentRState = rds_FEvade;
        }
        else CurrentRState = rds_FRun;
    }
    else if (cov >= CoverThreshold1)  //intermediate cover
    {
        if (dist >= 50)
        {
            CurrentRState = rds_FEvade;
        }
        else  CurrentRState = rds_FRun;
    }
    else //poor cover
    {
        if (dist >= 70)
        {
            CurrentRState = rds_FEvade;
        }
        else  CurrentRState = rds_FRun;
    }
}
//---------------------------------------------------------------------------

/**
Roe_Female::FRun - Sets the female object to be in a running state - and checks if
safe after 1 time step in this state.
Calls Running().
*/
TRoeDeerStates Roe_Female::FRun() //State 19
{
   /**
   * Activity timeand energy accounting \n
   * Caring cost fixed by CostFemaleAct[reu_Run] * weight in kg
   * One unit of time spent
   */
    m_EnergyGained -= CostFemaleAct[reu_Run] * m_Size;
    m_MinInState[reu_Run] += 1; //add 1 to time spend in this state
    // End time energy accounting

    bool IsSafe = Running(m_danger_x, m_danger_y);
    if (m_IsDead)
    {
#ifdef __DEBUG_CJT1
        m_OurPopulation->g_RoeMortRec.AddToMort(trd_Roe_Female, rm_running);
#endif
        return rds_FDie; //Die
    }
    else if (!IsSafe)   //still not safe after 1 time step
    {
        return rds_FRun;  //Run
    }
    else
    {
        m_RecovCount++; //add 1 to time spend in recovery
        return rds_FRecover;   //recover
    }
}
//---------------------------------------------------------------------------
/**
Roe_Female::FRecover - Governs recovery after running from danger. Function counts
amount of time in state during the present period of recovovery (m_RecovCount) and
sets this to 0 when state is left. Female only transfers to this state if safe, whichs means
in good habitat. Returns states: feeding, recovery.

*/
TRoeDeerStates Roe_Female::FRecover()
{
    //Need to count amount of time in state during the present
    //period of recov. (m_RecovCount) and set this to 0 when state is left.
    //Roe only transfers to this state if safe (i.e. cover>=3) so we know it is in
    //good habitat
    /**
    * Activity timeand energy accounting \n
    * Caring cost fixed by CostFemaleAct[reu_Recover] * weight in kg
    * One unit of time spent
    */
    m_EnergyGained -= CostFemaleAct[reu_Recover] * m_Size;
    m_MinInState[reu_Recover] += 1; //add 1 to time spend in this state
    // End time energy accounting

    //allow roe the opportunity to return to this state after disturbance
    m_LastState = rds_FRecover;
    if (m_RecovCount <= AverageRecoveryTime)
    {
        m_RecovCount++;   //add 1 to time spend in recover and stay here
        return rds_FRecover;  //recover
    }
    else if ((m_RecovCount > AverageRecoveryTime) && (m_RecovCount < MaxRecoveryTime))
        //return to feed with certain probability
    {
        int stop = random(100);
        if (stop < 10)
        {
            m_RecovCount = 0;    //leaves state, so set counter=0
            m_FeedCount = 0;   //ruminating also takes place during recovery, so start
                             //from scratch in Feed
            return rds_NewBehaviour;
        }
        else
        {
            m_RecovCount++;   //add 1 to time spend in this state
            return rds_FRecover;
        }
    }
    else   //if still around after this long return to feeding
    {
        m_RecovCount = 0;
        return rds_NewBehaviour;
    }
}
//---------------------------------------------------------------------------
/**
Roe_Female::FEvade - checks direction to threath and takes 50 steps in opposite direction.
Returns to same state as before disturbance.
Calls DirectionTo(), NextStep()
*/
TRoeDeerStates Roe_Female::FEvade()  //Evade
{
    /**
   * Activity timeand energy accounting \n
   * Caring cost fixed by CostFemaleAct[reu_Evade] * weight in kg
   * One unit of time spent
   */
    m_EnergyGained -= CostFemaleAct[reu_Evade] * m_Size;
    m_MinInState[reu_Evade] += 1; //add 1 to time spend in this state
    // End time energy accounting
    int weight = 3;
    //get direction to threat
    int dir = DirectionTo(m_Location_x, m_Location_y, m_danger_x, m_danger_y);
    //take 50 steps in opposite direction
    NextStep(weight, ((dir + 4) & 0x07), 0);
    //return to same state as before disturbance
    return rds_NewBehaviour;
}
//---------------------------------------------------------------------------

/**
Roe_Female::On_InitCare - Returns true if female objects allows fawn to feed
(if her reserves are > 0) otherwise it returns false.
*/

bool Roe_Female::On_InitCare(Roe_Fawn* /* Fawn */)
{
    //mother will feed fawns if her reserves are > 0
    if (m_Size > m_OurPopulation->m_CriticalWeight_Females)
    {
        m_Care = true;
        CurrentRState = rds_FCareForYoung;
    }
    else
    {
        CurrentRState = rds_FFeed;
        m_Care = false;
    }
    return m_Care;
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_EndCare - Fawn ends it's care period - returns false.
*/
void Roe_Female::On_EndCare(Roe_Fawn* /* Fawn */) //fawn ends careperiod
{
    m_Care = false;
    CurrentRState = rds_FFeed;
}
//---------------------------------------------------------------------------

/**
Roe_Female::On_UpdateGroup - updates group status by changing range center to that
of group while storing old range center for when ghroup is dissolved and roe needs to
go back to own range.
*/
void Roe_Female::On_UpdateGroup(APoint a_centre)
{
    m_GroupUpdated = true;
    m_OldRange.m_x = m_RangeCentre.m_x;  //stores own rangecentre while in group range
    m_OldRange.m_y = m_RangeCentre.m_y;
    m_RangeCentre = a_centre;    //consider this as your rangecentre while in group
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_CanJoinGroup - Called by female object, and evaluates whether a new
female (found by searching for all females withing a range of the calling females location)
can  join a female group or not. Returns an int >0 if new roe can join group, returns 0 if
roe cannot join. First makes sure that new female is not a floater, since floaters
cannot have a group. Then looks at new females current group size and whether or not the new female is the
calling female's mother. If so, function returns group size. If new female does not have group
function returns 1. If new female has group, function checks if this group size is less
than optimal group size andd returns group size if so and 0 otherwise (cannot have larger group than
optimum group size)
Calls Supply_GroupSize()
*/
int Roe_Female::On_CanJoinGroup(Roe_Female* p_Female)
{
    //returns an int >0 if new roe can join group, Returns 0 if roe cannot join
    int answer = 0;
    if (!m_float)  //floaters don't have a group
    {
        size_t Group_Size = m_OurPopulation->Supply_GroupSize(m_MyGroup);
        //if sender is offspring
        for (auto it = begin(m_MyYoung); it != end(m_MyYoung); ++it)
        {
            if ((*it) == p_Female) //is on list
            {
                answer = (int)Group_Size;
            }
        }

        if (m_MyGroup == -1) answer = 1;
        else
        {
            if (Group_Size < m_OptGroupSize) answer = (int)Group_Size;
            else  answer = 0;   //don't want a larger group
        }
    }
    return answer;
}
//---------------------------------------------------------------------------
/**
Roe_Female::On_EndGroup - after group is dissolved, allows the deer to drift apart
*/

void Roe_Female::On_EndGroup()
{
    m_GroupUpdated = false;         //group not "active" anymore
    m_MyGroup = -1;
}
//---------------------------------------------------------------------------




