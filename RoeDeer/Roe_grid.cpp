
//Grid functions

#include <iostream>
#include <fstream>

#pragma hdrstop

#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "Roe_all.h"
#include "Roe_constants.h"

//-----------------------------------------------------------------------------
Roe_Grid::Roe_Grid(Landscape* L)
{
   m_LS = L;
   m_SizeOfCells = Cell_Size;
   m_extent_x = L->SupplySimAreaWidth() / Cell_Size;
   m_extent_y = L->SupplySimAreaHeight()/ Cell_Size;
   m_grid = new int[m_extent_x*m_extent_y];
   m_maxcells = m_extent_x*m_extent_y;
   for (int i = 0; i < m_maxcells; i++) m_grid[i] = 0;
}

//-----------------------------------------------------------------------------
Roe_Grid::~Roe_Grid()
{

}
//-----------------------------------------------------------------------------
bool Roe_Grid::AddGridValue(int a_x,int a_y,int a_value)
{
	// Debugged 22/07/2014 - warning on too large coordinates was not tripped.
	/** 
	* This is called with a reference to the gridcell, not coordinates! 
	* It returns the value at those grid coordinates. 
	*/
	if((a_x > m_extent_x )||(a_y > m_extent_y))
	{
		m_LS ->Warn("Roe_Grid::AddGridValue(): Coordinate outside grid!","");
		exit( 1 );
	}
	//write this value to the grid
	m_grid[a_y*m_extent_x + a_x] = a_value;
	return true;
}
//-----------------------------------------------------------------------------
/**
Roe_Grid::Supply_GridCoord - given a landscape x or y-coordinate the function returns the
corresponding x or y-entrance in the grid
*/

int Roe_Grid::Supply_GridCoord(int p_coor)
{ //given a landscape x or y-coordinate the function returns the
  //corresponding x or y-entrance in the grid

  double cell = (p_coor - Cell_HalfSize) * InvCell_Size;
  //to round to nearest int add 0.5 and round down
  return  (int) floor(cell + 0.5);

}
//-----------------------------------------------------------------------------
/**
Roe_Grid::Supply_GridValue - given a grid x and y-coordinate the function returns value/quality
for that coordinate
*/

int Roe_Grid::Supply_GridValue(int a_x, int a_y)
{
#ifdef __ROEDEER_DEBUG
    if ((a_x > m_extent_x) || (a_y > m_extent_y))
    {
        m_LS->Warn("Roe_Grid::Supply_GridValue(): Coordinate outside grid!", "");
        exit(1);
    }
    if ((m_extent_x > 500) || (m_extent_y > 500))
    {
        m_LS->Warn("Roe_Grid::Supply_GridValue(): Coordinate outside grid!", "");
        exit(1);
    }
#endif
  return m_grid[a_y*m_extent_x + a_x];
}
//----------------------------------------------------------------------------

