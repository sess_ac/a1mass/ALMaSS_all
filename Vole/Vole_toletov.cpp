#include "../Vole/Vole_toletov.h"

int vole_tole_move_quality(Landscape* m_TheLandscape, int x, int y)
{
	int poly_index = m_TheLandscape->SupplyPolyRefIndex(x, y);
	int score = -9999;
	TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementTypeFromVector(poly_index);
	switch (tole)
	{
	case tole_NaturalGrassDry: // 110
	case tole_NaturalGrassWet: // 110
	case tole_BeetleBank:
		score = 4;
		break;
	case tole_PermPasture: // 35
		if (m_TheLandscape->SupplyGrazingPressureVector(poly_index) > 0) score = 1; else score = 4;
		break;
	case tole_PermPastureLowYield: // 35
		if (m_TheLandscape->SupplyGrazingPressureVector(poly_index) > 0) score = 1; else score = 4;
		break;
	case tole_PermPastureTussocky:
		if (m_TheLandscape->SupplyGrazingPressureVector(poly_index) > 0) score = 2; else score = 4;
		break;
	case tole_Orchard: // 56
	case tole_Vineyard:
	case tole_OliveGrove:
		// Quality depends on when it was mown
		if (m_TheLandscape->SupplyJustMownVector(poly_index)) score = 0; else
		{
			if (m_TheLandscape->SupplyVegCoverVector(poly_index) < 0.8) score = 4;
			else if (m_TheLandscape->SupplyVegHeightVector(poly_index) <= 40) score = 4;
			else score = 4;
		}

		break;
	case tole_MownGrass: // 58
		// Quality depends on when it was mown
		if (m_TheLandscape->SupplyJustMownVector(poly_index)) score = 1; else score = 4;
		break;
	case tole_OrchardBand: // 57
		score = 4 - (int)(0.6 * m_TheLandscape->SupplyJustSprayedVector(poly_index));
		break;
	case tole_RoadsideSlope:
	case tole_Railway: // 118
	case tole_Pipeline:
	case tole_FieldBoundary: // 160
	case tole_RoadsideVerge: // 13
	case tole_HedgeBank:
	case tole_PermanentSetaside:
	case tole_YoungForest: // 60
	case tole_Vildtager:
	case tole_Wasteland:
	case tole_ForestAisle:
	case tole_SolarPanel:
		score = 4;
		break;
	case tole_Saltmarsh: // 95
	case tole_Marsh: // 95
	case tole_Scrub: // 70
	case tole_Heath:
	case tole_Hedges: // 130 (internal ALMaSS representation for Hedges)
	case tole_RiversidePlants: // 98
	case tole_RiceField:
	case tole_PitDisused: // 75
	case tole_MixedForest: // 60
	case tole_DeciduousForest: // 40
	case tole_MontadoCorkOak:
	case tole_MontadoHolmOak:
	case tole_MontadoMixed:
	case tole_AgroForestrySystem:
	case tole_CorkOakForest:
	case tole_HolmOakForest:
	case tole_OtherOakForest:
	case tole_ChestnutForest:
	case tole_EucalyptusForest:
	case tole_InvasiveForest:
	case tole_SwampForest:
	case tole_Copse:
		score = 3;
		break;
	case tole_RiversideTrees: // 97
	case tole_ConiferousForest: // 50
	case tole_BuiltUpWithParkland:
	case tole_Parkland:
	case tole_AmenityGrass:
	case tole_WoodyEnergyCrop:
	case tole_IndividualTree:
		score = 2;
		break;
	case tole_Garden: //11
	case tole_Track: // 123
	case tole_SmallRoad: // 122
	case tole_LargeRoad: // 121
	case tole_BareRock:
	case tole_Saltpans:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_SandDune:
	case tole_Churchyard:	//204
	case tole_Airport:
	case tole_Portarea:
		score = 1;
		break;
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Stream:
	case tole_HeritageSite:
	case tole_Building: // 5
	case tole_ActivePit: // 115
	case tole_Freshwater: // 90
	case tole_FishFarm:
	case tole_Pond:
	case tole_River: // 96
	case tole_Saltwater: // 80
	case tole_Coast: // 100
	case tole_StoneWall: // 15
	case tole_Fence: //225
	case tole_WindTurbine:
	case tole_Pylon:
		score = -1;
		break;
	case tole_Field: // 20 & 30
	case tole_UnsprayedFieldMargin:
	{
		TTypesOfVegetation  VType = m_TheLandscape->SupplyVegTypeVector(poly_index);
		switch (VType) {
		case tov_OCloverGrassSilage1:
		case tov_CloverGrassGrazed1:
		case tov_CloverGrassGrazed2:
		case tov_OCloverGrassGrazed1:
		case tov_OCloverGrassGrazed2:
		case tov_OPermanentGrassGrazed:
		case tov_PermanentGrassGrazed:
			score = 3 - m_TheLandscape->SupplyGrazingPressureVector(poly_index);
			break;
		default:
			double cover = m_TheLandscape->SupplyVegCoverVector(poly_index);
			double height = m_TheLandscape->SupplyVegHeightVector(poly_index);
			if ((cover > 0.80) && (height > 40)) score = 3;
			if ((cover < 0.50) || (height < 10)) score = 0;
			else score = 2;
		}
	}
	break;
	case tole_Foobar: // 999 !! type unknown - should not happen
	default:
		static char errornum[20];
		sprintf(errornum, "%d", m_TheLandscape->SupplyElementTypeFromVector(poly_index));
		m_TheLandscape->Warn("Vole_Base:MoveQuality: Unknown tole_type",
			errornum);
		exit(1);
	}
	return -1; // compiler happiness
}

int vole_tole_init_optimal(Landscape* m_TheLandscape, int x, int y)
{
	TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
	switch (tole)
	{
		//case tole_RoadsideVerge:
		//case tole_Railway:
		//case tole_FieldBoundary:
		//case tole_Scrub:
		//case tole_PermPastureLowYield:
		//case tole_PermPastureTussocky:
		//case tole_PermanentSetaside:
	case tole_NaturalGrassDry:
		//case tole_PitDisused:
	case tole_YoungForest:
		//case tole_Garden:
		//case tole_HedgeBank:
		//case tole_BeetleBank:
		return true;
	default:
		return false;
	}
}

int vole_tole_init_friendly(Landscape* m_TheLandscape, int x, int y)
{
	
	TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
	
	switch (tole)
	{
	case tole_Orchard:
	case tole_Vineyard:
	case tole_OliveGrove:
	case tole_OrchardBand:
	case tole_MownGrass:
	case tole_RoadsideVerge:
	case tole_RoadsideSlope:
	case tole_Railway:
	case tole_FieldBoundary:
	case tole_Scrub:
	case tole_Field:
	case tole_PermPastureLowYield:
	case tole_PermPastureTussocky:
	case tole_PermanentSetaside:
	case tole_PermPasture:
	case tole_NaturalGrassDry:
	case tole_NaturalGrassWet:
	case tole_PitDisused:
	case tole_YoungForest:
	case tole_Garden:
	case tole_HedgeBank:
	case tole_BeetleBank:
	case tole_SolarPanel:
		return true;
	default:
		return false;
	}

}

double vole_toletov_asses_habitat_score(Landscape* m_TheLandscape, int p_Polyref)
{
	TTypesOfLandscapeElement ElementType;
	TTypesOfVegetation VType;

	ElementType = m_TheLandscape->SupplyElementTypeFromVector(p_Polyref);
	
	double score = -9999;
	double Cover;

	switch (ElementType)
	{
	case tole_Railway: // 118
	case tole_FieldBoundary: // 160
	case tole_RoadsideVerge: // 13
	case tole_RoadsideSlope:
	case tole_HedgeBank:
	case tole_Hedges: // 130 (internal ALMaSS representation for Hedges)
	case tole_BeetleBank:
	case tole_NaturalGrassWet:
	case tole_NaturalGrassDry: // 110
		if (m_TheLandscape->SupplyVegHeightVector(p_Polyref) <= 5.0) score = 2.25;
		else score = 3.0;
		break;
	case tole_PermPasture: // 35
		if (m_TheLandscape->SupplyGrazingPressureVector(p_Polyref) > 0) score = 2.0; else score = 2.7;
		break;
	case tole_PermPastureLowYield:
		if (m_TheLandscape->SupplyGrazingPressureVector(p_Polyref) > 0) score = 2.1; else score = 2.8;
		break;
	case tole_PermPastureTussocky:
		if (m_TheLandscape->SupplyGrazingPressureVector(p_Polyref) > 0) score = 2.25; else score = 3.0;
		break;
	case tole_Orchard: // 56
	case tole_Vineyard:
	case tole_OliveGrove:
		// Quality depends on when it was mown
	{
		if (m_TheLandscape->SupplyVegCoverVector(p_Polyref) < 0.8) score = 2.5;
		else if (m_TheLandscape->SupplyVegHeightVector(p_Polyref) <= 40) score = 2.5;
		else score = 3.0;
	}
	if (m_TheLandscape->SupplyJustMownVector(p_Polyref)) score -= 2.0;
	break;
	case tole_MownGrass: // 58
		// Quality depends on when it was mown
		if (m_TheLandscape->SupplyJustMownVector(p_Polyref)) score = 2.5; else score = 3.0;
		break;
	case tole_OrchardBand: // 57
		// If spraying herbicide then quality depends on time since spraying. SupplyJustSprayed returns a counter that counts down to zero after spraying.
		// score = 3.0 - (0.5*m_TheLandscape->SupplyJustSprayedVector(p_Polyref)); // For herbicide
		score = 3.0;
		break;
	case tole_PermanentSetaside:
	case tole_YoungForest: // 60
		score = 2.2;
		break;
	case tole_Marsh: // 95
	case tole_Scrub: // 70
	case tole_Heath:
	case tole_RiversidePlants: // 98
	case tole_RiceField:
	case tole_PitDisused: // 75
	case tole_UnknownGrass:
	case tole_Wasteland:
		score = 1.5;
		break;
	case tole_MixedForest: // 60
	case tole_DeciduousForest: // 40
	case tole_MontadoCorkOak:
	case tole_MontadoHolmOak:
	case tole_MontadoMixed:
	case tole_AgroForestrySystem:
	case tole_CorkOakForest:
	case tole_HolmOakForest:
	case tole_OtherOakForest:
	case tole_ChestnutForest:
	case tole_EucalyptusForest:
	case tole_InvasiveForest:
	case tole_SwampForest:
	case tole_RiversideTrees: // 97
	case tole_ConiferousForest: // 50
	case tole_MaritimePineForest:
	case tole_StonePineForest:
	case tole_BuiltUpWithParkland:
	case tole_Parkland:
	case tole_Copse:
	case tole_AmenityGrass:
	case tole_MetalledPath:  //202
	case tole_WoodyEnergyCrop:// 59
	case tole_WoodlandMargin:
	case tole_IndividualTree:
		score = 1;
		break;
	case tole_Garden: //11
	case tole_Track: // 123
	case tole_SmallRoad: // 122
	case tole_LargeRoad: // 121
	case tole_BareRock:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_SandDune:
	case tole_Churchyard:
	case tole_HeritageSite:
	case tole_Saltmarsh:
	case tole_PlantNursery:
	case tole_WindTurbine:
	case tole_Pylon:
	case tole_Saltpans:
	case tole_Pipeline:
		score = 0;
		break;
	case tole_Building: // 5
	case tole_Carpark:
	case tole_ActivePit: // 115
	case tole_Freshwater: // 90
	case tole_Pond:
	case tole_Stream:
	case tole_Saltwater: // 80
	case tole_Coast: // 100
	case tole_StoneWall: // 15
	case tole_Fence: //225
	case tole_River:
	case tole_Airport:
	case tole_Portarea:
		score = -1;
		break;
	case tole_Field: // 20 & 30
	case tole_UnsprayedFieldMargin:
		VType = m_TheLandscape->SupplyVegTypeVector(p_Polyref);
		Cover = m_TheLandscape->SupplyVegCoverVector(p_Polyref);
		if (Cover < 0.20) score = 0;
		else switch (VType)
		{
		case tov_SeedGrass1:
		case tov_SeedGrass2:
			score = 2.25;
			break;
		case tov_BroadBeans:
		case tov_FieldPeas:
		case tov_FieldPeasSilage:
		case tov_WinterBarley:
		case tov_OWinterBarley:
		case tov_WinterRye:
		case tov_OWinterRye:
		case tov_Oats:
		case tov_AgroChemIndustryCereal:
		case tov_WinterWheat:
		case tov_WinterWheatStrigling:
		case tov_WinterWheatStriglingSingle:
		case tov_WinterWheatStriglingCulm:
		case tov_Triticale:
		case tov_OTriticale:
		case tov_WinterWheatShort:
		case tov_WWheatPControl:
		case tov_WWheatPToxicControl:
		case tov_WWheatPTreatment:
		case tov_WinterRape:
		case tov_SpringRape:
		case tov_OSpringBarley:
		case tov_SpringBarley:
		case tov_SpringBarleySpr:
		case tov_SpringBarleySeed:
		case tov_SpringBarleyStrigling:
		case tov_SpringBarleyStriglingSingle:
		case tov_SpringBarleyStriglingCulm:
		case tov_SpringBarleyCloverGrass:
		case tov_SpringBarleySilage:
		case tov_SpringBarleyPTreatment:
		case tov_SpringBarleySKManagement:
		case tov_OBarleyPeaCloverGrass:
		case tov_SpringBarleyPeaCloverGrassStrigling:
		case tov_OSBarleySilage:
		case tov_OWinterWheatUndersown:
		case tov_OWinterWheat:
		case tov_OFieldPeas:
		case tov_OFieldPeasSilage:
		case tov_OSpringBarleyPigs:
		case tov_OWinterRape:
		case tov_Maize:
		case tov_MaizeSilage:
		case tov_OMaizeSilage:
		case tov_OOats:
			score = 1;
			break;
		case tov_OGrazingPigs:
		case tov_FodderBeet:
		case tov_SugarBeet:
		case tov_OFodderBeet:
		case tov_Carrots:
		case tov_OCarrots:
		case tov_Potatoes:
		case tov_PotatoesIndustry:
		case tov_NoGrowth:
			score = 0;
			break;
			//  Needs some more thought
		case tov_OCloverGrassSilage1:
		case tov_FodderGrass:
		case tov_CloverGrassGrazed1:
		case tov_CloverGrassGrazed2:
		case tov_OCloverGrassGrazed1:
		case tov_OCloverGrassGrazed2:
		case tov_OSeedGrass1:
		case tov_OSeedGrass2:
			if (m_TheLandscape->SupplyGrazingPressureVector(p_Polyref) > 0) score = 1;
			else score = 2;
			break;
		case tov_OPermanentGrassGrazed:
		case tov_PermanentGrassGrazed:
		case tov_PermanentGrassLowYield:
			if (m_TheLandscape->SupplyGrazingPressureVector(p_Polyref) > 0) score = 2.2;
			else score = 2.6;
			break;
		case tov_PermanentGrassTussocky:
			if (m_TheLandscape->SupplyGrazingPressureVector(p_Polyref) > 0) score = 2.25;
			else score = 3.0;
			break;
		case tov_PermanentSetAside:
		case tov_OSetAside:
		case tov_SetAside:
			score = 3.0;
			break;
		case tov_OrchardCrop:
		case tov_YoungForest:
			// Quality depends on when it was mown
			if (m_TheLandscape->SupplyVegCoverVector(p_Polyref) < 0.8) score = 2.5;
			else if (m_TheLandscape->SupplyVegHeightVector(p_Polyref) <= 40) score = 2.5;
			else score = 3.0;
			break;
		default:
			//   Unknown vegetation type
			m_TheLandscape->Warn("Vole_Population_Manager::AssessHabitat - Unknown vegt  type", 0 );
			exit(1);
			break;
		}
		break;
	case tole_Foobar: // 999 !! type unknown - should not happen
	default:
		static char errornum[20];
		sprintf(errornum, "%d", ElementType);
		m_TheLandscape->Warn("Vole_PopulationManager:AssessHabitat: Unknown tole_type", errornum);
		exit(1);
	}

	return score;

}

bool vole_tole_assess_barrier(Landscape* m_TheLandscape, int p_Polyref)
{
	int Elem = m_TheLandscape->SupplyElementType(p_Polyref);

	switch (Elem) {
	case tole_MetalledPath:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_Saltmarsh:
	case tole_Stream:
	case tole_HeritageSite:
	case tole_Freshwater:
	case tole_Pond:
	case tole_River:
	case tole_Saltwater:
	case tole_LargeRoad:
	case tole_Building:
	case tole_Airport:
	case tole_Portarea:
	case tole_Coast:
	case tole_DeciduousForest:
	case tole_ConiferousForest:
		return false;
	default:
		break;
	}

	return true;

}