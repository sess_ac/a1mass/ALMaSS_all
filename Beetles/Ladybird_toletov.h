//
// Created by andrey on 5/4/21.
//

#ifndef ALMASS_LADYBIRD_TOLETOV_H
#define ALMASS_LADYBIRD_TOLETOV_H
#include <Landscape/ls.h>
# include <unordered_set>
#include <utility>
#include "Beetle_toletov.h"
using namespace std;
/**
using TToleList=std::unordered_set<TTypesOfLandscapeElement>;
using TTovList=std::unordered_set<TTypesOfVegetation> ;
*/
class LadybirdTovParams{
public:
    TTovList getList();
    explicit LadybirdTovParams(TTovList);
private:
    TTovList LadybirdTovList;
};
class LadybirdToleParams{
public:
    TToleList getList();
    explicit LadybirdToleParams(TToleList);
private:
    TToleList LadybirdToleList;
};


typedef struct LadybirdToleTovs_struct{
    LadybirdTovParams LadybirdProhibitedTovs{TTovList {
            tov_Undefined
    }};
    LadybirdToleParams LadybirdProhibitedToles{TToleList {
        tole_Saltwater,
        tole_Freshwater,
        tole_Canal,
        tole_Pond,
        tole_River,
        tole_Building,
        tole_Saltwater,
        tole_SandDune,
        tole_LargeRoad,
        tole_Canal,
        tole_FishFarm,
        tole_SolarPanel,
        tole_Stream,
        tole_UrbanNoVeg
    }};
    LadybirdTovParams LadybirdProhibitedTovsLarva{TTovList {
            tov_Undefined
    }};
    LadybirdToleParams LadybirdProhibitedTolesLarva{TToleList {
            tole_Saltwater,
            tole_Canal,
            tole_Building,
            tole_Freshwater,
            tole_River,
            tole_Pond,
            tole_Building,
            tole_Saltwater,
            tole_SandDune,
            tole_LargeRoad,
            tole_Canal,
            tole_FishFarm,
            tole_SolarPanel,
            tole_Stream,
            tole_UrbanNoVeg
    }};
    LadybirdTovParams LadybirdLongRangeForagingTovs{TTovList {
            tov_Carrots,
            tov_Potatoes,
            tov_BroadBeans,
            tov_OCarrots,
            tov_DKCabbages,
            tov_DKCerealLegume,
            tov_DKLegume_Peas,
            tov_DKLegume_Beans,
            tov_DKCarrots,
            tov_DKOCabbages,
            tov_DKOCerealLegume

    }};
    LadybirdToleParams LadybirdLongRangeForagingToles{TToleList {
            tole_PlantNursery,
            tole_Field
    }};
    LadybirdTovParams LadybirdAggregationTovs{TTovList {
            tov_Heath
    }};
    LadybirdToleParams LadybirdAggregationToles{TToleList {
            tole_ChestnutForest,
            tole_FarmForest,
            tole_Fence,
            tole_MixedForest,
            tole_InvasiveForest,
            tole_IndividualTree,
            tole_YoungForest,
            tole_ConiferousForest,
            tole_DeciduousForest,
            tole_EucalyptusForest,
            tole_CorkOakForest,
            tole_HolmOakForest,
            tole_OtherOakForest,
            tole_ChristmasTrees,
            tole_RiversideTrees,
            tole_RiversidePlants,
            tole_BareRock,
            tole_StonePineForest,
            tole_StoneWall,
            tole_RoadsideSlope,
            tole_RoadsideVerge
    }};
} TLadybirdToleTovs;
#endif //ALMASS_LADYBIRD_TOLETOV_H
