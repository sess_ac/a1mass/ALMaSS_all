\page Bembidion_page  ALMaSS Bembidion ODdox Documentation
 
<br>
<br>
\htmlonly
<CENTER>
Created by<br>
<br>
Chris J. Topping<br>
<small>Department of Wildlife Ecology<br>
National Environmental Research Institute, Aarhus University<br>
Grenaavej 14<br>
DK-8410 Roende<br>
Denmark<br>
<br>
15th February 2009,<br> Revised by Andrey Chuhutin 14th September 2021<br> Second revision by Andrey Chuhutin 1st June 2022 
</small>
<br>
</CENTER>
\endhtmlonly
<h2>Overview</h2>


The model organism is a spring breeding ground beetle (Carabidae) that occupies temperate agricultural landscapes representing a numerically large group of carabid beetles with common life-histories. We parameterised the model with data for Bembidion lampros Herbst (Carabidae), which is a Palearctic species that has been studied intensively as one of the most common beetles in European agroecosystems, and therefore has a well-described biology and natural history. The parameters are based on field and laboratory data on B. lampros from the literature. When necessary data are not available, we estimate parameters based on published data from other ground beetles with a similar life history.
<br>
\image html Bembidion_lampros.jpg "Figure 1: Bembidion lampros. Source: wikipedia. By Udo Schmidt, CC BY-SA 2.0" width=50%
<br>

## 1. Purpose


The Bembidon model represents a typical group of flightless beetles and represents a species with limited dispersal
power inhabiting the agricultural systems of norther temperate Europe. The main usage of this model is in evaluating
impacts of human land management on a species of this type in terms particularly agricultural management and changes
in landscape structure.
<br>
## 2. State variables and scales #
<br>

### State Variables ###

#### Beetles #
<br>
The Bembidion classes are based on the generic beetle classes:<br>
1. Beetle_Egg_List 
2. Beetle_Larvae 
3. Beetle_Pupae 
4. Beetle_Adult
<br>

Beetle classes are based on Beetle_Base, a class with the functionality common to all beetles:<br>
1. A variable containing the current beetle behavioural state: Beetle_Base::CurrentBState<br>
2. A pointer to the population manager: Beetle_Base::m_OurPopulation<br>
3. Attributes inherited from TAnimal, primarily location co-ordinates e.g. TAnimal::m_Location_x <br>

Individual Bembidion beetles has a set of common state variables each with its own value for the individual organism. These are:<br>
      
Type of beetle:<br>
- Bembidion_Egg_List <br>
- Bembidion_Larvae <br>
- Bembidion_Pupae <br>
- Bembidion_Adult<br>
<br>

Bembidion_Egg_List adds a single attribute to the base class attributes recording the day the egg is laid (inherited from Beetle_Egg_List):<br>
- Beetle_Egg_List::m_DayMade<br>

Bembidion_Larvae adds three attributes to the base class (inherited from Beetle_Larvae):<br>
- Beetle_Larvae::m_AgeDegrees recording the larval development in day degrees<br>
- Beetle_Larvae::m_LarvalStage being a record of the larval stage I-III<br>
- Beetle_Larvae::m_DayMade, as for the egg a record of the day the larva came into existence (egg hatch)<br>
<br>

Bembidion_Pupae adds two attributes to the base class (inherited from Beetle_Pupae):<br>
- Beetle_Pupae::m_AgeDegrees recording the larval development in day degrees<br>
- Beetle_Pupae::m_DayMade, as for the egg and larvae, a record of the day the pupa came into existence (larval pupation)<br>
<br>

Bembidion_Adult adds eight attributes to the base class (inherited from Beetle_Adult):<br>
- Beetle_Adult::m_negDegrees is used to keep track of the number of negative day degrees experienced
- Beetle_Adult::m_HibernateDegrees is used to keep track of the number of day degrees experienced in the spring which may trigger dispersal
- Beetle_Adult::OldDirection is used to record the last direction moved
- Beetle_Adult::m_EggCounter used to count the number of eggs produced
- Beetle_Adult::m_CanReproduce used to signal reproductive readiness
- Beetle_Adult::pList is a helper attribute when simulating movement
- Beetle_Adult::pPoint is a helper attribute when simulating movement
<br>

### Environment #
<br>
ALMaSS is spatially explicit, requiring a detailed two-dimensional map of the landscape and having a variable time-step (the 24 hours timesteps are chosen to be used for the beetle modelling).
Model landscape is a square raster representation of a landscape (e.g. 10 km x 10 km; 5 km x 5 km etc.) of a variable extent
at 1 m<SUP>2</SUP> resolution, with wrap around. State variables associated with each raster map that are used in the model are:<br>

- Id: Unique identification number associated with each polygon
- Habitat:  Land cover code for each polygon
- Farm unit:  If the land cover code is a field then it also has an owner associated with it. The owner represents the farmer who carries out farm management on their farm unit. 
- Vegetation Type - Vegetation type for the polygon (if any vegetation grows in the polygon).
<br>

The environment model in ALMaSS is weather dependent and the state variables associated with the weather sub-model that utilised by Bembidion are:<br>

- Temp: mean temperature on the given day (Celsius)
- Wind: mean wind speed on the given day (m/s)
- Rain:  total precipitation on given day (mm)
<br>

ALMaSS is a complex ecological simulation system \cite topping_almass_2003  with several sub-models, each with its unique set of state
variables. E.g. Farm management is a crucial part of ALMaSS, consisting of 3 elements: farm units, farm types, and crop husbandry plans.
There are currently at least 113 possible crops types in ALMaSS each with its own crop husbandry plan (e.g. WinterWheat). Each crop type is a nationally localised sub-model (NLPotatoes vs PLPotatoes vs DEPotatoes) and has its own set
of state variables. Documentation of the environment section is an ongoing process but the classes indicated above provide useful starting points.
Readers may refer to \cite topping_almass_2003 for more information on farm units, crop types, crop rotations, and farm types in ALMaSS.
<br>
<br>
      
## 3. Process overview and Scheduling #
      
<br>
The model considers beetle behaviour of all four life stages on a daily basis together with data driven daily
temperature and simulated changes in daily management of fields and the crops inside them by farmers. The model describes female individuals only. The eggs that hold male beetles are removed from the simulation prior to hatching. <br>
<br>
![Figure 2: Bembidion state diagram. NB All stages could transition to state Dying from any other behavioural state due to external messaging.](BemFig1.png)
<br>
<br>

### Basic process for all beetle stages #

<br>
The beetle life-stage classes are organised following a state-transition concept such 
that at any time each beetle object is in a specific behavioural state (given by the state variable Beetle_Base::CurrentBState) 
and exhibits behaviour associated with that state. As a result of this behaviour or external stimuli a
transition may occur to another state. 
In the descriptions below, behavioural states are described together with some general functionality common to more than one life-stage.
The methods (Begin_Step() (e.g Bembidion_Adult::BeginStep()), Step() (e.g. Bembidion_Adult::Step()) and EndStep() (not used in the Bembidion model) are an integral part of the general scheduling carried out by any agent based model, 
implemented in ALMaSS, and executed for each agent at each timestep. Each of the steps is executed iterating through Thearray_new that stores the pointers to all the living objects in the simulation. The methods DoFirst(), DoBefore() and DoAfter() are executed for each timestep only once and are implemented on the level of Population_Manager (Beetle_Population_Manager). See [Interconnections] (#interconnections) for more information. <br>
<br>

#### Begin Step #
One of three structural controls within each time-step of one day. The BeginStep is called
once for each object in each time-step. When all objects had completed this step, the Step function is called.
The BeginStep function is responsible for generating mortality and disturbance events, ageing and checking for
physiological death. (See e.g. Bembidion_Adult::BeginStep)<br>
<br>
#### Step #
This function is responsible for calling the behavioural state code for each object. Each beetle object
would have its Step function called at least once per time step. Each object in the list for that life stage would execute
the Step function. This would result in a return parameter denoting the behavioural state the beetle currently is in
together with a Boolean value to signal whether activity had been completed for that time step (TALMaSSObject::m_StepDone). If activity is not
completed then Step would be called again on the next pass through the list. When all objects have finished the Step
code (TALMaSSObject::m_StepDone is True for all objects), then the time step is completed for that beetle. (See e.g. Bembidion_Adult::Step)<br>
<br> -
-++-+++


### Reproduction #
<br>
Only beetles that are in preferred habitat (open agricultural fields) in spring may initiate reproduction.
The rate of egg production  \f$f\left(\mbox{egg}\right)\f$ is temperature dependent and is calculated as:<br>

\f[
f\left(\mbox{egg}\right) \left(T\right) = (T - T_0) \cdot C
\f]

\f$T\f$ =  temperature<br>
\f$T_0\f$ = lower threshold for egg production<br>
\f$C\f$ = egg production constant<br>
<br>
Once BeetleConstantClass::p_cfg_TotalNoEggs eggs have been produced, the beetle stops egg production, and subsequently dies.<br>
<br>
### Development #
<br>
Temperature dependent development is calculated for each developmental stage: egg, 1<SUP>st</SUP>, 2<SUP>nd</SUP>, 3<SUP>rd</SUP> instar and pupa
based on data from \cite jensen_effect_1990. Development \f$f\left(\mbox{dev}\right)\f$ is calculated as:<br>
\f[
f \left( \mbox{dev} \right) \left( T \right) = T - T_0 / L
\f]
\f$T\f$ =  temperature<br>
\f$T_0\f$ = lower threshold for development <br>
\f$L\f$ = duration of stage in day degrees<br>
<br>
In order to save time the day degree calculations for each day are carried out globally (since temperature in uniform in space in our model).See
Bembidion_Larvae::st_Develop & Bembidion_Population_Manager::DoFirst for details.<br>
<br>

### Mortality #
<br>
Beetles could die because they reached the end of their life span, from external events or due to density dependent
factors (i.e. competition). External factors causing mortality are either farm operations, mainly soil cultivation
techniques or temperature dependent winter mortality. The different cultivation methods are assigned
different probabilities of beetle mortality, which are implemented when the farm operation in question occurred (see
Farm management). <br>
The resolution of the landscape model is 1m<SUP>2</SUP>. Density dependence is introduced by limiting the number of beetles
that could be present on 3 x 3 m surrounding any beetle, to a fixed number of adults and larvae. Additional beetles are
removed from the simulation (died, assumed eaten). Background mortality is implemented for the egg stage and the
three larval instars. Beetles are assessed for density-dependent and background mortality once each simulation day.
In order to avoid bias due to concurrency problems, the order of assessment of beetles is randomised at each time
step.<br>
<br>

### Special behaviours for Bembidion_Adult #
<br>
The adult beetle starts life when it emerges from the pupa, most likely in the middle of a field. It then spends time
foraging until late summer when aggregation behaviour is triggered and then moves towards field margins and other
beetles. When it finds a suitable overwintering site, or when winter arrives the beetle enters hibernation. In spring
the beetles come out of hibernation and disperse away from field boundaries and other beetles. During this time they
will also lay eggs as they move. Once all the eggs are laid, or once the aggregation time has come, the beetle
dies. This behaviour is modelled using the following methods:<br>
<br>
Bembidion_Adult::st_Forage - this is the basic behaviour when not aggregating or hibernating. This state calls
Bembidion_Adult::MoveTo.<br>
<br>
Bembidion_Adult::MoveTo which carries out the actual movement and then calls Bembidion_Adult::CanReproduce. <br>
<br>
Bembidion_Adult::CanReproduce determines the number of eggs produced and their locations along the path moved over
the previous day. The number of eggs produced is also a function of temperature experienced see Bembidion_Population_Manager::DoFirst().
If all eggs are used up, then Bembidion_Adult::m_EggCounter becomes negative which ultimately kills the beetle in Bembidion_Adult::st_Aging.
Eggs that are successfully laid enter the simulation via a call to Bembidion_Adult::Reproduce. <br>
<br>
Bembidion_Adult::st_Aggregate calls Bembidion_Adult::MoveToAggr for aggregative movement.<br>
Bembidion_Adult::st_Hibernate accumulates day degrees from the beginning of the year and starts dispersal when
the sum exceeds a threshold or at the beginning of March. At the end of hibernation the beetle is allowed to reproduce via the Beetle_Adult::m_CanReproduce attribute.<br>
<br>
Bembidion_Adult::st_Dispersal initiates the first call to Bembidion_Adult::MoveTo to accomplish the first move from the hibernation site towards the fields and 
moves the next (foraging) state (see Bembidion_Adult::st_Forage).<br>
<br>

### Scheduling #

Beetle objects interact with each other and their environment during the simulation run. Since the time-step of the model is one day,
this means that interactions need to be integrated into a single daily time-step.
That gives rise to potential concurrency problems that are solved using a hierarchical structure to execution.
The structure ensures that objects receive information and interact in a sensible order. For the beetle objects eggs are the first to perform the time-step, followed by larva, pupae and, finally, adults.
Outside the need for this kind of interaction control the order of execution of individual objects is random.
Random execution order prevents the generation of bias between individuals, e.g. where the same individual would always obtain food first
just by virtue of its position in a list of individuals (see [Interconnections] (#interconnections) for more information).<br>
<br>

# Design

## 4. Design Concepts #

### Emergence #

Emergent properties are patterns of population sizes and spatial distribution of beetles as a result of reproductive
and dispersal processes interacting with land management mortalities.
### Adaptation #

Adaptation is not used in the model.
### Fitness #

Fitness is incorporated in terms of successful reproduction. In this version there is no genetic fitness concept only
phenotypic stochasticity.

### Prediction #

Prediction is not used in the Bembidion model.
### Sensing #

Beetles can sense other beetles in close proximity of themselves (+/- 3m) and can sense the type and suitability of
habitat in which they are in. Beetles can also sense the suitability of their current location for hibernation.
Management induced mortality is also sensed by the beetle, although conceptually this is event driven in terms of
implementation it is sensing (see e.g. Bembidion_Adult::OnFarmEvent).<br>

### Interaction #

Interactions between model beetles are primarily through density-dependent mortalities
<br>
Interactions between beetles and their environment  is based on the information flow from the environment to the beetle. 
The information content is of three types:
- habitat type, vegetation structure (height and density), vegetation type, palatability
- information on management, e.g. if a field is cut for silage
- temperature data for development calculations.
The first type of information is used to determine movement and feeding behaviour, the second resulted in a probabilistic mortality dependent
upon life-stage and type of management. The third type is managed as at a higher hierarchical level by the Bembidion_Population_Manager which
circumvented the need for individual development calculations (see [Interconnections] (#interconnections) for more information) <br>
<br>
### Stochasticity #

Stochasticity is used extensively within the model for applying probabilities primarily related to mortality and movement.<br>
<br>
### Collectives #

The class Bembidion_Egg_List is a collective used to optimise the code for speed by preventing multiple identical
calculations of day degree changes.<br>
All beetles are managed as lists of objects by the Bembidion_Population_Manager. This object is responsible for
initiating daily activity for all beetles, and for output of population information as required.<br>

### Observation #

Since each beetle object is managed through the Bembidion_Population_Manager, it is relatively easy to extract
information from a single individual, or any set of objects. Information is collected by writing C++ probes that
simply sort through the list of beetles and extract information from those matching given criteria, e.g. output of
the numbers of beetles within a certain area  (see e.g. Population_Manager::Probe). Hence information is easily
available at the population and individual level.<br>
<br>

## 5. Initialisation #

The model is initiated with a starting population of Bembidion_all::cfg_beetlestartnos adult beetles, each
randomly placed on the landscape.
<br>

## 6. Inputs #

Input parameters vary with each application of the ALMaSS Bembidion model but cover these basic types: 
1. Landscape structure. Implemented as a GIS map, converted to ALMaSS data structures. Input fields consist of landscape element type (e.g. field, road, hedge) and extent. 
2. Climate data on a daily basis. This data is looped if the simulation is longer than the data set. Data used in the model is mean daily temperature, daily precipitation and mean daily wind-speed. 
3. Farming information depends on the type and location of the farms being modelled and is implemented as a crop rotation and a set of management plans covering each crop used. These plans determine the timing and order of farm events, but these are in turn dependent on weather and farmers choices (both stochastic and mechanistic).
4. Other values related to specific application e.g. pesticide usage, toxicity, or timing of population crashes (e.g. see Bembidion_Population_Manager::Catastrophe)
5. Specific ecological variables. These should be fixed during the parameterization and fitting processes of model development and ought not to be modified subsequently. Values are given by Table 1:

<br>
![Table 1: Default parameters and sources for the Bembidion model.](BemTable1.png)
<br>



## 7. Interconnections #  {#interconnections}

Apart from connections to the Landscape, Farm and Crop classes, in common with all other ALMaSS models, the bembidion agent
classes are administered by a population manager class, in this case Bembidion_Population_Manager.
The Bembidion_Population_Manager is descended from Population_Manager and performs the role of an auditor in the
simulation.
<br>

###	The Beetle Population Manager #

The population manager keeps track of individual beetle objects (eggs, larvae, pupae and adult females) in the model
and initiates their daily behaviour via management of lists. The Population_Manager::Run method is the main daily
behaviour control functions splitting each day into 5 steps as follows: <br>
1. At the start of each time step all beetle objects within each list are randomised to avoid concurrency problems.
2. After randomisation the BeginStep of each object in each list is called.
Hierarchical execution starts with the list of infant objects and ends with the execution of female objects. 
3. The Step function for each object is initiated in the same way as the BeginStep. The Step code differs from BeginStep
and EndStep in that it is repeatedly called by the population manager until all beetles signal that they have finished Step behaviours
for that day. This allows linking of behaviours and more flexibility in terms of daily activities.
4. Any objects representing beetles that died or are redundant due to hatching or emergence are removed from
the simulation and any output requests handled.
<br>

Beetle objects created in the simulation during these steps are initiated by the population manager together with their initial
parameter values.<br>
<br>

Bembidion_Population_Manager is also responsible for precalcuation of many of
the temperature or density-dependent pre-requisites for the simulation in Bembidion_Population_Manager::Init.<br>
In order to optimise speed, the locations of beetles are mapped using PositionMap ( Bembidion_Population_Manager::m_LarvaePosMap
and Bembidion_Population_Manager::m_AdultPosMap) and updated constantly.
These data constructs are used to rapidly determine local densities. Movement is optimised using a MovementMap
data structure (Bembidion_Population_Manager::m_MoveMap) rather than interrogate Landscape directly via interface methods.<br>
<br>
## 8. References #

\cite bilde_life_2004 Bilde, T., Axelsen, J.A. & S. Toft, 2000. The value of Collembola from agricultural soils as food for a generalist predator. Journal of Applied Ecology, 37: 672-683.<br>
\cite jensen_effect_1990 Boye Jensen, L., 1990. Effects of temperature on the development of the immature stages of Bembidion lampros (Coleoptera: Carabidae). Entomophaga, 35: 277-281. <br>
\cite coombes_dispersal_1986 Coombes, D.S. & N.W. Sotherton, 1986. The dispersal and distribution of polyphagous predatory Coleoptera in cereals. Annals of Applied Biology, 108: 461-474. <br>
\cite ekbom_polyphagous_1985 Ekbom, B.S. & S. Wiktelius, 1985. Polyphagous arthropod predators in cereal crops in central Sweden, 1979-1982. Journal of Applied Entomology, 99: 433-442. <br>
\cite hance_effect_1987 Hance, T. & C. Gregoire-Wibo, 1987. Effect of agricultural practices on carabid populations. Acta Phytopathologica et Entomologica Hungarica, 22: 147-160. <br>
\cite kromp_carabid_1999 Kromp, B., 1999. Carabid beetles in sustainable agriculture: a review on pest control efficacy, cultivation impacts and enhancement. Agriculture, Ecosystem and Environment, 74: 187-228. <br>
\cite kromp_entomological_1997 Kromp, B. & P. Meindl, 1997. Entomological Research in Organic Agriculture - Selected papers from the European Workshop on Entomological Research in Organic Agriculture, Austrian Federal Ministry of Science and Research, Vienna, March 1995. Biological Argriculture & Horticulture, 15: III-IV. <br>
\cite lys_surface_1991 Lys, J.A. & W. Nentwig. 1991. Surface activity of carabid beetles inhabiting cereal fields. Seasonal phenology and the influence of farming operations on five abundant species. Pedobiologia, 35: 129-138. <br>
\cite mitchell_ecology_1963 Mitchell, B. 1963b. Ecology of two carabid beetles, Bembidion lampros (Herbst) and Trechus quadristriatus (Schrank) II. Studies on populations of adults in the field, with special reference to the technique of pitfall trapping. Journal of Animal Ecology, 32: 377-392. <br>
\cite petersen_temperature_1996 Petersen, M.K., 1997. Life histories of two predaceous beetles, Bembidion lampros and Tachyporus hypnorum, in the agroecosystem. Ph.D. thesis, Swedish University of Agricultural Sciences, Sweden. <br>
\cite petersen_life_1997 Petersen, M.K., Ekbom, B. & H.P. Ravn, 1996. Temperature dependent winter survival of Bembidion lampros and Tachyporus hypnorum. Journal of Insect Physiology, 42: 997-1005. <br>
\cite purvis_observations_2020 Purvis, G., Carter, N. & W. Powell, 1988. Observations on the effects of an autumn application of a pyrethroid insecticide on non-target predatory species in winter cereals. Pages 153-166 in R. Cavarello and K.D. Sunderland (eds.). Integrated crop protection in cereals. A.A. Balkema. Rotterdam, Brookfield. <br>
\cite riedel_adult_1998 Riedel, W. & T. Steenberg, 1998. Adult polyphagous coleopterans overwintering in cereal boundaries: winter mortality and susceptibility to the entomopathogenic fungus Beauveria bassiana. Biocontrol, 43: 175-188. <br>
\cite stinner_arthropods_1990 Stinner, B.R. & G.J. House, 1990. Arthropods and other invertebrates in conservation-tillage agriculture. Annual Review of Entomology, 35: 299-318. <br>
\cite vickerman_effects_1977 Vickerman, G.P & K.D. Sunderland, 1977. Some effects of Dimethoate on arthropods in winter wheat. Journal of Applied Ecology, 14: 767-777. <br>
<br>

