//
// Created by andrey on 1/27/21.
//

/* TESTING APHIDS MODEL
 * This is a simplistic stub model of Aphids_debug which is designed to run together with ladybird model for the development/debugging purposes
 * The distribution of Aphids_debug is stationary and defined in the start of the simulation.
 * All the model does it provides a functionality to interact with the ladybird model in development.
 *
 *
 *
 * !!!! Check it: probably I do not need to do all this mess with pointers here, since blitz is smart enough to allocate and deallocate memory
 * !!!! The only place where pointer is needed is Aphids_Population_Manager::getAphidsMap()
 * */
#include "AphidsD_all.h"
#include "../Landscape/ls.h" // included in h file
#include "../BatchALMaSS/PopulationManager.h"


AphidsD_Population_Manager::AphidsD_Population_Manager(Landscape* p_L)
        : Population_Manager(p_L){
    int height=p_L->SupplySimAreaHeight();
    int width=p_L->SupplySimAreaWidth();
    p_AphidsMap=std::make_unique<blitz::Array<long,2>> (width/cfg_AphidCellWidth.value(), height/cfg_AphidCellHeight.value());
    Init();
}

AphidsD_Population_Manager::~AphidsD_Population_Manager() {
    ;
}

void AphidsD_Population_Manager::Init() {
    if (cfg_ConstantAphidsDistribution.value()){
        initConstantAphidsDistribution((int) (cfg_ConstantAphidsDistributionValue.value()));
    }

    m_SimulationName = "AphidsD";
}

void AphidsD_Population_Manager::initConstantAphidsDistribution(int value) {

    *p_AphidsMap=value;

}

void AphidsD_Population_Manager::setAphidsNumber(int x, int y, int number) {
    (*p_AphidsMap)(x, y) = number;
}

std::shared_ptr<blitz::Array<long,2>> AphidsD_Population_Manager::getAphidsMap() {
    //std::shared_ptr<blitz::Array<int,2>> s_ptr{(*p_AphidsMap).copy()}
    return std::make_shared<blitz::Array<long,2>> ((*p_AphidsMap).copy());//TODO: note this is a copy-- it means,
                                                                                // the ladybirds don't really influence the number of aphids here
}

int AphidsD_Population_Manager::getAphidsNumber(int x, int y) {
    return (*p_AphidsMap)(x, y);
}

int AphidsD_Population_Manager::deductAphidsNumber(int x, int y, int number_to_deduct) {
    (*p_AphidsMap)(x, y) = (*p_AphidsMap)(x, y) - number_to_deduct;
    return 0;
}

int AphidsD_Population_Manager::getAphidsCellWidth() {
    return cfg_AphidCellWidth.value();
}

int AphidsD_Population_Manager::getAphidsCellHeight() {
    return cfg_AphidCellHeight.value();
}

void AphidsD_Population_Manager::Run(int NoTSteps) {
/*
    if (m_TheLandscape->SupplyDayInYear()==2){
        initConstantAphidsDistribution((int) (cfg_ConstantAphidsDistributionValue.value()));
        for(int x=50; x<350; x++){
            for (int y=50; y<350; y++){
                TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
                //if (tole==tole_Field) { // we put aphids only on the field
                    setAphidsNumber(x, y, cfg_ConstantAphidsDistributionValue.value());
                //}
                tole = m_TheLandscape->SupplyElementType(1000-x, 1000-y);
                //if (tole==tole_Field) { // we put aphids only on the field
                    setAphidsNumber(1000-x, 1000-y, 200000); //checking the upper limit
                //}
            }
        }
    }
    if (m_TheLandscape->SupplyDayInYear()==142){
        initConstantAphidsDistribution((int) (0));
        for(int x=250; x<550; x++){
            for (int y=50; y<350; y++){
                TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);
             //   if (tole==tole_Field){ // we put aphids only on the field
                    setAphidsNumber(x,y,cfg_ConstantAphidsDistributionValue.value());
               // }
                tole = m_TheLandscape->SupplyElementType(1000-x, 1000-y);
                //if (tole==tole_Field) { // we put aphids only on the field
                    setAphidsNumber(1000-x, 1000-y, cfg_ConstantAphidsDistributionValue.value());
               // }
            }
        }
    }
*/
}
