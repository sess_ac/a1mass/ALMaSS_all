/*
*******************************************************************************************************
Copyright (c) 2016, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Newt_Population_Manager.h
\brief <B>The header file for the newt population manager classes</B>
*
Version of  1 January 2016 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef Newt_Population_ManagerH
#define Newt_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class Newt;

//------------------------------------------------------------------------------

/**
\brief
Used for creation of a new Newt object
*/
struct struct_Newt
{
	 /** \brief The newt age */
	 int age = 0;
	 /** \brief x-coord */
	 int x = 0;
	 /** \brief y-coord */
	 int y = 0;
	 /** \brief Born x-coord */
	 vector<unsigned> pondrefs;
	 /** \brief Newt weight */
	 double weight;
	 /** \brief Landscape pointer */
	 Landscape* L = nullptr;
	 /** \brief Newt_Population_Manager pointer */
	 Newt_Population_Manager * NPM = nullptr;
	 /** \brief Holds a reproductive effect flag */
	 bool reproinhib;
};

/**
\brief
The class to handle all newt population related matters
*/
class Newt_Population_Manager : public Population_Manager
{
	/**
	* This class takes care of newt object creation and any list management methods. This is handled by
	* In addition the newt population managner must control the breeding season flag, done in #SetUnsetBreedingSeason
	* 
	*/
public:
// Methods
   /** \brief Newt_Population_Manager Constructor */
   Newt_Population_Manager(Landscape* L);
   /** \brief Newt_Population_Manager Destructor */
   virtual ~Newt_Population_Manager (void);
   /** \brief Method for creating a new individual Newt */
   void CreateObjects(int ob_type, TAnimal *pvo, struct_Newt* data, int number);
   /** \brief Returns whether it is breeding season or not */
   int IsBreedingSeason() {
	   return m_BreedingSeasonFlag;
   }
   /** \brief Calculate the daily background mortality chance based on weather */
   void SetFreeLivingMortChance();
   /** \brief Returns the value from the precalulated m_NewtEgg_DDTempRate to get effective day degrees */
   double GetEggDDRateTransformation(double a_temperature) {
	   /** 
	   * Uses the precalulated m_NewtEgg_DDTempRate array for speed optimisation. Rounds to the nearest degree. 
	   */
	   int temp = int(round(a_temperature));
	   if (temp <= 1) return 0.0;
	   if (temp > 29) return 0.0;
	   return m_NewtEgg_DDTempRate[temp]*a_temperature;
   }
   /**   \brief Special output functionality */
   virtual void TheAOROutputProbe();
   /** \brief Add a new adult to the stats record */
   void RecordAdultProduction(int a_adult);
   /** \brief Add a new egg production to the stats record */
   void RecordEggProduction(int a_eggs);
   /** \brief Add a new metamorphosis time to the stats record */
   void RecordMetamorphosis(int a_age);
   /** \brief Initialises output mean and variance for meatamorphosis times this year */
   void InitOutputMetamorphosisStats();
   /** \brief Output mean and variance for meatamorphosis times this year */
   void OutputMetamorphosisStats();
protected:
// Attributes
	/** \brief  Set to 0 when it is newt breeding season, then records the number of days after */
	int m_BreedingSeasonFlag;
	/** \brief An array to hold a precalulated day-degree rate transformation for egg development */
	double m_NewtEgg_DDTempRate[31];
	/** \brief A class for holding the stats on newt metamorphosis development times */
	SimpleStatistics m_NewtMetamorphosisStats;
	/** \brief A class for holding the stats on newt egg production */
	SimpleStatistics m_NewtEggProdStats;
	/** \brief A class for holding the stats on newt adult production */
	SimpleStatistics m_NewtAdultProdStats;
	// Methods
   /** \brief  Things to do before anything else at the start of a timestep  */
	virtual void DoFirst();
	/** \brief  Controls when it is breeding season  */
	void SetUnsetBreedingSeason();
};

#endif