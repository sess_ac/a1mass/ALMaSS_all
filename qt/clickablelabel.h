#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QWidget>
#include <Qt>
#include <qevent.h>

class QResizeEVent;

class ClickableLabel : public QLabel { 
    Q_OBJECT 

public:
    explicit ClickableLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~ClickableLabel();

signals:
    void clicked(QMouseEvent* event);

protected:
    void mousePressEvent(QMouseEvent* event);
    void resizeEvent(QResizeEvent* event);

};

#endif // CLICKABLELABEL_H
