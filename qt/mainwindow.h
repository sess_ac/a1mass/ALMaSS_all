#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <iostream>
#include "habitatmap.h"
#include <QActionGroup>
#include "mwhivetool.h"



class Landscape;
class Population_Manager;
class TPredator_Population_Manager;
class Hunter_Population_Manager;
class Disturber_Population_Manager;
class QCheckBox;
class QwtPlotBarChart;
class QMouseEvent;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int getSubValue();
    int getCurrentSpecies();
    bool isAnimalChecked(const unsigned i);
    Population_Manager_Base* getPopManager();
    shared_ptr<Population_Manager_Base> getPopManager_smart();

private:
    Ui::MainWindow *ui;

protected:
    QActionGroup * viewGroup;

    HabitatMap habMap;
    bool m_Paused;
    bool m_StopSignal;
    int m_torun;
    bool m_SimInitiated;
    char* m_files[100];
    char* m_Predfiles[100];
    char m_ResultsDir[255];
    char m_PredResultsDir[255];
    int m_time;
    int m_Year;
    int m_Steps;
    int m_NoProbes;
    bool m_ProbesSet;
    int m_NoPredProbes;
    Landscape* m_ALandscape;
    PopulationManagerList* m_PopulationManagerList;
    std::shared_ptr<Population_Manager_Base> m_AManager;
    std::shared_ptr<Hunter_Population_Manager> m_Hunter_Population_Manager;
    std::shared_ptr<Disturber_Population_Manager> m_Disturber_Population_Manager;
    std::shared_ptr<TPredator_Population_Manager> m_PredatorManager;
    bool m_BatchRun;
    int m_currentSpecies;
    int m_lifestagelists;
    MWHiveTool* hivetool;
    bool drawingMap;

    QActionGroup* clickGroup;
    QVector<double> m_plot_x;
    vector<QVector<double>> m_plot_y;
    double m_plotmax_x, m_plotmax_y;
    bool m_reploty;

  // Methods

    void MakePlots(int a_number_series);
    void Replot1(int a_x);
    void DataInit( );
    void Go();
    int GetTimeToRun();
    void RunTheSim();
    bool ParseBatchLine( char* a_line );
    //void RunBatch1Click();
    void CloseDownSim();
    bool ReadBatchINI(const char* filename);
    void GetProbeInput_ini();
    bool CreatePopulationManager();
    void CreateLandscape();
    bool DumpMap(int day);
    bool GetSimInitiated() { return m_SimInitiated; }
    bool IsAnimalChecked(int a_index);
    void setUpForm();
    void UpdateGraphics(int withinadaystep = 0);
    void ShowAnimals(HabitatMap * hm);
    void DisplayMapInfo(const unsigned x, const unsigned y);
    void FindAnimal(const unsigned x, const unsigned y);
    void setUIMapSize();

private slots:
    void on_actionStart_triggered();
    void on_actionPause_triggered();
    void on_actionContinue_triggered();
    void on_actionStop_triggered();
    void on_actionPhoto_triggered();
    void on_stepSpinBox_valueChanged(const QString &arg1);
    void on_stepSpinBox_valueChanged(int arg1);
    void on_actionMap_toggled(bool arg1);
    void on_actionVeg_Type_toggled(bool arg1);
    void on_actionBiomass_toggled(bool arg1);
    void on_actionFarm_ownership_toggled(bool arg1);
    void on_actionPesticide_Load_toggled(bool arg1);
    void on_actionGoose_Numbers_toggled(bool arg1);
    void on_actionGoose_Food_Resource_toggled(bool arg1);
    void on_actionGoose_Grain_Resource_toggled(bool arg1);
    void on_actionGoose_Grazing_Resource_toggled(bool arg1);
    void on_actionSoil_Type_Rabbits_toggled(bool arg1);
    void on_actionHunters_Locations_toggled(bool arg1);
    void on_actionPollen_toggled(bool arg1);
    void on_actionNectar_toggled(bool arg1);
    void on_animalComboBox_currentIndexChanged(int index);
    void on_actionMap_triggered();
    void on_map_clicked(QMouseEvent * e);
    void on_map_clicked2();
    void on_actionHive_Tool_triggered();
    void on_actionPause_toggled(bool arg1);
    void on_actionMap_Update_toggled(bool arg1);
    void on_actionExit_triggered();
    void on_actionExit_triggered(bool checked);
};

#endif // MAINWINDOW_H
