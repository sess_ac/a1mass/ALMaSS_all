#include "../Partridge/Partridge_toletov.h"

double partridge_tole_habitat_eval(Landscape* landscape, int polyref )
{
    TTypesOfLandscapeElement tole = landscape->SupplyElementType(polyref);

    switch (tole) {

        // Terrible stuff
    case tole_IndividualTree:
    case tole_PlantNursery:
    case tole_WindTurbine:
    case tole_WoodyEnergyCrop:
    case tole_WoodlandMargin:
    case tole_Pylon:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_Building:
    case tole_UrbanNoVeg:
    case tole_AmenityGrass:
        return -2.0;

        // Bad stuff.
    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Churchyard:
    case tole_Saltmarsh:
    case tole_Stream:
    case tole_HeritageSite:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_Garden:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
    case tole_Pond:
    case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_BareRock:
    case tole_Parkland:
    case tole_UrbanPark:
    case tole_BuiltUpWithParkland:
    case tole_SandDune:
    case tole_Copse:
    case tole_Wasteland:
    case tole_UrbanVeg:
        return -1.0;

        // Questionable.
    case tole_NaturalGrassWet:
    case tole_StoneWall:
    case tole_Fence:
    case tole_Hedges:
    case tole_Marsh:
    case tole_PitDisused:
    case tole_OrchardBand:
        return 0.0;

        // OK stuff
    case tole_RoadsideSlope:
    case tole_PermPasture:
    case tole_PermPastureLowYield:
        return 2.0;
        // Good Stuff
    case tole_PermPastureTussocky:
    case tole_RoadsideVerge:
    case tole_Railway:
    case tole_FieldBoundary:
    case tole_PermanentSetaside:
    case tole_NaturalGrassDry:
    case tole_YoungForest: // ?
    case tole_Heath:
    case tole_Orchard:
    case tole_MownGrass:
    case tole_Scrub:
    case tole_Vildtager:
        return cfg_terr_qual_good.value();

        // Really good stuff!
    case tole_BeetleBank:
        return (double)cfg_nest_hedgebank1.value();
    case tole_HedgeBank:
    {
        int st = landscape->SupplyElementSubType(tole);
        switch (st) {
        case 0:
            return (double)cfg_nest_hedgebank0.value();
        case 1:
            return (double)cfg_nest_hedgebank1.value();
        case 2:
            return (double)cfg_nest_hedgebank2.value();
        default:
            return 100;
        }
    }
        break;
        // Variable, depending on vegetation and condition of same.
    case tole_Field:
    case tole_UnsprayedFieldMargin:
        return (double) partridge_tov_habitat_eval_field(landscape,polyref);
        
    default:
        g_msg->Warn(WARN_BUG, "partridge_tole_habitat_eval(): Unknown tole type", landscape->PolytypeToString(tole));
        exit(1);
    }

}

double partridge_tole_habitat_eval2(Landscape* landscape, int polyref) {

    TTypesOfLandscapeElement tole = landscape->SupplyElementType(polyref);

    switch (tole)
    {

        // Terrible stuff
    case tole_IndividualTree:
    case tole_WindTurbine:
    case tole_WoodyEnergyCrop:
    case tole_WoodlandMargin:
    case tole_Pylon:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_Building:
    case tole_UrbanNoVeg:
        return -2.0;

        // Bad stuff.
    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Churchyard:
    case tole_Saltmarsh:
    case tole_Stream:
    case tole_HeritageSite:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_Garden:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
    case tole_Pond:
    case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_BareRock:
    case tole_AmenityGrass:
    case tole_Parkland:
    case tole_UrbanPark:
    case tole_BuiltUpWithParkland:
    case tole_SandDune:
    case tole_Copse:
    case tole_PlantNursery:
    case tole_Wasteland:
    case tole_UrbanVeg:
        return -1.0;

        // Questionable.
    case tole_StoneWall:
    case tole_Fence:
    case tole_Hedges:
    case tole_Marsh:
    case tole_PitDisused:
        return 0.0;

        // OK stuff
    case tole_NaturalGrassWet:
    case tole_RoadsideSlope:
    case tole_PermPasture:
    case tole_PermPastureLowYield:
        return 2.0;

    case tole_PermPastureTussocky:
    case tole_RoadsideVerge:
    case tole_Railway:
    case tole_FieldBoundary:
    case tole_PermanentSetaside:
    case tole_NaturalGrassDry:
    case tole_YoungForest: // ?
    case tole_Heath:
    case tole_Orchard:
    case tole_OrchardBand:
    case tole_MownGrass:
    case tole_Scrub:
    case tole_Vildtager:
        return 4.0;

        // Really good stuff!
    case tole_BeetleBank:
        return cfg_nest_hedgebank1.value();
    case tole_HedgeBank:
        int st;
        st = landscape->SupplyElementSubType(polyref);
        switch (st)
        {
        case 0:
            return cfg_nest_hedgebank0.value();
        case 1:
            return cfg_nest_hedgebank1.value();
        case 2:
            return cfg_nest_hedgebank2.value();
        default:
            return 100;
        }
        // Variable, depending on vegetation and condition of same.
    case tole_Field:
    case tole_UnsprayedFieldMargin:
        return partridge_tov_habitat_eval_field2(landscape, polyref);

    default:
        g_msg->Warn(WARN_BUG, "partridge_tole_habitat_eval2(): Unknown Element type", landscape->PolytypeToString(tole));
        exit(10);
    }
}

int partridge_tov_habitat_eval_field(Landscape* landscape, int polyref)
{

    TTypesOfVegetation l_tov = landscape->SupplyVegType(polyref);
    int score;
    switch (l_tov) {
    case tov_OFirstYearDanger:
    case tov_OGrazingPigs:
    case tov_OPermanentGrassGrazed:
    case tov_PermanentGrassGrazed:
    case tov_PermanentGrassLowYield:
        score = 0;
        break;

    case tov_Carrots:
    case tov_FieldPeas:
    case tov_FodderBeet:
    case tov_SugarBeet:
    case tov_OFodderBeet:
    case tov_Maize:
    case tov_MaizeSilage:
    case tov_OMaizeSilage:
    case tov_NoGrowth:
    case tov_None:
    case tov_OPotatoes:
    case tov_WinterRape:
    case tov_Potatoes:
    case tov_PotatoesIndustry:
        score = 1;
        break;

    case tov_PermanentGrassTussocky:
    case tov_PermanentSetAside:
    case tov_FodderGrass:
    case tov_CloverGrassGrazed1:
    case tov_CloverGrassGrazed2:
    case tov_Oats:
    case tov_OOats:
    case tov_OSeedGrass1:
    case tov_OSeedGrass2:
    case tov_OSpringBarley:
    case tov_OSpringBarleyExt:
    case tov_OSpringBarleyClover:
    case tov_OSpringBarleyGrass:
    case tov_OSpringBarleyPigs:
    case tov_OTriticale:
    case tov_OWinterBarley:
    case tov_OWinterBarleyExt:
    case tov_OWinterRape:
    case tov_OWinterRye:
    case tov_OWinterWheatUndersown:
    case tov_OWinterWheat:
    case tov_SeedGrass1:
    case tov_SeedGrass2:
    case tov_SpringBarley:
    case tov_SpringBarleySpr:
    case tov_SpringBarleyPTreatment:
    case tov_SpringBarleySKManagement:
    case tov_SpringBarleyCloverGrass:
    case tov_SpringBarleyGrass:
    case tov_SpringBarleySeed:
    case tov_SpringBarleySilage:
    case tov_SpringRape:
    case tov_SpringWheat:
    case tov_Triticale:
    case tov_WinterBarley:
    case tov_WinterRye:
    case tov_WinterWheat:
    case tov_WWheatPControl:
    case tov_WWheatPToxicControl:
    case tov_WWheatPTreatment:
    case tov_AgroChemIndustryCereal:
    case tov_WinterWheatShort:
    case tov_OCloverGrassGrazed1:
    case tov_OCloverGrassGrazed2:
    case tov_OBarleyPeaCloverGrass:
    case tov_OSBarleySilage:
    case tov_OCarrots:
    case tov_OCloverGrassSilage1:
    case tov_OFieldPeas:
    case tov_OFieldPeasSilage:
    case tov_YoungForest:
    case tov_Heath:
    case tov_BroadBeans:

        score = 2;
        break;

        /*  case tov_OGrassClover1: case tov_OGrassClover2: case tov_OBarleyPeaCloverGrass: case tov_OCarrots:
        case tov_OCloverGrassGrazed1: case tov_OCloverGrassGrazed2: case tov_OCloverGrassSilage1: case tov_OFieldPeas:
        case tov_OPermanentGrassGrazed: case tov_PermanentGrassGrazed: case tov_PermanentGrassTussocky: return 3.0; */

    case tov_SetAside:
    case tov_NaturalGrass:
    case tov_OSetAside:
        score = 4;
        break;

    case tov_Undefined:
    default:
        return 0;
    }
    return score;
}

int partridge_tov_habitat_eval_field2(Landscape* landscape, int polyref) {

    TTypesOfVegetation l_tov = landscape->SupplyVegType(polyref);
    int score = 0;
    switch (l_tov)
    {
    case tov_OFirstYearDanger:
    case tov_OGrazingPigs:
    case tov_OPermanentGrassGrazed:
    case tov_PermanentGrassGrazed:
    case tov_PermanentGrassLowYield:
        score = 0;
        break;

    case tov_Carrots:
    case tov_FieldPeas:
    case tov_FodderBeet:
    case tov_SugarBeet:
    case tov_OFodderBeet:
    case tov_Maize:
    case tov_NoGrowth:
    case tov_None:
    case tov_OPotatoes:
    case tov_WinterRape:
    case tov_Potatoes:
    case tov_PotatoesIndustry:
        score = 1;
        break;

    case tov_PermanentGrassTussocky:
    case tov_PermanentSetAside:
    case tov_FodderGrass:
    case tov_CloverGrassGrazed1:
    case tov_CloverGrassGrazed2:
    case tov_Oats:
    case tov_OOats:
    case tov_OSeedGrass1:
    case tov_OSeedGrass2:
    case tov_OSpringBarley:
    case tov_OSpringBarleyExt:
    case tov_OSpringBarleyClover:
    case tov_OSpringBarleyGrass:
    case tov_OSpringBarleyPigs:
    case tov_OTriticale:
    case tov_OWinterBarley:
    case tov_OWinterBarleyExt:
    case tov_OWinterRape:
    case tov_OWinterRye:
    case tov_OWinterWheatUndersown:
    case tov_OWinterWheat:
    case tov_SeedGrass1:
    case tov_SeedGrass2:
    case tov_SpringBarley:
    case tov_SpringBarleySpr:
    case tov_SpringBarleyPTreatment:
    case tov_SpringBarleySKManagement:
    case tov_SpringBarleyCloverGrass:
    case tov_SpringBarleyGrass:
    case tov_SpringBarleySeed:
    case tov_SpringBarleySilage:
    case tov_SpringRape:
    case tov_SpringWheat:
    case tov_Triticale:
    case tov_WinterBarley:
    case tov_WinterRye:
    case tov_WinterWheat:
    case tov_WWheatPControl:
    case tov_WWheatPToxicControl:
    case tov_WWheatPTreatment:
    case tov_AgroChemIndustryCereal:
    case tov_WinterWheatShort:
    case tov_OCloverGrassGrazed1:
    case tov_OCloverGrassGrazed2:
    case tov_OBarleyPeaCloverGrass:
    case tov_OSBarleySilage:
    case tov_OCarrots:
    case tov_OCloverGrassSilage1:
    case tov_OFieldPeas:
    case tov_OFieldPeasSilage:
    case tov_YoungForest:
    case tov_Heath:
    case tov_BroadBeans:
        score = 2;
        if (landscape->SupplyVegPatchy(polyref)) score += 2;
        //if (landscape->SupplyHasTramlines(polyref)) score+=1;
        break;

        /*  case tov_OGrassClover1: case tov_OGrassClover2: case tov_OBarleyPeaCloverGrass: case tov_OCarrots:
        case tov_OCloverGrassGrazed1: case tov_OCloverGrassGrazed2: case tov_OCloverGrassSilage1: case tov_OFieldPeas:
        case tov_OPermanentGrassGrazed: case tov_PermanentGrassGrazed: case tov_PermanentGrassTussocky: return 3.0; */

    case tov_SetAside:
    case tov_NaturalGrass:
    case tov_OSetAside:
        score = 4;
        if (landscape->SupplyVegPatchy(polyref)) score += 2;
        break;

    case tov_Undefined:
    default:
        score = 0;
    }

    return score;
}

int partridge_tole_tramlines1(Landscape* landscape, int polyref) {
    int value = 0 ;
    TTypesOfLandscapeElement Type = landscape->SupplyElementType(polyref);
    if ((Type == tole_Field || Type == tole_PermPastureTussocky || Type == tole_PermPasture || Type == tole_PermanentSetaside || Type == tole_PermPastureLowYield)) {
        value = 1;
        if (landscape->SupplyHasTramlines(polyref)) {
            value = 2;
        }
    }
    return value;
}

int partrige_tole_canMove(Landscape* landscape, int polyref){
    
    TTypesOfLandscapeElement tole = landscape->SupplyElementType(polyref);

    switch (tole) {
    case tole_StoneWall:
    case tole_Fence:
    case tole_Garden:
    case tole_Building:
    case tole_Pond:
    case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_Churchyard:
    case tole_Stream:
        return -1;

    case tole_IndividualTree:
    case tole_PlantNursery:
    case tole_WindTurbine:
    case tole_WoodyEnergyCrop:
    case tole_WoodlandMargin:
    case tole_Pylon:
    case tole_PermPasture:
    case tole_Scrub:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_Marsh:
    case tole_PitDisused:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
    case tole_Hedges:
    case tole_Railway:
    case tole_Field:
    case tole_UnsprayedFieldMargin:
    case tole_PermPastureTussocky:
    case tole_PermPastureLowYield:
    case tole_PermanentSetaside:
    case tole_YoungForest:
    case tole_NaturalGrassDry:
    case tole_Heath:
    case tole_OrchardBand:
    case tole_MownGrass:
    case tole_Orchard:
    case tole_BareRock:
    case tole_AmenityGrass:
    case tole_Parkland:
    case tole_UrbanNoVeg:
    case tole_UrbanPark:
    case tole_BuiltUpWithParkland:
    case tole_SandDune:
    case tole_Copse:
    case tole_NaturalGrassWet:
    case tole_RoadsideSlope:
    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Saltmarsh:
    case tole_HeritageSite:
        return 0;

    case tole_RoadsideVerge:
    case tole_FieldBoundary:
    case tole_HedgeBank:
    case tole_BeetleBank:
    case tole_Vildtager:
        return 1;

    default:
        g_msg->Warn(WARN_BUG, "Partridge_Covey::MoveCanMove(): Unknown tole type", landscape->PolytypeToString(tole));
        exit(1);
        break;
    }
}

void partridge_tov_init_movemodif(double *v){
    v[tov_Carrots] = 1.0;
    v[tov_FodderGrass] = 1.0;
    v[tov_CloverGrassGrazed1] = 1.0;
    v[tov_CloverGrassGrazed2] = 1.0;
    v[tov_BroadBeans] = 1.0;
    v[tov_FieldPeas] = 1.0;
    v[tov_FodderBeet] = 1.0;
    v[tov_SugarBeet] = 1.0;
    v[tov_OFodderBeet] = 1.0;
    v[tov_Maize] = 1.0;
    v[tov_MaizeSilage] = 1.0;
    v[tov_NaturalGrass] = 1.0;
    v[tov_NoGrowth] = 1.0;
    v[tov_None] = 1.0;
    v[tov_Oats] = 1.0;

    // 10
    v[tov_OBarleyPeaCloverGrass] = 1.0;
    v[tov_OSBarleySilage] = 1.0;
    v[tov_OCarrots] = 1.0;
    v[tov_OCloverGrassGrazed1] = 1.0;
    v[tov_OCloverGrassGrazed2] = 1.0;
    v[tov_OCloverGrassSilage1] = 1.0;
    v[tov_OFieldPeas] = 1.0;
    v[tov_OFieldPeasSilage] = 1.0;
    v[tov_OFirstYearDanger] = 1.0;
    v[tov_OCloverGrassGrazed1] = 1.0;
    v[tov_OCloverGrassGrazed2] = 1.0;
    v[tov_OGrazingPigs] = 1.0;

    // 20
    v[tov_OOats] = 1.0;
    v[tov_OPermanentGrassGrazed] = 1.0;
    v[tov_OPotatoes] = 1.0;
    v[tov_OSeedGrass1] = 1.0;
    v[tov_OSeedGrass2] = 1.0;
    v[tov_OSetAside] = 1.0;
    v[tov_OSpringBarley] = 1.0;
    v[tov_OSpringBarleyClover] = 1.0;
    v[tov_OSpringBarleyGrass] = 1.0;
    v[tov_OSpringBarleyPigs] = 1.0;

    // 30
    v[tov_OTriticale] = 1.0;
    v[tov_OWinterBarley] = 1.0;
    v[tov_OWinterRape] = 1.0;
    v[tov_OWinterRye] = 1.0;
    v[tov_OWinterWheatUndersown] = 1.0;
    v[tov_OWinterWheat] = 1.0;
    v[tov_PermanentGrassGrazed] = 1.0;
    v[tov_PermanentGrassLowYield] = 1.0;
    v[tov_PermanentGrassTussocky] = 1.0;
    v[tov_PermanentSetAside] = 1.0;
    v[tov_Potatoes] = 1.0;
    v[tov_PotatoesIndustry] = 1.0;

    // 40
    v[tov_SeedGrass1] = 1.0;
    v[tov_SeedGrass2] = 1.0;
    v[tov_SetAside] = 1.0;
    v[tov_SpringBarley] = 1.0;
    v[tov_SpringBarleySpr] = 1.0;
    v[tov_SpringBarleyCloverGrass] = 1.0;
    v[tov_SpringBarleyGrass] = 1.0;
    v[tov_SpringBarleySeed] = 1.0;
    v[tov_SpringBarleySilage] = 1.0;
    v[tov_SpringRape] = 1.0;
    v[tov_SpringWheat] = 1.0;

    // 50
    v[tov_Triticale] = 1.0;
    v[tov_WinterBarley] = 1.0;
    v[tov_WinterRape] = 1.0;
    v[tov_WinterRye] = 1.0;
    v[tov_WinterWheat] = 1.0;
    v[tov_WWheatPControl] = 1.0;
    v[tov_WWheatPToxicControl] = 1.0;
    v[tov_WWheatPTreatment] = 1.0;
    v[tov_AgroChemIndustryCereal] = 1.0;
    v[tov_WinterWheatShort] = 1.0;
    v[tov_PermanentGrassGrazed] = 1.0;
    v[tov_OSpringBarleyExt] = 1.0;
    v[tov_SpringBarleyPTreatment] = 1.0;
    v[tov_SpringBarleySKManagement] = 1.0;
    v[tov_OMaizeSilage] = 1.0;
    v[tov_Heath] = 1.0;

    // Must be here. tov_Undefined is returned by polygons
    // without growth.
    v[tov_Undefined] = 1.0;
    v[tov_NoGrowth] = 1.0;
}

int partridge_tole_nestGoodSpot(Landscape* landscape, int x, int y) {
    int score;
    TTypesOfLandscapeElement tole = landscape->SupplyElementType(x, y);
    switch (tole) {
    case tole_Field:
        score = cfg_nest_field.value();
        // Need to add something about patchy and tramlines
        // TO SIMULATE 1950s SITUATION WITH ALL ACCESS GRANTED
#ifndef __P1950s
        if (landscape->SupplyVegPatchy(x, y)) score *= 4; // doubled for patchy
        //else if (m_map->SupplyHasTramlines( a_x, a_y )) score*=2; // doubled for tramlines
#else
        score *= 4;
#endif
        break;
    case tole_RoadsideVerge:
        score = cfg_nest_roadside.value();
        break;
    case tole_Railway:
        score = cfg_nest_railway.value();
        break;
    case tole_FieldBoundary:
        score = cfg_nest_fieldboundary.value();
        break;
    case tole_PermPastureLowYield:
        score = cfg_nest_permpastlowyield.value();
        break;
    case tole_PermPastureTussocky:
        score = cfg_nest_permpasttussocky.value();
        break;
    case tole_PermanentSetaside:
        score = cfg_nest_permsetaside.value();
        break;
    case tole_Heath:
    case tole_NaturalGrassDry:
        score = cfg_nest_naturalgrass.value();
        break;
    case tole_BeetleBank:
        score = cfg_nest_hedgebank1.value();
        break;
    case tole_HedgeBank:
    {
        int st = landscape->SupplyElementSubType(x, y);
        switch (st) {
        case 0:
            score = cfg_nest_hedgebank0.value();
            break;
        case 1:
            score = cfg_nest_hedgebank1.value();
            break;
        case 2:
            score = cfg_nest_hedgebank2.value();
            break;
        default:
            score = 100;
        }
    }
        break;
    default:
        score = -1;
    }

    return score;
}

bool partridge_tole_nestBadArea(Landscape* landscape, int x, int y){

    TTypesOfLandscapeElement tole = landscape->SupplyElementType(x, y);

    switch (tole) {
    case tole_PermPasture:
    case tole_Scrub:
    case tole_RiversidePlants:
    case tole_RiversideTrees:
    case tole_DeciduousForest:
    case tole_MixedForest:
    case tole_ConiferousForest:
    case tole_StoneWall:
    case tole_Fence:
    case tole_Track:
    case tole_SmallRoad:
    case tole_LargeRoad:
    case tole_ActivePit:
    case tole_Hedges:
    case tole_Marsh:
    case tole_PitDisused:
    case tole_RoadsideVerge:
    case tole_Railway:
    case tole_FieldBoundary:
    case tole_PermPastureLowYield:
    case tole_PermPastureTussocky:
    case tole_PermanentSetaside:
    case tole_NaturalGrassDry:
    case tole_Heath:
    case tole_Orchard:
    case tole_YoungForest:
    case tole_HedgeBank:
    case tole_BeetleBank:
    case tole_UnsprayedFieldMargin:
    case tole_Field:
    case tole_OrchardBand:
    case tole_MownGrass:
    case tole_NaturalGrassWet:
    case tole_RoadsideSlope:
    case tole_Vildtager:
        return false;

    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Churchyard:
    case tole_Saltmarsh:
    case tole_Stream:
    case tole_HeritageSite:
    case tole_Garden:
    case tole_Building:
    case tole_Pond:
    case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_BareRock:
    case tole_AmenityGrass:
    case tole_Parkland:
    case tole_UrbanNoVeg:
    case tole_UrbanPark:
    case tole_BuiltUpWithParkland:
    case tole_SandDune:
    case tole_Copse:
    case tole_IndividualTree:
    case tole_PlantNursery:
    case tole_WindTurbine:
    case tole_WoodyEnergyCrop:
    case tole_WoodlandMargin:
    case tole_Pylon:
    case tole_Wasteland:
    case tole_UrbanVeg:
        return true;

    default:
        g_msg->Warn(WARN_BUG, "Partridge_Covey::NestBadArea(): Unknown tole type", landscape->PolytypeToString(tole));
        exit(1);
    }
}

bool partridge_tole_initOK(Landscape* landscape, int x, int y){
    TTypesOfLandscapeElement tole = landscape->SupplyElementType(x, y);
    switch (tole) {
    // all bad plces for initialization
    case tole_StoneWall:
    case tole_Fence:
    case tole_Garden:
    case tole_Building:
    case tole_Pond:
    case tole_Freshwater:
    case tole_River:
    case tole_Saltwater:
    case tole_Coast:
    case tole_BareRock:
    case tole_AmenityGrass:
    case tole_Parkland:
    case tole_UrbanNoVeg:
    case tole_UrbanPark:
    case tole_BuiltUpWithParkland:
    case tole_SandDune:
    case tole_Copse:
    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Churchyard:
    case tole_Saltmarsh:
    case tole_Stream:
    case tole_HeritageSite:
        return false;
    default: 
        return true;
    }
}


int partridge_tole_movemap_init(Landscape* m_OurLandscape, int x, int y) {

    TTypesOfLandscapeElement tole = m_OurLandscape->SupplyElementType(x, y);
    int colour = 0;
    switch (tole)
    {
    case tole_Hedges: // 130
    case tole_PermanentSetaside: // 33
    case tole_RoadsideVerge: // 13
    case tole_FieldBoundary: // 160
    case tole_HedgeBank:
    case tole_BeetleBank:
    case tole_UnsprayedFieldMargin: // 31
    case tole_Vildtager:
    case tole_WaterBufferZone:
        colour = 3;
        break;
    case tole_Railway: // 118
    case tole_Scrub: // 70
    case tole_Field:  // 20 & 30
    case tole_PermPasture: // 35
    case tole_NaturalGrassDry: // 110
    case tole_RiversidePlants: // 98
    case tole_RoadsideSlope:
    case tole_RiversideTrees: // 97
    case tole_YoungForest:  //55
    case tole_Wasteland:
    case tole_UnknownGrass:
        colour = 2;
        break;
    case tole_PitDisused: // 75
    case tole_NaturalGrassWet:
    case tole_Marsh: // 95
    case tole_Garden: //11
    case tole_Track:  // 123
    case tole_SmallRoad:  // 122
    case tole_LargeRoad:  // 121
        colour = 1;
        break;
    case tole_IndividualTree:
    case tole_PlantNursery:
    case tole_WindTurbine:
    case tole_WoodyEnergyCrop:
    case tole_WoodlandMargin:
    case tole_Pylon:
    case tole_DeciduousForest: // 40
    case tole_MixedForest:     // 60
    case tole_ConiferousForest: // 50
    case tole_StoneWall: // 15
    case tole_Fence: // 225
    case tole_Building: // 5
    case tole_ActivePit: // 115
    case tole_Pond:
    case tole_Freshwater: // 90
    case tole_FishFarm:
    case tole_River: // 96
    case tole_Saltwater:  // 80
    case tole_Coast: // 100
    case tole_BareRock: // 59
    case tole_AmenityGrass:
    case tole_Parkland:
    case tole_UrbanNoVeg:
    case tole_UrbanPark:
    case tole_BuiltUpWithParkland:
    case tole_SandDune:
    case tole_Copse:
    case tole_MetalledPath:
    case tole_Carpark:
    case tole_Churchyard:
    case tole_Saltmarsh:
    case tole_Stream:
    case tole_HeritageSite:
        colour = 0;
        break;
    case tole_Foobar: // 999   !! type unknown - should not happen
    default:
        g_msg->Warn(WARN_FILE,
            "MovementMap::Init(): Unknown landscape element type:",
            int(tole));
        exit(1);
    }

    return colour;
}