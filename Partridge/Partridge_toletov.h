#ifndef Partridge_toletov_H
#define Partridge_toletov_H

#include "../Landscape/ls.h"

extern CfgFloat cfg_terr_qual_good;
extern CfgInt cfg_nest_hedgebank0;
extern CfgInt cfg_nest_hedgebank1;
extern CfgInt cfg_nest_hedgebank2;
extern CfgInt cfg_nest_field;
extern CfgInt cfg_nest_roadside;
extern CfgInt cfg_nest_railway;
extern CfgInt cfg_nest_fieldboundary;
extern CfgInt cfg_nest_permpastlowyield;
extern CfgInt cfg_nest_permpasttussocky;
extern CfgInt cfg_nest_permsetaside;
extern CfgInt cfg_nest_naturalgrass;

double partridge_tole_habitat_eval(Landscape* landscape, int polyref);
double partridge_tole_habitat_eval2(Landscape* landscape, int polyref);
int partridge_tov_habitat_eval_field(Landscape* landscape, int polyref);
int partridge_tov_habitat_eval_field2(Landscape* landscape, int polyref);
int partridge_tole_tramlines1(Landscape* landscape, int polyref);
int partrige_tole_canMove(Landscape* landscape, int polyref);
void partridge_tov_init_movemodif(double* v);
int partridge_tole_nestGoodSpot(Landscape * landscape, int x, int y);
bool partridge_tole_nestBadArea(Landscape* landscape, int x, int y);
bool partridge_tole_initOK(Landscape* landscape, int x, int y);
int partridge_tole_movemap_init(Landscape* m_OurLandscape, int x, int y);

#endif