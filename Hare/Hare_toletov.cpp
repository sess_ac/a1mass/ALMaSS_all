#include "../Landscape/ls.h"
#include "Hare_toletov.h"

bool hare_tole_init_friendly(Landscape *m_TheLandscape, int x, int y)
{

	if (m_TheLandscape->SupplyAttIsWater(x, y))
		return false;

	TTypesOfLandscapeElement te = m_TheLandscape->SupplyElementType(x, y);
	
	switch (te)
	{
	// Sort of Urban No Veg
	case tole_Building:
	case tole_Parkland:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_AmenityGrass:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_UrbanNoVeg:
	// Wet Landscape 		
	case tole_FishFarm:
	case tole_SwampForest: // new
	case tole_RiceField: // new
	case tole_Copse:
	case tole_MetalledPath:
	// Barren 		
	case tole_Coast:
	case tole_BareRock:
	case tole_Saltpans: // new
	case tole_HeritageSite:
	case tole_SandDune:
		return false;
	}

	return true;
}

bool hare_tole_can_walkacross(Landscape* m_TheLandscape, int x, int y)
{
	TTypesOfLandscapeElement tole = m_TheLandscape->SupplyElementType(x, y);

	if (m_TheLandscape->SupplyAttIsWater(x, y))
		return false;

	switch (tole) {
	case tole_Building:
	case tole_Coast:
	case tole_BareRock:
	case tole_UrbanNoVeg:
	case tole_UrbanPark:
	case tole_SandDune:
		return false;
	}

	return true;
}

void hare_tov_SetVegPalatability(double* vp)
{
	// First fill in default value
	for (int i = 0; i < tov_Undefined; i++) 
		vp[i] = 1.0;

	// Now set any special values
	vp[tov_Potatoes] = 0.1;
	vp[tov_PotatoesIndustry] = 0.1;
	vp[tov_PlantNursery] = 0.1;
	vp[tov_NoGrowth] = 0.0;
	vp[tov_None] = 0.0;
	vp[tov_OPotatoes] = 0.1;
	vp[tov_Carrots] = 1.5;
	vp[tov_OCarrots] = 1.5;

	/*
	vp[tov_FodderBeet] = 1.25;
	vp[tov_SugarBeet] = 1.25;
	vp[tov_OFodderBeet] = 1.25;
	*/
}
/** Used as an example of seeting userdefine attribute bool for LEs */
bool hare_tole_init_friendly(TTypesOfLandscapeElement a_tole)
{
	switch (a_tole)
	{
		// Sort of Urban No Veg
	case tole_Building:
	case tole_Parkland:
	case tole_UrbanPark:
	case tole_BuiltUpWithParkland:
	case tole_AmenityGrass:
	case tole_Carpark:
	case tole_Churchyard:
	case tole_UrbanNoVeg:
		// Wet Landscape 		
	case tole_Saltwater:
	case tole_River:
	case tole_Pond:
	case tole_RiverBed:
	case tole_Freshwater:
	case tole_Canal:
	case tole_Stream:
	case tole_DrainageDitch:
	case tole_FishFarm:
	case tole_SwampForest: // new
	case tole_RiceField: // new
	case tole_Copse:
	case tole_MetalledPath:
		// Barren 		
	case tole_Coast:
	case tole_BareRock:
	case tole_Saltpans: // new
	case tole_HeritageSite:
	case tole_SandDune:
		return false;
	}
	return true;
}
