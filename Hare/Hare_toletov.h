#ifndef Hare_toletov_H
#define Hare_toletov_H

bool hare_tole_init_friendly(Landscape * m_TheLandscape, int x, int y);
bool hare_tole_can_walkacross(Landscape* m_TheLandscape, int x, int y);
bool hare_tole_init_friendly(TTypesOfLandscapeElement a_tole);

void hare_tov_SetVegPalatability(double *vp);

#endif