/* 
*******************************************************************************************************
Copyright (c) 2011, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
//---------------------------------------------------------------------------
#ifndef SpiderPopulationManagerH
#define SpiderPopulationManagerH
//---------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Forward Declarations

class struct_Spider;
class SimplePositionMap;
class MovementMap;

//---------------------------------------------------------------------------

/**
Enumerator for spider object types
*/
typedef enum {
  spob_Egg, 
  spob_Juvenile, 
  spob_Female 
}
Spider_Object;


class Erigone_Population_Manager: public Spider_Population_Manager
{
 public:
    Erigone_Population_Manager(Landscape* p_L);
    virtual void Init (void);
    virtual void DoFirst (void);
	virtual ~Erigone_Population_Manager();
    void CreateObjects(int ob_type, TAnimal *pvo, struct_Spider * data,int number);
  // Other interface functions
	virtual void TheAOROutputProbe();
    virtual void TheRipleysOutputProbe(FILE* a_prb);
protected:
	// Methods
	void Catastrophe();
// Special functionality
#ifdef __RECORD_RECOVERY_POLYGONS
   /** \brief Special pesticide recovery code */
   void RecordRecoveryPolygons();
   int m_RecoveryPolygons[101];
   int m_RecoveryPolygonsC[101];
#endif
};
//---------------------------------------------------------------------------

#endif
