/*
*******************************************************************************************************
Copyright (c) 2019, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file Haplodrassus.h
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file Haplodrassus.h
Version of  07 April 2021  \n
By Lu�s Amaro OLIVESIM Team \n \n
*/

///---------------------------------------------------------------------------
#ifndef HaplodrassusH
#define HaplodrassusH
//---------------------------------------------------------------------------

#include <memory>

//---------------------------------------------------------------------------

class Spider_Base;
class Haplodrassus;
class Haplodrassus_Population_Manager;

//------------------------------------------------------------------------------
/**
Used for the population manager's list of Haplodrassus
*/
//typedef vector<Haplodrassus*> TListOfHaplodrassus;
//------------------------------------------------------------------------------


// Attributes



//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START Haplodrassus_Egg ----------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------


class Haplodrassus_Egg : public Spider_Egg
{
    // Attributes
protected:
    // Saves the Day Degree Accumulation need for the Eggsac to develop
    int m_Egg_Devel_Degrees;

public:
    /** \brief Haplodrassus constructor */
    Haplodrassus_Egg(int p_x, int p_y, Landscape* p_L, Haplodrassus_Population_Manager* p_NPM, int p_noEggs);
    /** \brief Haplodrassus destructor */
    virtual ~Haplodrassus_Egg();
protected:
    // 17/11/2020 - Step() Method 
    void Step();

    // 18/11/2020 BeginStep() Method
    void BeginStep();

    // 18/11/2020 - st_Develop() method describes how the Eggs develop inside the the Eggsac
    TTypesOfSpiderState st_Develop();

    // 20/11/2020 - Hatch Behavior
    // Describes hatch behavioural state
    void st_Hatch(); 
    // Determines the number and location of spiderlings to survive hatching and triggers creation of juvenile objects
    void Hatch(int a_eggsackspread, unsigned a_doubleeggsacspread, unsigned a_noeggs, unsigned a_range);

    /** \brief Used to filter and act on responses to external events (usually farming) */
    virtual bool OnFarmEvent(FarmToDo event) {
        return true;
    }
};


//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- END Haplodrassus_Egg ----------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START Haplodrassus_Spiderling ---------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------


class Haplodrassus_Spiderling : public Spider_Juvenile
{
public:
    /** \brief Haplodrassus constructor */
    Haplodrassus_Spiderling(int p_x, int p_y, Landscape* p_L, Haplodrassus_Population_Manager* p_NPM);

    /* \brief Haplodrassus destructor */
    virtual ~Haplodrassus_Spiderling();
   
    /** \brief The BeginStep code  */
    void BeginStep();

    /** \brief The Step code  */
    void Step();

protected:
    /*
    // Atributes
    // Mortality Probability Constant
    double m_molt_mort_const = 0.1;
    */

    /*NOTE: The threshold for the Juvenile Development is saved inside the cfg_Haplodrassus_JuvDevelConst2 object.
        The value of this object is accessed by the m_JuvDevelConst variable. So we just need to change the value
        of the cfg_Haplodrassus_JuvDevelConst2. */
    
    // Methods
    // 23/11/2020
    // Describes what happens when the Juvevnile is developing
    TTypesOfSpiderState st_Develop();

    // 03/12/2020
    // Describes the Maturation process
    void Maturation();

    // 04/12/2020
    // Describes the Walk behavior of the Juvenile
    TTypesOfSpiderState st_Walk(); // Describes what happens when the Juvenile Walks
    int Walk(); // Makes the Juvenile move in a certain direction
    int WalkTo(int direction); // Moves the spider 2 meters in the direction given by the variable direction
    int GetDistBasedOnRefuge(); // 30/03/2021 - Obtains the distance that the Juvenile can walk given the refuge level of the tole

    // 10/12/2020
    // Assess Habitat Behaviour state
    TTypesOfSpiderState st_AssesHabitat() {
        return AssessHabitat();
    };
    // Describes what happens when the Juvenil assess the habitat
    TTypesOfSpiderState AssessHabitat();
    

    /** \brief Used to filter and act on responses to external events (usually farming) */
    virtual bool OnFarmEvent(FarmToDo event) {
        return true;
    }
};


//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- END Haplodrassus_Spiderling ---------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------





//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- START Haplodrassus_Female -------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

class Haplodrassus_Female : public Spider_Female
{
    // Attributes
protected:
    // 07/10/2020
    bool m_CanReproduce = false; // Are we Reproducing?
    // 29/10/2020
    int m_Eggsacs_Produced; // Number of Eggsacs produced 
    // 11/11/2020
    double m_Overwinter_DDAccum; // Accumulates Day Degrees to Stop Overwintering Behavior
    
public:
    /** \brief Haplodrassus constructor */
    Haplodrassus_Female(int p_x, int p_y, Landscape* p_L, Haplodrassus_Population_Manager* p_NPM); // Constructor
    /** \brief Haplodrassus destructor */
    virtual ~Haplodrassus_Female(); // Destructor
    /** \brief Used to filter and act on responses to external events (usually farming) */

    // Methods
protected:
    virtual bool OnFarmEvent(FarmToDo event) { return true; }
    /** \brief Determines the number of eggs per egg sac */
    virtual int CalculateEggsPerEggSac();
    

    // 02/10/2020
   /* Need to overried Step() method, in order to describe what happens when the Female State is tops_FOverWintering*/
    void Step();

    // 20/10/2020
    /* Going to call the BeginStep() method in order to see if the Days of the simulation Change */
    void BeginStep();

    // 05/03/2021
    void EndStep();

    // 30-09-2020
    TTypesOfSpiderState st_OverWintering();
    TTypesOfSpiderState OverWinterMort(); // 09/03/2021
    

   
    // 07/10/2020
    /* Started to address the Reproduction of Haplodrassus. 
    NOTE: The method CreateEggSac() from the Spider_Female Base Class will be used, given that 
    they process of creating the Eggsac is the same */
    int st_Reproduce(); // Simulates the reproduction behaviour 
    bool ProduceEggSac(); // Its going to be used to create the EggSac in a specific location
    void CanReproduce(); // Checks if the Spider is able to Reproduce
    void CreateEggSac(int a_NoEggs); // Needs to be overrided because our spider doesn�t ballon
    bool CheckToleRefugeLvl(); // Checks the refuge level of the current tole

    // 13/10/2020
    TTypesOfSpiderState st_Walk(); // Describes how the Spider Walks
    int WalkTo(int direction); // Moves the spider 5 meters in the direction given by the variable direction
    int Walk();
    int GetDistBasedOnRefuge(); // 12/03/2021

    // 04/11/2020
    TTypesOfSpiderState AssessHabitat();

    // 06/11/2020
    TTypesOfSpiderState st_Protect();

};

//---------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------- END Haplodrassus_Female ---------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------

#endif

