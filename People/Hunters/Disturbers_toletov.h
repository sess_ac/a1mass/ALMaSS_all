//
// Created by andrey on 3/26/21.
//

#ifndef ALMASS_DISTURBERS_TOLETOV_H
#define ALMASS_DISTURBERS_TOLETOV_H
using TTolelist=std::unordered_set<TTypesOfLandscapeElement>;
//using TTovlist=std::unordered_set<TTypesOfVegetation>;

TTolelist DisturberProhibitedToles{tole_Saltwater,tole_Stream};
#endif //ALMASS_DISTURBERS_TOLETOV_H
