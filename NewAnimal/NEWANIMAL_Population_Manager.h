/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file NEWANIMAL_Population_Manager.h 
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file NEWANIMAL_Population_Manager.h
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

//---------------------------------------------------------------------------
#ifndef NEWANIMAL_Population_ManagerH
#define NEWANIMAL_Population_ManagerH
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

class NEWANIMAL;

//------------------------------------------------------------------------------

/**
\brief
Used for creation of a new NEWANIMAL object
*/
class struct_NEWANIMAL
{
 public:
  /** \brief x-coord */
  int x;
  /** \brief y-coord */
  int y;
  /** \brief species ID */
  int species;
  /** \brief Landscape pointer */
  Landscape* L;
  /** \brief NEWANIMAL_Population_Manager pointer */
  NEWANIMAL_Population_Manager * NPM;
};

/**
\brief
The class to handle all predator population related matters
*/
class NEWANIMAL_Population_Manager : public Population_Manager
{
public:
// Methods
   /** \brief NEWANIMAL_Population_Manager Constructor */
   NEWANIMAL_Population_Manager(Landscape* L);
   /** \brief NEWANIMAL_Population_Manager Destructor */
   virtual ~NEWANIMAL_Population_Manager (void);
   /** \brief Method for creating a new individual NEWANIMAL */
   void CreateObjects(int ob_type, TAnimal *pvo, struct_NEWANIMAL* data, int number);

protected:
// Attributes
// Methods
   /** \brief  Things to do before anything else at the start of a timestep  */
   virtual void DoFirst(){}
   /** \brief Things to do before the Step */
   virtual void DoBefore(){}
   /** \brief Things to do before the EndStep */
   virtual void DoAfter(){}
   /** \brief Things to do after the EndStep */
   virtual void DoLast(){}
};

#endif