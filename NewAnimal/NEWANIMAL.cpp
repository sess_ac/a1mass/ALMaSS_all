/*
*******************************************************************************************************
Copyright (c) 2012, Christopher John Topping, Aarhus University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/
/** \file NEWANIMAL.cpp
\brief <B>The main source code for all predator lifestage and population manager classes</B>
*/
/**  \file NEWANIMAL.cpp
Version of  2 November 2012 \n
By Chris J. Topping \n \n
*/

/*
To make a new animal from this code:
1) Copy all four NEWANIMAL files to a new directory and rename them e.g. Elephant.h Elephant_Population_Manager.cpp etc
2)Open all four files and replace all occurences of NEWANIMAL with Elephant or whatever you chose to use
3) Go to ALMaSS_GUI.cpp and add a new item to the item9Strings.
4) Add a new call to population manager in bool MyDialog::CreatePopulationManager() with the next number in sequence
5) Add the renamed files to the project and compile
*/

#include <iostream>
#include <fstream>
#include<vector>
#include "../BatchALMaSS/ALMaSS_Setup.h"
#include "../ALMaSSDefines.h"
#include "../Landscape/ls.h"
#include "../BatchALMaSS/PopulationManager.h"
#include "../NEWANIMAL/NEWANIMAL.h"
#include "../NEWANIMAL/NewAnimal_Population_Manager.h"

extern MapErrorMsg *g_msg;

using namespace std;

NEWANIMAL::NEWANIMAL(int p_x, int p_y,Landscape* p_L, NEWANIMAL_Population_Manager* p_NPM) : TAnimal(p_x,p_y,p_L)
{
	// Assign the pointer to the population manager
	m_OurPopulationManager = p_NPM;
	m_CurrentNAState = toNEWANIMALs_InitialState;
	m_SpeciesID = 999; // This can be any number since its just here to demonstrate how you might add some information
	m_DispersalMax = (unsigned) floor((g_rand_uni() * 90.0)+0.5) + 10; // Max movement is a number between 10 & 100
}

NEWANIMAL::~NEWANIMAL(void)
{
	;
}

void NEWANIMAL::Step(void)
{
  if (m_StepDone || m_CurrentStateNo == -1) return;
  switch (m_CurrentNAState)
  {
   case toNEWANIMALs_InitialState: // Initial state always starts with develop
    m_CurrentNAState=toNEWANIMALs_Develop;
    break;
   case toNEWANIMALs_Develop:
    m_CurrentNAState=st_Develop(); // Will return movement or die
    break;
   case toNEWANIMALs_Move:
    m_CurrentNAState=st_Movement(); // Will return develop
    m_StepDone=true;
    break;
   case toNEWANIMALs_Die:
	st_Dying(); // No return value - no behaviour after this
    m_StepDone=true;
    break;
   default:
	   m_OurLandscape->Warn("NEWANIMAL::Step()","unknown state - default");
    exit(1);
   }
}

TTypeOfNEWANIMALState NEWANIMAL::st_Movement( void )
{
	// How far to move today
	int step = (int) floor(m_DispersalMax * g_rand_uni() + 0.5);
	// What direction - pick one of 8 possible
	int direc = (int) floor(8 * g_rand_uni());
	// Load up local variables with the current coords
	int x = m_Location_x;
	int y = m_Location_y;
	switch (direc)
	{
	case 0:
		// North
		y -= step;
		break;
	case 1:
		// NE
		y-=step;
		x+=step;
		break;
	case 2:
		// E
		x+=step;
		break;
	case 3:
		// SE
		y+=step;
		x+=step;
		break;
	case 4:
		// S
		y+=step;
		break;
	case 5:
		// SW
		y+=step;
		x-=step;
		break;
	case 6:
		// W
		x-=step;
		break;
	case 7:
		// NW
		y-=step;
		x-=step;
		break;
	default:
		// Raise an error
		g_msg->Warn("NEWANIMAL - unknown direction: ",direc);
	}
	// this corrects for wrap-around situations - but it is slow. In your code you might want to test for
	// the need to do this before calling
	m_OurLandscape->CorrectCoords(x,y);
	// now assign the new coordinates
	m_Location_x = x;
	m_Location_y = y;
	return toNEWANIMALs_Develop;
}

void NEWANIMAL::st_Dying( void )
{
	m_CurrentStateNo = -1; // this will kill the animal object and free up space
}

TTypeOfNEWANIMALState NEWANIMAL::st_Develop( void )
{
	if (g_rand_uni() < 0.01) return toNEWANIMALs_Die; // 1% chance of death
	return toNEWANIMALs_Move;
}
