

/*! \file GooseMemoryMap.h \brief Header file for the Goose Memory Map and associated classes. */

/*
*******************************************************************************************************
Copyright (c) 2013, Christopher John Topping, University of Aarhus
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************************************************
*/


//---------------------------------------------------------------------------
#ifndef goosememorymapH
#define goosememorymapH
//----------------------------------------------------------------------------

#define __DEBUG_GOOSE


class Goose_Base;

/** \brief a data structure to hold goose memory location attributes */
class GooseMemoryLocation
{
public:
	/** \brief how old the memory is */
	int m_age;
	/** \brief x-coordinate */
	int m_x;
	/** \brief y-coordinate */
	int m_y;
	/** \brief the unique polygon identification */
	int m_polygonid;
	/** \brief the grain resource at the location as intake rate kJ/min */
	double m_grain;
	/** \brief the maize resource at the location as intake rate kJ/min */
	double m_maize;
	/** \brief the grazing resource at the location as intake rate kJ/min  */
	double m_grazing;
	/** \brief The max food intake rate (kJ/min) at the location */
	double m_foodresource;
	/** \brief the threat-level memory strength */
	double m_threat;
	/** \brief A score used to assess the location */
	double m_score;
	/** \brief the age at which a memory is removed. */
	static int m_infinitememorystop;
	
// Methods
	/** \brief GooseMemoryLocation default constructor */
	GooseMemoryLocation() {
		m_age = 0;  m_x = -1; m_y = -1; m_polygonid = -1, m_threat = 0, m_grain = 0; m_maize = 0.0; m_grazing = 0.0; m_foodresource = 0.0; m_infinitememorystop = 0;
	}
	/** \brief operator definition for comparing polygon id numbers */
	bool operator<(const GooseMemoryLocation& gml)
	{
		return m_polygonid < gml.m_polygonid;
	}
	/** \brief Updates the m_score value */
	void CalcScore(int a_dist, double a_flightcost, int a_expectedforagingtime)
	{
		double cost = a_dist * a_flightcost;
		double score = (m_foodresource * a_expectedforagingtime) - cost;
		m_score = score * (1 - m_threat);
	}
	/** \brief Test to see if a memory is too old to remember any longer */
	bool ShouldDelete() { return (m_age > m_infinitememorystop); }

	//-------------------------------------------------------------------------------------------------------

};
//----------------------------------------------------------------------------

/** \brief The class describing both local and seasonal goose memories and cognition */
class GooseMemoryMap
{
protected:
	/** \brief pointer to the owner */
	Goose_Base* m_myOwner;
	/** \brief The decay rate per day for threats */
	double m_threatdecayrate;
	/** \brief the time in minutes a goose expects to be able to forage when evaluating cost benefit of flying to a location. */
	int m_expectedforagingtime;
	/** \brief The list of memory locations */
	std::vector<GooseMemoryLocation> m_memorylocations;
public:
	/** \brief Constructor
	*/
	GooseMemoryMap(Goose_Base* a_owner);
	/** \brief Destructor
	*/
	~GooseMemoryMap() { 
		//m_memorylocations.erase(m_memorylocations.begin(),m_memorylocations.end());
		if (m_memorylocations.size()>0)
            m_memorylocations.clear();
	}
	/** \brief Add a new memory location
	* \param a_gml [in] A goose memory location to add to the memory list.
	*/
	void MemAdd(GooseMemoryLocation a_gml);

	/** \brief Delete a memory location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \return success
	*/
	bool MemDel(int a_polyid);

	/** \brief Add to food at a memory location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \param a_foodres [in] A measure of the strength of food resource memory.
	* \return success
	*/
	bool ChangeAddFoodRes(int a_polyid, double a_foodres);

	/** \brief Add to threat at a memory location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \param a_threat [in] A measure of the strength of threat memory.
	* \return success
	*/
	bool ChangeAddThreat(int a_polyid, double a_threat);

	/** \brief Multiply food at a memory location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \param a_foodres [in] A measure of the strength of food resource memory.
	* \return success
	*/
	bool ChangeMultFoodRes(int a_polyid, double a_foodres);

	/** \brief Set food at a memory location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \param a_grain [in] The grain food resource.
	* \param a_maize [in] The maize food resource.
	* \param a_grazing [in] The grazing food resource.
	* \return success
	*/
	bool ChangeSetFoodRes(int a_polyid, double a_grain, double a_maize, double a_grazing);

	/** \brief Multiply threat at a memory location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \param a_threat [in] The memory of a threat at this location
	* \return success
	*/
	bool ChangeMultThreat(int a_polyid, double a_threat);

	/** \brief Get the food resource at location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \return Food resource memory at a_polyid
	*/
	double GetFoodRes( int a_polyid );

	/** \brief Get the threat level at location
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \return Threat memory at a_polyid
	*/
	double GetThreat( int a_polyid );

	/** \brief Find the current best feeding location
	* \return Best food resource memory in memory after taking threats and distance from roost into account
	*/
	GooseMemoryLocation GetBestFeedingScore();
		
	/** \brief Inline function to calulate overall score for a distance, resource and threat
	* \param [in] a_dist The distance to the location
	* \param [in] a_foodresource The foodresource at the location
	* \param [in] a_threat A measure of the strength of threat memory.
	* \return the memory score calculation result
	*/
	double CalcScore(int a_dist, double a_foodresource, double a_threat);

	/** \brief Check if this location is known
	* \param a_polyid [in] A unique polygon reference to a goose memory location.
	* \return true if we have this polygon otherwise false.
	*/
	bool IsKnownArea( int a_polyid );

	/** \brief Decay all memory */
	void DecayAllMemory();
	/** \brief Get total of threat currently remembered
	* \return The sum of total threats remembered
	*/
	double GetTotalThreats( void );
	/** \brief Remove all memory
	* \return None
	*/
	void ClearAllMemory(void);
protected:
	/** \brief Error message functionality
	* \param [in] a_str A string to send as output
	* \param [in] a_value A value to send as output
	*/
	void GooseMemoryError(std::string a_str, int a_value);
};
//----------------------------------------------------------------------------


#endif
